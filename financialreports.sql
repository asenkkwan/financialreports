-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 04, 2018 at 06:56 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `financialreports`
--

-- --------------------------------------------------------

--
-- Table structure for table `agen`
--

CREATE TABLE `agen` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `npwp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pic_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pic_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pic_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `agen`
--

INSERT INTO `agen` (`id`, `name`, `address`, `phone_number`, `npwp`, `pic_name`, `pic_email`, `pic_phone`, `updated_by`, `created_by`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'kinan noise', 'bali', '8900909', '12344', 'madea', 'menan', '89000', 0, 0, NULL, NULL, '2018-05-31 00:01:12', '2018-06-01 23:21:01'),
(2, 'kinan', 'bali', '8900909', '12344', 'mades', 'mena', '89000', 0, 0, NULL, NULL, '2018-05-31 00:02:19', '2018-06-01 23:23:05'),
(3, 'mine', 'usa', '89000', '1242', 'bryan', 'bryna', '0315151', 0, 0, '2018-06-01 23:21:10', NULL, '2018-05-31 00:07:18', '2018-06-01 23:21:10'),
(4, 'Chatime Senayan City', 'PIM', '021-878787871', '12.000.00001000.1', 'Bryan', 'bryan@chatime.com', '0983231131111', 0, 0, NULL, NULL, '2018-07-03 10:14:18', '2018-07-03 10:14:18');

-- --------------------------------------------------------

--
-- Table structure for table `alat_angkut`
--

CREATE TABLE `alat_angkut` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_po_keluar` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `volume` int(11) NOT NULL,
  `nama_supir` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `alat_angkut`
--

INSERT INTO `alat_angkut` (`id`, `id_po_keluar`, `tanggal`, `nopol`, `volume`, `nama_supir`, `updated_by`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 5, '2018-06-27', 'B 1345 WBR', 1000, 'Joko', 0, 0, NULL, '2018-06-26 17:57:03', '2018-06-26 17:57:03'),
(2, 2, '2018-06-27', 'B 1345 WBR', 1000, 'Joko', 0, 0, NULL, '2018-06-26 17:59:32', '2018-06-26 17:59:32'),
(3, 1, '2018-06-28', 'B 1345 WBR', 1000, 'Joko', 0, 0, NULL, '2018-06-28 01:58:31', '2018-06-28 01:58:31');

-- --------------------------------------------------------

--
-- Table structure for table `caddress`
--

CREATE TABLE `caddress` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `caddress`
--

INSERT INTO `caddress` (`id`, `id_customer`, `address`, `address_code`, `updated_by`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(18, 5, 'Jl. Ende No. 33-35 Tg. Priok, Jakarta Utara', '790628', 0, 0, NULL, '2018-06-26 23:46:18', '2018-06-26 23:46:18'),
(19, 1, 'Jl. Muara Baru Ujung , Jakarta Utara', '790627', 0, 0, NULL, '2018-06-26 23:47:44', '2018-06-26 23:47:44'),
(20, 6, 'Jl. Raya Kamal JORR W 1 paket 6& 7 (Depan Polsek Cikarang)', '790630', 0, 0, NULL, '2018-06-26 23:48:12', '2018-06-26 23:48:12'),
(21, 7, 'Jl. Gresik Blok. A12 No. 1KBN Marunda, Jakarta Utara', '790632', 0, 0, NULL, '2018-06-26 23:48:31', '2018-06-26 23:48:31'),
(22, 8, 'KUD Mina Darma Ds. Tanjung Pasir Kec. Teluk Naga, Tangerang', '790633', 0, 0, NULL, '2018-06-26 23:48:57', '2018-06-26 23:48:57');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` text COLLATE utf8_unicode_ci,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `npwp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_skp_migas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_sold_to_penyalur` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `permission` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `kode`, `name`, `address`, `password`, `logo`, `phone_number`, `npwp`, `fax`, `no_skp_migas`, `no_sold_to_penyalur`, `status`, `permission`, `role`, `updated_by`, `created_by`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ALJ', 'PT.  AUDRI LUTFIA JAYA', 'KP. TURI JAYA RT. 08/07 NO. 34 DESA SEGARAMAKMUR KEC. TARUMAJAYA, BEKASI 17211', 'admin123', NULL, '021-88992082', '02.198.641.9-435.000', '021-4413830', '7180/Ket/15/DMO/2013', '745866', 0, '', '', 0, 0, NULL, NULL, NULL, '2018-06-26 17:30:22'),
(2, 'SAJ', 'PT. SUMA ADI JAYA', 'KP.  TURI JAYA RT. 010/07 SEGARAMAKMUR TARUMAJAYA - BEKASI', 'admin123', NULL, '021-44855354', '21.023.530.5-435.000', '000000', '0000000', '00000', 0, '', '', 0, 0, NULL, NULL, NULL, '2018-07-03 02:01:37'),
(3, 'PP', 'PT. pete pete', 'Pondok indah', '', NULL, '021-989211', '12..0001.01111', '021-888888', 'Ai763-89001-111', '12-A-98291', 0, '', '', 0, 0, '2018-07-03 02:19:27', NULL, '2018-07-03 02:19:14', '2018-07-03 02:19:27');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_usaha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `npwp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pic_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pic_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pic_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_customer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `jenis_usaha`, `phone_number`, `npwp`, `pic_name`, `pic_email`, `pic_phone`, `jenis_customer`, `role`, `permission`, `updated_by`, `created_by`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'PT. First Marine Seafods', 'Industri Makanan Laut', '021 7498065', '02.192.865.0-057.000', 'Jhones', 'jhon@admin.com', '089231231231', 'Darat', '', '', 0, 0, NULL, NULL, NULL, '2018-06-06 21:25:41'),
(2, 'PT. First Marine Seafods', 'Industri Makanan Laut', '021 7498065', '02.192.865.0-057.000', 'Jhon', 'jhon@admin.com', '08945785633', 'Darat', 'mei', '', 0, 0, '2018-06-10 00:22:20', NULL, NULL, '2018-06-10 00:22:20'),
(3, 'Pepper Lunch Senayan', 'Pabrik textile', '6767474747', '12.33.33.3', 'Nadia', 'nanaa', '0315151', 'Darat', '', '', 0, 0, '2018-06-25 20:30:31', NULL, '2018-06-06 20:44:22', '2018-06-25 20:30:31'),
(4, 'senja', 'puisi', '0896372718', '12332414121', 'biru', 'lantunanhati', '89000', 'Puisi Build', '', '', 0, 0, '2018-06-25 20:30:35', NULL, '2018-06-06 21:07:00', '2018-06-25 20:30:35'),
(5, 'PT. Gita Terminal Sarana', 'Jasa Transportasi', '021-00000', '01.691.965.1-042.000', 'jaka turi', 'jaka@turi@gmail.com', '021-11111', 'Darat', '', '', 0, 0, NULL, NULL, '2018-06-25 20:32:51', '2018-06-25 20:32:51'),
(6, 'PT. Hutama Karya', 'Kontraktor Jalan', '021-9999', '01.001.611.1-051.000', 'haka', 'haka@gmail.com', '021-2222', 'Darat', '', '', 0, 0, NULL, NULL, '2018-06-25 20:34:01', '2018-06-25 20:34:01'),
(7, 'PT. Karya Tehnik Pasirindo', 'Jasa Transportasi', '021-3333', '02.388.039.6-045.000', 'trimus', 'trimustofa@gmail.com', '021-4444', 'Darat', '', '', 0, 0, NULL, NULL, '2018-06-25 20:36:01', '2018-06-25 20:36:01'),
(8, 'PT. Kws Sejahtera Utama Engeenering', 'KOSONG', '021-0000', '00000', 'Nadia', 'nadnad@gmail.com', '0000', 'kosong', '', '', 0, 0, NULL, NULL, '2018-06-25 20:40:50', '2018-06-25 20:40:50'),
(9, 'bakso 21', 'bakso', '021-921212', '121212', 'Nadia', 'baksomr', '021211212', 'restoran', '', '', 0, 0, NULL, NULL, '2018-06-28 23:19:12', '2018-06-28 23:19:12');

-- --------------------------------------------------------

--
-- Table structure for table `item_po_keluar`
--

CREATE TABLE `item_po_keluar` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_po_keluar` int(10) UNSIGNED NOT NULL,
  `material` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `uom` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `unit_price` decimal(11,2) NOT NULL DEFAULT '0.00',
  `amount` decimal(11,2) NOT NULL DEFAULT '0.00',
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_po_masuk`
--

CREATE TABLE `item_po_masuk` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_po_masuk` int(10) UNSIGNED NOT NULL,
  `material` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `unit_price` decimal(11,2) NOT NULL DEFAULT '0.00',
  `amount` decimal(11,2) NOT NULL DEFAULT '0.00',
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_po_masuk`
--

INSERT INTO `item_po_masuk` (`id`, `id_po_masuk`, `material`, `description`, `uom`, `qty`, `unit_price`, `amount`, `updated_by`, `created_by`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(14, 12, '0', 'asdasdas', 'sdas', 2, '35.00', '0.00', 0, 0, NULL, NULL, '2018-06-07 10:09:10', '2018-06-07 10:09:10'),
(15, 12, '1', 'ffaf', 'faf', 2, '20.00', '1.00', 0, 0, NULL, NULL, '2018-06-07 10:09:10', '2018-06-07 10:09:10'),
(18, 15, 'c123', 'bensin', 'liter', 300, '1200.00', '360000.00', 0, 0, NULL, NULL, '2018-06-07 23:46:36', '2018-06-07 23:46:36'),
(19, 15, 'd321', 'solar', 'liter', 100, '200.00', '20000.00', 0, 0, NULL, NULL, '2018-06-07 23:46:36', '2018-06-07 23:46:36'),
(20, 13, 'c3121', 'dada', 'liter', 2, '23.00', '46.00', 0, 0, NULL, NULL, '2018-06-07 23:48:43', '2018-06-07 23:48:43'),
(30, 7, 'v123', 'solar', 'liter', 10, '100.00', '1000.00', 0, 0, NULL, NULL, '2018-06-17 00:38:18', '2018-06-17 00:38:18'),
(31, 4, 'bensin', 'bensin banyak', 'liter', 12, '1200.00', '14400.00', 0, 0, NULL, NULL, '2018-06-26 20:40:46', '2018-06-26 20:40:46'),
(33, 1, 'C01241', 'bensin', 'liter', 100, '1200.00', '120000.00', 0, 0, NULL, NULL, '2018-06-28 05:33:04', '2018-06-28 05:33:04'),
(34, 2, 'BNSN123', 'Bensin Premium', 'liter', 100, '900000.00', '90000000.00', 0, 0, NULL, NULL, '2018-06-28 06:19:54', '2018-06-28 06:19:54');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_bbm`
--

CREATE TABLE `jenis_bbm` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis_bbm` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kode_bbm` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jenis_bbm`
--

INSERT INTO `jenis_bbm` (`id`, `jenis_bbm`, `kode_bbm`, `updated_by`, `created_by`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'PATRA DIESEL/HSD', 'HSD', 0, 0, NULL, NULL, '2018-06-26 03:27:08', '2018-06-26 03:27:08'),
(2, 'PATRA FUEL/HSFO 180/MFO 180', '', 0, 0, NULL, NULL, '2018-06-26 03:27:59', '2018-06-26 03:27:59'),
(3, 'PATRA FUEL/HSFO 380/MFO 380', NULL, 0, 0, NULL, NULL, '2018-06-26 03:30:43', '2018-06-26 03:30:43'),
(4, 'PATRA DIESEL PLUS/IDO/MDF/MDO', NULL, 0, 0, NULL, NULL, '2018-06-26 03:34:19', '2018-06-26 03:34:19'),
(5, 'PATRA ULTRA/GASOLINE 88', NULL, 0, 0, NULL, NULL, '2018-06-26 03:34:46', '2018-06-26 03:34:46'),
(6, 'PATRA BIO DIESEL', NULL, 0, 0, NULL, NULL, '2018-06-26 03:35:03', '2018-06-26 03:35:03'),
(7, 'PATRA KEROSENE', NULL, 0, 0, NULL, NULL, '2018-06-26 03:35:26', '2018-06-26 03:35:26'),
(8, 'PATRA AVTUR', NULL, 0, 0, NULL, NULL, '2018-06-26 03:35:38', '2018-06-26 03:35:38'),
(9, 'PATRA DIESEL/HSD', NULL, 0, 0, '2018-06-26 07:18:59', NULL, '2018-06-26 03:44:20', '2018-06-26 07:18:59'),
(10, 'PATRA DIESEL/HSD', NULL, 0, 0, '2018-06-26 07:19:10', NULL, '2018-06-26 07:19:05', '2018-06-26 07:19:10');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_05_23_034041_create_company_table', 1),
(4, '2018_05_23_041548_create_customer', 2),
(5, '2018_05_27_080121_create_agen_table', 3),
(6, '2018_05_27_080833_create_po_keluar_table', 4),
(7, '2018_05_27_080914_create_jenis_bbm_table', 4),
(8, '2018_05_27_081037_create_penjualan_table', 5),
(9, '2018_05_27_081259_create_po_masuk_table', 5),
(10, '2018_05_27_081342_create_item_po_masuk', 6),
(11, '2018_05_28_064325_create_alat_angkut', 7),
(12, '2018_05_28_102853_create_c_address_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_invoice` int(10) UNSIGNED NOT NULL,
  `no_po` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `nama_customer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_faktur_pajak` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `harga_jual` decimal(11,2) NOT NULL DEFAULT '0.00',
  `harga_dasar` decimal(11,2) NOT NULL DEFAULT '0.00',
  `dpp` decimal(11,2) NOT NULL DEFAULT '0.00',
  `ppn` decimal(11,2) NOT NULL DEFAULT '0.00',
  `pbbkb` decimal(11,2) NOT NULL DEFAULT '0.00',
  `total` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id`, `no_invoice`, `no_po`, `tanggal`, `nama_customer`, `id_customer`, `type`, `no_faktur_pajak`, `qty`, `harga_jual`, `harga_dasar`, `dpp`, `ppn`, `pbbkb`, `total`, `status`, `updated_by`, `created_by`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 12314, 141241, '0000-00-00', '', 0, '', '412414', 1121, '1500.00', '7800.00', '0.00', '0.00', '0.00', 121323, 0, 0, 0, NULL, NULL, '2018-06-13 06:41:06', '2018-06-13 06:41:06');

-- --------------------------------------------------------

--
-- Table structure for table `po_keluar`
--

CREATE TABLE `po_keluar` (
  `id` int(10) UNSIGNED NOT NULL,
  `nomor_po` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_skp_penyalur` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_sold_penyalur` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_ship_to_konsumen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npwp_penyalur` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `penyalur` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat_penyalur` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_konsumen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat_konsumen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sales_district` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_konsumen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jenis_bbm` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `jumlah_bbm` int(11) NOT NULL,
  `pbbkb` int(11) NOT NULL,
  `pph` int(11) NOT NULL,
  `ppn` int(11) NOT NULL,
  `pbbkb_total` int(11) NOT NULL,
  `pph_total` int(11) NOT NULL,
  `dasar_pengenaan_ppn_pph` int(11) NOT NULL,
  `nilai_transaksi` int(11) NOT NULL,
  `bukti_setor_bank` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supply_point` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama_pic_penyalur` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_pic_penyalur` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama_pic_agen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_pic_agen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `harga_jual` decimal(11,2) NOT NULL DEFAULT '0.00',
  `harga_dasar` decimal(11,2) NOT NULL DEFAULT '0.00',
  `harga_jual_penyalur` decimal(11,2) NOT NULL DEFAULT '0.00',
  `harga_dasar_penyalur` decimal(11,2) NOT NULL DEFAULT '0.00',
  `margin_penyalur` decimal(11,2) NOT NULL DEFAULT '0.00',
  `cara_pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jatuh_tempo_hari` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `po_keluar`
--

INSERT INTO `po_keluar` (`id`, `nomor_po`, `no_skp_penyalur`, `no_sold_penyalur`, `no_ship_to_konsumen`, `npwp_penyalur`, `penyalur`, `alamat_penyalur`, `nama_konsumen`, `alamat_konsumen`, `sales_district`, `jenis_konsumen`, `jenis_bbm`, `jumlah_bbm`, `pbbkb`, `pph`, `ppn`, `pbbkb_total`, `pph_total`, `dasar_pengenaan_ppn_pph`, `nilai_transaksi`, `bukti_setor_bank`, `supply_point`, `nama_pic_penyalur`, `phone_pic_penyalur`, `nama_pic_agen`, `phone_pic_agen`, `harga_jual`, `harga_dasar`, `harga_jual_penyalur`, `harga_dasar_penyalur`, `margin_penyalur`, `cara_pembayaran`, `jatuh_tempo_hari`, `status`, `updated_by`, `created_by`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '423/ALJ/XI/2012', '7180/Ket/15/DMO/2013', '745866', '790628', '02.198.641.9-435.000', 'PT.  AUDRI LUTFIA JAYA', 'KP. TURI JAYA RT. 08/07 NO. 34 DESA SEGARAMAKMUR KEC. TARUMAJAYA, BEKASI 17211', 'PT. First Marine Seafods', 'Jl. Ende No. 33-35 Tg. Priok, Jakarta Utara', 'Jakarta', 'Industri Makanan Laut', 'PATRA FUEL/HSFO 380/MFO 380', 1000, 17, 0, 11, 0, 0, 11, 5000000, 'mandiri', 'PN-TT TANJUNG GEREM', 'ajeng', '0983525', 'desi', '0212', '8200.00', '7800.00', '8500.00', '1234.00', '1222.00', 'CASH', '20 hari', 0, 0, 0, NULL, NULL, '2018-06-28 01:38:24', '2018-06-28 04:48:51');

-- --------------------------------------------------------

--
-- Table structure for table `po_masuk`
--

CREATE TABLE `po_masuk` (
  `id` int(11) UNSIGNED NOT NULL,
  `customer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nomor_po` varchar(255) NOT NULL,
  `tanggal_po` date NOT NULL,
  `tanggal_kirim` date NOT NULL,
  `po_tujuan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `kode_po_tujuan` varchar(255) DEFAULT NULL,
  `telp_po_tujuan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fax_po_tujuan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pic_po_tujuan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `delivery_office` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `contact_person_1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `contact_person_2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `invoice_status` int(1) DEFAULT '0',
  `subtotal` int(11) NOT NULL,
  `ppn` int(11) NOT NULL,
  `pph` int(11) NOT NULL,
  `add_cost` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `notes` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `term` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `rejected_by` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `po_masuk`
--

INSERT INTO `po_masuk` (`id`, `customer_name`, `nomor_po`, `tanggal_po`, `tanggal_kirim`, `po_tujuan`, `kode_po_tujuan`, `telp_po_tujuan`, `fax_po_tujuan`, `pic_po_tujuan`, `delivery_office`, `contact_person_1`, `contact_person_2`, `status`, `invoice_status`, `subtotal`, `ppn`, `pph`, `add_cost`, `total`, `notes`, `updated_by`, `created_by`, `remember_token`, `created_at`, `updated_at`, `term`, `deleted_at`, `approved_by`, `rejected_by`) VALUES
(1, 'PT. First Marine Seafods', '423/ALJ/XI/2015', '2018-06-28', '2018-06-29', 'PT.  AUDRI LUTFIA JAYA', 'ALJ', '021-88992082', '021-4413830', 'Jakuw', 'Jl. Ende No. 33-35 Tg. Priok, Jakarta Utara', '021-0000', '021-0011', 1, 0, 0, 0, 0, 0, 0, '', 0, 0, '', '2018-06-28 05:21:52', '2018-07-03 09:07:03', 0, NULL, 'admin', NULL),
(2, 'PT. Gita Terminal Sarana', 'C213', '2018-06-29', '2018-06-30', 'PT.  AUDRI LUTFIA JAYA', 'ALJ', '021-88992082', '021-4413830', 'Ariel', 'Jl. Muara Baru Ujung , Jakarta Utara', '021-999999', '021-88888', 0, 0, 0, 100, 0, 200000, 0, '', 0, 0, '', '2018-06-28 06:17:54', '2018-06-28 06:17:54', 0, NULL, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` text COLLATE utf8_unicode_ci,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `permission` text COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `address`, `email`, `photo`, `password`, `company_id`, `phone_number`, `status`, `permission`, `role`, `updated_by`, `created_by`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Catherine', 'catcat', 'PIM', 'maghoni_ivan@yahoo.co.id', NULL, '$2y$10$4Z8E1qNL.5w4hApUXW4/COSi3UbbJ9JCx56QhW8C7x22aThAxrCpq', 1, '021-777777', 0, '', 'superadmin', 0, 0, NULL, NULL, '2018-06-28 20:44:13', '2018-06-28 21:07:21'),
(2, 'asenk', 'admin', 'PIM', 'andikapranata@yahoo.com', NULL, '$2y$10$LQtXb.i8mbuo8CJ5cxUO7e27l4ADKZ5.xK6p2k29rqx9e8V0a//Vq', 1, '021-8888', 0, '', 'admin', 0, 0, NULL, 'OViWvTIk0DbDmRea27hHsjVzgs6oEHG1KZhbcmIAqcpLKV3gNKH1lonTTyUB', '2018-06-28 21:06:05', '2018-06-28 21:08:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agen`
--
ALTER TABLE `agen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alat_angkut`
--
ALTER TABLE `alat_angkut`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `caddress`
--
ALTER TABLE `caddress`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_po_masuk`
--
ALTER TABLE `item_po_masuk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_bbm`
--
ALTER TABLE `jenis_bbm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `po_keluar`
--
ALTER TABLE `po_keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `po_masuk`
--
ALTER TABLE `po_masuk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agen`
--
ALTER TABLE `agen`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `alat_angkut`
--
ALTER TABLE `alat_angkut`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `caddress`
--
ALTER TABLE `caddress`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `item_po_masuk`
--
ALTER TABLE `item_po_masuk`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `jenis_bbm`
--
ALTER TABLE `jenis_bbm`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `po_keluar`
--
ALTER TABLE `po_keluar`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `po_masuk`
--
ALTER TABLE `po_masuk`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
