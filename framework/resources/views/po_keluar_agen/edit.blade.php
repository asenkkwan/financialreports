@extends('layouts.app')
 
@section('css')
 <!-- Select2 -->
  <link rel="stylesheet" href="{{ url('plugins/select2/select2.min.css') }}">
@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">  
      <h1>Form 
        <small>Edit Po Keluar</small> 
      </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Forms</a></li>
          <li class="active">Edit Po Keluar</li>
        </ol>
    </section> 
  <!-- Main content -->
    <section class="content">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            @if (Session::has('message'))
              <div class="alert alert-{{Session::get('alert')}}">
                <button data-dismiss="alert" class="close"></button>
                {!! Session::get('message') !!} 
              </div>
            @endif

            @if ($errors->any())
              <div class="alert alert-danger">
                <button data-dismiss="alert" class="close"></button>
                  {!! implode('', $errors->all('<p>:message</p>')) !!}
              </div>
            @endif
            <!-- general form elements -->
            <div class="box box-success">
              <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
              </div>
              <!-- /.box-header --> 
              <!-- form start -->
              <form role="form" action="{{ url('po-keluar-agen/update/'.$po_keluar_agen->id) }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
                <div class="box-body"> 
                  <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid #eee; ">
                      <div class="form-group">
                        <label for="tanggal">Tanggal *</label>
                        <input type="date" class="form-control" id="tanggal" name="tanggal" value="{{ $po_keluar_agen->tanggal }}" required>
                      </div> 
                      <div class="form-group">
                        <label for="tanggal">Tanggal Pengaliran*</label>
                        <input type="date" class="form-control" id="tanggal_pengaliran" name="tanggal_pengaliran" value="{{ $po_keluar_agen->tanggal_pengaliran }}" required>
                      </div> 
                      <div class="form-group"> 
                        <label for="nomor_po">Nomor PO *</label>
                        <input type="text" class="form-control" id="nomor_po" name="nomor_po"  value="{{ $po_keluar_agen->nomor_po }}" required>
                      </div>   
                       
                      <div class="form-group">
                        <label for="role">Nama Agen *</label>
                        <select class="form-control select2" name="nama_agen" id="nama_agen" onchange="fetchDataAddress(this)" required>
                          <option value="">-- Nama Agen --</option>
                          @foreach($nama_agen as $val) 
                           @if($val->name == $po_keluar_agen->nama_agen)
                            <option value="{{ $val->id }}" selected>{{ $val->name }}</option>
                             @else
                            <option value="{{ $val->id }}">{{ $val->name }}</option>
                            @endif
                          @endforeach
                        </select> 
                      </div> 
                       <div class="form-group"> 
                        <label for="role">Alamat Agen </label>
                        <textarea class="form-control" name="alamat_agen" id="alamat_agen" readonly="">{{ $po_keluar_agen->alamat_agen }}</textarea>
                       </div> 
                      <div class="form-group">
                        <label for="role">Nama Penyalur *</label>
                          <select class="form-control" name="penyalur" id="penyalur" required>
                              @foreach($kantor_cabang as $val)
                                @if($val->name == $po_keluar_agen->penyalur)
                                <option value="{{ $val->id }}" selected>{{ $val->name }}</option>
                                @else
                                <option value="{{ $val->id }}">{{ $val->name }}</option>
                                @endif
                              @endforeach
                          </select>
                      </div>
                     <div class="form-group">
                        <label for="nama_pic_penyalur">Nama PIC Penyalur *</label>
                        <input type="text" class="form-control" id="nama_pic_penyalur"  name="nama_pic_penyalur" value="{{ $po_keluar_agen->nama_pic_penyalur }}" required>
                      </div>
                      <div class="form-group">
                        <label for="nama_pic_agen">Nama PIC Agen </label>
                        <input type="text" class="form-control" id="nama_pic_agen" name="nama_pic_agen"  value="{{ $po_keluar_agen->nama_pic_agen }}" required>
                      </div>
                      <div class="form-group">
                        <label for="role">Jenis BBM </label>
                         <select class="form-control" name="jenis_bbm" id="jenis_bbm" required>    
                            @foreach($jenis_bbm as $val)
                            @if($val->jenis_bbm == $po_keluar_agen->jenis_bbm)
                            <option value="{{ $val->id }}" selected>{{ $val->jenis_bbm }}</option>
                            @else
                            <option value="{{ $val->id }}">{{ $val->jenis_bbm }}</option>
                            @endif
                            @endforeach
                         </select>
                      </div>
                      <div class="form-group">
                        <label for="alat_angkut">Alat Angkut</label>
                         <select name="alat_angkut" class="form-control" id="alat_angkut" >
                          <option selected="selected" >{{ $po_keluar_agen->alat_angkut }}</option>
                          <option value="KAPAL">KAPAL</option>
                          <option value="TANGKI">TANGKI</option>
                        </select>
                      </div>  
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                        <label for="qty">Qty *</label>
                        <input type="text" class="form-control" id="qty" name="qty"  value="{{ $po_keluar_agen->qty }}" required>
                      </div>
                      <div class="form-group">
                        <label for="harga_beli">Harga Beli </label>
                       <input type="text" class="form-control" id="harga_beli" name="harga_beli"  value="{{ $po_keluar_agen->harga_beli }}" required>
                      </div>
                      <div class="form-group">
                        <label for="harga_jual">Harga Jual </label>
                       <input type="text" class="form-control" id="harga_jual" name="harga_jual"  value="{{ $po_keluar_agen->harga_jual }}" required>
                      </div> 
                      <div class="form-group">
                        <label for="pecahan">Pecahan</label>
                       <input type="text" class="form-control" id="pecahan" name="pecahan"  value="{{ $po_keluar_agen->pecahan }}" >
                      </div>
                      <div class="form-group">
                        <label for="ppn">PPN *</label>
                       <input type="text" class="form-control" id="ppn" name="ppn" placeholder="PPN" value="10" required>
                      </div>
                      <div class="form-group">
                        <label for="pbbkb">PBBKB</label> 
                         <select name="pbbkb" class="form-control" id='pbbkb'>
                          <option value="0" @if($po_keluar_agen->pbbkb=="0") selected @endif>{{App\Utilities\Constant::$PBBKB['0.000']}}</option>
                          <option value="0.858" @if($po_keluar_agen->pbbkb=="0.858") selected @endif>{{App\Utilities\Constant::$PBBKB['0.858']}}</option>
                          <option value="1.288" @if($po_keluar_agen->pbbkb=="1.288") selected @endif>{{App\Utilities\Constant::$PBBKB['1.288']}}</option>
                          <option value="4.5" @if($po_keluar_agen->pbbkb=="4.5") selected @endif>{{App\Utilities\Constant::$PBBKB['4.500']}}</option>
                          <option value="5" @if($po_keluar_agen->pbbkb=="5") selected @endif>{{App\Utilities\Constant::$PBBKB['5.000']}}</option>
                          <option value="6" @if($po_keluar_agen->pbbkb=="6") selected @endif>{{App\Utilities\Constant::$PBBKB['6.000']}}</option>
                          <option value="7.5" @if($po_keluar_agen->pbbkb=="7.5") selected @endif>{{App\Utilities\Constant::$PBBKB['7.500']}}</option>
                          <option value="10" @if($po_keluar_agen->pbbkb=="10") selected @endif>{{App\Utilities\Constant::$PBBKB['10.000']}}</option>
                         </select>
                      </div>   
                      <div class="form-group">
                        <label for="pph">PPH</label>
                         <select name="pph" class="form-control" id="pph" required>
                          <option value="0" @if($po_keluar_agen->pph=="0") selected @endif>{{App\Utilities\Constant::$PPH['0.000']}}</option>
                          <option value="0.300" @if($po_keluar_agen->pph=="0.300") selected @endif>0,300%</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="cara_pembayaran">Cara Pembayaran </label>
                         <select name="cara_pembayaran" class="form-control" id="cara_pembayaran" required>
                          <option selected="selected" >{{ $po_keluar_agen->cara_pembayaran }}</option>
                          <option value="CASH">CASH</option>
                          <option value="KREDIT">KREDIT</option>
                        </select>
                      </div>  
                     <!--  <div class="form-group">
                        <label for="bukti_setor_bank">Bukti Setor Bank </label>
                       <input type="text" class="form-control" id="bukti_setor_bank" name="bukti_setor_bank"  value="{{ $po_keluar_agen->bukti_setor_bank }}" required>
                      </div> -->
                       <div class="form-group">
                        <label for="supply_point"> Supply Point </label>
                         <select class="form-control" name="supply_point" id="supply_point" required>    
                            @foreach($supply_point as $val)
                            @if($val->supply_point == $po_keluar_agen->supply_point)
                            <option value="{{ $val->id }}" selected>{{ $val->supply_point }}</option>
                            @else
                            <option value="{{ $val->id }}">{{ $val->supply_point }}</option>
                            @endif
                            @endforeach
                          </select>
                      </div>
                       <div class="form-group">
                        <label for="alamat_kirim">Alamat Kirim </label>
                        <textarea type="text" class="form-control" name="alamat_kirim" id="alamat_kirim" >{{ $po_keluar_agen->alamat_kirim }}</textarea>
                      </div>
                     <!--  <div class="form-group">
                        <label for="notes">Note *</label>
                       <input type="text" class="form-control" id="notes" name="notes"  value="{{ $po_keluar_agen->notes }}" required>
                      </div>  --> 
                  </div>
                </div>
              </div>
                <div class="box-footer">
                  <button action="{{ url('po-keluar-agen/item-po-keluar/'.$po_keluar_agen->id) }}" type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
           </form>
          </div>
      </div>
    </section>
   </div>
    
@endsection
@section('javascript')
<!-- Select2 -->
<script src="{{ url('plugins/select2/select2.full.min.js')}}"></script>
<script>
  $(function () 
    {
      //Initialize Select2 Elements
      $(".select2").select2();

      
    });

  function fetchDataAddress(elem){
    var agen = $(elem).val();

    $.ajax({
      url:"{{ url('agen/getAddress') }}?id="+agen,
      type:"get",
      dataType:"json",
      beforeSend:function(){

      },
      success:function(result){
        if(result.status = "ok"){
          var element = $("#alamat_agen");
          var html = result.data.address;
          element.html(html);
        }else{
          var html = "<option value=''>Silahkan Pilih Agen</option>";
          element.html(html);
        }
      },
      error:function(){

      },
      complete:function(){

      }
    });
  }
</script>
@endsection