<!DOCTYPE html>
<html>
<head>
  <link rel="shortcut icon" href="{{ url( 'dist/img/logoFR.png') }}"> 
  <meta charset="utf-8"> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Financial Reports  | Invoice</title> 
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 --> 
  <link rel="stylesheet" href="{{ url('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('dist/css/AdminLTE.min.css') }}">
   <style> 
         body{
               margin:auto; 
              }  
          p {
            font-size: 15px !important; 
            font-family: times new roman;
            word-spacing: 2px;
          }
          .wrapper {
                   /* height: 100%;*/
                    position: relative;
                    overflow-x: hidden;
                    overflow-y: hidden;
                    display: hidden;
            }
           page[size="A4"] {
            background: white;
            width: 21cm;
            height: 29.7cm;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
          }
           .kop_surat {
              position: absolute;
              width: 21cm;  
              margin-top: -45px;
            }
          .invoice {
              position: relative;
              background: none; 
              border: 0px solid #f4f4f4;
              margin-left: 15px;
          }
          .note {
                  font-style: italic;
                }
          .footer {
            font-size: 12px !important;
          } 
          @media print {
            body, page[size="A4"] {
              width: 21cm;
              height: 29.7cm;
              margin: 0;
              box-shadow: 0;
            }
           .wrapper {
                   /* height: 100%;*/
                    position: relative;
                    overflow-x: hidden;
                    overflow-y: hidden;
                    display: hidden;
            }
           .form{
                  border:none;
                }
           .invoice {
              position: relative;
              background: none; 
              border: 0px solid #f4f4f4;
              margin-left: 5px;
          }  
           .note {
                  font-style: italic;
                } 
         
      }
  </style>
</head>
<?php
 $image ="";
  if($company->kop_surat){
    $image = unserialize($company->kop_surat);
    if($image){
      $image = url('dist/img/')."/".$image['original'];
    }
  }
?>                    
  <page size="A4" layout="portrait">
    <img src='{{ $image }}' class="kop_surat">
<body onload="window.print();">
  <div class="wrapper">
    <!-- Main content -->
    <section class="invoice" >
      <div class="row" style="margin-top: 130px;">
        <div class="col-xs-12">
          <table border="0" style="width:100%">
            <tr>
             <!--  <td style="width: 39%">
               <div style="text-decoration:underline; font-size: 17px;"><b>PURCHASE ORDER</b></div>
               <div style="float:left; font-size: 15px;">No</div>
               <div style="float:left; margin-left:15px;">:</div>
               <div style="float:left; font-size: 15px; margin-left:5px;">{{ $po_keluar_agen->nomor_po}}</div>
               <div style="float:left; font-size: 15px; margin-top:20px; margin-left: -105px;">Date</div>
               <div style="float:left; font-size: 15px; margin-top:20px; margin-left: -70px; margin-bottom: 8px;">: {{ date('d F Y ') }}</div>
              </td><br><br> -->
              <td style="width: 39%; font-size: 17px; text-decoration: underline;"><strong>PURCHASE ORDER</strong></td><br>
            </tr>
            <tr>
              <td style="float: left; width: 10%;">No</td>
              <td style="float: left; width: 3%;">:</td>
              <td style="float: left; width: 42%;">{{ $po_keluar_agen->nomor_po}}</td>
            </tr>
            <tr>
              <td style="float: left; width: 10%;">Date</td>
              <td style="float: left; width: 3%;">:</td>
              <td style="float: left; width: 35%;">{{ date('d F Y ') }}</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 50%">
               <p style="margin-bottom: 20px;">Kepada Yth,<br>
                {{ $po_keluar_agen->nama_agen}}<br>
                Di {{ $po_keluar_agen->alamat_agen}}</p>
              </td>
              <td style="width: 30%"></td>
              <td style="width: 30%">
                <!-- Contact Person 1 : {{ $po_keluar_agen->contact_person_1 }}<br>
                Contact Person 2 : {{ $po_keluar_agen->contact_person_2 }}<br> -->
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="row" style="margin-top: 20px;">
        <div class="col-xs-12 table-responsive">
          <td>
            <p>Dengan Hormat, <br>
            Kami Konfirmasikan Pemesanan BBM untuk Customer kami sbb : <br>
          </td>
        
        </div>
      </div>
       <div class="row" style="margin-top: 20px;">
          <div class="col-xs-12">
            <table border="1 solid black" class="table table-striped table-bordered" style="margin-top: -25px; width: 99%; border: 1px solid black">
              <thead>
              <tr>
                <th style="width:5%; text-align:center; border: 1px solid black;" >No</th>
                <th style="width:22%; text-align:center; border: 1px solid black;">Nama Barang/Product</th>       
                <th style="width:8%; text-align:center; border: 1px solid black;">Quantity</th>
                <th style="width:8%; text-align:center; border: 1px solid black;">Harga/Liter</th>
                <th style="width:8%; text-align:center; border: 1px solid black;">Jumlah</th>
              </tr>
              </thead>
              <tbody> 
                <?php 
                  // $total_amount = 0;
                  $counter = 1;
                ?>
                @foreach($po_keluar_agen as $val)
                <tr>
                  <td style="text-align:center; border: 1px solid black;">{{ $counter }}</td>          
                  <td style="text-align:center; border: 1px solid black;">{{ $po_keluar_agen->jenis_bbm }}</td>
                  <td style="text-align:center; border: 1px solid black;">{{ number_format( $po_keluar_agen->qty, 0, ",", ".") }}</td>
                  <td style="text-align:center; border: 1px solid black;">Rp. {{ number_format($po_keluar_agen->harga_beli, 2, ",", ".") }}</td>
                  <td style="text-align:center; border: 1px solid black;">Rp. {{ number_format($po_keluar_agen->total, 0, ",", ".") }}</td>
                  
                </tr>
               <!-- <?php 
                  $counter++;
                ?>
                @endforeach  -->
                <tr>
                  <td style="border: 1px solid black;" rowspan="1" colspan="1" >
                   <!--  <p>Payment Term : {{ $po_keluar_agen->payment_term }}</p> -->
                  </td>
                  <td style="border: 1px solid black;" rowspan="1" colspan="1" >
                    Pecahan : {{ $po_keluar_agen->pecahan }}
                  </td>
                  <td style="border: 1px solid black;" rowspan="1" colspan="1" >
                    <!-- <p>Payment Term : {{ $po_keluar_agen->payment_term }}</p> -->
                  </td>
                  <td style="border: 1px solid black;" rowspan="1" colspan="1" ></td> 
                  <td style="border: 1px solid black;" rowspan="1" colspan="1" ></td>
                </tr>
                <tr>
                  <td style="border: 1px solid black;" rowspan="1" colspan="1" >
                    <!-- <p>Payment Term : {{ $po_keluar_agen->payment_term }}</p> -->
                  </td>
                  <td rowspan="1" colspan="3" style="text-align: left; border: 1px solid black;">
                    <?php 
                     $stringSearch = "MFO";
                     $stringSearch2 = "balongan";
                     $str= $po_keluar_agen->jenis_bbm;
                     $strSupply= $po_keluar_agen->supply_point;
                    ?>
                    @if(stripos($str, $stringSearch) !== false)
                    <div>
                      Include PPN 10% 
                    </div> 
                    @elseif(stripos($strSupply, $stringSearch2) !== false)
                    <div>
                      Include PPN 10% 
                    </div> 
                    @else
                     <div>
                      Include PPN 10% & {{$po_keluar_agen->pbbkb}} %
                    </div>
                    @endif
                  </td>                
                  <!-- <td style="text-align:center;"><b>Rp. {{ number_format( $po_keluar_agen->harga_beli, 0, ",", ".") }}</b></td> -->
                  <td style="text-align:center; border: 1px solid black;"><b>Rp. {{ number_format( $po_keluar_agen->total, 0, ",", ".") }}</b></td>               
                </tr>
              
              </tbody>
            </table>
          </div> 
          <!-- /.col -->
        </div>

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-12">
          <p style="margin-top: 10px;">
            <b>Note :</b><br></p>
            <p style="word-spacing: 5px; margin-left: 15px;">
            = Spesifikasi sesuai dengan product Pertamina <br>
            = Lokasi Pengambilan {{ $po_keluar_agen->supply_point }} <br>
            = Pembayaran {{ $po_keluar_agen->cara_pembayaran }}  <br>
            = Diangkut Dengan {{ $po_keluar_agen->alat_angkut }} <br>
            = Pengaliran Tgl, {{ date('d F Y', strtotime($po_keluar_agen->tanggal_pengaliran)) }} <br><br></p>
            <p>
            Demikian PO ini kami sampaikan Atas kerjasamanya kami ucapkan terima kasih <br></p>
          </p>
        </div>
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-12">
          <table border="0" style="width: 100%;">
            <tr>
              <td style="width: 27%; font-family: times new roman; font-size: 15px;">
                Bekasi,  {{ date('d F Y ') }}<br>
                <p><strong>Dibuat Oleh,</strong></p><br>
                <br><br><br>
               <p> ( {{ Auth::user()->name }} )</p> <br>
              </td>
              <td style="width: 50%"></td>
              <td style="width: 50%">
                <p><strong>Disetujui Oleh,</strong></p><br>
                 <br><br><br>
                  @if($company->id ==1)
                  <div>
                   <img src="{{ url('dist/img/ttdALJ.png') }}" style="width: 100%; margin-top: -85px; margin-left: -45px;">
                   <p style="margin-left:43px; margin-top:-35px;">Andi</p>
                  </div>
                  @else($company->id ==2)
                  <div style="margin-top: -25px;">
                   <img src="{{ url('dist/img/ttdSuma.png') }}" style="width: 100%; margin-top: -65px; margin-left: -45px;">
                   <p style="margin-left:40px; margin-top:-35px;">Andi</p>
                  </div>
                  @endif
              </td>
            </tr>
          </table>
        </div>
      </div>
      <br>
      <br>
      <div class="row" align="center">
          @if($company->id ==1)
          <div class="footer" style="    margin-top: 40px;" >
          <!--  <p>Jl. Turi Jaya Rt. 08/07 No. 34 Ds. Segaramakmur Kec. Tarumajaya - Bekasi 17211<br> tlp. (021) 88992082 - 88992083 Fax. (021) 443830  <br> <strong>E-mail : audrilutfiajaya@ymail.com </strong></p> -->
          </div>
          @else($company->id ==2)
          <div style="margin-top: 15px;" >
            <p class="footer" style="color:#176db9 !important;">Jl. Turi Jaya Rt.10/07 Ds. Segaramakmur Kec. Tarumajaya - Bekasi <br> Telp. (021) 88992082 <strong style="color:red !important;">Email.Sumaadijaya.com</strong></p>
          </div>
          @endif
        </div>
      <br>
    </section>
    <!-- /.content -->
    <!-- /.content -->
</div>
</body>
</html>

