@extends('layouts.app')
 
@section('css')

@endsection
   
@section('content')
<style>
   @media print
    {    
        .no-print, .no-print *
        {
            display: none !important;
        }
        #print 
        { 
          display: none ;
        } 
    }
 
</style>
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Purchase Order
        <small>{{ $po_keluar_agen->nomor_po}}</small> 
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Detail</a></li>
        <li class="active">PO Keluar</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="invoice">
      <!-- info row -->
       <div class="row">
        <div class="col-xs-12 invoice-col" style="text-align:center;">
          <table class="table">
            <tr>
              <td align="center">
               <font size="6" style="text-decoration:underline;">Purchase Order</font><br>
                {{ $po_keluar_agen->nomor_po }}
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <table border="0" style="width:100%">
            <tr>
              <td style="width: 30%">
                  <b>Kepada : </b><br>
                  {{ $po_keluar_agen->nama_agen}}<br>
                  <p>Ditempat<br></p>
                  <p>Attn : {{ $po_keluar_agen->nama_pic_agen}}</p>
                  <!-- Up : {{ $po_keluar_agen->pic_po_tujuan }} -->
                </td>
                <td style="width: 40%"></td>
                <td style="width: 30%"> 
                  <!-- Nomor PO (Order No) : {{ $po_keluar_agen->no_po }}<br> -->
                  Tanggal : {{ date('d F Y', strtotime($po_keluar_agen->tanggal)) }}<br>
                  Tanggal Pengaliran : {{ date('d F Y', strtotime($po_keluar_agen->tanggal_pengaliran)) }}<br>
                  Alamat Kirim :  <strong>{{ $po_keluar_agen->alamat_kirim}}</strong><br>
                  <!-- Mata Uang : IDR<br> -->
                </td>
              </tr>
              
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td style="width: 30%">
                 <!--  <b>Delivery Office</b><br>
                 {{ $po_keluar_agen->lokasi_perusahaan}}<br> -->
                </td>
                <td style="width: 40%"></td>
                <td style="width: 30%">
                  <!-- Contact Person 1 : {{ $po_keluar_agen->contact_person_1 }}<br>
                  Contact Person 2 : {{ $po_keluar_agen->contact_person_2 }}<br> -->
                </td>
            </tr>
          </table>
        </div>
      </div>
      <!-- /.row -->

        <!-- Table row -->
        <div class="row" style="margin-top: 20px;">
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
              <tr>
                <th style="width:5%; text-align: center;" >No</th>
                <th style="width:22%; text-align: center;">Jenis Barang</th>            
                <th style="width:8%; text-align: center;">Quantity</th>
                <th style="width:8%; text-align: center;">Harga/Liter</th>
                <th style="width:8%; text-align: center;">Jumlah</th>
                <!-- <th style="width:8%;">DPP</th>
                <th style="width:8%;">PPN</th>
                <th style="width:8%;">PBBKB Dasar</th>
                <th style="width:8%;">PBBKB Jual</th> -->
                <!-- <th style="width:8%; text-align:center;">Amount</th> -->
              </tr>
              </thead> 
              <tbody> 
                <?php 
                  // $total_amount = 0;
                  $counter = 1;
                ?>
                @foreach($po_keluar_agen as $val)
                <tr>
                  <td style="text-align:center;">{{ $counter }}</td>          
                  <td style="text-align:center;">{{ $po_keluar_agen->jenis_bbm }}</td>
                  <td style="text-align:center;">{{ number_format( $po_keluar_agen->qty, 0, ",", ".") }}</td>
                  <td style="text-align:center;">Rp. {{ number_format($po_keluar_agen->harga_dasar, 3, ",", ".") }}</td>
                  <td style="text-align:center;">Rp. {{ number_format($po_keluar_agen->dpp, 0, ",", ".") }}</td>
                  <!-- <td style="text-align:right;">{{ number_format($po_keluar_agen->pbbkb_dasar, 2, ",", ".") }}</td>
                  <td style="text-align:right;">{{ number_format($po_keluar_agen->pbbkb_jual, 2, ",", ".") }}</td>
                  <td style="text-align:right;">{{ number_format($po_keluar_agen->dpp, 2, ",", ".") }}</td>
                  <td style="text-align:right;">{{ number_format($po_keluar_agen->jumlah_ppn, 2, ",", ".") }}</td>
                  <td style="text-align:right;">{{ number_format($po_keluar_agen->jumlah, 2, ",", ".") }}</td> -->
                </tr>
               <!-- <?php 
                  $counter++;
                ?>
                @endforeach  -->
                <tr>
                  <td rowspan="1" colspan="1" >
                    <!-- <p>Payment Term : {{ $po_keluar_agen->payment_term }}</p> -->
                  </td>
                  <td rowspan="1" colspan="1" >
                    <p style="text-align:left;">Pecahan : {{ $po_keluar_agen->pecahan }}</p>
                  </td>
                  <td rowspan="1" colspan="1" >
                    <!-- <p>Payment Term : {{ $po_keluar_agen->payment_term }}</p> -->
                  </td>
                  <th style="text-align: center;">PPN 10%</th> 
                  <td style="text-align:center;">Rp. {{ number_format( $po_keluar_agen->jumlah_ppn, 0, ",", ".") }}</td>
                </tr>
                <tr>
                  <td rowspan="1" colspan="3" style="text-align: center;">
                    <b>Jumlah</b>
                  </td>                
                  <td style="text-align:center;"><b>Rp. {{ number_format( $po_keluar_agen->harga_beli, 2, ",", ".") }}</b></td>
                  <td style="text-align:center;"><b>Rp. {{ number_format( $po_keluar_agen->jumlah, 0, ",", ".") }}</b></td>               
                </tr>
                <tr>
                  <td rowspan="1" colspan="10" >
                    <p>Terbilang :</p>
                    <p>{{ terbilang($po_keluar_agen->jumlah, 1) }} RUPIAH</p>
                  </td>
                  <!-- <th>TOTAL</th>
                  <td style="text-align:right;">{{ number_format($po_keluar_agen->jumlah, 2, ",", ".") }}</td> -->
                </tr>
                <tr>
                  <td colspan="10">Note : {{ $po_keluar_agen->notes }}</td>
                </tr>
              </tbody>
            </table> 
          </div>
          <!-- /.col -->
        </div>
      
         
        <div class="row">
          <!-- accepted payments column -->
        @if($company->id ==1)
          <div class="col-xs-12">
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
              Note :<br>
              - Pembayaran dilakukan 1 minggu setelah Pihak PT. Audri Lutfia Jaya menerima Receipt Bunker <br>
              - Mohon Dilampirkan Receipt Bunker PT. Audri Lutfia Jaya Pada saat Pengiriman Barang. <br>
              - Berita Acara Bunker dan Resi Bunker dapat diminta langsung dan ditanda tangani oleh kedua belah pihak. <br>
            </p>
          </div>
           @else($company->id ==2)
           <div class="col-xs-12">
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
              Note :<br>
              - Pembayaran dilakukan 1 minggu setelah Pihak PT. Suma Adi Jaya menerima Receipt Bunker <br>
              - Mohon Dilampirkan Receipt Bunker PT. Suma Adi Jaya Pada saat Pengiriman Barang. <br>
              - Berita Acara Bunker dan Resi Bunker dapat diminta langsung dan ditanda tangani oleh kedua belah pihak. <br>
            </p>
          </div>
          @endif
        </div>
        <!-- /.row -->

        <div class="row">
          <div class="col-xs-12">
            <table border="0" style="width: 100%">
              <tr>
                <td style="width: 30%">
                   <strong>Bekasi,  {{ date('d M Y ') }}</strong><br>
                  <br><br><br><br><br><br>
                  ( {{ Auth::user()->name }} )<br>
                </td>
                <td style="width: 50%"></td>
                <td style="width: 20%">
                  <strong>Disetujui Oleh,</strong><br>
                  {{ strtoupper($po_keluar_agen->nama_perusahaan) }}<br>
                  <br><br><br><br><br>
                  {{ strtoupper($po_keluar_agen->approved_by_client) }}<br>
                </td>
              </tr>
            </table>
            
          </div>
        </div>
        <br>
        <br>

     <!--  <div class="row">
        <div class="col-xs-12">
          <table border="0" style="width: 100%">
            <tr>
              <td style="width: 30%">
                <strong>Prepared By,</strong><br>
                <br><br><br><br><br><br>
                {{ strtoupper($po_keluar_agen->prepared_by) }}<br>
              </td>
              <td style="width: 50%"></td>
              <td style="width: 20%">
                <strong>Approved By,</strong><br>
                {{ strtoupper($po_keluar_agen->nama_perusahaan) }}<br>
                <br><br><br><br><br>
                {{ strtoupper($po_keluar_agen->approved_by_client) }}<br>
              </td>
            </tr>
          </table>
          
        </div>
      </div> -->
      <!-- this row will not appear when printing -->
      <div class="row" style="margin-top: 20px;">
        <div class="col-xs-12">
         
          @if($po_keluar_agen->status == 0)
            @if(Auth::user()->enable_approval)
             <button type="button" class="btn btn-success pull-right" onclick="approve({{ $po_keluar_agen->id }})"><i class="fa fa-credit-card"></i> Approve</button>
            <button type="button" class="btn btn-danger pull-right" onclick="reject({{ $po_keluar_agen->id }})" style="margin-right: 5px;"><i class="fa fa-credit-card"></i> Reject</button>
            <form id="form-approval" action="{{ url('po-keluar-agen/approve') }}" method="post">
               {{ csrf_field() }}
               <input type="hidden" name="po_keluar_agen_id" value="{{ $po_keluar_agen->id }}">
             </form>
             <form id="form-reject" action="{{ url('po-keluar-agen/reject') }}" method="post">
               {{ csrf_field() }}
               <input type="hidden" name="po_keluar_agen_id" value="{{ $po_keluar_agen->id }}">
             </form>
            @endif
          @else
           <!--  <a href="{{ url('po-keluar-agen/invoice-printAgen/'.$po_keluar_agen->id) }}" id="print" target="_blank" class="btn btn-default" ><i class="fa fa-print"onclick="window.print()"></i> Print</a> -->
             <a href="{{ url('po-keluar-agen/invoice-printAgen/'.$po_keluar_agen->id) }}"  target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print Agen</a>
              <a href="{{ url('po-keluar-agen/invoice-printNasional/'.$po_keluar_agen->id) }}"  target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print Nasional</a>
          @endif 

        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
@endsection
@section('javascript') 
<script>
  function approve(id){
    if(confirm("Apakah anda menyetujui PO Keluar ini?")){
      $("#form-approval").submit();
    }
  } 

  function reject(id){
    if(confirm("Apakah anda menolak PO Keluar ini?")){
      $("#form-reject").submit();
    }
  } 
</script>
@endsection