@extends('layouts.app')
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{url('plugins/datatables/dataTables.bootstrap.css')}}">
@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"> 
    <h1>
        Form
        <small>Pemesanan BBM</small>
      </h1> 
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a>
      </li>
      <li><a href="#">Forms</a>
      </li> 
      <li class="active">Pemesanan BBM</li>
    </ol>
  </section> 
  <!-- Main content --> 
  <section class="content">
    <div class="row">  
      <!-- left column -->
      <div class="col-md-12"> 
         @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
          
        <!-- general form elements -->
         <div class="box box-primary">
          <div class=" col-sm-12 box-header with-border" style="text-align:center;"> 
            <h2 style="text-decoration:underline;">Formulir Pemesanan BBM Penyalur</h2><br>
          <p style="margin-top: -30px; font-size: 17px;">Nomor : {{ $po_keluar->nomor_po }}</p>
          </div>
          <!-- /.box-header -->
       
                <!-- text input -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid #eee; ">
                      <div class="form-group"> 
                        <label for="nomor_po">Nama Penyalur *</label>
                        <input type="text" class="form-control" id="nomor_po" name="nomor_po" value="{{ $po_keluar->penyalur }}" disabled required >
                      </div>  
                      <div class="form-group">
                        <label for="no_skp_penyalur">No SKP Migas *</label>
                        <input type="text" class="form-control" id="no_skp_penyalur" name="no_skp_penyalur" value="{{ $po_keluar->no_skp_penyalur }}" disabled required>
                      </div>
                      <div class="form-group">
                        <label for="alamat_penyalur">Alamat Penyalur *</label>
                       <input type="text" class="form-control" id="alamat_penyalur"  name="alamat_penyalur" value="{{ $po_keluar->alamat_penyalur }}" disabled required>
                      </div>
                      <div class="form-group">
                        <label for="no_sold_penyalur">No Sold to Penyalur *</label>
                       <input type="text" class="form-control" id="no_sold_penyalur" name="no_sold_penyalur" value="{{ $po_keluar->no_sold_penyalur }}" disabled  required>
                      </div>
                      <div class="form-group">
                        <label for="npwp_penyalur">NPWP Penyalur *</label>
                       <input type="text" class="form-control" id="npwp_penyalur" name="npwp_penyalur" value="{{ $po_keluar->npwp_penyalur }}" disabled required>
                      </div> 
                      <div class="form-group">
                        <label for="jenis_konsumen">Jenis Konsumen *</label>
                       <input type="text" class="form-control" id="jenis_konsumen" name="jenis_konsumen" value="{{ $po_keluar->jenis_konsumen }}" disabled required>
                      </div>
                       <div class="form-group">
                        <label for="nama_konsumen">Diserahkan Kepada *</label>
                       <input type="text" class="form-control" id="nama_konsumen" name="nama_konsumen" value="{{ $po_keluar->nama_konsumen }}" disabled required>
                      </div>
                    <div class="form-group">
                        <label for="alamat_konsumen">Alamat Konsumen *</label>
                       <input type="text" class="form-control" id="alamat_konsumen" name="alamat_konsumen" value="{{ $po_keluar->alamat_konsumen }}" disabled required>
                      </div>
                      <div class="form-group">
                        <label for="sales_district">Sales District Konsumen *</label>
                       <input type="text" class="form-control" id="sales_district" name="sales_district" value="{{ $po_keluar->sales_district }}" disabled required>
                      </div>
                      <div class="form-group"> 
                        <label for="kode_lokasi_konsumen">No Ship-to Konsumen *</label>
                       <input type="text" class="form-control" id="kode_lokasi_konsumen" name="kode_lokasi_konsumen" value="{{ $po_keluar->kode_lokasi_konsumen }}" disabled required>
                      </div>
                      <div class="form-group"> 
                        <label for="jenis_usaha">Jenis Usaha Konsumen *</label>
                       <input type="text" class="form-control" id="jenis_usaha" name="jenis_usaha" value="{{ $po_keluar->jenis_usaha }}" disabled required>
                      </div>
                      <div class="form-group">
                        <label for="jenis_bbm">Jenis BBM *</label>
                       <input type="text" class="form-control" id="jenis_bbm" name="jenis_bbm" value="{{ $po_keluar->jenis_bbm }}" disabled required>
                      </div> 
                      <div class="form-group">
                        <label for="qty">Jumlah BBM *</label>
                       <input type="text" class="form-control" id="qty" name="qty" value="{{ $po_keluar->qty }}" disabled required>
                      </div>
                      <div class="form-group">
                      <label for="supply_point">Supply Point *</label>
                     <input type="text" class="form-control" id="supply_point" name="supply_point" value="{{ $po_keluar->supply_point }}" disabled required>
                    </div>
                      <div class="form-group">
                        <label for="cara_pembayaran">Cara Pembayaran *</label>
                       <input type="text" class="form-control" id="cara_pembayaran" name="cara_pembayaran" value="{{ $po_keluar->cara_pembayaran }}" disabled required>
                      </div> 
                    </div>
                    <div class="col-md-6">                
                      <div class="form-group">
                        <label for="dpp">Dasar Pengenaan PPN dan PPH *</label>
                       <input type="text" class="form-control" id="dpp" name="dpp" value="{{ number_format($po_keluar->dpp, 0, ",", ".") }}" disabled required>
                      </div>
                      <div class="form-group">
                        <label for="ppn">PPN *</label>
                       <input type="text" class="form-control" id="ppn" name="ppn" value="{{ number_format($po_keluar->jumlah_ppn, 0, ",", ".") }}" disabled required>
                      </div>
                      <div class="form-group">
                        <label for="pbbkb">PBBKB (DPP end user x tarif PBBKB) *</label>
                       <input type="text" class="form-control" id="pbbkb" name="pbbkb" value="{{ number_format($po_keluar->pbbkb_jual, 0, ",", ".") }}" disabled required>
                      </div>
                      <div class="form-group">
                        <label for="pph">PPH *</label> 
                       <input type="text" class="form-control" id="pph" name="pph" value= "@if($po_keluar->pph==NULL) @endif {{App\Utilities\Constant::$PPH['NULL']}}" disabled required>
                      </div>
                      <div class="form-group">
                        <label for="jumlah">Nilai Transaksi (13x17) termasuk pajak *</label>
                       <input type="text" class="form-control" id="jumlah" name="jumlah" value="{{ number_format($po_keluar->jumlah, 0, ",", ".") }}" disabled required>
                      </div>
                      <div class="form-group">
                        <label for="bukti_setor_bank">Bukti Setor Bank *</label>
                       <input type="text" class="form-control" id="bukti_setor_bank" name="bukti_setor_bank" value="{{ $po_keluar->bukti_setor_bank }}" disabled required>
                      </div>
                      <div class="form-group">
                        <label for="tanggal">Tanggal Pengisian *</label>
                        @foreach($alat_angkut as $val)
                       <input type="text" class="form-control" id="tanggal" name="tanggal" value="{{ date('d F Y', strtotime($val->tanggal)) }}" disabled required>
                       @endforeach
                      </div>
                      <div class="form-group">
                        <label for="nopol">No. Pol truk tangki/tongkang *</label>
                        @foreach($alat_angkut as $val)
                       <input type="text" class="form-control" id="nopol" name="nopol" value="{{ $val->nopol }}" disabled required>
                       @endforeach
                      </div>
                      <div class="form-group">
                        <label for="volume">Volume BBM *</label>
                       @foreach($alat_angkut as $val)
                       <input type="text" class="form-control" id="volume" name="volume" value="{{ $val->volume }}" disabled required>
                       @endforeach
                      </div>
                      <div class="form-group">
                        <label for="nama_supir">Nama Supir/Nahkoda *</label>
                       @foreach($alat_angkut as $val)
                       <input type="text" class="form-control" id="nama_supir" name="nama_supir" value="{{ $val->nama_supir }}" disabled required>
                       @endforeach
                      </div>
                      <div class="form-group">
                        <label for="harga_jual">Harga Jual ke end User/Liter tanpa pajak *</label>
                       <input type="text" class="form-control" id="harga_jual" name="harga_jual" value="{{ number_format($po_keluar->harga_jual, 3, ",", ".") }}" disabled required>
                      </div>
                      <div class="form-group">
                        <label for="pbbkb_dasar">Harga Dasar ke end User/Liter tanpa pajak *</label>
                       <input type="text" class="form-control" id="pbbkb_dasar" name="pbbkb_dasar" value="{{ number_format($po_keluar->pbbkb_dasar, 3, ",", ".") }}" disabled required>
                      </div>
                      <div class="form-group">
                        <label for="harga_beli">Harga Tebus ke end User/Liter tanpa pajak *</label>
                       <input type="text" class="form-control" id="harga_beli" name="harga_beli" value="{{ number_format($po_keluar->harga_beli, 3, ",", ".") }}" disabled required>
                      </div>
                      <div class="form-group">
                        <label for="harga_dasar">Harga Dasar Penyalur/Liter tanpa pajak *</label> 
                       <input type="text" class="form-control" id="harga_dasar" name="harga_dasar" value="{{ number_format($po_keluar->harga_dasar, 3, ",", ".") }}" disabled required>
                      </div>
                      <div class="form-group">
                        <label for="margin_penyalur">Margin/Liter Penyalur (15-17) *</label>
                       <input type="text" class="form-control" id="margin_penyalur" name="margin_penyalur" value="{{ number_format(($po_keluar->pbbkb_dasar-$po_keluar->harga_dasar), 3, ",", ".") }}" disabled required>
                      </div>
                      
                  </div>                 
                </div>
              </div> 
               <div class="row no-print" style="padding:40px;"> 
                <div class="col-xs-12"> 
                  @if($po_keluar->status == 0)

                    @if(Auth::user()->enable_approval)
                     <button type="button" class="btn btn-success pull-right" onclick="approve({{ $po_keluar->id }})"><i class="fa fa-credit-card"></i> Approve</button>
                    <button type="button" class="btn btn-danger pull-right" onclick="reject({{ $po_keluar->id }})" style="margin-right: 5px;"><i class="fa fa-credit-card"></i> Reject</button>
                     <form id="form-approval" action="{{ url('po-keluar/approve') }}" method="post">
                       {{ csrf_field() }}
                       <input type="hidden" name="po_keluar_id" value="{{ $po_keluar->id }}">
                     </form>
                     <form id="form-reject" action="{{ url('po-keluar/reject') }}" method="post">
                       {{ csrf_field() }}
                       <input type="hidden" name="po_keluar_id" value="{{ $po_keluar->id }}">
                     </form>
                    @endif
                    @else
                      <a href="{{ url('po-keluar/invoice-print/'.$po_keluar->id) }}"  target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                     <!--  <a href="{{ url('po-keluar/invoice-printAgen/'.$po_keluar->id) }}"  target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print Agen</a>
                      <a href="{{ url('po-keluar/invoice-printNasional/'.$po_keluar->id) }}"  target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print Nasional</a> -->
                  @endif     
                </div>
              </div>        
        </section>
      </div> 
@endsection 
@section('javascript')
<script>
  function approve(id){
    if(confirm("Apakah anda menyetujui PO Keluar ini?")){
      $("#form-approval").submit();
    }
  } 

  function reject(id){
    if(confirm("Apakah anda menolak PO Keluar ini?")){
      $("#form-reject").submit();
    }
  } 
</script>
@endsection