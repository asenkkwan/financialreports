<!DOCTYPE html>
<html>
<head>
  <link rel="shortcut icon" href="{{ url( 'dist/img/logoFR.png') }}">
  <meta charset="utf-8"> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
  <title>Financial Reports | Invoice</title>
  <!-- Bootstrap 3.3.6 --> 
  <link rel="stylesheet" href="{{ url('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('dist/css/AdminLTE.min.css') }}">
  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
<style>
   body{
            margin:auto;
            font-size: 12px !important; 
          }  
           page[size="A4"] {
            background: white;
            width: 21cm;
            height: 29.7cm;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
          }
          .kop_surat {
              position: absolute;
              width: 21cm;  
              margin-top: -45px;
            }
            .invoice-detail {
              position: relative;
               background: none; 
              border: 0px solid #f4f4f4;
          }
      @media print {
            body, page[size="A4"] {
              width: 21cm;
              height: 29.7cm;
              margin: 0;
              box-shadow: 0;
            }
            .invoice-detail {
              font-size: 12px;
              margin-top: -7px !important;
              margin-left: 9px;
            }
            .harga {
              color: red !important;
            }
            .wrapper {
                   /* height: 100%;*/
                    position: relative;
                    overflow-x: hidden;
                    overflow-y: hidden;
                    display: hidden;
            }
            .form{
                  border:none;
                }
            .form1 {
              float:left; 
              color:deepskyblue !important;
            }
            .form2 {
              float:left; 
              
            }
            .form3 {
              float:left; 
              
            }
            .ttd {
              text-align: left; 
              width: 125px; 
              padding-top: 62px; 
              padding-left: 561px;
            }
            .tanggal{
              margin-top: 6px;
            }
             .nopol{
              margin-top: -2px;
            }
      }
</style>
</head>
<body class="A4 potrait" onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
<page size="A4" layout="portrait">
  <section class="sheet padding-0mm" style="margin:auto;">
    <div class="row" align="left ">
      <img src="{{ url( 'dist/img/formPatra.png') }} " style="width: 100%;  margin-left: 15px; margin-top: -27px; position: absolute; ">
      </div>
    <div class="row">
       <div class=" col-sm-12" style="text-align:center;"> 
         <p style="margin-top: 105px; font-size: 13px; margin-left: -50px;">{{ $po_keluar->nomor_po }}</p>
      <div class=" col-xs-4" style="margin-left:265px;"></div>
        </div>
          <div class="box-body" style="margin-left: 30px;">
            <div class="row">
              <div class="col-md-12">              
                 <div class="invoice-detail" style="font-size: 13px; margin-top: 2px; margin-left: 9px; ">       
                    <table width="710" style="text-align:center; margin-left:2px;" >
                      <tr>
                        <td style="width:40px;"></td>
                        <td style="text-align: left;"></td>
                        <td style="width:40px;"></td>
                        <td style="width:347px; text-align:left; padding-bottom: 1px;">{{ $po_keluar->penyalur }}</td>
                      </tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="margin-top: 5px; width:347px; text-align:left; padding-top: 3px;">{{ $po_keluar->no_skp_penyalur }}</td>
                      <tr>
                      </tr>
                      <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="padding-top:7px; font-size:10px; text-align:left;">{{ $po_keluar->alamat_penyalur }}</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="padding-top:7px; text-align:left;">{{ $po_keluar->no_sold_penyalur }}</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="padding-top:7px;text-align:left;">{{ $po_keluar->npwp_penyalur }}</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="padding-top:18px; text-align:left;">{{ $po_keluar->jenis_konsumen }}</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="padding-top:6px; text-align:left;">{{ $po_keluar->nama_konsumen }}</td>
                      </tr>
                      <tr>
                        <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="padding-top:2px; text-align:left;font-size: 11.5px;">{{ $po_keluar->alamat_konsumen }}</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="padding-top:6px; text-align:left;">{{ $po_keluar->sales_district }}</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="padding-top:6px; text-align:left;">{{ $po_keluar->kode_lokasi_konsumen }}</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="padding-top:4px; text-align:left;">{{ $po_keluar->jenis_usaha }}</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="padding-top:25px; text-align:left;">{{ $po_keluar->jenis_bbm }}</td>
                      </tr>
                      <tr> 
                        <td></td>
                        <td></td>
                        <td></td> 
                        <!--  <div class="form-group">
                        <label for="pbbkb">PBBKB</label> 
                         <select name="pbbkb" class="form-control" id='pbbkb'>
                          <option value="0" @if($po_keluar->pbbkb=="0") selected @endif>{{App\Utilities\Constant::$PBBKB['0.000']}}</option>
                          <option value="0.858" @if($po_keluar->pbbkb=="0.858") selected @endif>{{App\Utilities\Constant::$PBBKB['0.858']}}</option>
                          <option value="4.5" @if($po_keluar->pbbkb=="4.5") selected @endif>{{App\Utilities\Constant::$PBBKB['4.500']}}</option>
                          <option value="5" @if($po_keluar->pbbkb=="5") selected @endif>{{App\Utilities\Constant::$PBBKB['5.000']}}</option>
                          <option value="7.5" @if($po_keluar->pbbkb=="7.5") selected @endif>{{App\Utilities\Constant::$PBBKB['7.500']}}</option>
                         </select>
                      </div>   -->
                        <td style="text-align:left; padding-top: 18px;">
                          <div class="form1" style="float:left; color:skyblue;">{{ number_format( $po_keluar->qty, 0, ",", ".") }} </div>
                           <div class="form2" style="float:left;  margin-left: 130px;">                           @if($po_keluar->pbbkb=="NULL") 
                           <div>{{App\Utilities\Constant::$PBBKB['NULL']}}</div>
                           @elseif($po_keluar->pbbkb=="0.000")  
                           <div>{{App\Utilities\Constant::$PBBKB['0.000']}}</div>
                           @elseif($po_keluar->pbbkb=="0.858")  
                           <div>{{App\Utilities\Constant::$PBBKB['0.858']}}</div>
                           @elseif($po_keluar->pbbkb=="4.5") 
                           <div>{{App\Utilities\Constant::$PBBKB['4.500']}}</div>
                           @elseif($po_keluar->pbbkb=="5")  
                           <div>{{App\Utilities\Constant::$PBBKB['5.000']}}</div>
                           @else($po_keluar->pbbkb=="7.5") 
                           <div>{{App\Utilities\Constant::$PBBKB['7.500']}}</div>
                            @endif
                          <div class="form3" style="float:left;  margin-left: 159px; margin-top: -20px">
                          @if($po_keluar->pph=="0.000") 
                          <div>{{App\Utilities\Constant::$PPH['0.000']}}</div>
                          @else($po_keluar->pph=="0.300")  
                          <div>{{App\Utilities\Constant::$PPH['0.300']}}</div>
                          @endif
                        </td>

                        <!-- <td style="text-align:center; padding-top: 19px;">{{ $po_keluar->qty }}</td>
                        <td style="text-align:right; padding-top: 19px;">{{ $po_keluar->qty }}</td> -->
                      </tr>
                      <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td class="harga" style="padding-top:4px; padding-left:143px; color:red; text-align:left;">{{ number_format( $po_keluar->harga_jual, 3, ",", ".") }} </td>
                      </tr>
                      <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td  style="text-align:center; padding-right: 85px; padding-top: 8px;">{{ number_format( $po_keluar->pbbkb_dasar, 3, ",", ".") }}</td>
                      </tr>
                        <tr>
                        <td></td>
                        <td style="width: 150px; text-align:left; "></td>
                        <td></td>
                        <td class="harga" style="padding-top:8px; color:red;  text-align:center; padding-right: 85px;"> {{ number_format( $po_keluar->harga_beli, 3, ",", ".") }}</td>
                      </tr>
                        <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="padding-top:15px; text-align:center; padding-right: 85px;"> {{ number_format( $po_keluar->harga_dasar, 3, ",", ".") }}</td>
                      </tr>
                        <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td> </td> 
                        <td style="padding-top:23px; text-align:center; padding-right: 85px;">{{ number_format(($po_keluar->pbbkb_dasar-$po_keluar->harga_dasar), 3, ",", ".") }}</td>
                      </tr>
                        <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="padding-top:4px; text-align:left;"> 
                          <div style="float: left; margin-left: 50px;">{{ $po_keluar->cara_pembayaran }}</div>
                          <div style="float: left; margin-left: 165px;">{{ $po_keluar->jatuh_tempo_hari }}</div>
                        </td>
                      </tr>
                        <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="padding-top:4px; text-align:center; padding-right: 85px;"> {{ number_format( $po_keluar->dpp, 0, ",", ".") }}</td>
                      </tr>
                        <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="padding-top:7px; text-align:center; padding-right: 85px;"> {{ number_format( $po_keluar->jumlah_ppn, 0, ",", ".") }}</td>
                      </tr>
                        <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="padding-top:4px; text-align:center; padding-right: 85px;"> {{ number_format( $po_keluar->pbbkb_jual, 0, ",", ".") }}</td>
                      </tr>
                        <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td style="padding-top:6px; text-align:center; padding-right: 85px;"> {{  $po_keluar->pph_total }}</td>
                      </tr>
                        <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left; text-align:left;"></td>
                        <td></td>
                        <td style="padding-top:5px; text-align:center; padding-right: 85px;">{{ number_format( $po_keluar->jumlah, 0, ",", ".") }} </td>
                      </tr>
                      <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left; "></td>
                        <td></td>
                        <td style="padding-top: 5px; float: left;">{{ $po_keluar->bukti_setor_bank }}</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td style="width: 150px; text-align: left;"></td>
                        <td></td>
                        <td></td>
                      </tr>
                    </table>   
                  <table style="margin-left: 205px; width: 385px; margin-top:24px; text-align:center;">
                    <tr>                       
                       @foreach($alat_angkut as $val)
                      <td  style="width:260px; padding-left: 145px; padding-top: 0px;">
                        <div class="tanggal" style="margin-top: 6px;">{{ date('d F Y', strtotime($val->tanggal)) }}</div>
                      </td> 
                      @endforeach
                    </tr>
                    <tr> 
                       @foreach($alat_angkut as $val)
                      <td style="padding-left: 145px;">
                        <div class="nopol" style="margin-top: -3px;">{{ $val->nopol }}</div>
                        </td>
                       @endforeach
                    </tr>
                    <tr>
                      @foreach($alat_angkut as $val)
                      <td style="padding-left: 145px; ">
                        <div style="margin-top: -2px;">{{ $val->volume }} KL</div>
                      </td>
                       @endforeach
                    </tr>
                    <tr>
                      @foreach($alat_angkut as $val)
                      <td style="padding-left: 145px;">
                        <div style="margin-top: -2px;">{{ $val->nama_supir }}</div>
                      </td>
                       @endforeach
                    </tr>
                    <tr> 
                        @foreach($alat_angkut as $val)
                        <td style="padding-left: 145px;" >
                          <div style="margin-top: 2px;">{{ $po_keluar->supply_point }}</div>
                        </td>
                       @endforeach
                      </tr>
                  </table>
                <table style="margin-left: 23px; width: 773px; margin-top:39px; text-align:center;">
                  <tr>
                    <td style="text-align: padding-top: 18px; padding-left: 355px;">Jakarta,  {{ date('d F Y ') }}</td>
                  </tr>
                  <tr>
                    <td style="text-align: left; padding-left: 510px; padding-top:0px; position: relative;">{{ $po_keluar->penyalur}}</td>
                  </tr>
                  <tr>
                    <td style="text-align: left; width: 125px; padding-left: 90px; padding-top: 12px;"><!-- ( {{ $po_keluar->nama_pic_agen}} ) --></td>  
                  </tr>
                  <tr> 
                    <td>
                      @if($company->id ==1)
                  <div>
                   <img src="{{ url('dist/img/ttdALJ.png') }}" style="width: 17%; margin-top: -32px; margin-left: 313px;">
                   <p style="margin-left:349px; margin-top:-40px;">Andi</p>
                  </div>
                  @else($company->id ==2)
                  <div style="margin-top: -25px;">
                   <img src="{{ url('dist/img/ttdSuma.png') }}" style="width: 15%; margin-top: -4px; margin-left: 310px;">
                   <p style="margin-left:333px; margin-top:-35px;">Andi</p>
                  </div>
                  @endif
                    </td> 
                  </tr>
                  <tr>
                  <td style="padding-top: 2px; text-align: left; width: 125px; font-size: 8px;">*) Nilai Transaksi adalah nilai yang dibayarkan kepada PT. Pertamina Patra Niaga (rounding 3 angka dibelakang apabila < 001-449 maka dibulatkan kebawah dan jika > 500-999  dibulatkan ke atas)</td>
                </tr>
                </table>       
              </div>              
            </div>
          </div>
      </section>
    </div>
  </body>
</html>
