@extends('layouts.app')
 
@section('css')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ url('plugins/select2/select2.min.css') }}">
@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content --> 
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
        Form
        <small>Tambah PO Keluar</small>    
      </h1>   
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a>
      </li>
      <li><a href="#">Forms</a>
      </li> 
      <li class="active">Tambah PO Keluar</li>
    </ol>
  </section>
  <!-- Main content -->   
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
         @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Form</h3>
          </div>
          <!-- /.box-header -->
              <form role="form" action="{{ url('po-keluar/store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- text input -->
                <div class="box-body"> 
                  <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid #eee; ">
                      <div class="form-group">
                        <label for="tanggal">Tanggal</label>
                        <input type="date" class="form-control" id="tanggal" name="tanggal" required>
                      </div> 
                      <div class="form-group"> 
                        <label for="nomor_po">Nomor PO *</label>
                        <input type="text" class="form-control" id="nomor_po" name="nomor_po" placeholder="Masukkan Nomor PO" required>
                      </div>  
                      <div class="form-group">
                        <label for="role">Nama Konsumen *</label>
                          <select class="form-control select2" name="nama_konsumen" id="nama_konsumen" onchange="fetchDataAddress(this)" required>
                            <option  selected="selected">-- Pilih Konsumen--</option>
                            @foreach($nama_konsumen as $val)
                            <option value="{{ $val->id }}">{{ $val->name }}</option>
                            @endforeach
                          </select>
                      </div>
                      <div class="form-group"> 
                        <label for="alamat_konsumen">Alamat Konsumen *</label>
                          <select class="form-control" name="alamat_konsumen" id="alamat_konsumen" required>
                            <option value="">-- Silahkan Pilih Konsumen --</option>
                          </select>
                      </div> 
                      <div class="form-group">
                        <label for="role">Nama Penyalur *</label>
                        <select class="form-control" name="penyalur" id="penyalur" required>
                          <option value="">-- Pilih Kantor Penyalur --</option>
                          @foreach($kantor_cabang as $val)
                          <option value="{{ $val->id }}">{{ $val->name }}</option>
                          @endforeach
                        </select>
                      </div>
                     <div class="form-group">
                        <label for="nama_pic_penyalur">Nama PIC Penyalur *</label>
                        <input type="text" class="form-control" id="nama_pic_penyalur" name="nama_pic_penyalur" placeholder="Masukkan Nama PIC Penyalur " required>
                      </div>
                      <div class="form-group">
                        <label for="nama_pic_agen">Nama PIC Konsumen </label>
                        <input type="text" class="form-control" id="nama_pic_agen" name="nama_pic_agen"placeholder="Masukkan Nama PIC Agen " required>
                      </div>
                      <div class="form-group">
                        <label for="qty">Qty *</label>
                        <input type="text" class="form-control" id="qty" name="qty" placeholder="Masukkan Jumlah BBM (Liter)" required>
                      </div>
                         <div class="form-group">
                         <label for="role">Jenis BBM </label>
                        <select class="form-control" name="jenis_bbm" id="jenis_bbm" required>
                          <option value="">-- Pilih BBM --</option>
                          @foreach($jenis_bbm as $val)
                          <option value="{{ $val->id }}">{{ $val->jenis_bbm }}</option>
                          @endforeach
                        </select>
                      </div>  
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="harga_beli">Harga Beli </label>
                       <input type="text" class="form-control" id="harga_beli" name="harga_beli" placeholder="Masukkan Harga Dasar end user/liter (tanpa pajak)" required>
                      </div>
                      <div class="form-group">
                        <label for="harga_jual">Harga Jual </label>
                       <input type="text" class="form-control" id="harga_jual" name="harga_jual" placeholder="Masukkan Harga Jual ke end user/liter (tanpa pajak) " required>
                      </div> 
                      <div class="form-group">
                        <label for="ppn">PPN *</label>
                       <input type="text" class="form-control" id="ppn" name="ppn" placeholder="PPN" value="10" required>
                      </div>
                      <div class="form-group">
                        <label for="pbbkb">PBBKB</label>
                         <select name="pbbkb" class="form-control" id='pbbkb'>
                          <option value="0"> - </option>
                          <option value="0.858">17,17% dari 5%</option>
                          <option value="1.288">1,288%</option>
                          <option value="4.5">90% dari 5%</option>
                          <option value="5">100% dari 5%</option>
                          <option value="7.5">150% dari 5%</option>
                          <option value="10">100% dari 10%</option>
                         </select>
                      </div>  
                      <div class="form-group">
                        <label for="pph">PPH</label>
                        <select name="pph" class="form-control" id="pph" >
                          <option value="0.000"> - </option>
                          <option value="0.300">0,300%</option>
                        </select>
                      </div> 
                      <div class="form-group">
                        <label for="cara_pembayaran">Cara Pembayaran</label>
                        <select name="cara_pembayaran" class="form-control" id="cara_pembayaran" required>
                          <option value="">-- Pilih Tipe --</option>
                          <option value="CASH">CASH</option>
                          <option value="KREDIT">KREDIT</option>
                        </select>
                      </div>  
                      <div class="form-group">
                        <label for="jatuh_tempo_hari">Lama Pembayaran</label>
                       <input type="text" class="form-control" id="jatuh_tempo_hari" name="jatuh_tempo_hari" placeholder="Masukkan Lama Pembayaran">
                      </div>
                      <div class="form-group">
                        <label for="bukti_setor_bank">Bukti Setor Bank </label>
                       <input type="text" class="form-control" id="bukti_setor_bank" name="bukti_setor_bank"placeholder="Masukkan Nama Bank " required>
                      </div>
                      <div class="form-group">
                        <label for="supply_point">Supply Point</label>
                        <select class="form-control" name="supply_point" id="supply_point" required>
                          <option value="">-- Pilih Supply Point --</option>
                          @foreach($supply_point as $val)
                          <option value="{{ $val->id }}">{{ $val->supply_point }}</option>
                          @endforeach
                        </select>
                      </div> 
                      <div class="form-group">
                        <label for="sales_district">Sales District Konsumen *</label>
                       <input type="text" class="form-control" id="sales_district" name="sales_district" placeholder="Masukkan Area Sales" required>
                      </div>  
                  </div>
                </div>
              </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </form> 
            </div> 
          </div> 
        </section>
      </div>
@endsection  
@section('javascript')
<script src="{{ url('plugins/select2/select2.full.min.js')}}"></script>
<script>
  $(function () 
    {
      //Initialize Select2 Elements
      $(".select2").select2();
    });

  function fetchDataAddress(elem){
    var customer = $(elem).val();

    $.ajax({
      url:"{{ url('customer/getAddress') }}?customer_id="+customer,
      type:"get",
      dataType:"json",
      beforeSend:function(){

      },
      success:function(result){
        if(result.status = "ok"){
          var element = $("#alamat_konsumen");
          var html = "<option value=''>Pilih Alamat</option>";

          $.each(result.data, function(index, value){
            html += "<option value='"+value.id+"'>"+value.address+"</option>";
          });

          element.html(html);
        }else{
          var html = "<option value=''>Silahkan Pilih Konsumen</option>";
          element.html(html);
        }
      },
      error:function(){

      },
      complete:function(){

      }
    });
  }
</script>
@endsection