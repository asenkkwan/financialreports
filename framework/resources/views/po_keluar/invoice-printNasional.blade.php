
<!DOCTYPE html>
<html>
<head>
  <link rel="shortcut icon" href="{{ url( 'dist/img/logoFR.png') }}"> 
  <meta charset="utf-8"> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Financial Reports  | Invoice</title> 
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 --> 
  <link rel="stylesheet" href="{{ url('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('dist/css/AdminLTE.min.css') }}">
   <style> 
    body{
            margin:auto;
            font-size: 12px !important; 
          }  
           page[size="A4"] { 
            background: white;
            width: 21cm;
            height: 29.7cm;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
          }
           .kop_surat {
              position: absolute;
              width: 21cm;  
              margin-top: -45px;
            }
          .invoice {
              position: relative;
               background: none; 
              border: 0px solid #f4f4f4;
              margin-left: 15px;
          }
          @media print {
            body, page[size="A4"] {
              margin: 0;
              box-shadow: 0;
            }
      }
  </style>
</head>
<body >
<?php
 $image ="";
  if($company->kop_surat){
    $image = unserialize($company->kop_surat);
    if($image){
      $image = url('dist/img/')."/".$image['original'];
    }
  }
?>                    
  <page size="A4" layout="portrait">
    <img src='{{ $image }}' class="kop_surat">
<body onload="window.print();">
  <div class="wrapper">
    <!-- Main content -->
    <section class="invoice" >
      <div class="row" style="margin-top: 180px; font-size: 12px;">
        <div class="col-xs-12">
          <table border="0" style="width:100%">
            <tr>
              <td colspan="3" align="center">
                <font size="5" style="text-decoration:underline;">Purchase Order</font><br>
                Nomor : {{ $po_keluar->nomor_po }}<br><br><br>
              </td>
            </tr>
            <tr>
              <td style="width: 30%">
                <b>Ref : </b>
                {{ $po_keluar->nomor_po}}<br>
                <p >Subject : Purchase Order<br></p>
                <p>Date : {{ date('d M Y ') }}</p> <br>
                Kepada <br>
                PT. Pertamina Patra Niaga <br>
                Jakarta
                <!-- Up : {{ $po_keluar->pic_po_tujuan }} -->
              </td>
              <td style="width: 40%">

              </td>
              <td style="width: 30%">
                <!-- Nomor PO (Order No) : {{ $po_keluar->no_po }}<br> -->
                <!-- Tanggal PO (Order Date) : {{ $po_keluar->tanggal }}<br> -->
               <!--  Tanggal Kirim (Delivery Date) : {{ $po_keluar->tanggal_kirim }}<br> -->
                <!-- Mata Uang : IDR<br> -->
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 30%">
               <!--  <b>Delivery Office</b><br>
               {{ $po_keluar->lokasi_perusahaan}}<br> -->
              </td>
              <td style="width: 40%"></td>
              <td style="width: 30%">
                <!-- Contact Person 1 : {{ $po_keluar->contact_person_1 }}<br>
                Contact Person 2 : {{ $po_keluar->contact_person_2 }}<br> -->
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="row" style="margin-top: 20px;">
        <div class="col-xs-12 table-responsive">
          <td>
            Dengan Hormat, <br><br>
            Kami Konfirmasikan Pemesanan BBM untuk Customer kami sbb : <br><br><br>
          </td>
          <td>  
            Product : {{ $po_keluar->jenis_bbm}}<br>
            Qty : {{ $po_keluar->qty}} =  <input style="width: 35%; "type="text" name="qty"><br>
            Lokasi Pengaliran : {{ $po_keluar->supply_point}} <br>
           <!--  No. SH : {{ $po_keluar->kode_lokasi_konsumen}}<br> -->
            Pembayaran : {{ $po_keluar->cara_pembayaran}}<br>
            Tgl Pengaliran : {{ date('d M Y ') }}<br><br>
          </td>
        </div>
      </div>

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-12">
          <p style="margin-top: 10px;">
            Ketentuan Supply antara lain :<br>
            - Specifikasi sesuai dengan product pertamina <br>
            - Termasuk PPN 10% & PBBKB 0,858% <br> <br> <br>
            Atas perhatian dan kerjasamanya kami ucapkan terima kasih <br>
           
          </p>
        </div>
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-12">
          <table border="0" style="width: 100%">
            <tr>
              <td style="width: 30%">
                 <strong>Dibuat Oleh,</strong><br>
                <br><br><br><br><br><br>
                ( {{ Auth::user()->name }} )<br>
              </td>
              <td style="width: 50%"></td>
              <td style="width: 20%">
                <strong>Disetujui Oleh,</strong><br>
                {{ strtoupper($po_keluar->nama_perusahaan) }}<br>
                <br><br><br><br><br>
                {{ strtoupper($po_keluar->approved_by_client) }}<br>
              </td>
            </tr>
          </table>
          
        </div>
      </div>
      <br>
      <br>
      <div class="row" align="center">
          @if($company->id ==1)
          <div class="footer" style="    margin-top: 55px;" >
           <p style="color:#176db9 !important;">Jl. Turi Jaya Rt. 08/07 No. 34 Ds. Segaramakmur Kec. Tarumajaya - Bekasi 17211<br> tlp. (021) 88992082 - 88992083 Fax. (021) 443830  <br> <strong style="color:red !important;">E-mail : audrilutfiajaya@ymail.com </strong></p>
          </div>
          @else($company->id ==2)
          <div class="footer" style="    margin-top: 130px;" >
            <p style="color:#176db9 !important;">Jl. Turi Jaya Rt.10/07 Ds. Segaramakmur Kec. Tarumajaya - Bekasi <br> Telp. (021) 88992082 <strong style="color:red !important;">Email.Sumaadijaya.com</strong></p>
          </div>
          @endif
        </div>
    </section>
    <!-- /.content -->
    <!-- /.content -->
</div>
</body>
</html>
