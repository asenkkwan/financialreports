@extends('layouts.app')
@section('css')
@endsection
  
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>  
        Data 
        <small>Po Keluar</small> 
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Po Keluar</li> 
      </ol> 
    </section> 

    <!-- Main content -->
      <!-- Main content --> 
    <section class="content"> 
      <div class="row">
        <div class="col-xs-12">
          @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!}  
            </div>
          @endif
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
              <div style="float: right">
                <button class="btn btn-primary" onclick="window.location='<?php echo url('po-keluar/create'); ?>'">
                  <i class="fa fa-plus"></i> Tambah
                </button> 
              </div>
            </div>
            <!-- /.box-header -->
          <div class="box-body">
            <form method="get" action="#">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Start Date</label>
                    <input type="date" name="sdate" id="sdate" class="form-control" value="{{ $sdate }}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>End Date</label>
                    <input type="date" name="edate" id="edate" class="form-control" value="{{ $edate }}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Nomor PO</label>
                    <input type="text" name="nomor_po" id="nomor_po" class="form-control" placeholder="filter nomor Po" value="{{ $nomor_po }}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Nama Konsumen</label>
                    <input type="text" name="nama_konsumen" id="nama_konsumen" class="form-control" placeholder="filter nama konsumen" value="{{ $nama_konsumen }}" value="{{ $nama_konsumen }}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Nama Penyalur</label>
                    <input type="text" name="penyalur" id="penyalur" class="form-control" placeholder="filter nama penyalur" value="{{ $penyalur }}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Produk</label>
                    <input type="text" name="jenis_bbm" id="jenis_bbm" class="form-control" placeholder="filter jenis bbm" value="{{ $jenis_bbm }}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <button type="submit" class="form-control btn btn-danger">Filter</button>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <a href="{{ url('po-keluar') }}" class="form-control btn btn-default">Clear</a>
                  </div>
                </div>
              </div>
            </form>
              <div class="table-responsive">
              <table id="datatable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Tanggal</th>
                  <th>Nomor PO</th>
                  <th>Nama Konsumen</th>
                  <th>Alamat Konsumen</th>
                  <th>Nama Penyalur</th>
                  <th>Produk</th>
                  <th>Qty</th>           
                  <th>Harga Beli</th>                             
                  <th>Harga Jual</th>
                  <th>Harga Dasar</th>
                  <th>DPP</th>
                  <th>PPN</th>
                  <th>PBBKB Dasar</th>
                  <th>PBBKB Jual</th>
                  <th>Jumlah</th>
                  <th>Status</th>
                  <th><i class="fa fa-ellipsis-h"></i></th>
                </tr>
                </thead>
                <tbody> 
                  @foreach($po_keluar as $val)
                  <tr>
                    <td>{{ date('d M Y', strtotime($val->tanggal)) }}</td>
                    <td>{{ $val->nomor_po }}</td> 
                    <td>{{ $val->nama_konsumen }}</td>
                    <td>{{ $val->alamat_konsumen }}</td>
                    <td>{{ $val->penyalur }}</td>
                    <td>{{ $val->jenis_bbm }}</td>
                    <td>{{ number_format($val->qty, 0, ",", ".") }}</td>
                    <td>{{ number_format($val->harga_beli, 3, ",", ".") }}</td>  
                    <td>{{ number_format($val->harga_jual, 3, ",", ".") }}</td>
                    <td>{{ number_format($val->harga_dasar, 3, ",", ".") }}</td>
                    <td>{{ number_format($val->dpp, 0, ",", ".") }}</td>
                    <td>{{ number_format($val->jumlah_ppn, 0, ",", ".") }}</td>
                    <td>{{ number_format($val->pbbkb_dasar, 3, ",", ".") }}</td>
                    <td>{{ number_format($val->pbbkb_jual, 0, ",", ".") }}</td>
                    <td>{{ number_format($val->jumlah, 0, ",", ".") }}</td>                   
                    <th>
                       @if($val->status == 1)
                        Approved
                      @elseif($val->status == -1)
                        Rejected
                      @else($val->status == 3)
                        Pending
                      @endif 
                    </th>
                    <td>  
                      <a href="{{ url('po-keluar/detail-po-keluar/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-server"></i> Detail PO</a> 
                      @if($val->status == 0)
                       <a href="{{ url('po-keluar/send-email/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-envelope"></i> Send Email</a>                  
                      <a href="{{ url('po-keluar/edit/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> Edit PO</a> 
                      <a href="{{ url('po-keluar/add-item/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Alat Angkut</a>  
                      <a class="btn btn-danger btn-xs" href="{{ url('po-keluar/delete/'.$val->id) }}" onclick="event.preventDefault();if(confirm('Hapus Po Keluar ?')){ $('#form-{{ $val->id }}').submit() }"><i class="fa fa-trash"></i> Hapus Item</a>
                      <form action="{{ url('po-keluar/delete/'.$val->id) }}" method="post" id="form-{{ $val->id }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="delete">
                      </form>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              </div>
              {{ $po_keluar->appends(['nama_konsumen' => $nama_konsumen,'nomor_po' => $nomor_po,'penyalur' => $penyalur,'produk' => $jenis_bbm])->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('javascript')
@endsection