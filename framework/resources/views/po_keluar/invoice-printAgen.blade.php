<!DOCTYPE html>
<html>
<head>
  <link rel="shortcut icon" href="{{ url( 'dist/img/logoFR.png') }}"> 
  <meta charset="utf-8"> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Financial Reports  | Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 --> 
  <link rel="stylesheet" href="{{ url('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('dist/css/AdminLTE.min.css') }}">

  <style> 
    body{
            margin:auto;
            font-size: 12px !important; 
          }  
           page[size="A4"] {
            background: white;
            width: 21cm;
            height: 29.7cm;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
          }
           .kop_surat {
              position: absolute;
              width: 21cm;  
              margin-top: -45px;
            }
          .invoice {
              position: relative;
               background: none; 
              border: 0px solid #f4f4f4;
          }
          @media print {
            body, page[size="A4"] {
              margin: 0;
              box-shadow: 0;
            }
      }
  </style>
</head>
<body >
<?php
 $image ="";
  if($company->kop_surat){
    $image = unserialize($company->kop_surat);
    if($image){
      $image = url('dist/img/')."/".$image['original'];
    }
  }
?>                    
  <page size="A4" layout="portrait">
    <img src='{{ $image }}' class="kop_surat">
<!-- <body onload="window.print();"> -->
  <div class="wrapper">
      <!-- Main content -->
      <section class="invoice" >
        <div class="row" style="margin-top: 185px;">
          <div class="col-xs-12">
            <table border="0" style="width:100%">
              <tr>
                <td colspan="3" align="center">
                  <font size="5" style="text-decoration:underline;">Purchase Order</font><br>
                  Nomor : {{ $po_keluar->nomor_po }}<br><br><br>
                </td>
              </tr>
              <tr>
                <td style="width: 30%">
                  <b>Kepada : </b><br>
                  {{ $po_keluar->nama_konsumen}}<br>
                  <p>Ditempat<br></p>
                  <p>Attn : {{ $po_keluar->nama_pic_agen}}</p>
                  <!-- Up : {{ $po_keluar->pic_po_tujuan }} -->
                </td>
                <td style="width: 40%"></td>
                <td style="width: 30%">
                  <!-- Nomor PO (Order No) : {{ $po_keluar->no_po }}<br> -->
                  <p style="margin-top: -15px;">Tanggal PO (Order Date) : {{ $po_keluar->tanggal }}</p><br>
                 <!--  Tanggal Kirim (Delivery Date) : {{ $po_keluar->tanggal_kirim }}<br> -->
                  <!-- Mata Uang : IDR<br> -->
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td style="width: 30%">
                 <!--  <b>Delivery Office</b><br>
                 {{ $po_keluar->lokasi_perusahaan}}<br> -->
                </td>
                <td style="width: 40%"></td>
                <td style="width: 30%">
                  <!-- Contact Person 1 : {{ $po_keluar->contact_person_1 }}<br>
                  Contact Person 2 : {{ $po_keluar->contact_person_2 }}<br> -->
                </td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row" style="margin-top: 20px;">
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
              <tr>
                <th style="width:5%; text-align: center;" >No</th>
                <th style="width:22%; text-align: center;">Jenis Barang</th>            
                <th style="width:8%; text-align: center;">Quantity</th>
                <th style="width:8%; text-align: center;">Harga/Liter</th>
                <th style="width:8%; text-align: center;">Jumlah</th>
                <!-- <th style="width:8%;">DPP</th>
                <th style="width:8%;">PPN</th>
                <th style="width:8%;">PBBKB Dasar</th>
                <th style="width:8%;">PBBKB Jual</th> -->
                <!-- <th style="width:8%; text-align:center;">Amount</th> -->
              </tr>
              </thead>
              <tbody> 
                <?php 
                  // $total_amount = 0;
                  $counter = 1;
                ?>
                @foreach($po_keluar as $val)
                <tr>
                  <td style="text-align:center;">{{ $counter }}</td>          
                  <td style="text-align:center;">{{ $po_keluar->jenis_bbm }}</td>
                  <td style="text-align:center;">{{ $po_keluar->qty }}</td>
                  <td style="text-align:center;">Rp. {{ number_format($po_keluar->harga_beli, 2, ",", ".") }}</td>
                  <td style="text-align:center;">Rp. {{ number_format($po_keluar->harga_jual_penyalur, 0, ",", ".") }}</td>
                  <!-- <td style="text-align:right;">{{ number_format($po_keluar->pbbkb_dasar, 2, ",", ".") }}</td>
                  <td style="text-align:right;">{{ number_format($po_keluar->pbbkb_jual, 2, ",", ".") }}</td>
                  <td style="text-align:right;">{{ number_format($po_keluar->dpp, 2, ",", ".") }}</td>
                  <td style="text-align:right;">{{ number_format($po_keluar->jumlah_ppn, 2, ",", ".") }}</td>
                  <td style="text-align:right;">{{ number_format($po_keluar->jumlah, 2, ",", ".") }}</td> -->
                </tr>
               <!-- <?php 
                  $counter++;
                ?>
                @endforeach  -->
                <tr>
                  <td rowspan="1" colspan="1" >
                    <!-- <p>Payment Term : {{ $po_keluar->payment_term }}</p> -->
                  </td>
                  <td rowspan="1" colspan="1" >
                    <!-- <p>Payment Term : {{ $po_keluar->payment_term }}</p> -->
                  </td>
                  <td rowspan="1" colspan="1" >
                    <!-- <p>Payment Term : {{ $po_keluar->payment_term }}</p> -->
                  </td>
                  <th style="text-align: center;">PPN 10%</th> 
                  <td style="text-align:center;">Rp. {{ number_format( $po_keluar->harga_ppn, 0, ",", ".") }}</td>
                </tr>
                <tr>
                  <td rowspan="1" colspan="3" style="text-align: center;">
                    <b>Jumlah</b>
                  </td>                
                  <td style="text-align:center;"><b>Rp. {{ number_format( $po_keluar->harga_jual_ppn, 0, ",", ".") }}</b></td>
                  <td style="text-align:center;"><b>Rp. {{ number_format( $po_keluar->total, 0, ",", ".") }}</b></td>               
                </tr>
                <tr>
                  <td rowspan="1" colspan="10" >
                    <p>Terbilang :</p>
                    <p>{{ terbilang($po_keluar->total, 1) }}</p>
                  </td>
                  <!-- <th>TOTAL</th>
                  <td style="text-align:right;">{{ number_format($po_keluar->jumlah, 2, ",", ".") }}</td> -->
                </tr>
                <!-- <tr>
                  <td colspan="10">Note : {{ $po_keluar->keterangan }}</td>
                </tr> -->
              </tbody>
            </table>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
         <div class="row">
          <div class="col-xs-12">
            <td> Alamat Kirim : {{ $po_keluar->dellivery_office}}</td>
            
          </div>
          
        </div>

        <div class="row">
          <!-- accepted payments column -->
          <div class="col-xs-12">
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
              Note :<br>
              - Pembayaran dilakukan 1 minggu setelah Pihak PT. Audri Lutfia Jaya Pengiriman Receipt Bunker <br>
              - Mohon Dilampirkan Receipt Bunker PT. Audri Lutfia Jaya Pada saat Pengiriman Barang. <br>
              - Berita Acara Bunker dan Resi Bunker dapat diminta langsung dan ditanda tangani oleh kedua belah pihak. <br>
             
            </p>
          </div>
        </div>
        <!-- /.row -->

        <div class="row">
          <div class="col-xs-12">
            <table border="0" style="width: 100%">
              <tr>
                <td style="width: 30%">
                   <strong>Bekasi,  {{ date('d M Y ') }}</strong><br>
                  <br><br><br><br><br><br>
                  ( {{ Auth::user()->name }} )<br>
                </td>
                <td style="width: 50%"></td>
                <td style="width: 20%">
                  <strong>Disetujui Oleh,</strong><br>
                  {{ strtoupper($po_keluar->nama_perusahaan) }}<br>
                  <br><br><br><br><br>
                  {{ strtoupper($po_keluar->approved_by_client) }}<br>
                </td>
              </tr>
            </table>
            
          </div>
        </div>
        <br>
        <br>

        <div class="row" align="center">
          @if($company->id ==1)
          <div class="footer" style="    margin-top: 55px;" >
           <p style="color:#176db9 !important;">Jl. Turi Jaya Rt. 08/07 No. 34 Ds. Segaramakmur Kec. Tarumajaya - Bekasi 17211<br> tlp. (021) 88992082 - 88992083 Fax. (021) 443830  <br> <strong style="color:red !important;">E-mail : audrilutfiajaya@ymail.com </strong></p>
          </div>
          @else($company->id ==2)
          <div class="footer" style="    margin-top: 130px;" >
            <p style="color:#176db9 !important;">Jl. Turi Jaya Rt.10/07 Ds. Segaramakmur Kec. Tarumajaya - Bekasi <br> Telp. (021) 88992082 <strong style="color:red !important;">Email.Sumaadijaya.com</strong></p>
          </div>
          @endif
        </div>
        
        <!-- this row will not appear when printing -->
       <!--  <div class="row" style="margin-top: 20px;">
          <div class="col-xs-12">
            <a href="{{ url('po_keluar/invoice-print/'.$po_keluar->id) }}"  target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
            @if($po_keluar->status == 0)
              @if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin')
               <button type="button" class="btn btn-success pull-right" onclick="approve({{ $po_keluar->id }})"><i class="fa fa-credit-card"></i> Approve</button>
              <button type="button" class="btn btn-danger pull-right" onclick="reject({{ $po_keluar->id }})" style="margin-right: 5px;"><i class="fa fa-credit-card"></i> Reject</button>
              <form id="form-approval" action="{{ url('po_keluar/approve') }}" method="post">
                 {{ csrf_field() }}
                 <input type="hidden" name="po_keluar_id" value="{{ $po_keluar->id }}">
               </form>
               <form id="form-reject" action="{{ url('po_keluar/reject') }}" method="post">
                 {{ csrf_field() }}
                 <input type="hidden" name="po_keluar_id" value="{{ $po_keluar->id }}">
               </form>
              @endif
            @endif     
          </div>
        </div> -->
      </section>
      </page>
      <!-- /.content -->
      <!-- /.content -->
  </div>
</body>
</html>
