@extends('layouts.app')
@section('css')
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ url('plugins/datatables/dataTables.bootstrap.css') }}">
@endsection
   
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) --> 
    <section class="content-header">
      <h1> 
        Add Item
        <small>{{ $po_keluar->nama_konsumen }}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Po Keluar</a></li>
        <li class="active">Add Item</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="invoice">
      <!-- Table row -->
      <div class="row" style="">
        <div class="col-xs-12 table-responsive">
          @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif
          <form action="{{ url('po-keluar/add-item') }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $po_keluar->id }}" name="id_po_keluar">
            <table class="table table-striped">
              <thead>
              <tr>
                <th style="width:3%;"><i class="fa fa-ellipsis-v"></i></th>
                <th style="width:5%;">Tanggal Pengisian</th>
                <th style="width:8%;">No. Pol Truk Tanki/Tongkang</th>
                <th style="width:5%;">Volume BBM</th>
                <th style="width:8%;">Nama Supir/Nahkoda</th>
                
              </tr>
              </thead>
              <tbody id="item-body">
                @if(count($alat_angkut) > 0)
                  @foreach($alat_angkut as $val)
                     <tr>
                      <td onclick="deleteItemField(this)"><i class="fa fa-trash"></i></td>
                      <td><input type="date" name="tanggal[]" id="tanggal" value="{{ $val->tanggal }}" class="form-control"></td>
                      <td><input type="text" name="nopol[]" id="nopol" value="{{ $val->nopol }}" class="form-control"></td>
                      <td><input type="text" name="volume[]" id="volume" value="{{ $val->volume }}" class="form-control"></td>
                      <td><input type="text" name="nama_supir[]" id="nama_supir" value="{{ $val->nama_supir }}" class="form-control"></td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
            <div style="text-align:right;">
              <button class="btn btn-success" type="button" onclick="addItemField()">Tambah Item Baru</button>
              <button class="btn btn-info" type="submit">Simpan</button>
            </div>
          </form>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
@endsection
@section('javascript') 
<!-- DataTables -->
<script src="{{ url('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });

  function addItemField(){
    var body = jQuery("#item-body");
    var html = "<tr>";
    html += '<td onclick="deleteItemField(this)"><i class="fa fa-trash"></i></td>';
    html += '<td><input type="date" name="tanggal[]" id="tanggal" class="form-control"></td>';
    html += '<td><input type="text" name="nopol[]" id="nopol" class="form-control"></td>';
    html += '<td><input type="text" name="volume[]" id="volume" class="form-control"></td>';
    html += '<td><input type="text" name="nama_supir[]" id="nama_supir" class="form-control"></td>';
    html += "</tr>";           
    body.append(html);  
  }
  function deleteItemField(elem){
    $(elem).parent().remove();
  }
</script>
@endsection