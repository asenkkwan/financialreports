@extends('layouts.app')

@section('css')
    <!-- DataTables -->
  <link rel="stylesheet" href="{{('bower_components/datatables.net-bs/css/dataTables.bootstrap.css')}}">
@endsection
  
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> 
        Master
        <small>jenis bbm</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data Jenis BBM</li>
      </ol>
    </section>

    <!-- Main content -->
      <!-- Main content --> 
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
              
              <div style="float: right">
                <button class="btn btn-primary btn-xs" onclick="window.location='<?php echo url('jenis-bbm/create'); ?>'">
                  <i class="fa fa-plus"></i> Tambah
                </button> 
              </div>
              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="datatable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th style="width: 30px;">No</th>
                  <th>Jenis BBM</th>
                  <th><i class="fa fa-ellipsis-h"></i></th>
                </tr>
                </thead>
                <tbody>
                  @foreach($jenis_bbm as $val)
                  <tr>
                    <td>{{ $val->id }}</td>
                    <td>{{ $val->jenis_bbm }}</td>
                    <td>                     
                      <a href="{{ url('jenis-bbm/edit/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i>Edit</a>
                      <a class="btn btn-danger btn-xs" href="{{ url('jenis-bbm/delete/'.$val->id) }}" onclick="event.preventDefault();if(confirm('Hapus Cabang ?')){ $('#form-{{ $val->id }}').submit() }"><i class="fa fa-trash"></i>Hapus</a>
                      <form action="{{ url('jenis-bbm/delete/'.$val->id) }}" method="post" id="form-{{ $val->id }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="delete">
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('javascript')
<!-- DataTables -->
<script src="{{('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#datatable').DataTable({
      "columnDefs": [{ "orderable": false, "targets": 2 }]
    });
  });
</script>
@endsection