@extends('layouts.app') 
@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{url('plugins/datatables/dataTables.bootstrap.css')}}">
@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
        Form
        <small>Tambah BBM</small>
      </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a>
      </li>
      <li><a href="#">Forms</a>
      </li>
      <li class="active">Tambah Jenis BBM</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-8">
         @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Tambah Jenis BBM</h3>
          </div>
          <!-- /.box-header -->
         
              <form role="form" action="{{ url('jenis-bbm/store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- text input -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid #eee; ">
                      <div class="form-group">
                        <label>Jenis BBM *</label>
                        <input type="text" class="form-control" id="jenis_bbm" name="jenis_bbm" placeholder="Masukkan Jenis BBM" value="{{ old('jenis_bbm') }}" required>
                      </div>  
                    </div>
                  </div>
                <div class="box-footer">
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </form> 
            </div> 
          </div> 
        </section>
      </div>

@endsection 
@section('javascript')
<!-- DataTables -->
<script src="{{('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
          $('#datatable').DataTable({
            "columnDefs": [{ "orderable": false, "targets": 2 }]
          });
      
        });
</script>
@endsection