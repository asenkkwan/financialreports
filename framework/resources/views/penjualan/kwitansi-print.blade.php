
<!DOCTYPE html>
<html lang="en">
 
<head>
  <meta charset="utf-8">
  <title>Financial Reports  | Kwitansi</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need --> 
  <style>
@page {  
  size: A5 landscape 
}
p {
  font-family: arial;
}
.terbilang {
            margin-left: -51px; 
            margin-top:262px; 
            font-size: 14px; 
            max-width: 27%; 
            position: fixed;
          }
</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->

<body onload="window.print();" class="A5 landscape">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 --> 
  <section class="sheet padding-0mm">
    <!-- <img src="{{ url('dist/img/kwitansiSumadi.jpeg') }}" style="width: 100%;"> -->
  <div style="z-index: 9999;position: absolute; top: -11px; margin-left: -5px; font-size: 14px;">
    <p style="margin-left: 200px; margin-top:85px;"><strong></strong> {{ $penjualan->no_invoice}} </p>
    <p style="margin-left: 159px; margin-top: 0px;"> {{ date('d M Y', strtotime($penjualan->tanggal_pengaliran)) }}<br> </p>
    <p style=" margin-left: 159px; margin-top:-10px;"> {{ $penjualan->no_po }}</p>
    <p style=" margin-left: 159px; margin-top:-12px;"> {{ $penjualan->nama_bunker }}</p>  
  </div>
   <div style="z-index: 9999;position: absolute;top: 0; margin-left: 316px; margin-top: 6px; font-size: 14px;
    word-spacing: -1px;">
    <p style="margin-left: 171px; margin-top:88px; width: 290px; line-height:20px;"><strong></strong> {{ $penjualan->nama_perusahaan}} </p>
    <p style="margin-left: 170px; margin-top:-12px; position: absolute;  width: 265px; line-height:20px; font-size:12px;"> {{ $penjualan->lokasi_perusahaan }}<br> </p>
  </div>
  <div style="z-index: 9999;position: absolute; top: 0; margin-left: 190px; margin-top: 92px;">
    <p style="margin-left: 88px; margin-top:88px;"><strong></strong> {{ $penjualan->jenis_kendaraan}} </p>
    <p style="margin-left: 225px; margin-top:-35px;"> {{ $penjualan->nopol }}<br> </p>
  </div>
  <?php     
    $counter = 1;
    $absolute = 0;
  ?>
  @foreach($items as $val)
   <div style="z-index: 9999;position: absolute;top: {{ $absolute }}px; margin-left: 198px; margin-top: 148px;font-size: 14px; ">
    <p style="margin-left: -134px; margin-top:94px;">{{ $counter }}</p>
    <p style="margin-left: -27px; margin-top:-29px; width: 170px;"> {{ $val->produk }}<br> </p>
    <p style="margin-left: 190px; margin-top:-30px;"> {{ number_format($val->qty, 0, ",", ".") }}  <br></p>
    <p style="margin-left: 244px; margin-top:-30px;"> {{ $val->uom}}  <br> </p>
    <p style="margin-left: 320px; margin-top:-29px;">{{ number_format($val->harga_dasar, 2, ",", ".") }}<br> </p>
    <p style="margin-left: 449px; margin-top:-29px; text-align: right;"> {{ number_format($val->dpp, 0, ",", ".") }}<br> </p>
  </div>
  <?php     
    $counter++;
    $absolute += 20;
  ?> 
  @endforeach  

  @if($penjualan->show_pbbkb == 1)
   <div style="z-index: 9999;position: absolute;top: {{ $absolute }}px; margin-left: 187px; margin-top: 249px;font-size: 14px; ">
    <p style="margin-left: -123px; margin-top:0px;">{{ $counter }}</p>
    <p style="margin-left: -19px; margin-top:-29px;"> PBBKB <br> </p>
    <p style="margin-left: 460px; margin-top:-29px; text-align: right;"> {{ number_format($val->pbbkb_jual, 0, ",", ".") }}<br> </p>
  </div>
  @endif
  @if($penjualan->show_add_cost == 2)
  <div style="z-index: 9999;position: absolute;top: {{ $absolute }}px; margin-left: 202px; margin-top: 249px;font-size: 14px; ">
    <p style="margin-left: -140px; margin-top:0px;">{{ $counter }}</p>
    <p style="margin-left: -33px; margin-top:-29px;"> Add Cost<br> </p>
    <p style="margin-left: 449px; margin-top:-29px; text-align: right;"> {{ number_format($po_masuk->add_cost, 0, ",", ".") }}<br> </p>
  </div>
  @endif
  <?php 
    $total_amount = 0;
    $counter = 1;
  ?>
  @foreach($items as $val) 
  <?php 
    $counter++;
    $total_amount += ($val->dpp);
    $ppn = ($val->dpp);
  ?> 
  @endforeach
  <?php
    $ppn = $total_amount*((int)$po_masuk->ppn/100);
    $pph = 0;
    if($po_masuk->pph != 0.000){
      $pph = $total_amount*($po_masuk->pph/100);
    }
    // print_r($po_masuk->add_cost);die();
    
    $grand_total = $total_amount + (int)$pph + $po_masuk->add_cost + $ppn + $val->pbbkb_jual;
    $grand_total =  ceil($grand_total);
    // print_r($ppn);die();
  ?>
  @if($penjualan->ppn == 0)
  <div style="z-index: 9999;position: absolute;top: 0; margin-left: 196px; margin-top: 443px; font-size: 14px;">
    <p style="margin-left: 450px; margin-top:-99px;">0<br> </p>
    <p style="margin-left: 449px; margin-top:-8px;"> {{ number_format($grand_total, 0, ",", ".") }}<br> </p>
  </div>
  @else
  <div style="z-index: 9999;position: absolute;top: 0; margin-left: 196px; margin-top: 443px; font-size: 14px;">
    <p style="margin-left: 450px; margin-top:-103px;"> {{ number_format($ppn, 0, ",", ".") }}<br> </p>
    <p style="margin-left: 449px; margin-top:-4px;"> {{ number_format($grand_total, 0, ",", ".") }}<br></p>
  </div>
  @endif
  <div class="terbilang" style="z-index: 9999;position: absolute;top: 0; margin-left: 196px; margin-top: 134px; font-size: 14px;">
    <p class="print" style="margin-left: -51px; margin-top:274px; font-size: 14px; max-width: 432px; position:fixed;"><b> # {{ terbilang($grand_total, 1) }} RUPIAH #</b><br> </p>
  </div>
  </section>
</body>
</html>