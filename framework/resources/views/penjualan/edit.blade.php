@extends('layouts.app')
@section('css')
 <!-- Select2 -->
  <link rel="stylesheet" href="{{ url('plugins/select2/select2.min.css') }}">
@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) --> 
  <section class="content-header">
    <h1> 
        Form
        <small>Penjualan Baru</small> 
      </h1>   
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a>
      </li>
      <li><a href="#">Forms</a>
      </li> 
      <li class="active">Penjualan Baru</li>
    </ol>
  </section>
  <!-- Main content --> 
  <section class="content">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12">
         @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger"> 
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border"> 
            <h3 class="box-title">Form</h3>
          </div> 
          <!-- /.box-header --> 
         
           <form role="form" action="{{ url('penjualan/update/'.$penjualan->id) }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <h3>Informasi Produk</h3>
                  <table class="table table-striped">
                    <thead>
                    <tr> 
                      <th style="width:11%;">Produk</th>
                      <th style="width:11%;">Jenis BBM</th>
                      <th style="width:11%;">Quantity</th>
                      <th style="width:11%;">Satuan</th>
                      <th style="width:7%;">Harga Jual</th>
                      <th style="width:11%;">Harga Dasar</th>
                      <th style="width:8%;">PBBKB</th>
                      <!-- <th style="width:5%;">PPN</th> -->
                      <th style="width:15%;">Keterangan</th>
                    </tr>
                    </thead>
                    <tbody id="item-body">
                      @if(count($item_penjualan) > 0)
                        @foreach($item_penjualan as $val)
                           <tr>
                            <input type="hidden" name="id_item[]" value="{{ $val->id }}">
                            <td><input type="text" name="produk[]" id="produk" value="{{ $val->produk }}" class="form-control" readonly></td>
                            <td>
                              <select name="jenis_bbm[]" class="form-control" id='jenis_bbm' required>
                                <option value="HSD" @if($val->jenis_bbm == 'HSD') selected @endif>HSD</option>
                                <option value="MFO" @if($val->jenis_bbm == 'MFO') selected @endif>MFO</option>
                              </select>
                            </td>
                            <td><input type="text" name="quantity[]" id="quantity" value="{{ (int)$val->qty }}" class="form-control" required></td>
                            <td><input type="text" name="uom[]" id="uom" value="{{ $val->uom }}" class="form-control" required></td>
                            <td><input type="text" name="harga_jual[]" id="harga_jual" value="{{ $val->harga_jual }}" class="form-control" readonly></td>
                            <td><input type="text" name="harga_dasar[]" id="harga_dasar" value="{{ $val->harga_dasar }}" class="form-control" readonly required></td>
                            <td>
                              <select name="pbbkb[]" class="form-control" id='pbbkb'>
                                <option value="0" @if($val->pbbkb=="0") selected @endif>{{App\Utilities\Constant::$PBBKB['0.000']}}</option>
                                <option value="0.858" @if($val->pbbkb=="0.858") selected @endif>{{App\Utilities\Constant::$PBBKB['0.858']}}</option>
                                <option value="1.288" @if($val->pbbkb=="1.288") selected @endif>{{App\Utilities\Constant::$PBBKB['1.288']}}</option>
                                <option value="4.5" @if($val->pbbkb=="4.5") selected @endif>{{App\Utilities\Constant::$PBBKB['4.500']}}</option>
                                <option value="5" @if($val->pbbkb=="5") selected @endif>{{App\Utilities\Constant::$PBBKB['5.000']}}</option>
                                <option value="7.5" @if($val->pbbkb=="7.5") selected @endif>{{App\Utilities\Constant::$PBBKB['7.500']}}</option>
                                <option value="10" @if($val->pbbkb=="10") selected @endif>{{App\Utilities\Constant::$PBBKB['10.000']}}</option>
                              </select>
                            </td>
                            <!-- <td><input type="text" name="ppn[]" id="ppn" value="{{ $val->ppn }}" class="form-control"></td> -->
                            <td><input type="text" name="keterangan[]" id="keterangan" value="{{ $val->keterangan }}" class="form-control"></td>
                          </tr>
                          <?php 

                           ?>
                        @endforeach
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <h3>Informasi Penjualan</h3>
                </div>
                <div class="col-md-6" style="border-right: 1px solid #eee; ">
                  <div class="form-group">
                    <label for="tanggal_pengaliran">Tanggal Pengaliran</label>
                    <input type="date" class="form-control" id="tanggal_pengaliran" name="tanggal_pengaliran" value="{{ $penjualan->tanggal_pengaliran}}" required>
                  </div> 
                  <div class="form-group">
                    <label for="no_invoice">No Invoice</label>
                    <input type="text" class="form-control" id="no_invoice" name="no_invoice" value="{{ $penjualan->no_invoice}}" readonly>
                  </div> 
                  <div class="form-group">
                    <label for="no_faktur_pajak">No Faktur Pajak</label>
                    <input type="text" class="form-control" id="no_faktur_pajak" name="no_faktur_pajak" value="{{ $penjualan->no_faktur_pajak}}" required>
                  </div>
                  <div class="form-group">
                    <label for="nama_bunker">Nama Kapal</label>
                    <input type="text" class="form-control" id="nama_bunker" name="nama_bunker" value="{{ $penjualan->nama_bunker}}" required>
                  </div> 
                  <div class="form-group">
                    <label for="show_pbbkb">Show PBBKB</label>
                      <select class="form-control select2" name="show_pbbkb" id="show_pbbkb">
                        <option value="-1" @if($penjualan->show_pbbkb=="-1") selected @endif>{{App\Utilities\Constant::$SHOW_PBBKB['-1']}}</option>
                        <option value="1" @if($penjualan->show_pbbkb=="1") selected @endif>YES</option>
                        <option value="0" @if($penjualan->show_pbbkb=="0") selected @endif>NO</option>
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="show_add_cost">Show Add Cost</label>
                      <select class="form-control select2" name="show_add_cost" id="show_add_cost">
                        <option value="-2" @if($penjualan->show_add_cost=="-2") selected @endif>{{App\Utilities\Constant::$SHOW_ADD_COST['-2']}}</option>
                        <option value="2" @if($penjualan->show_add_cost=="2") selected @endif>YES</option>
                        <option value="0" @if($penjualan->show_add_cost=="0") selected @endif>NO</option>
                      </select>
                  </div>  
                   <div class="form-group">
                    <label for="nama_supplier">Nama Penyalur * </label>
                    <select class="form-control select2" name="nama_supplier" id="nama_supplier">
                      <option selected="selected" >{{ $penjualan->nama_supplier }}</option>
                      <option value="">-- Pilih Penyalur --</option>
                      @foreach($nama_supplier as $val)
                      <option value="{{ $val->id }}">{{ $val->name }}</option>
                      @endforeach
                    </select>
                  </div>               
                </div>
                <div class="col-md-6">
                 
                  <div class="form-group"> 
                    <label for="telp">Telepon Agen *</label>
                    <input type="text" class="form-control" id="telp" name="telp" value="{{ $penjualan->telp }}" required>
                  </div>
                  <div class="form-group">
                    <label for="no_po">Nomor PO</label>
                    <input type="text" class="form-control" id="no_po" name="no_po" value="{{ $penjualan->no_po }}" readonly required> 
                  </div>
                   <div class="form-group">
                    <label for="ppn">PPN</label>
                    <input type="text" class="form-control" id="ppn" name="ppn" value="{{ $penjualan->ppn }}"  required>
                  </div>
                  <div class="form-group">
                    <label for="tanggal">Tanggal</label>
                    <input type="date" class="form-control" id="tanggal" name="tanggal" value="{{ $penjualan->tanggal }}" readonly required>
                  </div> 
                  <div class="form-group">
                    <label for="role">Nama Perusahaan *</label>
                     <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="{{ $penjualan->nama_perusahaan }}" readonly required>
                  </div>
                  <div class="form-group">  
                    <label for="kode_alamat_perusahaan">Kode Alamat Perusahaan *</label>
                    <input type="text" class="form-control" id="kode_alamat_perusahaan" name="kode_alamat_perusahaan" value="{{ $penjualan->kode_lokasi_perusahaan }}" readonly required>
                  </div> 
                  <div class="form-group"> 
                    <label for="lokasi_perusahaan">Alamat Perusahaan *</label>
                    <input type="text" class="form-control" id="lokasi_perusahaan" name="lokasi_perusahaan" value="{{ $penjualan->lokasi_perusahaan }}" readonly required>
                  </div> 
                   
                </div>
              </div>
            
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-success pull-right">Simpan</button>
            </div>
            
          </form> 
        </div> 
      </div>
    </div> 
  </section>
</div>
@endsection 
@section('javascript')
<!-- Select2 -->
<script src="{{ url('plugins/select2/select2.full.min.js')}}"></script>

<script>
  $(".select2").select2();

</script>
@endsection