@extends('layouts.app')
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{url('plugins/datatables/dataTables.bootstrap.css')}}">
@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
        Form
        <small>Penjualan</small>
      </h1> 
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a>
      </li>
      <li><a href="#">Forms</a>
      </li> 
      <li class="active">Penjualan</li>
    </ol>
  </section> 
  <!-- Main content --> 
  <section class="content">
    <div class="row">  
      <!-- left column -->
      <div class="col-md-12"> 
         @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
         
        <!-- general form elements -->
         <div class="box box-primary">
          <div class=" col-sm-12 box-header with-border" style="text-align:center;"> 
            <h2 style="text-decoration:underline;">Detail Penjualan</h2><br>
          <p style="margin-top: -30px; font-size: 17px;">Nomor : {{ $penjualan->no_po }}</p>
          </div>
          <!-- /.box-header -->
              <form role="form" action="{{ url('penjualan/store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- text input -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid #eee; ">
                      <div class="form-group">
                        <label for="no_po">Nomor PO</label>
                        <input type="text" class="form-control" id="no_po" name="no_po" value="{{ $penjualan->no_po }}" disabled required>
                      </div>
                      <div class="form-group">
                        <label for="tanggal">Tanggal</label>
                        <input type="date" class="form-control" id="tanggal" name="tanggal" value="{{ $penjualan->tanggal }}" disabled required>
                      </div> 
                      <div class="form-group">
                        <label for="no_invoice">No Invoice</label>
                        <input type="text" class="form-control" id="no_invoice" name="no_invoice"  value="{{ $penjualan->no_invoice }}"  disabled required>
                      </div>
                       <div class="form-group">
                        <label for="no_faktur_pajak">No Faktur Pajak *</label>
                         <input type="text" class="form-control" name="no_faktur_pajak" id="no_faktur_pajak" value="{{ $penjualan->no_faktur_pajak }}" disabled required></input>
                      </div> 
                      
                      <div class="form-group">
                        <label for="role">Nama Perusahaan *</label>
                        <select class="form-control" name="nama_perusahaan" id="nama_perusahaan" onchange="fetchDataAddress(this)" disabled required>
                          <option value="">-- Nama Perusahaan --</option>
                          @foreach($nama_perusahaan as $val) 
                            @if($customer->id == $val->id)
                            <option value="{{ $val->id }}" selected>{{ $val->name }}</option>
                            @else
                            <option value="{{ $val->id }}">{{ $val->name }}</option>
                            @endif
                          @endforeach
                        </select> 
                      </div>
                      <div class="form-group"> 
                        <label for="lokasi_perusahaan">Lokasi Perusahaan *</label>
                        <select class="form-control" name="lokasi_perusahaan" id="lokasi_perusahaan" disabled required>
                          @foreach($lokasi_perusahaan as $val)
                            @if($caddress->id == $val->id)
                            <option value="{{ $val->id }}" selected>{{ $val->address }}</option>
                            @else
                            <option value="{{ $val->id }}">{{ $val->address }}</option>
                            @endif
                          @endforeach
                        </select>
                      </div> 
                      <div class="form-group">
                        <label for="nama_supplier">Nama Supplier * </label>
                        <select class="form-control" name="nama_supplier" id="nama_supplier" value="{{ $penjualan->nama_supplier }}" disabled required>
                          @foreach($agen as $val)
                          @if($penjualan->nama_supplier == $val->id)
                            <option value="{{ $val->id }}" selected>{{ $val->name }}</option>
                          @else
                            <option value="{{ $val->id }}">{{ $val->name }}</option>
                          @endif 
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="produk">Produk *</label>
                       <input type="text" class="form-control" id="produk" name="produk" value="{{ $penjualan->produk }}" disabled required>
                      </div>  
                       <div class="form-group">
                        <label for="qty">QTY *</label>
                         <input type="number" class="form-control" name="qty" id="qty"  value="{{ $penjualan->qty }}" disabled required></input>
                      </div> 
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="harga_beli">Harga Beli *</label>
                        <input type="number" class="form-control" name="harga_beli" id="harga_beli" value="{{ (int)$penjualan->harga_beli }}" disabled required></input>
                      </div>
                      <div class="form-group">
                        <label for="harga_jual">Harga Jual *</label>
                         <input type="number" class="form-control" name="harga_jual" id="harga_jual" value="{{ $penjualan->harga_jual }}" disabled required></input>
                      </div>
                      <div class="form-group">
                        <label for="pbbkb">PBBKB</label>
                         <select name="pbbkb" class="form-control" id='pbbkb' disabled>
                          <option value="0.858" @if($penjualan->pbbkb == 0.858) selected @endif>0.858%</option>
                          <option value="1.288" @if($penjualan->pbbkb == 1.288) selected @endif>1.288%</option>
                          <option value="7.5" @if($penjualan->pbbkb == 7.5) selected @endif>7.5%</option>
                          <option value="35" @if($penjualan->pbbkb == 35) selected @endif>35%</option>
                         </select>
                         <script type="text/javascript">
                           document.getElementById('pbbkb').value = "{{ $penjualan->pbbkb }}";
                         </script>
                      </div>
                      <div class="form-group">
                        <label for="pbbkb_dasar">PBBKB Dasar</label>
                         <input type="number" class="form-control" name="pbbkb_dasar" id="pbbkb_dasar" value="{{ (int)$penjualan->pbbkb_dasar }}" disabled></input>
                      </div>
                      <div class="form-group">
                        <label for="pbbkb_jual">PBBKB Jual</label>
                         <input type="number" class="form-control" name="pbbkb_jual" id="pbbkb_jual" value="{{ (int)$penjualan->pbbkb_jual }}" disabled></input>
                      </div>
                      <div class="form-group">
                        <label for="ppn">PPN</label>
                         <input type="number" class="form-control" name="ppn" id="ppn" value="10" disabled></input>
                      </div>
                      <div class="form-group">
                        <label for="ppn_total">PPN Total</label>
                         <input type="number" class="form-control" name="ppn_total" id="ppn_total" value="{{ $penjualan->ppn_total }}" disabled></input>
                      </div>
                      <div class="form-group">
                        <label for="dpp">DPP</label>
                         <input type="number" class="form-control" name="dpp" id="dpp" value="{{ $penjualan->dpp }}" disabled></input>
                      </div>
                      <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                         <input type="text" class="form-control" name="keterangan" id="keterangan" value="{{ $penjualan->keterangan }}" disabled></input>
                      </div>
                    </div>
                  </div>
               <div class="row no-print" style="padding:40px;"> 
                <div class="col-xs-12">
                  <a href="{{ url('penjualan/invoice-print/'.$penjualan->id) }}"  target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print Detail</a>
                  <a href="{{ url('penjualan/kwitansi-print/'.$penjualan->id) }}"  target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print Kwitansi</a>
                  @if($penjualan->status == 1)
                    @if($penjualan->invoice_status == 1)
                    <button type="button" class="btn btn-info pull-right"><i class="fa fa-credit-card"></i> Show Invoice </button>
                    @else
                    <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Generate Invoice </button>
                    @endif
                  @else
                    @if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin')
                     <button type="button" class="btn btn-success pull-right" onclick="approve({{ $penjualan->id }})"><i class="fa fa-credit-card"></i> Approve</button>
                    <button type="button" class="btn btn-danger pull-right" onclick="reject({{ $penjualan->id }})" style="margin-right: 5px;"><i class="fa fa-credit-card"></i> Reject</button>
                    <form id="form-approval" action="{{ url('penjualan/approve') }}" method="post">
                       {{ csrf_field() }}
                       <input type="hidden" name="penjualan_id" value="{{ $penjualan->id }}">
                     </form>
                     <form id="form-reject" action="{{ url('penjualan/reject') }}" method="post">
                       {{ csrf_field() }}
                       <input type="hidden" name="penjualan_id" value="{{ $penjualan->id }}">
                     </form>
                    @endif
                  @endif     
                </div>
              </div>        
        </section>
      </div> 
@endsection 
@section('javascript')
<script>
  function approve(id){
    if(confirm("Apakah anda menyetujui Penjualan ini?")){
      $("#form-approval").submit();
    }
  } 

  function reject(id){
    if(confirm("Apakah anda menyetujui Penjualan ini?")){
      $("#form-reject").submit();
    }
  } 
</script>
@endsection