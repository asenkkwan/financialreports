@extends('layouts.app')
@section('css')
 <!-- Select2 -->
  <link rel="stylesheet" href="{{ url('plugins/select2/select2.min.css') }}">
@endsection
    
@section('content')
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) --> 
  <section class="content-header">
    <h1> 
        Form
        <small>Penjualan Baru</small> 
      </h1>   
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a>
      </li>
      <li><a href="#">Forms</a>
      </li> 
      <li class="active">Penjualan Baru</li>
    </ol> 
  </section> 
  <!-- Main content --> 
  <section class="content">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12">
         @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif 

          @if ($errors->any())
            <div class="alert alert-danger"> 
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border"> 
            <h3 class="box-title">Form</h3>
          </div>
          <!-- /.box-header --> 
         
          <form role="form" action="{{ url('penjualan/store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <!-- text input --> 
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <center>
                    <label>Nomor PO</label><br>
                    <select class="select2" name="filter_po_masuk" id="filter_po_masuk" onchange="openForm(this)" style="width: 200px" required>
                          <option value="">-- Pilih PO --</option>
                          @foreach($list_po_masuk as $val)
                          @if(isset($po_masuk->nomor_po) and $po_masuk->nomor_po == $val)
                          <option value="{{ $val }}" selected>{{ $val }}</option>
                          @else
                          <option value="{{ $val }}">{{ $val }}</option>
                          @endif
                          
                          @endforeach
                        </select> 
                  </center>
                </div>
              </div>
             
              @if(isset($po_masuk))
              <div class="row">
                <div class="col-md-12">
                  <h3>Informasi Produk</h3>
                  <table class="table table-striped">
                    <thead>
                    <tr>  
                      <th style="width:11%;">Produk</th>
                      <th style="width:11%;">Jenis BBM</th>
                      <th style="width:11%;">Quantity</th>
                      <th style="width:11%;">Satuan</th>
                      <th style="width:7%;">Harga Jual</th>
                      <th style="width:8%;">PBBKB</th>
                     <!--  <th style="width:5%;">PPN</th> -->
                      <th style="width:15%;">Keterangan</th>
                    </tr>
                    </thead>
                    <tbody id="item-body">
                      @if(count($item_po_masuk) > 0)
                        @foreach($item_po_masuk as $val)
                           <tr>
                            <td><input type="text" name="produk[]" id="produk" value="{{ $val->description }}" class="form-control" readonly></td>
                            <td> 
                              <select name="jenis_bbm[]" class="form-control" id='jenis_bbm' required>
                                <option value="HSD">HSD</option>
                                <option value="MFO">MFO</option>
                              </select>
                            </td> 
                            <td><input type="text" name="quantity[]" id="quantity" value="{{ (int)$val->qty  }}" class="form-control" required></td>
                            <td><input type="text" name="uom[]" id="uom" value="{{ $val->uom  }}" class="form-control" required></td>
                            <td><input type="text" name="harga_jual[]" id="harga_jual" value="{{ ($val->unit_price) }}" class="form-control" readonly></td>
                            <td>
                              <select name="pbbkb[]" class="form-control" id='pbbkb'>
                                <option value="0" @if($po_masuk->pbbkb=="0") selected @endif>{{App\Utilities\Constant::$PBBKB['0.000']}}</option>
                                <option value="0.858" @if($po_masuk->pbbkb=="0.858") selected @endif>{{App\Utilities\Constant::$PBBKB['0.858']}}</option>
                                <option value="1.288" @if($po_masuk->pbbkb=="1.288") selected @endif>{{App\Utilities\Constant::$PBBKB['1.288']}}</option>
                                <option value="4.5" @if($po_masuk->pbbkb=="4.5") selected @endif>{{App\Utilities\Constant::$PBBKB['4.500']}}</option>
                                <option value="5" @if($po_masuk->pbbkb=="5") selected @endif>{{App\Utilities\Constant::$PBBKB['5.000']}}</option>
                                <option value="7.5" @if($po_masuk->pbbkb=="7.5") selected @endif>{{App\Utilities\Constant::$PBBKB['7.500']}}</option>
                                <option value="10" @if($po_masuk->pbbkb=="10") selected @endif>{{App\Utilities\Constant::$PBBKB['10.000']}}</option>
                              </select>
                            </td>
                           <!--  <td><input type="text" name="ppn[]" id="ppn" value="10" class="form-control"></td> -->
                            <td><input type="text" name="keterangan[]" id="keterangan" value="" class="form-control"></td>
                          </tr>
                          <?php 

                           ?>
                        @endforeach
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <h3>Informasi Penjualan</h3> 
                </div>
                <div class="col-md-6" style="border-right: 1px solid #eee; ">
                  <div class="form-group">
                    <label for="tanggal_pengaliran">Tanggal Pengaliran</label>
                    <input type="date" class="form-control" id="tanggal_pengaliran" name="tanggal_pengaliran" placeholder="Masukkan Tanggal Pengaliran" required>
                  </div>
                  <div class="form-group">
                    <label for="no_invoice">No Invoice</label>
                    <input type="text" class="form-control" id="no_invoice" name="no_invoice" placeholder="Masukkan Nomor Invoice" required>
                  </div> 
                  <div class="form-group">
                    <label for="no_faktur_pajak">No Faktur Pajak</label>
                    <input type="text" class="form-control" id="no_faktur_pajak" name="no_faktur_pajak" placeholder="Masukkan Nomor Faktur Pajak" required>
                  </div>
                  <div class="form-group">
                    <label for="nama_bunker">Nama Kapal *</label>
                    <input type="text" class="form-control" id="nama_bunker" name="nama_bunker" placeholder="Masukkan Nama Kapal" required>
                  </div>  
                  <div class="form-group">
                    <label for="show_pbbkb">Tampilkan PBBKB</label>
                      <select class="form-control select2" name="show_pbbkb" id="show_pbbkb">
                        <option value="-1">-- Tampilkan PBBKB --</option>
                        <option value="1">YES</option>
                        <option value="0">NO</option>                    
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="show_add_cost">Tampilkan  Add Cost</label>
                      <select class="form-control select2" name="show_add_cost" id="show_add_cost">
                        <option value="-2">-- Tampilkan Add Cost --</option>
                        <option value="2">YES</option>
                        <option value="0">NO</option>                    
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="nama_supplier">Nama Penyalur * </label>
                    <select class="form-control select2" name="nama_supplier" id="nama_supplier">
                      <option value="">-- Pilih Penyalur --</option>
                      @foreach($nama_supplier as $val)
                      <option value="{{ $val->name }}">{{ $val->name }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group"> 
                    <label for="telp">Telepon Agen *</label>
                    <input type="text" class="form-control" id="telp" name="telp" value="" required>
                  </div>
                  <div class="form-group">
                    <label for="no_po">Nomor PO</label>
                    <input type="text" class="form-control" id="no_po" name="no_po" value="{{ $po_masuk->nomor_po }}" readonly required> 
                  </div> 
                  <div class="form-group">
                    <label for="ppn">PPN</label>
                    <input type="text" class="form-control" id="ppn" name="ppn" value="{{ $po_masuk->ppn }}"  required>
                  </div>
                  <div class="form-group">
                    <label for="tanggal">Tanggal PO</label>
                    <input type="date" class="form-control" id="tanggal" name="tanggal" value="{{ $po_masuk->tanggal_po }}" readonly required>
                  </div>
                  <div class="form-group">
                    <label for="role">Nama Perusahaan *</label>
                     <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="{{ $po_masuk->nama_perusahaan }}" readonly required>
                     <input type="hidden" class="form-control" id="id_customer" name="id_customer" value="{{ $po_masuk->id_customer }}">
                  </div>
                  <div class="form-group"> 
                    <label for="kode_alamat_perusahaan">Kode Alamat Perusahaan *</label>
                    <input type="text" class="form-control" id="kode_alamat_perusahaan" name="kode_alamat_perusahaan" value="{{ $po_masuk->kode_lokasi_perusahaan }}" readonly required>
                  </div> 
                  <div class="form-group"> 
                    <label for="lokasi_perusahaan">Alamat Perusahaan *</label>
                    <input type="text" class="form-control" id="lokasi_perusahaan" name="lokasi_perusahaan" value="{{ $po_masuk->alamat_perusahaan }}" readonly required>
                  </div> 
                   
                </div>
              </div>
            
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-success pull-right">Simpan</button>
            </div>
              @endif
          </form> 
        </div> 
      </div>
    </div> 
  </section>
</div>
@endsection 
@section('javascript')
<!-- Select2 -->
<script src="{{ url('plugins/select2/select2.full.min.js')}}"></script>
<script>
  
  function addItemField(){
    var body = jQuery("#item-body");
    var html = "<tr>";
    html += '<td onclick="deleteItemField(this)"><i class="fa fa-trash"></i></td>';
    html += '<td><input type="text" name="produk[]" id="produk" class="form-control"></td>';
    html += '<td><input type="text" name="qty[]" id="qty" class="form-control"></td>';
    html += '<td><input type="text" name="harga_beli[]" id="harga_beli" class="form-control"></td>';
    html += '<td><input type="text" name="harga_jual[]" id="harga_jual" class="form-control"></td>';
    html += '<td><input type="text" name="pbbkb[]" id="pbbkb" class="form-control"></td>';
    html += '<td><input type="text" name="ppn[]" id="ppn" class="form-control"></td>';
    html += "</tr>";           
    body.append(html);  
  }
  function deleteItemField(elem){
    $(elem).parent().remove();
  }
</script>
<script>
  $(".select2").select2();

  function openForm(elem){
    var data = $(elem).val();
    window.location = "{{ url('/penjualan/create?filter_po_masuk=') }}"+data;
  }
  
   function fetchDataAddress(elem){
    var customer = $(elem).val();

    $.ajax({
      url:"{{ url('customer/getAddress') }}?customer_id="+customer,
      type:"get",
      dataType:"json",
      beforeSend:function(){

      },
      success:function(result){
        if(result.status = "ok"){
          var element = $("#lokasi_perusahaan");
          var html = "<option value=''>Pilih Alamat</option>";

          $.each(result.data, function(index, value){
            html += "<option value='"+value.id+"'>"+value.address+"</option>";
          });

          element.html(html);
        }else{
          var html = "<option value=''>Silahkan Pilih Konsumen</option>";
          element.html(html);
        }
      },
      error:function(){

      },
      complete:function(){

      }
    });
  }
</script>
@endsection