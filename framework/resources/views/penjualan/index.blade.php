@extends('layouts.app')

@section('css')
@endsection
  
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> 
        Penjualan BBM
      </h1>
    </section> 
  
    <!-- Main content -->
      <!-- Main content -->   
    <section class="content"> 
      <div class="row">
        <div class="col-xs-12"> 
          @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
              <div style="float: right">
                <button class="btn btn-primary" onclick="window.location='<?php echo url('penjualan/create'); ?>'">
                  <i class="fa fa-plus"></i> Tambah
                </button> 
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <form method="get" action="#" id="form-filter">
              <div class="row">
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Start Date</label>
                    <input type="date" name="sdate" id="sdate" class="form-control" value="{{ $sdate }}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>End Date</label>
                    <input type="date" name="edate" id="edate" class="form-control" value="{{ $edate }}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>No PO</label>
                    <input type="text" name="no_po" id="no_po" class="form-control" id="filter_no_po" placeholder="filter no po" value="{{ $no_po }}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>No Invoice</label>
                    <input type="text" name="no_invoice" id="filter_no_invoice" class="form-control" placeholder="filter No Invoice" value="{{ $no_invoice }}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Nama Perusahaan</label>
                    <input type="text" name="nama_perusahaan" id="filter_nama_perusahaan" class="form-control" placeholder="filter nama perusahaan" value="{{ $nama_perusahaan }}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Nama Supplier</label>
                    <input type="text" name="nama_supplier" id="filter_nama_supplier" class="form-control" placeholder="filter nama supplier" value="{{ $nama_supplier }}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Status</label>
                    <select class="form-control" name="status" id="filter_status">
                      <option value="0" @if($status =="0") selected @endif>-- Tampilkan Status --</option>
                      <option value="1" @if($status =="1") selected @endif>Lunas</option>
                      <option value="2" @if($status =="2") selected @endif>Belum Lunas</option>
                    </select>
                  </div>
                </div>
                
                <div class="col-md-2">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <button type="submit" class="form-control btn btn-danger" type='button' onclick='filter()'>Filter</button>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <a href="{{ url('penjualan') }}" class="form-control btn btn-default">Clear</a>
                  </div>
                </div>
                 <div class="col-md-2" style="float: right;">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <button class='btn btn-default pull-right' type='button' onclick='exportExcel()'>Export Excel</button>
                  </div>
                </div>
              </div>
              </form>
              <br>
              <div class="table-responsive">
              <table id="datatable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Tanggal</th>
                  <th>Nama Perusahaan</th> 
                  <th>Lokasi Perusahaan</th> 
                  <th>Nama Supplier</th>
                  <th>No PO</th>
                  <th>No Invoice</th>
                  <th>Produk</th>
                  <th>Jenis BBM</th>
                  <th>Qty</th>           
                  <th>Harga Jual</th>                             
                  <th>Harga Dasar</th> 
                  <th>DPP</th>
                  <th>PPN</th>          
                  <!-- <th>PBBKB Dasar</th> -->
                  <th>PBBKB Jual</th>
                  <th>Jumlah</th>
                  <th>Jumlah Pembulatan</th>
                  <th style="width: 7%;">Status</th>
                  <th><i class="fa fa-ellipsis-h"></i></th>
                </tr>
                </thead>
                <tbody>
                  @foreach($penjualan as $val)
                    <?php
                      $items = DB::table("item_penjualan")->where("id_penjualan", $val->id)->get();
                      $jumlah =  count($items);
                    ?> 
                    @if($jumlah > 1) 
                      <?php
                        $counter = 1;
                      ?>
                      @foreach($items as $value)
                        @if($counter == 1)
                        <tr>
                          <td rowspan="{{ $jumlah }}">{{ date('d M Y', strtotime($val->tanggal)) }}</td>
                          <td rowspan="{{ $jumlah }}">{{ $val->nama_perusahaan }}</td>
                          <td rowspan="{{ $jumlah }}">{{ $val->lokasi_perusahaan }}</td>
                          <td rowspan="{{ $jumlah }}">{{ $val->nama_supplier }}</td> 
                          <td rowspan="{{ $jumlah }}">{{ $val->no_po }}</td> 
                          <td rowspan="{{ $jumlah }}">{{ $val->no_invoice }}</td> 
                          <td>{{ $value->produk }}</td> 
                          <td>{{ $value->jenis_bbm }}</td>
                          <td>{{ number_format($value->qty, 0, ",", ".") }}</td>
                          <td>{{ number_format($value->harga_jual, 0, ",", ".")}}</td>  
                          <td>{{ number_format($value->harga_dasar, 2, ",", ".")}}</td>  
                          <td>{{ number_format($value->dpp, 0, ",", ".") }}</td>
                          <td rowspan="{{ $jumlah }}" style="vertical-align: middle;">{{ number_format($val->ppn_total, 0, ",", ".") }}</td>
                          <!-- <td>{{ number_format($value->pbbkb_dasar, 3, ",", ".") }}</td>  -->
                          <td>{{ number_format($value->pbbkb_jual, 3, ",", ".") }}</td>                       
                          <td>{{ number_format($value->jumlah, 0, ",", ".") }}</td>
                         <td>{{ number_format($value->harga_jual*$value->qty, 0, ",", ".") }}</td>
                          <th> 
                            @if($val->status == 1)
                              Lunas
                            @elseif($val->status == -1)
                              Rejected
                            @else
                              Belum Lunas
                            @endif 
                          </th>
                          <td rowspan="{{ $jumlah }}">   
                            <a href="{{ url('penjualan/detail-penjualan/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-server"></i> Detail </a> 
                            <a href="{{ url('penjualan/edit/'.$val->id) }}" class="btn btn-success btn-xs">
                              <i class="fa fa-pencil"></i> Edit</a>
                              <!--  <a href="{{ url('penjualan/add-item/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Edit Item</a>   -->
                            <a class="btn btn-danger btn-xs" href="{{ url('penjualan/'.$val->id) }}" onclick="event.preventDefault();if(confirm('Hapus Data penjualan ?')){ $('#form-{{ $val->id }}').submit() }"><i class="fa fa-trash"></i> Hapus </a>
                            <form action="{{ url('penjualan/delete/'.$val->id) }}" method="post" id="form-{{ $val->id }}">
                              {{ csrf_field() }}
                              <input type="hidden" name="_method" value="delete">
                            </form>
                          </td>
                        </tr>
                        @else
                        <tr>
                          <td>{{ $value->produk }}</td>
                          <td>{{ $value->jenis_bbm }}</td>
                          <td>{{ number_format($value->qty, 0, ",", ".") }}</td>
                          <td>{{ number_format($value->harga_jual, 0, ",", ".")}}</td>
                          <td>{{ number_format($value->harga_dasar, 2, ",", ".")}}</td>  
                          <td>{{ number_format($value->dpp, 0, ",", ".") }}</td>
                          <!-- <td>{{ number_format($value->pbbkb_dasar, 3, ",", ".") }}</td>  -->
                          <td>{{ number_format($value->pbbkb_jual, 3, ",", ".") }}</td>                       
                          <td>{{ number_format($value->jumlah, 0, ",", ".") }}</td>
                          <td>{{ number_format($value->harga_jual*$value->qty, 0, ",", ".") }}</td>
                        </tr>
                        @endif 
                        <?php
                          $counter++;
                        ?>
                      @endforeach
                      
                    </tr>
                     
                    @else
                    <tr>
                      <td>{{ date('d M Y', strtotime($val->tanggal)) }}</td>
                      <td>{{ $val->nama_perusahaan }}</td>
                      <td>{{ $val->lokasi_perusahaan }}</td>
                      <td>{{ $val->nama_supplier }}</td> 
                      <td>{{ $val->no_po }}</td> 
                      <td>{{ $val->no_invoice }}</td> 
                      @foreach($items as $value)
                      <td>{{ $value->produk }}</td>
                      <td>{{ $value->jenis_bbm }}</td>
                      <td>{{ number_format($value->qty, 0, ",", ".") }}</td>
                      <td>{{ number_format($value->harga_jual, 0, ",", ".")}}</td> 
                      <td>{{ number_format($value->harga_dasar, 2, ",", ".")}}</td> 
                      <td>{{ number_format($value->dpp, 0, ",", ".") }}</td>
                      <td>{{ number_format($val->ppn_total, 0, ",", ".") }}</td>
                      <!-- <td>{{ number_format($value->pbbkb_dasar, 3, ",", ".") }}</td> --> 
                      <td>{{ number_format($value->pbbkb_jual, 3, ",", ".") }}</td>                       
                      <td>{{ number_format($value->jumlah, 0, ",", ".") }}</td>
                      <td>{{ number_format($value->harga_jual*$value->qty, 0, ",", ".") }}</td>
                      @endforeach
                       <th> 
                        @if($val->status == 1)
                          Lunas
                        @elseif($val->status == -1)
                          Rejected
                        @else
                          Belum Lunas
                        @endif 
                      </th>
                      <td>  
                        <a href="{{ url('penjualan/detail-penjualan/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-server"></i> Detail </a> 
                        <a href="{{ url('penjualan/edit/'.$val->id) }}" class="btn btn-success btn-xs">
                          <i class="fa fa-pencil"></i> Edit</a>
                          <!--  <a href="{{ url('penjualan/add-item/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Edit Item</a>   -->
                        <a class="btn btn-danger btn-xs" href="{{ url('penjualan/'.$val->id) }}" onclick="event.preventDefault();if(confirm('Hapus Data penjualan ?')){ $('#form-{{ $val->id }}').submit() }"><i class="fa fa-trash"></i> Hapus </a>
                        <form action="{{ url('penjualan/delete/'.$val->id) }}" method="post" id="form-{{ $val->id }}">
                          {{ csrf_field() }}
                          <input type="hidden" name="_method" value="delete">
                        </form>
                      </td>
                    </tr>
                    @endif
                  @endforeach 
                    <tr style="background: #eee;text-align: center">
                      <td colspan="8"><b>TOTAL ALL</b></td>
                      <td><b>{{ number_format($total_all_qty, 0, ",", ".") }}</b></td>
                      <td><b>{{ (int)$total_all_harga_jual }}</b></td>
                      <td><b>{{ number_format($total_all_harga_dasar, 2, ",", ".") }}</b></td>
                      <td><b>{{ number_format($total_all_dpp, 0, ",", ".") }}</b></td>
                      <td><b>{{ number_format($total_all_ppn_total, 0, ",", ".") }}</b></td>
                      <!-- <td><b>{{ number_format($total_all_pbbkb_dasar, 3, ",", ".") }}</b></td> -->
                      <td><b>{{ number_format($total_all_pbbkb_jual, 0, ",", ".") }}</b></td>
                      <td><b>{{ number_format($total_all_jumlah, 0, ",", ".") }}</b></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                </tbody>
                 
              </table>
              </div>
              {{ $penjualan->appends(['nama_perusahaan' => $nama_perusahaan,'nama_supplier' => $nama_supplier,'no_po' => $no_po,'no_invoice' => $no_invoice,'status' => $status,])->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('javascript')
<script>
  function exportExcel(){
    var form = $("#form-filter");
    form.attr("action","{{ url('penjualan/export') }}")
    form.submit();
  }
  function filter(){
    var form = $("#form-filter");
    form.attr("action","#")
    form.submit();
  }
</script>
@endsection