<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"> 

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Invoice</title>

  <!-- Bootstrap 3.3.6 --> 
  <link rel="stylesheet" href="{{ url('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('dist/css/AdminLTE.min.css') }}">
  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: A4 potrait }</style>

</head>
<body class="A4 potrait" onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="sheet padding-0mm" style="margin:auto;">
    <!-- title row -->
    <div class="row">
       <div class=" col-sm-12 box-header with-border" style="text-align:center;"> 
          <h2 style="text-decoration:underline;">Formulir Kwitansi Penjualan</h2><br>
            <p style="margin-top: -30px; font-size: 17px;">Nomor : {{ $penjualan->no_po }}</p>
          <div class=" col-xs-4" style="border-bottom: 1.3px dashed #000; margin-left:275px;"></div>
        </div>
          <div class="box-body" style="margin-left: 80px; word-spacing: 1px;"> 
            <div class="row">
              <div class="col-md-8">              
                 <div class="invoice-detail" style="font-size: 13px; margin-top:40px;">             
                    <table width="600" style="text-align:center;" >
                      <tr>
                        <td style="width:40px;">1</td>
                        <td style=" text-align: left;">No Invoice</td>
                        <td style="width:40px;"> :</td>
                        <td style="width:347px; border-bottom: 1.3px dashed #000; text-align:left;">{{ $penjualan->no_invoice }}</td>
                      </tr>
                        <td>2</td>
                        <td style="width: 150px; text-align: left;">No Faktur Pajak</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $penjualan->no_faktur_pajak }}</td>
                      <tr>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td style="width: 150px; text-align: left;">Nama Perusahaan</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $penjualan->nama_perusahaan }}</td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td style="width: 150px; text-align: left;">Alamat Perusahaan</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $penjualan->lokasi_perusahaan }}</td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td style="width: 150px; text-align: left;">Nama Supplier</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $penjualan->nama_supplier }}</td>
                      </tr>
                      <tr>
                        <td>6</td>
                        <td style="width: 150px; text-align: left;">Produk</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; ">{{ $penjualan->produk }}</td>
                      </tr>
                      <tr>
                        <td>7</td>
                        <td style="width: 150px; text-align: left;">Qty</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; word-spacing: 5px;">{{ $penjualan->qty }} Liter</td>
                      </tr>
                      <tr>
                        <tr>
                        <td>8</td>
                        <td style="width: 150px; text-align: left;">Harga Beli</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; word-spacing: 120px;">Rp. {{ (int)$penjualan->harga_beli }}</td>
                      </tr>
                      <tr>
                        <td>9</td>
                        <td style="width: 150px; text-align: left;">Harga Jual </td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; word-spacing: 120px;"> Rp. {{ (int)$penjualan->harga_jual }}</td>
                      </tr>
                      <tr>
                        <td>10</td>
                        <td style="width: 150px; text-align: left;">PBBKB</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $penjualan->pbbkb }}</td>
                      </tr>
                      <tr>
                        <td>11</td>
                        <td style="width: 150px; text-align: left;">PBBKB Dasar</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $penjualan->pbbkb_dasar }}</td>
                      </tr>
                      <tr>
                        <td>12</td>
                        <td style="width: 150px; text-align: left;">PBBKB Jual </td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $penjualan->pbbkb_jual }}</td>
                      </tr>
                      <tr>
                        <td>13</td>
                        <td style="width: 150px; text-align:left;">PPN</td>
                        <td> :</td>
                        <td style="text-align: center; letter-spacing: 2px; border-bottom: 1.3px dashed #000;">{{ $penjualan->ppn }}</td>
                      </tr>
                      <tr>
                        <td>14</td>
                        <td style="width: 150px; text-align: left;">PPN Total</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; word-spacing: 120px;">Rp. {{ (int) $penjualan->ppn_total }}</td>
                      </tr>
                      <tr>
                        <td>15</td>
                        <td style="width: 150px; text-align: left;">DPP</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; word-spacing: 120px;">Rp. {{ (int)$penjualan->dpp }}</td>
                      </tr>
                        <tr>
                        <td>16</td>
                        <td style="width: 150px; text-align: left; text-align:left;">Keterangan</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;"> {{  $penjualan->keterangan }}</td>
                      </tr>                                       
                    </table>                         
                </div>     
              </div>              
            </div>
              <div class="row">
                <div class="col-xs-12">
                  <table border="0" style="width: 100%; margin-top:35px;" >
                    <tr>
                      <td style="width: 2%"></td>
                      <td style="width: 25%">
                        <p></p>
                        <strong>Prepared By,</strong><br>
                        <br><br><br><br><br><br>
                        {{ strtoupper($penjualan->nama_supplier) }}<br>
                      </td>
                      <td style="width: 15%"></td>
                      <td style="width: 20%">
                        <strong>Approved By,</strong><br>
                        <p>Jakarta, {{ date ('d M Y') }}</p><br>
                        <br><br><br><br><br>
                        <p>( Penanggung Jawab )</p><br>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
        </section>
    </div>
  </body>
</html>
