
<!DOCTYPE html>
<html>
<head>
  <link rel="shortcut icon" href="{{ url( 'dist/img/logoFR.png') }}"> 
  <meta charset="utf-8"> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Financial Reports  | Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 --> 
  <link rel="stylesheet" href="{{ url('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('dist/css/AdminLTE.min.css') }}">
   <style> 
        body{
                margin:auto;  
                font-size: 12px !important; 
              }  
               page[size="A4"] {
                background: white;
                width: 21cm;
                height: 29.7cm;
                display: block;
                margin: 0 auto;
                margin-bottom: 0.5cm;
                box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
              }
               .kop_surat {
                  position: absolute;
                  width: 21cm;  
                  margin-top: -45px;
                }
              .invoice {
                  position: relative;
                   background: none; 
                  border: 0px solid #f4f4f4;
              }
              @media print {
                body, page[size="A4"] {
                  margin: 0;
                  box-shadow: 0;
                }
          }
      </style>
  </head>
<body >
<?php 
 $image ="";
  if($company->kop_surat){
    $image = unserialize($company->kop_surat);
    if($image){
      $image = url('dist/img/')."/".$image['original'];
    }
  }
?>                    
  <page size="A4" layout="portrait">
    <img src='{{ $image }}' class="kop_surat">
<body onload="window.print();">
<div class="wrapper">
    <!-- Main content -->
    <section class="invoice" >
      <div class="row" style="margin-top: 155px; font-size: 12px;">
        <div class="col-xs-12">
          <table border="0" style="width:100%">
            <tr>
              <td colspan="3" align="center">
                <font size="5">Order Penjualan</font><br>
                (Purchase Order)
              </td>
              <br>
            </tr>
            <tr>
              <td style="width: 30%">
                <b>Kepada (Order To)</b><br>
                {{ $po_masuk->po_tujuan }}<br>
                Telp/Fax : {{ $po_masuk->telp_po_tujuan }}<br>
                Up : {{ $po_masuk->pic_po_tujuan }}
              </td>
              <td style="width: 25%"></td>
              <td style="width: 45%">
                Nomor Invoice : {{ $penjualan->no_invoice }}<br>
                Nomor PO (Order No) : {{ $penjualan->no_po }}<br>
                Tanggal : {{ date('d F Y', strtotime($penjualan->tanggal)) }}<br>
                Tanggal Pengaliran : {{ date('d F Y', strtotime($penjualan->tanggal_pengaliran)) }}<br>
                Mata Uang : IDR<br>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr> 
              <td style="width: 30%">
                <b>Delivery Office</b><br>
               {{ $penjualan->lokasi_perusahaan }}<br>
              </td>
              <td style="width: 40%"></td>
              <td style="width: 30%">
                Contact Person 1 : {{ $po_masuk->contact_person_1 }}<br>
                Contact Person 2 : {{ $po_masuk->contact_person_2 }}<br>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row" style="margin-top: 20px;">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped table-bordered">
            <thead>
            <tr>
              <th style="width:5%; border:1px solid black;">No</th>
              <th style="width:22%; border:1px solid black;">Description</th>            
              <th style="width:8%; border:1px solid black;">Quantity</th>
              <th style="width:8%; border:1px solid black;">Harga Jual</th>
              <th style="width:8%; border:1px solid black;">DPP</th>
              <th style="width:8%; border:1px solid black;">PPN</th>
              <!-- <th style="width:8%; border:1px solid black;">PBBKB Dasar</th> -->
              <th style="width:8%; border:1px solid black;">PBBKB Jual</th>
              <th style="width:8%; border:1px solid black; text-align:center;">Amount</th>
            </tr>
            </thead>
            <tbody> 
              <?php 
                $total_amount = 0;
                $counter = 1;
              ?>
              @foreach($items as $val)
              <tr> 
                <td style="border:1px solid black;">{{ $counter }}</td>          
                <td style="border:1px solid black;">{{ $val->produk }}</td>
                <td style="text-align:right; border:1px solid black;">{{ number_format($val->qty, 0, ",", ".") }}</td>
                <td style="text-align:right; border:1px solid black;">{{ number_format($val->harga_jual, 6, ",", ".") }}</td>
                <!-- <td style="text-align:right; border:1px solid black;">{{ number_format($val->harga_dasar, 2, ",", ".") }}</td> -->
                
                <td style="text-align:right; border:1px solid black;">{{ number_format($val->dpp, 0, ",", ".") }}</td>
                <td style="text-align:right; border:1px solid black;">{{ number_format($penjualan->ppn_total, 0, ",", ".") }}</td>
                <td style="text-align:right; border:1px solid black;">{{ number_format($val->pbbkb_jual, 2, ",", ".") }}</td>
                <td style="text-align:right; border:1px solid black;">{{ number_format($val->jumlah, 0, ",", ".") }}</td>
              </tr>
             <?php 
                $counter++;
                $total_amount += ceil($val->jumlah);
              ?>
              @endforeach
              <?php
                $ppn = $total_amount*((int)$po_masuk->ppn/100);
                $pph = 0;
                if($po_masuk->pph != 0.000){
                  $pph = $total_amount*($po_masuk->pph/100);
                }
                $grandTotal = $total_amount + (int)$pph + (int)$po_masuk->add_cost;
                $grandTotal = ceil($grandTotal); 
              ?>
              <tr> 
                <td style="border:1px solid black;" rowspan="1" colspan="6" >
                  <p>Payment Term : {{ $po_masuk->payment_term }}</p>
                </td>
                <th style="border:1px solid black;">TOTAL:</th>
                <td style="text-align:right; border:1px solid black;">{{ number_format( $grandTotal, 0, ",", ".") }}</td>
              </tr>
              <tr>               
              </tr>
              <tr>
                <td style="border:1px solid black;" rowspan="1" colspan="9" >
                  <p>Terbilang :</p>
                  <p>{{ terbilang($grandTotal, 1) }} RUPIAH</p>
                </td>
                <!-- <th>TOTAL</th>
                <td style="text-align:right;">{{ number_format($penjualan->jumlah, 2, ",", ".") }}</td> -->
              </tr>
              <tr>
                <td style="border:1px solid black;" colspan="10">Note : {{ $po_masuk->keterangan }}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

       <div class="row">
          @if($company->id ==1)
          <div class="col-xs-12">
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
              Keterangan :<br>
              - Pembayaran mohon ditransfer atau Cek/bilyet Giro atas nama : <br> PT. AUDRI LUTFIA JAYA No. Rek. 414.1600.111 BCA KCP. Kramat Jaya Tj. Priok. <br>
              - Pembayaran dianggap sah bila Cek/bilyet Giro yang diserahkan telah selesai di kliring/cair.  <br>
            </p>
          </div>
           @else($company->id ==2)
           <div class="col-xs-12">
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
              Keterangan :<br>
              - Pembayaran mohon ditransfer atau Cek/bilyet Giro atas nama : PT. SUMA ADI JAYA No. Rek. Mandiri 125.007.8138. 138 <br>
              - Pembayaran dianggap sah bila Cek/bilyet Giro yang diserahkan telah selesai di kliring/cair.
            </p>
          </div>
          @endif  
        </div> 
       <div class="row" align="center">
        @if($company->id ==1) 
        <div class="footer" style="    margin-top: 220px;" >
         <p style="color:#176db9 !important;">Jl. Turi Jaya Rt. 08/07 No. 34 Ds. Segaramakmur Kec. Tarumajaya - Bekasi 17211<br> tlp. (021) 88992082 - 88992083 Fax. (021) 443830  <br> <strong style="color:red !important;">E-mail : audrilutfiajaya@ymail.com </strong></p>
        </div>
        @else($company->id ==2)
        <div class="footer" style="    margin-top: 245px;" >
          <p style="color:#176db9 !important;">Jl. Turi Jaya Rt.10/07 Ds. Segaramakmur Kec. Tarumajaya - Bekasi <br> Telp. (021) 88992082 <strong style="color:red !important;">Email.Sumaadijaya.com</strong></p>
        </div>
        @endif
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
</body>
</html>

