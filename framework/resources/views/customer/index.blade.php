@extends('layouts.app')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{('bower_components/datatables.net-bs/css/dataTables.bootstrap.css')}}">
@endsection
  
@section('content') 
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> 
        Master
        <small>customer</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data customer</li>
      </ol>
    </section>

    <!-- Main content -->
      <!-- Main content --> 
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
              <button class="btn btn-primary btn-md pull-right" onclick="window.location='<?php echo url('customer/create'); ?>'">
                <i class="fa fa-plus"></i> Tambah
              </button>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="datatable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Nama</th>
                  <th>Jenis Usaha</th>
                  <th>No Telp</th>
                  <!-- <th>Npwp</th> -->
                  <th>Nama Pic </th> 
                  <th>Email Pic</th>
                  <th>Telp Pic </th>
                  <th>Jenis Customer</th>
                  <th>Created Date</th>
                  <th style="width: 92px;"><i class="fa fa-ellipsis-h"></i></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('javascript') 
<!-- DataTables -->
<script src="{{('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
  var table = $('#datatable').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url":"{{ url('customer/fetchData') }}", 
        "type":"GET",
        "data": function (d){
            // d.filter_name = $('#filter_name').val();
            // d.filter_jenis_usaha = $('#filter_jenis_usaha').val();
            // d.filter_phone_number = $('#filter_phone_number').val();
            // d.filter_npwp = $('#filter_npwp').val();            
            // d.filter_pic_name = $('#filter_pic_name').val();
            // d.filter_pic_email = $('#filter_pic_email').val();
            // d.filter_pic_phone = $('#filter_pic_phone').val();
            // d.filter_jenis_customer = $('#filter_jenis_customer').val();
            }
        },
    "columns": [
          {"data": "name"},
          {"data": "jenis_usaha"},
          {"data": "phone_number"},
          {"data": "pic_name"},
          {"data": "pic_email"},
          {"data": "pic_phone"},
          {"data": "jenis_customer"},
          {"data": "created_at"},
          {"data": "action"}
      ],
      "columnDefs": { "targets": 8, "orderable": false},
      "order": [[0, 'asc']],
      "language": {
          "lengthMenu": '_MENU_ entries per page',
          "search": '<i class="fa fa-search"></i>',
          "paginate": {
              "previous": '<i class="fa fa-angle-left"></i>',
              "next": '<i class="fa fa-angle-right"></i>'
          }
      }
  });

  // $(".export-btn").html("<button class='btn btn-default pull-right' type='button' onclick='exportExcel()'>Export Excel</button>");

  function reloadData(){
    table.ajax.reload();
  }

  function deleteData(id){
    event.preventDefault();
    if(confirm('Hapus Customer ?')){ 
      $('#form-'+id).submit() 
    }
  }

  function exportExcel(){
    var form = $("#form-filter");
    form.submit();
  }
</script>
@endsection