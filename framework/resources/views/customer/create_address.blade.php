@extends('layouts.app')
@section('css')
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
<!-- DataTables -->
<link rel="stylesheet" href="{{ url('plugins/datatables/dataTables.bootstrap.css') }}">
@endsection
   
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah Alamat
        <small>"{{($customer->name)}}"</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tambah</a></li>
        <li class="active">Alamat</li>
      </ol>
    </section>
    <!-- Main content --> 
    <section class="invoice">
      <!-- Table row --> 
      <div class="row" style="">
        <div class="col-xs-12 table-responsive">
          @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif 
          <form action="{{ url('customer/add-address') }}" method="post">
            {{ csrf_field() }} 
            <input type="hidden" value="{{ $customer->id }}" name="id_customer">
            <table class="table table-striped">
              <thead>
              <tr>
                <th style="width:3%;"><i class="fa fa-ellipsis-v"></i></th>
                <th style="width:11%;">Kode Alamat</th>
                <th style="width:22%;">Alamat</th>
                <th style="width:11%;">NPWP</th>
              </tr>
              </thead>
              <tbody id="item-body">
                @if(count($add_address) > 0)
                  @foreach($add_address as $val)
                     <tr>
                      <td onclick="deleteAddressField(this)"><i class="fa fa-trash"></i></td>
                      <td><input type="text" name="address_code[]" id="address_code" value="{{ $val->address_code }}" class="form-control"></td>
                      <td><input type="text" name="address[]" id="address" value="{{ $val->address }}" class="form-control"></td>
                       <td><input type="text" name="npwp[]" id="npwp" value="{{ $val->npwp }}" class="form-control"></td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
            <div style="text-align:right;">
              <button class="btn btn-success" type="button" onclick="addAddressField()">Tambah Alamat Baru</button>
              <button class="btn btn-info" type="submit">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </section>
  </div>
@endsection
@section('javascript')
<!-- DataTables -->
<script src="{{ url('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });

  function addAddressField(){
    var body = jQuery("#item-body");
    var html = "<tr>";
    html += '<td onclick="deleteAddressField(this)"><i class="fa fa-trash"></i></td>';
    html += '<td><input type="text" name="address_code[]" id="address_code" class="form-control"></td>';
    html += '<td><input type="text" name="address[]" id="address" class="form-control"></td>';
    html += '<td><input type="text" name="npwp[]" id="npwp" class="form-control"></td>';
    html += "</tr>";           
    body.append(html);  
  }
  function deleteAddressField(elem){
    $(elem).parent().remove();
  }
</script>
@endsection