@extends('layouts.app')
@section('css')
   <!-- Select2 -->
  <link rel="stylesheet" href="{{ url('plugins/select2/select2.min.css') }}">
@endsection 
   
@section('content') 
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>  
        Form
        <small>Pembelian Baru</small>
      </h1> 
  </section>
  <!-- Main content --> 
  <section class="content">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12">
         @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger"> 
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border"> 
            <h3 class="box-title">Form</h3>
          </div>
          <!-- /.box-header --> 
         
              <form role="form" action="{{ url('pembelian-agen') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- text input -->
                <div class="box-body">
                  <div class="row">
                <div class="col-md-12">
                  <center>
                    <label>Nomor PO</label><br>
                    <select class="select2" name="filter_po_keluar" id="filter_po_keluar" onchange="openForm(this)" style="width: 200px" required>
                          <option value="">-- Pilih PO --</option>
                          @foreach($list_po_keluar as $val)
                          @if(isset($po_keluar_agen->nomor_po) and $po_keluar_agen->nomor_po == $val)
                          <option value="{{ $val }}" selected>{{ $val}}</option>
                          @else
                          <option value="{{ $val }}">{{ $val}}</option>
                          @endif
                          
                          @endforeach
                        </select> 
                  </center>
                </div>
              </div> <br>
             
              @if(isset($po_keluar_agen))
                  <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid #eee; ">
                      <div class="form-group">
                        <label for="no_invoice">No Invoice</label>
                        <input type="text" class="form-control" id="no_invoice" name="no_invoice" placeholder="Masukkan Nomor Invoice" required>
                      </div>
                      <div class="form-group">
                        <label for="no_faktur_pajak">No Faktur Pajak</label>
                        <input type="text" class="form-control" id="no_faktur_pajak" name="no_faktur_pajak" placeholder="Masukkan Nomor Faktur Pajak" required>
                      </div>
                      <div class="form-group">
                        <label for="no_po">Nomor Po</label>
                        <input type="text" class="form-control" id="no_po" name="no_po" value="{{ $po_keluar_agen->nomor_po }}" readonly  required>
                      </div>
                      <div class="form-group">
                        <label for="tanggal">Tanggal</label>
                        <input type="date" class="form-control" id="tanggal" name="tanggal" value="{{ $po_keluar_agen->tanggal }}" readonly  required>
                      </div>
                         
                      <div class="form-group">
                        <label for="nama_agen">Nama Agen *</label>
                        <input type="text" class="form-control" id="nama_agen" name="nama_agen" value="{{ $po_keluar_agen->nama_agen }}" readonly required>
                      </div>
                      <div class="form-group"> 
                        <label for="alamat_agen">Alamat Agen *</label>
                        <input type="text" class="form-control" id="alamat_agen" name="alamat_agen" value="{{ $po_keluar_agen->alamat_agen }}" readonly required>
                      </div> 
                      <div class="form-group">
                        <label for="nama_supplier">Nama Company * </label>
                        <input type="text" class="form-control" id="nama_supplier" name="nama_supplier" value="{{ $po_keluar_agen->penyalur }}" readonly required>
                      </div>
                     <div class="form-group">
                        <label for="alamat_kirim">Alamat Kirim * </label>
                        <input type="text" class="form-control" id="alamat_kirim" name="alamat_kirim" value="{{ $po_keluar_agen->alamat_kirim }}" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6"> 
                       <div class="form-group">
                        <label for="produk">Jenis BBM *</label>
                       <input type="text" class="form-control" id="produk" name="produk" value="{{ $po_keluar_agen->jenis_bbm }}" readonly required>
                      </div> 
                      <div class="form-group">
                        <label for="qty">QTY *</label>
                         <input type="number" class="form-control" name="qty" id="qty" value="{{ $po_keluar_agen->qty }}" readonly required></input>
                      </div> 
                      <div class="form-group">
                        <label for="harga_beli">Harga Beli *</label>
                         <input type="number" class="form-control" name="harga_beli" id="harga_beli"  value="{{ $po_keluar_agen->harga_beli }}" readonly required></input>
                      </div>
                      <div class="form-group">
                        <label for="harga_jual">Harga Jual *</label>
                         <input type="number" class="form-control" name="harga_jual" id="harga_jual" value="{{ $po_keluar_agen->harga_jual }}" readonly required></input>
                      </div>
                      <div class="form-group">
                        <label for="harga_dasar">Harga Dasar *</label>
                         <input type="number" class="form-control" name="harga_dasar" id="harga_dasar" value="{{ $po_keluar_agen->harga_dasar }}" readonly required></input>
                      </div>
                      <div class="form-group">
                        <label for="pbbkb">PBBKB</label>
                        <input type="hidden" name="pbbkb" value="{{ $po_keluar_agen->pbbkb }}" hidden>
                        <input type="text" class="form-control" name="pbbkb2" id="pbbkb2" value="@if($po_keluar_agen->pbbkb==NULL) @endif {{App\Utilities\Constant::$PBBKB['NULL']}}" readonly required></input>
                      </div>  
                      <div class="form-group">
                        <label for="ppn">PPN</label>
                         <input type="number" class="form-control" name="ppn" id="ppn" value="{{ $po_keluar_agen->ppn }}" readonly></input>
                      </div>
                      <!-- <div class="form-group">
                        <label for="notes">Note</label>
                         <input type="text" class="form-control" name="notes" id="note" value="{{ $po_keluar_agen->notes }}" readonly></input>
                      </div> -->
                    </div>
                  </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
                @endif
              </form> 
            </div> 
          </div> 
        </section>
      </div>
@endsection 
@section('javascript')
<!-- Select2 -->
<script src="{{ url('plugins/select2/select2.full.min.js')}}"></script>
<script>
  $(".select2").select2();

  function openForm(elem){
    var data = $(elem).val();
    window.location = "{{ url('/pembelian-agen/create?filter_po_keluar=') }}"+data;
  }
   function fetchDataAddress(elem){
    var customer = $(elem).val();

    $.ajax({
      url:"{{ url('customer/getAddress') }}?customer_id="+customer,
      type:"get",
      dataType:"json", 
      beforeSend:function(){

      },
      success:function(result){
        if(result.status = "ok"){
          var element = $("#lokasi_perusahaan");
          var html = "<option value=''>Pilih Alamat</option>";

          $.each(result.data, function(index, value){
            html += "<option value='"+value.id+"'>"+value.address+"</option>";
          });

          element.html(html);
        }else{
          var html = "<option value=''>Silahkan Pilih Konsumen</option>";
          element.html(html);
        }
      },
      error:function(){

      },
      complete:function(){

      }
    });
  }
</script>
@endsection
