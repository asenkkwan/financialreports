@extends('layouts.app')
 
@section('css')
    <!-- DataTables -->
  <link rel="stylesheet" href="{{url('plugins/datatables/dataTables.bootstrap.css')}}">
@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
        Form
        <small>Tambah Perusahaan</small>
      </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a>
      </li>
      <li><a href="#">Forms</a>
      </li>
      <li class="active">Tambah Perusahaan</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
         @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Form</h3>
          </div>
          <!-- /.box-header -->
         
              <form role="form" action="{{ url('company/store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- text input -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid #eee; ">
                      <div class="form-group">
                        <label>Nama *</label>
                          <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan Nama" value="{{ old('name') }}" required>
                      </div>
                      <div class="form-group">
                        <label for="phone_number">Nomor Telepon *</label>
                          <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Masukkan Nomor Telepon"  required>
                      </div>
                      <div class="form-group">
                        <label for="fax">Fax *</label>
                          <input type="text" class="form-control" id="fax" name="fax" placeholder="Masukkan Nomor fax"  required>
                      </div>
                      <div class="form-group">
                        <label for="npwp">NPWP *</label>
                          <input type="text" class="form-control" id="npwp" name="npwp" placeholder="Masukkan Nomor NPWP"  required>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>No SKP MIGAS *</label>
                          <input type="text" class="form-control" id="no_skp_migas" name="no_skp_migas" placeholder="Masukkan Nomor SKP MIGAS" required>
                      </div>
                      <div class="form-group">
                        <label>No Sold to Penyalur *</label>
                          <input type="text" class="form-control" id="no_sold_to_penyalur"  name="no_sold_to_penyalur" placeholder="Masukkan No. Sold to Penyalur" required>
                      </div>
                       <div class="form-group">
                          <label>Alamat *</label>
                            <textarea class="form-control" name="address" id="address" placeholder="Masukkan Alamat"></textarea>
                        </div>
                      <div class="form-group">
                        <label for="kop_surat">Photo</label>
                        <input type="file" name="kop_surat" id="kop_surat">
                      </div>
                    </div>
                  </div>
                <div class="box-footer">
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </form> 
            </div> 
          </div> 
        </section>
      </div>

@endsection 
@section('javascript')
<!-- DataTables -->
<script src="{{('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
          $('#datatable').DataTable({
            "columnDefs": [{ "orderable": false, "targets": 6 }]
          });
      
        });
</script>
@endsection