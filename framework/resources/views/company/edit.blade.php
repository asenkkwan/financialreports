@extends('layouts.app')
 
@section('css')

@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form
        <small>Edit Perusahaan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Edit Perusahaan</li>
      </ol>
    </section>

        <!-- Main content -->
       <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ url('company/update/'.$company->id) }}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="_method" value="put">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6"  style="border-right: 1px solid #eee; ">
                    <div class="form-group">
                      <label for="name">Nama</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan Nama" value="{{ $company->name }}" required>
                    </div>
                    <div class="form-group">
                      <label for="phone_number">No. Telpon</label>
                        <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Masukkan No Telpon" value="{{ $company->phone_number }}">
                    </div>                  
                    <div class="form-group">
                      <label for="fax">FAX</label>
                        <input type="text" class="form-control" id="fax" name="fax" placeholder="Masukkan Nomor FAX" value="{{ $company->fax }}">
                    </div>
                    <div class="form-group">
                      <label for="npwp">NPWP</label>
                        <input type="text" class="form-control" id="npwp" name="npwp" placeholder="Masukkan No Telpon" value="{{ $company->npwp }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="no_skp_migas">No SKP MIGAS</label>
                        <input type="text" class="form-control" id="no_skp_migas" name="no_skp_migas" placeholder="Masukkan No Telpon" value="{{ $company->no_skp_migas }}">
                    </div>
                    <div class="form-group">
                      <label for="no_sold_to_penyalur">No Sold to Penyalur</label>
                        <input type="text" class="form-control" id="no_sold_to_penyalur" name="no_sold_to_penyalur" placeholder="Masukkan No Sold to Penyalur" value="{{ $company->no_sold_to_penyalur }}">
                    </div> 
                    <div class="form-group">
                      <label for="address">Alamat</label>
                        <textarea class="form-control" name="address" id="address">{{ $company->address }}</textarea>
                    </div> 
                    <?php
                        if($company->kop_surat){
                          $image = unserialize($company->kop_surat);
                          if($image){
                            $image = url('dist/img/')."/".$image['original'];
                          }else{
                            $image = url('/')."/dist/img/avatar04.png";
                          }
                        }else{
                          $image = url('/')."/dist/img/avatar04.png";
                        }
                      ?>
                      
                      <div class="form-group form_photo">
                        <label for="kop_surat">Photo</label>
                        <input type="file" name="kop_surat" id="kop_surat">
                      </div> 
                      <div class="form-group">
                        <img src="{{ $image }}" style="width: 60%;" id="kop_surat">
                      </div>
                      <!-- <div class="form-group">
                        <button class="btn btn-success btn-md" onclick="toggleFormPhoto()" id="btnGantiPhoto" type="button">Ganti Photo</button>
                        <button class="btn btn-warning btn-md" onclick="toggleFormPhoto()" id="btnCancelGantiPhoto" type="button">Batal</button>
                      </div>   -->           
                  </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">Simpan</button>
              </div>
            </form>
          </div>
      </div>
    </section>
  </div>
@endsection
@section('javascript')

@endsection