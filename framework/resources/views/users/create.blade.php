@extends('layouts.app')
 
@section('css')
    <!-- DataTables -->
  <link rel="stylesheet" href="{{url('plugins/datatables/dataTables.bootstrap.css')}}">
@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
        Form
        <small>Tambah User</small>
      </h1> 
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a>
      </li>
      <li><a href="#">Forms</a>
      </li>
      <li class="active">Tambah Users</li>
    </ol>
  </section>
  <!-- Main content -->  
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
         @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
              </div>
                 <form role="form" action="{{ url('user/store') }}" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <!-- text input -->
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-6" style="border-right: 1px solid #eee; ">
                        <div class="form-group">
                          <label>Nama *</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan Nama" value="{{ old('name') }}" required>
                        </div>
                        <div class="form-group">
                          <label>No. Telepon *</label>
                            <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Masukkan Nomor Telepon" value="{{ old('phone_number') }}" required>
                        </div>
                         <div class="form-group">
                          <label>Email *</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="Masukkan Email" required>
                        </div>
                        <div class="form-group">
                          <label for="role">Kantor Cabang *</label>
                            <select class="form-control" name="company_id" id="company_id" required>
                              <option value="">-- Pilih Kantor Cabang --</option>
                              @foreach($kantor_cabang as $val)
                              <option value="{{ $val->id }}">{{ $val->name }}</option>
                              @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                          <label>Alamat *</label>
                            <textarea class="form-control" name="address" id="address"value="{{ old('address') }}" required></textarea>
                        </div>
                      </div>
                      <!-- select -->
                      <div class="col-md-6">
                        @if(Auth::user()->role == "superadmin")
                        <div class="form-group">
                          <label for="role">Role *</label>
                            <select class="form-control" name="role" id="role" required>
                              <option value="admin">Admin</option>
                              <option value="superadmin">Super Admin</option>
                          </select>
                        </div> 
                        @else
                        <div class="form-group">
                          <label for="role">Role *</label>
                            <select class="form-control" name="role" id="role" required>
                              <option value="admin">Admin</option>
                          </select>
                        </div>
                        @endif
                        @if(Auth::user()->role == "superadmin")
                        <div class="form-group">
                          <label for="role">Enable Approval *</label>
                            <select class="form-control" name="enable_approval" id="enable_approval" required>
                              <option value="no">No</option>
                              <option value="yes">Yes</option>
                          </select>
                        </div> 
                        @endif
                         <div class="form-group">
                          <label for="username">Username *</label>
                            <input type="text" class="form-control" id="username" placeholder="Masukkan Username" name="username" required>
                        </div>
                        <div class="form-group">
                          <label for="password">Password *</label>
                            <input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
                        </div>
                        <div class="form-group">
                          <label for="konfirmasi_password">Konfirmasi Password *</label>
                            <input type="password" class="form-control" id="password_confirmation" placeholder="Konfirmasi Password" name="password_confirmation" required>
                        </div>
                      </div>
                    </div>          
                  <!-- /.box-body -->
                      <div class="box-footer">
                        <button type="submit" class="btn btn-success pull-right">Simpan</button>
                      </div>
                </form> 
              
            
  </section>
</div>    
@endsection 
@section('javascript')
<!-- DataTables -->
<script src="{{('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
$(function () {
        $('#datatable').DataTable({
          "columnDefs": [{ "orderable": false, "targets": 6 }]
        });
    
      });
</script>
@endsection