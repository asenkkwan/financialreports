@extends('layouts.app')
 
@section('css')
    <!-- DataTables -->
  <link rel="stylesheet" href="{{('bower_components/datatables.net-bs/css/dataTables.bootstrap.css')}}">
@endsection
   
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master
        <small>user</small> 
      </h1> 
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Users</a></li>
        <li class="active">Data user</li>
      </ol>
    </section>

    <!-- Main content -->
   <section class="content">
      <div class="row">
        <div class="col-xs-12">
          @if (Session::has('message')) 
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
              @if(Auth::user()->role == 'superadmin')
              <div style="float: right">
                <button class="btn btn-primary btn-xs" onclick="window.location='<?php echo url('user/create'); ?>'">
                  <i class="fa fa-plus"></i> Tambah
                </button>
              </div>
              @endif
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <table id="datatable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Nama</th>
                  <th>Username</th>
                  <th>No Telp</th>
                  <th>Alamat</th>
                  <th>Email</th>
                  <th>Role</th>
                  <th>Status</th>
                  <th><i class="fa fa-ellipsis-h"></i></th>
                </tr>
                </thead>
                <tbody>
                  @foreach($user as $val)
                  <tr>
                    <td>{{ $val->name }}</td> 
                     <td>{{ $val->username }}</td>              
                    <td>{{ $val->phone_number }}</td>
                    <td>{{ $val->address }}</td>
                    <td>{{ $val->email }}</td>
                    <td>{{ $val->role }}</td>
                    <td> 
                     @if($val->status)
                        <i class="fa fa-check-circle" style="color: green"></i>
                      @else
                        <i class="fa fa-minus" style="color: red;"></i>
                      @endif
                    </td>
                    <td>
                        @if(Auth::user()->id == $val->id)
                        <a href="{{ url('user/edit/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i>Edit</a>
                        @endif

                        @if(Auth::user()->role == 'superadmin')
                        <a href="{{ url('user/edit/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i>Edit</a>
                        <a class="btn btn-danger btn-xs" href="{{ url('user/delete/'.$val->id) }}" onclick="event.preventDefault();if(confirm('Hapus Pengguna ?')){ $('#form-{{ $val->id }}').submit() }"><i class="fa fa-trash"></i>Hapus</a>
                        <form action="{{ url('user/delete/'.$val->id) }}" method="post" id="form-{{ $val->id }}">
                          {{ csrf_field() }}
                          <input type="hidden" name="_method" value="delete">
                        </form>
                        @endif
                      
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('javascript')
<!-- DataTables -->
<script src="{{('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#datatable').DataTable({
      "columnDefs": [{ "orderable": false, "targets": 7 }]
    });
  });
</script>
@endsection