@extends('layouts.app')
 
@section('css')
    <!-- DataTables -->
  <link rel="stylesheet" href="{{('plugins/datatables/dataTables.bootstrap.css')}}">
@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form
        <small>Edit User</small>  
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Edit Users</li>
      </ol>
    </section>
 
        <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
          <!-- general form elements -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ url('user/update/'.$user->id) }}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="_method" value="put">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6"  style="border-right: 1px solid #eee; ">
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan Nama" value="{{ $user->name }}" required>
                    </div>
                    <div class="form-group">
                      <label for="phone">No. Telepon</label>
                      <input type="text" class="form-control" id="phone" name="phone_number" placeholder="Masukkan No Telpon" value="{{ $user->phone_number }}">
                    </div>     
                    <div class="form-group">
                      <label for="phone">Email</label>
                      <input type="text" class="form-control" id="email" name="email" placeholder="Masukkan Email" value="{{ $user->email }}">
                    </div>
                    <div class="form-group">
                      <label for="role">Kantor Cabang</label>
                       <select class="form-control" name="company_id" id="company_id">
                          
                          @foreach($kantor_cabang as $val)
                          <option selected="selected" value="{{ $val->id }}">{{ $val->name }}</option>
                          @endforeach
                        </select>
                    </div>
                     <div class="form-group">
                      <label for="alamat">Alamat</label>
                      <textarea class="form-control" name="address" id="address">{{ $user->address }}</textarea>
                    </div>
                  </div>
                  <div class="col-md-6">
                    @if(Auth::user()->role == "superadmin")
                    <div class="form-group">
                      <label for="role">Role</label>
                      <select class="form-control" name="role" id="role" required>
                        <option value="admin" @if($user->role == "admin") selected @endif>Admin</option>
                        <option value="superadmin" @if($user->role == "superadmin") selected @endif>Super Admin</option>  
                      </select>
                    </div>
                    @else
                    <div class="form-group">
                      <label for="role">Role</label>
                      <select class="form-control" name="role" id="role" required>
                        <option value="admin" @if($user->role == "admin") selected @endif>Admin</option>
                      </select>
                    </div>
                    @endif
                    @if(Auth::user()->role == "superadmin")
                    <div class="form-group">
                      <label for="role">Enable Approval *</label>
                        <select class="form-control" name="enable_approval" id="enable_approval" required>
                          <option value="no" @if($user->enable_approval == 0) selected @endif>No</option>
                          <option value="yes" @if($user->enable_approval == 1) selected @endif>Yes</option>
                      </select>
                    </div>
                    @endif
                     <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="username" placeholder="Masukkan Username" name="username" required value="{{ $user->username }}">
                      </div>
                      <div class="form-group form_password">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                      </div>
                      <div class="form-group form_password_confirmation">
                        <label for="konfirmasi_password">Konfirmasi Password</label>
                        <input type="password" class="form-control" id="password_confirmation" placeholder="Konfirmasi Password" name="password_confirmation">
                      </div>
                     <!--  <div class="form-group">
                        <button class="btn btn-success btn-md" id="btnGantiPassword" onclick="toggleGantiPass()" type="button">Ganti Password</button>
                        <button class="btn btn-warning btn-md" id="btnBatalGantiPassword" onclick="toggleGantiPass()" type="button">Batal</button>
                      </div> -->
                  </div>
                </div>

              
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.row -->
    </section>
        <!-- /.content -->
    </div>
@endsection
@section('javascript')
<!-- DataTables -->
<script src="{{('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script>
  $(function () {
    $('#datatable').DataTable({
      "columnDefs": [{ "orderable": false, "targets": 6 }]
    });

  });
</script>
@endsection