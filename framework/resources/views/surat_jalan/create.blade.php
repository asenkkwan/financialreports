@extends('layouts.app')
@section('css')
  <!-- Select2 -->
<link rel="stylesheet" href="{{ url('plugins/select2/select2.min.css') }}">
@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> 
        Form
        <small>Surat Jalan</small> 
      </h1>  
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a>
      </li>
      <li><a href="#">Forms</a>
      </li> 
      <li class="active">Tambah Surat Jalan Baru</li>
    </ol>
  </section>  
  <!-- Main content --> 
  <section class="content">
    <div class="row">   
      <!-- left column -->
      <div class="col-md-12"> 
         @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any()) 
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border"> 
            <h3 class="box-title">Form</h3>
          </div>
          <!-- /.box-header --> 
         
              <form role="form" action="{{ url('surat-jalan/store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- text input -->
                <div class="box-body">
                  <div class="row">
                <div class="col-md-12">
                  <center>
                    <label>Nomor PO</label><br>
                    <select class="select2"  name="filter_po_masuk" id="filter_po_masuk" onchange="openForm(this)" style="width: 165px" required>
                          <option value="">-- Pilih Nomor PO --</option>
                          @foreach($list_po_masuk as $val)
                          @if($filter_po_masuk == $val->no_po)
                          <option value="{{ $val->no_po }}" selected>{{ $val->no_po }}</option>
                          @else
                          <option value="{{ $val->no_po }}">{{ $val->no_po }}</option>
                          @endif
                          
                          @endforeach
                        </select> 
                  </center>
                </div>
              </div>
              @if(isset($penjualan))
                  <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid #eee; ">
                      <div class="form-group">
                        <label for="no_po">Nomor PO</label>
                        <input type="text" class="form-control" id="no_po" name="no_po" value="{{ $penjualan->no_po }}" readonly required>
                      </div> 
                      <div class="form-group">
                        <label for="tanggal">Tanggal</label> 
                        <input type="date" class="form-control" id="tanggal" name="tanggal"  value="{{ old('tanggal') }}" required>
                      </div>
                      <div class="form-group">
                        <label for="no_surat_jalan">Nomor Surat Jalan *</label>
                        <input type="text" class="form-control" id="no_surat_jalan" name="no_surat_jalan" placeholder="Masukkan Nomor Surat Jalan" value="{{ old('no_surat_jalan') }}" required>
                      </div>
                      <div class="form-group">
                        <label for="nama_perusahaan">Nama Perusahaan *</label>
                         <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="{{ $penjualan->nama_perusahaan }}" readonly required>
                      </div>
                      <div class="form-group">
                        <label for="kode_lokasi_perusahaan">Kode lokasi Perusahaan *</label>
                         <input type="text" class="form-control" id="kode_lokasi_perusahaan" name="kode_lokasi_perusahaan" value="{{ $penjualan->kode_lokasi_perusahaan }}" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="nama_driver">Nama Supir </label>
                        <input type="text" class="form-control" id="nama_driver" name="nama_driver" placeholder="Masukkan Nama Supir" value="{{ old('nama_driver') }}" >
                      </div>
                      <div class="form-group">
                        <label for="no_phone_driver">No Telp Supir </label>
                        <input type="text" class="form-control" id="no_phone_driver" name="no_phone_driver" placeholder="Masukkan Nomor Telepon Supir" value="{{ old('no_phone_driver') }}" >
                      </div>
                     <!--  <div class="form-group">
                        <label for="sim_driver">Nomor SIM Supir *</label>
                        <input type="text" class="form-control" id="sim_driver" name="sim_driver" value="{{ old('sim_driver') }}" required>
                      </div> -->
                      <div class="form-group">
                        <label for="nopol">Nomor Plat Kendaraan </label>
                       <input type="text" class="form-control" id="nopol" name="nopol" placeholder="Masukkan Nomor Kendaraan" value="{{ old('nopol') }}" >
                      </div>                  
                      <div class="form-group">
                        <label for="jenis_kendaraan">Jenis Kendaraan</label>
                       <input type="text" class="form-control" id="jenis_kendaraan" name="jenis_kendaraan" placeholder="Masukkan Jenis Kendaraan" value="{{ old('jenis_kendaraan') }}" >
                      </div>   
                     
                     <div class="form-group"> 
                      <label for="role">Alamat Proyek *</label>
                      <textarea class="form-control" id="alamat_perusahaan" name="alamat_perusahaan" placeholder="Masukkan Alamat Pengiriman" required></textarea> 
                    </div>                    
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
                 @endif
              </form> 
            </div> 
          </div> 
        </section>
      </div> 
@endsection 
@section('javascript')
<!-- Select2 -->
<script src="{{ url('plugins/select2/select2.full.min.js')}}"></script>
<script>
   function openForm(elem){
    var data = $(elem).val();
    window.location = "{{ url('/surat-jalan/create?filter_po_masuk=') }}"+data;
  }

  function fetchDataAddress(elem){
    var customer = $(elem).val();

    $.ajax({
      url:"{{ url('customer/getAddress') }}?customer_id="+customer,
      type:"get",
      dataType:"json",
      beforeSend:function(){

      },
      success:function(result){
        if(result.status = "ok"){
          var element = $("#alamat_perusahaan");
          var html = "<option value=''>Pilih Alamat</option>";

          $.each(result.data, function(index, value){
            html += "<option value='"+value.id+"'>"+value.address+"</option>";
          });

          element.html(html);
        }else{
          var html = "<option value=''>Silahkan Pilih Konsumen</option>";
          element.html(html);
        }
      },
      error:function(){

      },
      complete:function(){

      }
    });
  }
</script>
@endsection