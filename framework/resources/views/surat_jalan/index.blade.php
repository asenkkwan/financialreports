@extends('layouts.app')

@section('css')
@endsection
  
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> 
        Surat Jalan
      </h1>
    </section>

    <!-- Main content -->
      <!-- Main content --> 
    <section class="content">
      <div class="row">
        <div class="col-xs-12"> 
          @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
              <div style="float: right">
                <button class="btn btn-primary" onclick="window.location='<?php echo url('surat-jalan/create'); ?>'">
                  <i class="fa fa-plus"></i> Tambah
                </button> 
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <form method="get" action="#">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Start Date</label>
                    <input type="date" name="sdate" id="sdate" class="form-control" value="{{ $sdate }}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>End Date</label>
                    <input type="date" name="edate" id="edate" class="form-control" value="{{ $edate }}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Nomor Kendaraan</label>
                    <input type="text" name="nopol" id="nopol" class="form-control" placeholder="filter nomor kendaraan">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Nama Perusahaan</label>
                    <input type="text" name="nama_perusahaan" id="nama_perusahaan" class="form-control" placeholder="filter nama perusahaan">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>No Surat Jalan</label>
                    <input type="text" name="no_surat_jalan" id="no_surat_jalan" class="form-control" placeholder="filter no invoice">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group"> 
                    <label>No Po</label>
                    <input type="text" name="no_po" id="no_po" class="form-control" placeholder="filter No PO">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <button type="submit" class="form-control btn btn-danger">Filter</button>
                  </div>
                </div>
                 <div class="col-md-2">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <a href="{{ url('surat-jalan') }}" class="form-control btn btn-default">Clear</a>
                  </div>
                </div>
              </div>
            </form>
              <div class="table-responsive">
              <table id="datatable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Tanggal</th>
                  <th>Nama Perusahaan</th> 
                  <th>No PO</th>
                  <th>No Surat Jalan</th>
                  <th>Driver</th>
                  <th>Nomor Handphone Driver</th>
                  <th>Jenis Kendaraan</th> 
                  <th>Nomor Kendaraan</th>                             
                  
                  <th><i class="fa fa-ellipsis-h"></i></th>
                </tr>
                </thead>
                <tbody>
                  @foreach($surat_jalan as $val)
                  <tr>
                    <td>{{ date('d M Y', strtotime($val->tanggal)) }}</td>
                    <td>{{ $val->nama_perusahaan }}</td>
                    <td>{{ $val->no_po }}</td> 
                    <td>{{ $val->no_surat_jalan }}</td> 
                    <td>{{ $val->nama_driver }}</td>
                    <td>{{ $val->no_phone_driver }}</td>
                    <td>{{ $val->jenis_kendaraan}}</td> 
                    <td>{{ $val->nopol}}</td>
                    <td>  
                       <a href="{{ url('surat-jalan/detail-surat-jalan/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-server"></i> Detail </a> 
                      <a href="{{ url('surat-jalan/edit/'.$val->id) }}" class="btn btn-success btn-xs">
                        <i class="fa fa-pencil"></i>Edit</a>
                      <a class="btn btn-danger btn-xs" href="{{ url('surat-jalan/'.$val->id) }}" onclick="event.preventDefault();if(confirm('Hapus Data surat jalan ?')){ $('#form-{{ $val->id }}').submit() }"><i class="fa fa-trash"></i>Hapus</a>
                      <form action="{{ url('surat-jalan/delete/'.$val->id) }}" method="post" id="form-{{ $val->id }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="delete">
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              </div>
              {{ $surat_jalan->appends(['nama_perusahaan' => $nama_perusahaan,'nopol' => $nopol,'no_surat_jalan' => $no_surat_jalan,'no_po' => $no_po,])->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('javascript')
@endsection