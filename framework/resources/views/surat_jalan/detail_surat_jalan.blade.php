@extends('layouts.app')
 
@section('css')

@endsection
   
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Surat Jalan
        <small>{{ $surat_jalan->no_po}}</small> 
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Detail</a></li>
        <li class="active">Surat jalan</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        @if (Session::has('msg'))
          <div class="alert alert-{{Session::get('alert')}}">
            <button data-dismiss="alert" class="close"></button>
            {!! Session::get('msg') !!} 
          </div>
        @endif 
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Detail Surat Jalan
            <small class="pull-right">Date: {{ $surat_jalan->tanggal }}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
       <div class="row no-po">
        <div class="col-sm-12 invoice-col" style="text-align:center;">
          <h2 style="text-decoration:underline;">Surat Jalan</h2><br>
          <p style="margin-top: -30px; font-size: 17px;"></p>
        </div>
      </div>
      <div class="row po-info">
        <div class="col-sm-6 invoice-col">
          <address>
            <p><strong>Surat Jalan No.</strong> {{ $surat_jalan->no_surat_jalan}} </p>
            <p >TANGGAL : {{ date('d M Y', strtotime($surat_jalan->tanggal)) }}<br> </p>
           <p>No. PO : {{ $surat_jalan->no_po }}</p>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col" style="float:right;">
          <address>
            <p ><strong>Kepada Yth,</strong><br></p>
             <p>{{ $surat_jalan->nama_perusahaan }}<br></p>
            <p >{{ $surat_jalan->alamat_perusahaan }}<br></p>
          </address>
        </div>
      </div>
       <br>
     <br>
     <br>
      <div class="row po-info">
        <div class="col-sm-12 invoice-col">
          <address>
             <p style="word-spacing: 5px;">Diangkut dengan kendaraan : 
           <strong>{{ $surat_jalan->jenis_kendaraan }} </strong> No. <strong>{{ $surat_jalan->nopol }} </strong>
           Harap diterima dengan baik barang-barang sebagai berikut :</p>
          </address>
        </div>      
      </div>

      <div class="row" style="    margin-top: 20px;">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th style="width:5%;">No</th>
                <th style="width:13%;">Nama Barang</th>
                <th style="width:4%;">Banyaknya</th>
              </tr>
            </thead>
            <tbody> 
             <?php 
                $counter = 1;
              ?>
              @foreach($data_produk as $val)            
              <tr>
                <td>{{ $counter }}</td>
                <td>{{ $val->produk }}</td>                  
                <td>{{ number_format( $val->qty, 0, ",", ".") }}  LITER </td>
              </tr> 
              <?php
                $counter++;
              ?>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <br>
      <br>
      <br>
      <br>
      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6" style="width: 55%;">
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Sebelum barang diterima, barang tersebut harap diperiksa dalam kondisi segel, Kwantitas Baik
          Volume Cukup. Kami tidak menerima Claim di kemudian hari.</p>
          </p>
        </div>
      </div>
      <br>
      <br>
      <br>
      <br>
      <div class="row invoice-info" style="text-align: center; margin-bottom: 50px;">
        <div class="col-sm-4 invoice-col">
          Penerima barang,
          <address>
            <br>
            <br>
            <br>
            <br>
            <p style="word-spacing: 95px;">(          )</p>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          Pembawa barang,
          <address>
            <br>
            <br>
            <br>
            <br>
            <p style="word-spacing: 95px;">(          )</p>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          Hormat kami,
          <br>
            <br>
            <br>
            <br>
            <br>
            <p style="word-spacing: 95px;">(          )</p>
        </div>
        <!-- /.col -->
      </div>
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href= "{{ url('surat-jalan/invoice-print/'.$surat_jalan->id) }}"  target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
         <!--  @if($surat_jalan->status == 1)
            @if($surat_jalan->invoice_status == 1)
            <button type="button" class="btn btn-info pull-right"><i class="fa fa-credit-card"></i> Show Invoice </button>
            @else
            <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Generate Invoice </button>
            @endif
          @else
            @if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin')
             <button type="button" class="btn btn-success pull-right" onclick="approve({{ $surat_jalan->id }})"><i class="fa fa-credit-card"></i> Approve</button>
            <button type="button" class="btn btn-danger pull-right" onclick="reject({{ $surat_jalan->id }})" style="margin-right: 5px;"><i class="fa fa-credit-card"></i> Reject</button>
            <form id="form-approval" action="{{ url('po-masuk/approve') }}" method="post">
               {{ csrf_field() }}
               <input type="hidden" name="surat_jalan_id" value="{{ $surat_jalan->id }}">
             </form>
             <form id="form-reject" action="{{ url('surat-jalan/reject') }}" method="post">
               {{ csrf_field() }}
               <input type="hidden" name="surat_jalan_id" value="{{ $surat_jalan->id }}">
             </form>
            @endif
          @endif    -->  
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
@endsection
@section('javascript')
<script>
  function approve(id){
    if(confirm("Apakah anda menyetujui PO Masuk ini?")){
      $("#form-approval").submit();
    }
  } 

  function reject(id){
    if(confirm("Apakah anda menyetujui PO Masuk ini?")){
      $("#form-reject").submit();
    }
  } 
</script>
@endsection