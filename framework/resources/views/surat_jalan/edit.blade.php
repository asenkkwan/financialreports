@extends('layouts.app')
@section('css')
   <!-- Select2 -->
  <link rel="stylesheet" href="{{ url('plugins/select2/select2.min.css') }}">
@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> 
        Form
        <small> Edit Surat Jalan</small> 
      </h1> 
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a>
      </li>
      <li><a href="#">Forms</a>
      </li> 
      <li class="active">Edit Surat Jalan</li>
    </ol>
  </section>  
  <!-- Main content --> 
  <section class="content">
    <div class="row">  
      <!-- left column -->
      <div class="col-md-12"> 
         @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border"> 
            <h3 class="box-title">Edit Surat Jalan</h3>
          </div>
          <!-- /.box-header --> 
         <form role="form" action="{{ url('surat-jalan/update/'.$surat_jalan->id) }}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="_method" value="put">
              {{ csrf_field() }}
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid #eee; ">
                      <div class="form-group">
                        <label for="no_po">Nomor PO</label>
                        <input type="text" class="form-control" id="no_po" name="no_po" value="{{ $surat_jalan->no_po }}" readonly required>
                      </div> 
                      <div class="form-group">
                        <label for="no_surat_jalan">Nomor Surat Jalan </label>
                        <input type="text" class="form-control" id="no_surat_jalan" name="no_surat_jalan" value="{{ $surat_jalan->no_surat_jalan }}" readonly>
                      </div>
                      <div class="form-group">
                        <label for="tanggal">Tanggal</label> 
                        <input type="date" class="form-control" id="tanggal" name="tanggal"  value="{{ $surat_jalan->tanggal }}" required>
                      </div>
                      <div class="form-group">
                        <label for="nama_perusahaan">Nama Perusahaan *</label>
                         <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="{{ $surat_jalan->nama_perusahaan }}" readonly required>
                      </div>
                      <div class="form-group">
                        <label for="kode_lokasi_perusahaan">Kode lokasi Perusahaan *</label>
                         <input type="text" class="form-control" id="kode_lokasi_perusahaan" name="kode_lokasi_perusahaan" value="{{ $surat_jalan->kode_lokasi_perusahaan }}" readonly required>
                      </div>
                      <!-- <div class="form-group"> 
                        <label for="role">Alamat Perusahaan *</label>
                        <input type="text" class="form-control" id="alamat_perusahaan" name="alamat_perusahaan" value="{{ $surat_jalan->alamat_perusahaan }}" readonly required>
                      </div>  -->
                    </div>
                    <div class="col-md-6">
                      
                      <div class="form-group">
                        <label for="nama_driver">Nama Supir </label>
                        <input type="text" class="form-control" id="nama_driver" name="nama_driver" value="{{ $surat_jalan->nama_driver }}" required>
                      </div>
                      <div class="form-group">
                        <label for="no_phone_driver">No Telp Supir </label>
                        <input type="text" class="form-control" id="no_phone_driver" name="no_phone_driver" value="{{ $surat_jalan->no_phone_driver }}" required>
                      </div>
                      <div class="form-group">
                        <label for="sim_driver">Nomor Plat Kendaraan </label>
                        <input type="text" class="form-control" id="sim_driver" name="sim_driver" value="{{ $surat_jalan->sim_driver }}" required>
                      </div>                 
                      <div class="form-group">
                        <label for="jenis_kendaraan">Jenis Kendaraan </label>
                       <input type="text" class="form-control" id="jenis_kendaraan" name="jenis_kendaraan" value="{{ $surat_jalan->jenis_kendaraan }}" required>
                      </div> 
                      <div class="form-group">
                        <label for="alamat_perusahaan">Alamat Proyek *</label>
                       <input type="text" class="form-control" id="alamat_perusahaan" value="{{ $surat_jalan->alamat_perusahaan }}" required>
                      </div>                      
                  </div>
                </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button action="{{ url('surat_jalan/add-item'.$surat_jalan->id) }}" type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </form> 
            </div> 
          </div> 
        </section>
      </div> 
@endsection 
@section('javascript')
<!-- Select2 -->
<script src="{{ url('plugins/select2/select2.full.min.js')}}"></script>
<script>

  function fetchDataAddress(elem){
    var customer = $(elem).val();

    $.ajax({
      url:"{{ url('customer/getAddress') }}?customer_id="+customer,
      type:"get",
      dataType:"json",
      beforeSend:function(){

      },
      success:function(result){
        if(result.status = "ok"){
          var element = $("#alamat_perusahaan");
          var html = "<option value=''>Pilih Alamat</option>";

          $.each(result.data, function(index, value){
            html += "<option value='"+value.id+"'>"+value.address+"</option>";
          });

          element.html(html);
        }else{
          var html = "<option value=''>Silahkan Pilih Konsumen</option>";
          element.html(html);
        }
      },
      error:function(){

      },
      complete:function(){

      }
    });
  }
</script>
@endsection