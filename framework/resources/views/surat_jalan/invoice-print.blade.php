<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>A5 landscape</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: A5 landscape }</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A5 landscape">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="sheet padding-0mm">
 
    <!-- <img src="{{ url('dist/img/sJAudri.jpeg') }}" style="width: 100%;"> -->
  <div style="z-index: 9999;position: absolute; top: -18px; margin-left: 0px;">
    <p style="margin-left: 224px; margin-top:96px;"> {{ $surat_jalan->no_surat_jalan}} </p>
    <p style="margin-left: 174px; margin-top:-5px;"> {{ date('d M Y', strtotime($surat_jalan->tanggal)) }}<br> </p>
    <p style=" margin-left: 174px; margin-top:-14px;"> {{ $surat_jalan->no_po }}</p>  
  </div>
   <div style="z-index: 9999;position: absolute;top: 0; margin-left: 321px; margin-top: 15px;">
    <p style="margin-left: 163px; margin-top:88px;"> {{ $surat_jalan->nama_perusahaan}} </p>
    <p style="margin-left: 162px; margin-top:-12px; word-spacing: -1px;"> {{ $surat_jalan->alamat_perusahaan }}<br> </p>
  </div>
  <div style="z-index: 9999;position: absolute;top: 0; margin-left: 190px; margin-top: 92px;">
    <p style="margin-left: 88px; margin-top:88px;"> {{ $surat_jalan->jenis_kendaraan}} </p>
    <p style="margin-left: 225px; margin-top:-32px;"> {{ $surat_jalan->nopol }}<br> </p>
  </div>
    <?php     
      $counter = 1; 
      $absolute = 0;
    ?> 
  @foreach($data_produk as $val)
   <div style="z-index: 9999;position: absolute;top: {{ $absolute }}px; margin-left: 195px; margin-top: 185px;">
    <p style="margin-left: -116px; margin-top:88px;">{{ $counter }}</p>
    <p style="margin-left: 55px; margin-top:-35px;"> {{ $val->produk }}<br> </p>
    <p style="margin-left: 441px; margin-top:-35px;"> {{ number_format( $val->qty, 0, ",", ".") }}  LITER<br> </p>
  </div>
  <?php 
    $counter++; 
    $absolute += 20;
  ?>
@endforeach
  </section>

</body>

</html>