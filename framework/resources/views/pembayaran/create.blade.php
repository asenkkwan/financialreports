@extends('layouts.app')
@section('css')
 <!-- Select2 -->
  <link rel="stylesheet" href="{{ url('plugins/select2/select2.min.css') }}">
@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) --> 
  <section class="content-header">
    <h1> 
        Form
        <small>Pembayaran Baru</small> 
      </h1>   
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a>
      </li>
      <li><a href="#">Forms</a>
      </li> 
      <li class="active">Pembayaran Baru</li>
    </ol> 
  </section> 
  <!-- Main content --> 
  <section class="content">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12">
         @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger"> 
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border"> 
            <h3 class="box-title">Form</h3>
          </div>
          <!-- /.box-header --> 
         
          <form role="form" action="{{ url('pembayaran/store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <!-- text input --> 
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <center>
                    <label>Detail Item</label><br>
                    <select class="select2" name="filter_item_penjualan" id="filter_item_penjualan" onchange="openForm(this)" style="width: 200px" required>
                          <option value="">-- Pilih Item --</option>
                          @foreach($list_item_penjualan as $val)
                          @if(isset($item_penjualan->id) and $item_penjualan->id == $val->id_item_penjualan)
                          <option value="{{ $val->id_item_penjualan }}" selected>{{ $val->no_po }} ({{ $val->produk }} - {{ $val->jenis_bbm }} )</option>
                          @else
                          <option value="{{ $val->id_item_penjualan }}">{{ $val->no_po }} ({{ $val->produk }} - {{ $val->jenis_bbm }} )</option>
                          @endif
                          @endforeach
                        </select> 
                  </center>
                </div>
              </div> 
              @if(isset($item_penjualan))
              <div class="row">
                <div class="col-md-12">
                  <h3>Informasi Pembayaran</h3> 
                </div>
                <div class="col-md-6" style="border-right: 1px solid #eee; ">
                  <div class="form-group">
                    <label for="payment_date">Tanggal Pembayaran</label>
                    <input type="date" class="form-control" id="payment_date" name="payment_date" placeholder="Masukkan Tanggal Pembayaran" required>
                    <input type="hidden" class="form-control" id="id_item_penjualan" name="id_item_penjualan" value="{{ $item_penjualan->id }}">
                    <input type="hidden" class="form-control" id="id_penjualan" name="id_penjualan" value="{{ $item_penjualan->id_penjualan }}" >
                  </div>
                  <div class="form-group">
                    <label for="payment_number">No Pembayaran</label>
                    <input type="text" class="form-control" id="payment_number" name="payment_number" placeholder="Masukkan Nomor Pembayaran" required>
                  </div> 
                  <div class="form-group">
                    <label for="ntt">Tgl NTT</label>
                    <input type="date" class="form-control" id="ntt" name="ntt" placeholder="Masukkan Nota Tanda Terima" required>
                  </div>
                   <div class="form-group">
                    <label for="kredit">Cicilan Pembayaran*</label>
                    <input type="text" class="form-control" id="kredit" name="kredit" placeholder="Masukkan Jumlah Cicilan" required>
                  </div> 
                  <div class="form-group">
                    <label for="notes">Keterangan</label>
                    <input type="text" class="form-control" id="notes" name="notes" placeholder="Masukkan Keterangan Transaksi" required> 
                  </div>  
                  <div class="form-group">
                    <label for="jumlah">Jumlah Tagihan</label>
                    <input type="text" class="form-control" id="jumlah" name="jumlah" value="{{ number_format($sisa_tagihan, 0, ",", ".") }}" readonly required> 
                  </div>
                   <div class="form-group">
                    <label for="ppn">PPN</label>
                    <input type="text" class="form-control" id="ppn" name="ppn" value="{{ number_format($item_penjualan->ppn_total, 0, ",", ".") }}"  readonly required>
                  </div>  
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="tanggal">Tanggal PO</label>
                    <input type="date" class="form-control" id="tanggal" name="tanggal" value="{{ $item_penjualan->tanggal }}" readonly required>
                  </div>
                  <div class="form-group">
                    <label for="no_po">Nomor PO</label>
                    <input type="text" class="form-control" id="no_po" name="no_po" value="{{ $item_penjualan->no_po }}" readonly required> 
                  </div>
                  <div class="form-group">
                    <label for="no_invoice">Nomor Invoice</label>
                    <input type="text" class="form-control" id="no_invoice" name="no_invoice" value="{{ $item_penjualan->no_invoice }}" readonly required> 
                  </div> 
                  <div class="form-group">
                    <label for="role">Nama Perusahaan *</label>
                     <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="{{ $item_penjualan->nama_perusahaan }}" readonly required>
                  </div>
                  <div class="form-group"> 
                    <label for="lokasi_perusahaan">Alamat Perusahaan *</label>
                    <input type="text" class="form-control" id="lokasi_perusahaan" name="lokasi_perusahaan" value="{{ $item_penjualan->lokasi_perusahaan }}" readonly required>
                  </div>  
                  <div class="form-group">
                    <label for="produk">Produk</label>
                    <input type="text" class="form-control" id="produk" name="produk" value="{{ $item_penjualan->produk}}" readonly required> 
                  </div> 
                </div>
              </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-success pull-right">Simpan</button>
            </div>
              @endif
          </form> 
        </div> 
      </div>
    </div> 
  </section>
</div>
@endsection 
@section('javascript')
<!-- Select2 -->
<script src="{{ url('plugins/select2/select2.full.min.js')}}"></script>
<!-- <script>
  
  function addItemField(){
    var body = jQuery("#item-body");
    var html = "<tr>";
    html += '<td onclick="deleteItemField(this)"><i class="fa fa-trash"></i></td>';
    html += '<td><input type="text" name="produk[]" id="produk" class="form-control"></td>';
    html += '<td><input type="text" name="qty[]" id="qty" class="form-control"></td>';
    html += '<td><input type="text" name="harga_beli[]" id="harga_beli" class="form-control"></td>';
    html += '<td><input type="text" name="harga_jual[]" id="harga_jual" class="form-control"></td>';
    html += '<td><input type="text" name="pbbkb[]" id="pbbkb" class="form-control"></td>';
    html += '<td><input type="text" name="ppn[]" id="ppn" class="form-control"></td>';
    html += "</tr>";           
    body.append(html);  
  }
  function deleteItemField(elem){
    $(elem).parent().remove();
  }
</script> -->
<script>
  $(".select2").select2();

  function openForm(elem){
    var data = $(elem).val();
    window.location = "{{ url('/pembayaran/create?filter_item_penjualan=') }}"+data;
  }
  
   function fetchDataAddress(elem){
    var customer = $(elem).val();

    $.ajax({
      url:"{{ url('customer/getAddress') }}?customer_id="+customer,
      type:"get",
      dataType:"json",
      beforeSend:function(){

      },
      success:function(result){
        if(result.status = "ok"){
          var element = $("#lokasi_perusahaan");
          var html = "<option value=''>Pilih Alamat</option>";

          $.each(result.data, function(index, value){
            html += "<option value='"+value.id+"'>"+value.address+"</option>";
          });

          element.html(html);
        }else{
          var html = "<option value=''>Silahkan Pilih Konsumen</option>";
          element.html(html);
        }
      },
      error:function(){

      },
      complete:function(){

      }
    });
  }
</script>
@endsection