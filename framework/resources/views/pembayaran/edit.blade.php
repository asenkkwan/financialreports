@extends('layouts.app')
@section('css')
 <!-- Select2 -->
  <link rel="stylesheet" href="{{ url('plugins/select2/select2.min.css') }}">
@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) --> 
  <section class="content-header">
    <h1> 
        Form
        <small>Edit Pembayaran</small> 
      </h1>   
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a>
      </li>
      <li><a href="#">Forms</a>
      </li> 
      <li class="active">Edit Pembayaran</li>
    </ol>
  </section>
  <!-- Main content --> 
  <section class="content">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12">
         @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger"> 
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border"> 
            <h3 class="box-title">Form</h3>
          </div> 
          <!-- /.box-header --> 
         
           <form role="form" action="{{ url('pembayaran/update/'.$pembayaran->id) }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
            <div class="box-body">
            @if(isset($pembayaran))
              <div class="row">
                <div class="col-md-12">
                  <h3>Informasi Pembayaran</h3> 
                </div>
                <div class="col-md-6" style="border-right: 1px solid #eee; ">
                  <div class="form-group">
                    <label for="payment_date">Tanggal Pembayaran</label>
                    <input type="date" class="form-control" id="payment_date" name="payment_date" value="{{ $pembayaran->payment_date }}" required>
                    <input type="hidden" class="form-control" id="id_item_penjualan" name="id_item_penjualan" value="{{ $pembayaran->id }}" >
                  </div>
                  <div class="form-group">
                    <label for="payment_number">No Pembayaran</label>
                    <input type="text" class="form-control" id="payment_number" name="payment_number" value="{{ $pembayaran->payment_number }}" required>
                  </div> 
                  <div class="form-group">
                    <label for="ntt">Tgl NTT</label>
                    <input type="date" class="form-control" id="ntt" name="ntt" value="{{ $pembayaran->ntt }}" required>
                  </div>
                  <div class="form-group">
                    <label for="kredit">Cicilan Pembayaran*</label>
                    <input type="text" class="form-control" id="kredit" name="kredit" value="{{ $pembayaran->kredit }}" required>
                  </div> 
                  <div class="form-group">
                    <label for="notes">Keterangan</label>
                    <input type="text" class="form-control" id="notes" name="notes" value="{{ $pembayaran->notes }}" required>
                  </div> 
                  <div class="form-group">
                    <label for="jumlah">Jumlah Tagihan</label>
                    <input type="text" class="form-control" id="jumlah" name="jumlah" value="{{ number_format($sisa_tagihan, 0, ",", ".") }}" readonly required> 
                  </div>
                   <div class="form-group">
                    <label for="ppn">PPN</label>
                    <input type="text" class="form-control" id="ppn" name="ppn" value="{{ number_format($pembayaran->ppn_total, 0, ",", ".") }}"  readonly required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="tanggal">Tanggal PO</label>
                    <input type="date" class="form-control" id="tanggal" name="tanggal" value="{{ $pembayaran->tanggal }}" readonly required>
                  </div>
                  <div class="form-group">
                    <label for="no_po">Nomor PO</label>
                    <input type="text" class="form-control" id="no_po" name="no_po" value="{{ $pembayaran->no_po }}" readonly required> 
                  </div>
                  <div class="form-group">
                    <label for="no_invoice">Nomor Invoice</label>
                    <input type="text" class="form-control" id="no_invoice" name="no_invoice" value="{{ $pembayaran->no_invoice }}" readonly required> 
                  </div>
                  <div class="form-group">
                    <label for="role">Nama Perusahaan *</label>
                     <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="{{ $pembayaran->nama_perusahaan }}" readonly required>
                  </div>
                  <div class="form-group"> 
                    <label for="lokasi_perusahaan">Alamat Perusahaan *</label>
                    <input type="text" class="form-control" id="lokasi_perusahaan" name="lokasi_perusahaan" value="{{ $pembayaran->lokasi_perusahaan }}" readonly required>
                  </div>
                  <div class="form-group">
                  <label for="produk">Produk</label>
                  <input type="text" class="form-control" id="produk" name="produk" value="{{ $pembayaran->produk}}" readonly required> 
                </div>   
                </div>
                
              </div>
            
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-success pull-right">Simpan</button>
            </div>
            @endif
          </form> 
        </div> 
      </div>
    </div> 
  </section>
</div>
@endsection 
@section('javascript')
<!-- Select2 -->
<script src="{{ url('plugins/select2/select2.full.min.js')}}"></script>

<script>
  $(".select2").select2();

</script>
@endsection