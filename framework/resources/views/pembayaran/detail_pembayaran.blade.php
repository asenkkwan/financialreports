@extends('layouts.app')
 
@section('css')

@endsection 
   
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Purchase Order
        <small>{{ $pembayaran->no_po }}</small> 
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Detail</a></li>
        <li class="active">Pembayaran</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="invoice">  
       <div class="row">
        <div class="col-xs-12 invoice-col" style="text-align:center;">
          <table class="table">
            <tr>
              <td align="center">
                <font size="6">Pembayaran</font>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <table border="0" style="width:100%">
            <tr>
              <td style="width: 30%">
                <b>Kepada (Order To)</b><br>
                {{ $po_masuk->po_tujuan }}<br>
                Telp/Fax : {{ $po_masuk->telp_po_tujuan }}<br>
                <!-- Up : {{ $po_masuk->pic_po_tujuan }} -->
              </td>
              <td style="width: 40%"></td>
              <td style="width: 30%">
                Nomor Pembayaran : {{ $pembayaran->payment_number }}<br>
                Nomor PO (Order No) : {{ $penjualan->no_po }}<br>
                Tanggal : {{ date('d F Y', strtotime($pembayaran->payment_date)) }}<br>
                Mata Uang : IDR<br>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 30%">
                <!-- <b>Delivery Office</b><br>
                {{ $penjualan->nama_perusahaan}}<br>
                {{ $penjualan->lokasi_perusahaan}}<br> -->
              </td>
              <td style="width: 40%"></td>
              <td style="width: 30%">
                <!-- Contact Person 1 : {{ $po_masuk->contact_person_1 }}<br>
                Contact Person 2 : {{ $po_masuk->contact_person_2 }}<br> -->
              </td>
            </tr>
          </table>
        </div>
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row" style="margin-top: 20px;">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped table-bordered">
            <thead>
            <tr>
              <th style="width:5%;">No</th>
              <th style="width:22%;">Keterangan</th>            
              <th style="width:8%;">Quantity</th>
              <th style="width:8%;">Harga Jual</th>
              <th style="width:8%;">TGL NTT</th>
              <th style="width:8%;">DPP</th>
              <th style="width:8%;">PPN</th>
             <!--  <th style="width:8%;">PBBKB Dasar</th> -->
              <th style="width:8%;">PBBKB Jual</th>
              <th style="width:8%; text-align:center;">Amount</th>
            </tr>
            </thead>
            <tbody> 
              <?php 
                $total_amount = 0;
                $counter = 1;
              ?>
              @foreach($items as $val)
              <tr> 
                <td>{{ $counter }}</td>           
                <td>{{ $val->produk }}</td>
                <td style="text-align:right;">{{ number_format($val->qty, 0, ",", ".") }}</td>
                <td style="text-align:right;">{{ number_format($val->harga_jual, 2, ",", ".") }}</td>
                <td style="text-align:right;">{{ date('d F Y', strtotime($pembayaran->ntt)) }}</td>
                <td style="text-align:right;">{{ number_format($val->dpp, 0, ",", ".") }}</td>
                <td style="text-align:right;">{{ number_format($penjualan->ppn_total, 0, ",", ".") }}</td>
                <!-- <td style="text-align:right;">{{ number_format($val->pbbkb_dasar, 0, ",", ".") }}</td> -->
                <td style="text-align:right;">{{ number_format($val->pbbkb_jual, 0, ",", ".") }}</td>
                <td style="text-align:right;">{{ number_format($val->jumlah, 0, ",", ".") }}</td>
              </tr>
            <?php  
                $counter++;
                $total_amount += $val->jumlah;
              ?>
              @endforeach
             <?php
                $ppn = $total_amount*((int)$po_masuk->ppn/100);
                $pph = 0;
                if($po_masuk->pph != 0.000){
                  $pph = $total_amount*($po_masuk->pph/100);
                }
                
                $grand_total = $total_amount + $ppn +(int)$pph + $po_masuk->add_cost + $val->pbbkb_jual;
                $grand_total = ceil($grand_total);
                $kredit_total = $grand_total - $pembayaran->kredit;
                $kredit_total = ceil($kredit_total); 
                // print_r($po_masuk->add_cost);die();
              ?>  

              <tr>
                <td rowspan="1" colspan="7" >
                  <!-- <p>Payment Term : {{ $po_masuk->payment_term }} Hari</p> -->
                </td>
                <th>TOTAL </th>
                <td style="text-align:right;">{{ number_format( $grand_total, 0, ",", ".")  }}</td>
              </tr>
              <tr>
                <td rowspan="1" colspan="7" >
                <th>KREDIT </th>
                <td style="text-align:right;">{{ number_format( $pembayaran->kredit, 0, ",", ".")  }}</td>
              </tr>
              <tr>
                <td rowspan="1" colspan="7" >
                <th>SISA PEMBAYARAN </th>
                <td style="text-align:right;">{{ number_format( $kredit_total, 0, ",", ".")  }}</td>
              </tr>
              <tr>
                <td rowspan="1" colspan="10" >
                  <p>Terbilang :</p>
                  <p>{{ terbilang($kredit_total, 1) }} RUPIAH</p>
                </td>
              </tr>

            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
          @if($company->id ==1)
          <div class="col-xs-12">
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
              Keterangan :<br>
              - Pembayaran mohon ditransfer atau Cek/bilyet Giro atas nama : PT. AUDRI LUTFIA JAYA No. Rek. 414.1600.111 BCA KCP. Kramat Jaya Tj. Priok. <br>
              - Pembayaran dianggap sah bila Cek/bilyet Giro yang diserahkan telah selesai di kliring/cair.  <br>
            </p>
          </div>
           @else($company->id ==2)
           <div class="col-xs-12">
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
              Keterangan :<br>
              - Pembayaran mohon ditransfer atau Cek/bilyet Giro atas nama : PT. SUMA ADI JAYA No. Rek. Mandiri 125.007.8138. 138 <br>
              - Pembayaran dianggap sah bila Cek/bilyet Giro yang diserahkan telah selesai di kliring/cair.
            </p>
          </div>
          @endif  
        </div> 
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row" style="margin-top: 20px;">
        <div class="col-xs-12">
          <a href="{{ url('penjualan/invoice-print/'.$penjualan->id) }}"  target="_blank" class="btn btn-default">  <i class="fa fa-print"></i> Print PO</a>
             <a href="{{ url('penjualan/kwitansi-print/'.$penjualan->id) }}"  target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print Kwitansi</a>
             <a href="{{ url('penjualan/kwitansi-print2/'.$penjualan->id) }}"  target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print Kwitansi Pembulatan</a>
            @if($penjualan->status == 2)
              @if(Auth::user()->enable_approval)
             <button type="button" class="btn btn-success pull-right" onclick="approve({{ $penjualan->id }})"><i class="fa fa-credit-card"></i> Lunas </button>
            <button type="button" class="btn btn-danger pull-right" onclick="reject({{ $penjualan->id }})" style="margin-right: 5px;"><i class="fa fa-credit-card"></i> Belum Lunas </button>
            <form id="form-approval" action="{{ url('penjualan/approve') }}" method="post">
               {{ csrf_field() }}
               <input type="hidden" name="penjualan_id" value="{{ $penjualan->id }}">
             </form>
             <form id="form-reject" action="{{ url('penjualan/reject') }}" method="post">
               {{ csrf_field() }}
               <input type="hidden" name="penjualan_id" value="{{ $penjualan->id }}">
             </form>
            @endif
          @endif 
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
@endsection
@section('javascript')
<script>
  function approve(id){
    if(confirm("Apakah Tagihan sudah dilunasi?")){
      $("#form-approval").submit();
    }
  } 

  function reject(id){
    if(confirm("Apakah Tagihan ditolak?")){
      $("#form-reject").submit();
    }
  } 
</script>
@endsection