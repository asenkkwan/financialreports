@extends('layouts.app')

@section('css')
@endsection
  
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> 
        Detail Pembayaran
      </h1>
    </section>  
  
    <!-- Main content -->
      <!-- Main content -->   
    <section class="content"> 
      <div class="row">
        <div class="col-xs-12"> 
          @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
              <div style="float: right">
                <button class="btn btn-primary" onclick="window.location='<?php echo url('pembayaran/create'); ?>'">
                  <i class="fa fa-plus"></i> Tambah
                </button> 
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <form method="get" action="#" id="form-filter">
              <div class="row">
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Start Date</label>
                    <input type="date" name="sdate" id="sdate" class="form-control" value="{{ $sdate }}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>End Date</label>
                    <input type="date" name="edate" id="edate" class="form-control" value="{{ $edate }}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>No PO</label>
                    <input type="text" name="no_po" id="no_po" class="form-control" id="filter_no_po" placeholder="filter no po" value="{{ $no_po }}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>No Invoice</label>
                    <input type="text" name="no_invoice" id="no_invoice" class="form-control" id="filter_no_invoice" placeholder="filter no po" value="{{ $no_invoice }}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-2">
                  <div class="form-group">
                    <label>No Pembayaran</label>
                    <input type="text" name="payment_number" id="filter_payment_number" class="form-control" placeholder="filter Nomor Pembayaran" value="{{ $payment_number }}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Nama Perusahaan</label>
                    <input type="text" name="nama_perusahaan" id="filter_nama_perusahaan" class="form-control" placeholder="filter nama perusahaan" value="{{ $nama_perusahaan }}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <button type="submit" class="form-control btn btn-danger" type='button' onclick='filter()'>Filter</button>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <a href="{{ url('pembayaran') }}" class="form-control btn btn-default">Clear</a>
                  </div>
                </div>
                 <div class="col-md-2">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <button style="margin-top:25px; margin-right: 90px;" class='btn btn-default pull-right' type='button' onclick='exportExcel()'>Export Excel</button>
                  </div>
                </div>
              </div>
              </form>
              <br>
              <div class="table-responsive">
              <table id="datatable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Tanggal</th>
                  <th>Tgl NTT</th>
                  <th>Nama Perusahaan</th> 
                  <th>No PO</th>
                  <th>No Invoice</th>
                  <th>No Pembayaran</th>
                  <th>Produk</th>
                  <th>Harga /Liter</th>
                  <th>QTY</th>
                  <th>DPP</th>
                  <th>PPN</th>
                  <th>PBBKB</th>
                  <th>Jumlah Pembayaran</th> 
                  <th>Jumlah Tagihan</th>
                  <th>Keterangan</th>
                 <!--  <th style="width: 7%;">Status</th> -->
                  <th><i class="fa fa-ellipsis-h"></i></th>
                </tr>
                </thead>
                <tbody>
                  @foreach($pembayaran as $val)
                    <tr>
                      <td>{{ date('d M Y', strtotime($val->payment_date)) }}</td>
                      <td>{{ date('d M Y', strtotime($val->ntt)) }}</td>
                      <td>{{ $val->nama_perusahaan }}</td>
                      <td>{{ $val->no_po }}</td>
                      <td>{{ $val->no_invoice }}</td>
                      <td>{{ $val->payment_number }}</td>
                      <td>{{ $val->produk }} {{ $val->jenis_bbm }}</td>
                      <td>{{ number_format($val->harga_jual, 0, ",", ".")}}</td> 
                      <td>{{ number_format($val->qty, 0, ",", ".") }}</td>
                      <td>{{ number_format($val->dpp, 0, ",", ".") }}</td>
                      <td>{{ number_format($val->ppn_total, 0, ",", ".") }}</td>
                      <td>{{ number_format($val->pbbkb_jual, 3, ",", ".") }}</td>
                      <td>{{ number_format($val->kredit, 0, ",", ".") }}</td>
                      <td>{{ number_format($val->jumlah, 0, ",", ".") }}</td> 
                      <td>{{ $val->notes }}</td>
                      <!-- <th> 
                        @if($val->status == 1)
                          Lunas
                        @elseif($val->status == -1)
                          Rejected
                        @else
                          Belum Lunas
                        @endif 
                      </th> -->
                      <td>   
                       <!--  <a href="{{ url('pembayaran/detail-pembayaran/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-server"></i> Detail </a>  -->
                        <a href="{{ url('pembayaran/edit/'.$val->id) }}" class="btn btn-success btn-xs">
                          <i class="fa fa-pencil"></i> Edit</a>
                          <!--  <a href="{{ url('penjualan/add-item/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Edit Item</a>   -->
                        <a class="btn btn-danger btn-xs" href="{{ url('pembayaran/'.$val->id) }}" onclick="event.preventDefault();if(confirm('Hapus Data penjualan ?')){ $('#form-{{ $val->id }}').submit() }"><i class="fa fa-trash"></i> Hapus </a>
                        <form action="{{ url('pembayaran/delete/'.$val->id) }}" method="post" id="form-{{ $val->id }}">
                          {{ csrf_field() }}
                          <input type="hidden" name="_method" value="delete">
                        </form>
                      </td>
                    </tr>
                  @endforeach 
                    <tr style="background: #eee;text-align: center">
                      <td colspan="7"><b>TOTAL ALL</b></td>
                      <td>{{ number_format($total_all_harga_jual, 0, ",", ".") }}</td>
                      <td>{{ number_format($total_all_qty, 0, ",", ".") }}</td>
                      <td>{{ number_format($total_all_dpp, 0, ",", ".") }}</td>
                      <td>{{ number_format($total_all_ppn, 0, ",", ".") }}</td>
                      <td>{{ number_format($total_all_pbbkb, 3, ",", ".") }}</td>
                      <td>{{ number_format($total_all_kredit, 0, ",", ".") }}</td>
                      <td>{{ number_format($total_all_jumlah, 0, ",", ".") }}</td>
                      <td></td>
                      <td></td>
                    </tr>
                </tbody>
                 
              </table>
              </div>
              {{ $pembayaran->appends(['nama_perusahaan' => $nama_perusahaan,'no_po' => $no_po,'no_invoice' => $no_invoice,'payment_number' => $payment_number,'status' => $status,])->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('javascript')
<script>
  function exportExcel(){
    var form = $("#form-filter");
    form.attr("action","{{ url('pembayaran/export') }}")
    form.submit();
  }
  function filter(){
    var form = $("#form-filter");
    form.attr("action","#")
    form.submit();
  }
</script>

