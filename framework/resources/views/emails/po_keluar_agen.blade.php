
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"> 

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Financial Reports  | Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 --> 
  <link rel="stylesheet" href="{{ url('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('dist/css/AdminLTE.min.css') }}">

  <style> 
     body{
      margin:auto;
      font-size: 10px !important;
    }  
  </style>
</head>
<body >    
<body onload="window.print();">
  <div class="wrapper">
      <!-- Main content -->
      <section class="invoice" >
        <div class="row" style="margin-top: 20px;">
          <div class="col-xs-12">
            <table border="0" style="width:100%">
              <tr>
                <td colspan="3" align="center">
                  <font size="5" style="text-decoration:underline;">Purchase Order</font><br>
                  Nomor : {{ $po_keluar_agen->nomor_po }}<br><br><br>
                </td>
              </tr>
              <tr> 
                <td style="width: 30%">
                  <b>Kepada : </b><br>
                  {{ $po_keluar_agen->nama_agen}}<br>
                  <p>Ditempat<br></p>
                  <p>Attn : {{ $po_keluar_agen->nama_pic_agen}}</p>
                  <!-- Up : {{ $po_keluar_agen->pic_po_tujuan }} -->
                </td>
                <td style="width: 40%"></td>
                <td style="width: 30%">
                  <!-- Nomor PO (Order No) : {{ $po_keluar_agen->no_po }}<br> -->
                  Tanggal : {{ date('d F Y', strtotime($po_keluar_agen->tanggal)) }}<br>
                  Tanggal Pengaliran : {{ date('d F Y', strtotime($po_keluar_agen->tanggal_pengaliran)) }}<br>
                  Alamat Kirim :  <strong>{{ $po_keluar_agen->alamat_kirim}}</strong><br>
                  <!-- Mata Uang : IDR<br> -->
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td style="width: 30%">
                 <!--  <b>Delivery Office</b><br>
                 {{ $po_keluar_agen->lokasi_perusahaan}}<br> -->
                </td>
                <td style="width: 40%"></td>
                <td style="width: 30%">
                  <!-- Contact Person 1 : {{ $po_keluar_agen->contact_person_1 }}<br>
                  Contact Person 2 : {{ $po_keluar_agen->contact_person_2 }}<br> -->
                </td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
         <div class="col-xs-12 table-responsive">
          <table class="table table-striped table-bordered" border="1" style="margin-top: 20px;">
              <thead>
              <tr>
                <th style="width:5%; text-align: center;" >No</th>
                <th style="width:22%; text-align: center;">Jenis Barang</th>            
                <th style="width:8%; text-align: center;">Quantity</th>
                <th style="width:8%; text-align: center;">Harga/Liter</th>
                <th style="width:8%; text-align: center;">Jumlah</th>
                <!-- <th style="width:8%;">DPP</th>
                <th style="width:8%;">PPN</th>
                <th style="width:8%;">PBBKB Dasar</th>
                <th style="width:8%;">PBBKB Jual</th> -->
                <!-- <th style="width:8%; text-align:center;">Amount</th> -->
              </tr>
              </thead>
              <tbody> 
                <?php 
                  // $total_amount = 0;
                  $counter = 1;
                ?>
                @foreach($po_keluar_agen as $val)
                <tr>
                  <td style="text-align:center;">{{ $counter }}</td>          
                  <td style="text-align:center;">{{ $po_keluar_agen->jenis_bbm }}</td>
                  <td style="text-align:center;">{{ $po_keluar_agen->qty }}</td>
                  <td style="text-align:center;">Rp. {{ number_format($po_keluar_agen->harga_dasar, 3, ",", ".") }}</td>
                  <td style="text-align:center;">Rp. {{ number_format($po_keluar_agen->dpp, 0, ",", ".") }}</td>
                  <!-- <td style="text-align:right;">{{ number_format($po_keluar_agen->pbbkb_dasar, 2, ",", ".") }}</td>
                  <td style="text-align:right;">{{ number_format($po_keluar_agen->pbbkb_jual, 2, ",", ".") }}</td>
                  <td style="text-align:right;">{{ number_format($po_keluar_agen->dpp, 2, ",", ".") }}</td>
                  <td style="text-align:right;">{{ number_format($po_keluar_agen->jumlah_ppn, 2, ",", ".") }}</td>
                  <td style="text-align:right;">{{ number_format($po_keluar_agen->jumlah, 2, ",", ".") }}</td> -->
                </tr>
               <!-- <?php 
                  $counter++;
                ?>
                @endforeach  -->
                <tr>
                  <td rowspan="1" colspan="1" >
                    <!-- <p>Payment Term : {{ $po_keluar_agen->payment_term }}</p> -->
                  </td>
                 <td rowspan="1" colspan="1" >
                    <p style="text-align:left;">Pecahan : {{ $po_keluar_agen->pecahan }}</p>
                  </td>
                  <td rowspan="1" colspan="1" >
                    <!-- <p>Payment Term : {{ $po_keluar_agen->payment_term }}</p> -->
                  </td>
                  <th style="text-align: center;">PPN 10%</th> 
                  <td style="text-align:center;">Rp. {{ number_format( $po_keluar_agen->jumlah_ppn, 0, ",", ".") }}</td>
                </tr>
                <tr>
                  <td rowspan="1" colspan="3" style="text-align: center;">
                    <b>Jumlah</b>
                  </td>                
                  <td style="text-align:center;"><b>Rp. {{ number_format( $po_keluar_agen->harga_beli, 2, ",", ".") }}</b></td>
                  <td style="text-align:center;"><b>Rp. {{ number_format( $po_keluar_agen->jumlah, 0, ",", ".") }}</b></td>               
                </tr>
                <tr>
                  <td rowspan="1" colspan="10" >
                    <p>Terbilang :</p>
                    <p>{{ terbilang($po_keluar_agen->jumlah, 1) }} RUPIAH</p>
                  </td>
                  <!-- <th>TOTAL</th>
                  <td style="text-align:right;">{{ number_format($po_keluar_agen->jumlah, 2, ",", ".") }}</td> -->
                </tr>
                <tr>
                  <td colspan="10">Note : {{ $po_keluar_agen->notes }}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
         <div class="row">
          <div class="col-xs-12">
            <td> Alamat Kirim : {{ $po_keluar_agen->dellivery_office}}</td>
          </div> 
        </div>

        <div class="row">
          @if($company->id ==1)
          <div class="col-xs-12">
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
              Note :<br>
              - Pembayaran dilakukan 1 minggu setelah Pihak PT. Audri Lutfia Jaya menerima Receipt Bunker <br>
              - Mohon Dilampirkan Receipt Bunker PT. Audri Lutfia Jaya Pada saat Pengiriman Barang. <br>
              - Berita Acara Bunker dan Resi Bunker dapat diminta langsung dan ditanda tangani oleh kedua belah pihak. <br>
            </p>
          </div>
           @else($company->id ==2)
           <div class="col-xs-12">
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
              Note :<br>
              - Pembayaran dilakukan 1 minggu setelah Pihak PT. Suma Adi Jaya menerima Receipt Bunker <br>
              - Mohon Dilampirkan Receipt Bunker PT. Suma Adi Jaya Pada saat Pengiriman Barang. <br>
              - Berita Acara Bunker dan Resi Bunker dapat diminta langsung dan ditanda tangani oleh kedua belah pihak. <br>
            </p>
          </div>
          @endif  
        </div>
        <!-- /.row -->

        <div class="row">
          <div class="col-xs-12">
            <table border="0" style="width: 100%">
              <tr>
                <td style="width: 30%">
                   <strong>Bekasi,  {{ date('d M Y ') }}</strong><br>
                  <br><br><br><br><br><br>
                  ( {{ Auth::user()->name }} )<br>
                </td>
                <td style="width: 50%"></td>
                <td style="width: 20%">
                  <strong>Disetujui Oleh,</strong><br>
                  {{ strtoupper($po_keluar_agen->nama_perusahaan) }}<br>
                  <br><br><br><br><br>
                  {{ strtoupper($po_keluar_agen->approved_by_client) }}<br>
                </td>
              </tr>
            </table>
            
          </div>
        </div>
        <br>
        <br>
        <div class="row">
        <div class="col-xs-12">
          <table style="width:100%;margin-top: 20px;" border="0">
            <tr>
              <td align="center">
                @if($po_keluar_agen->status == 0)  
                <a href="{{ url('po-keluar-agen/approve?id='.$po_keluar_agen->id.'&username='.App\Setting::select('field_value')->where('field_name', 'username_approval')->first()->field_value) }}" target="_blank" style="color:white;background: green; border: 1px solid white;border-radius: 5px;padding: 10px;">Approve</a>
                <a href="{{ url('po-keluar-agen/reject?id='.$po_keluar_agen->id.'&username='.App\Setting::select('field_value')->where('field_name', 'username_approval')->first()->field_value) }}" style="color:white;background: red; border: 1px solid white;border-radius: 5px;padding: 10px;">Reject</a>
                @else
                @endif
              </td>
            </tr>
          </table>
        </div>
      </div>
      </section>
      <!-- /.content -->
      <!-- /.content -->
  </div>
</body>
</html>
