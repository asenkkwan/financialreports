<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"> 

  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
  <title>Financial Reports  |Invoice</title>
  <!-- Bootstrap 3.3.6 --> 
  <link rel="stylesheet" href="{{ url('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('dist/css/AdminLTE.min.css') }}">
  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
  
 <style> 
    body{
      margin:auto;
      font-size: 10px !important;
    }  
    .pph{
    width: 27%;
    float: left;
    margin-left: 312px;
    border-bottom: 1.3px dashed #000;
    margin-top: -21px !important;
    }
  </style>

</head>
<body class="A4 potrait" onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
<section class="sheet padding-0mm" style="margin:auto;">
    <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <table style="width:100%;margin-top: 20px;" border="0">
        <tr>
          <td align="center">
            <h2 style="text-decoration:underline;">Formulir Pemesanan BBM Penyalur</h2>
              <p style="margin-top:-15px !important; font-size:17px;">Nomor : {{ $po_keluar->nomor_po }}</p>
                <!-- <div class=" col-xs-4" style="border-bottom: 1.3px dashed #000;  width: 50%; margin-top: -15px;"></div> -->
          </td>
        </tr>
      </table>
    </div>
  </div>
  <!-- <div class="row">
    <div class=" col-sm-12 box-header with-border" style="text-align:center;"> 
      <h2 style="text-decoration:underline;">Formulir Pemesanan BBM Penyalur</h2>
        <p style="font-size: 17px; margin-top: -15px;">Nomor : {{ $po_keluar->nomor_po }}</p>
         <div class=" col-xs-4" style="border-bottom: 1.3px dashed #000; margin-left: 265px; width: 50%; margin-top: 5px;"></div>
        </div> -->
          <div class="box-body" style="margin-left: 30px;">
            <div class="row">
              <div class="col-md-12">              
                 <div class="invoice-detail" style="font-size: 13px; ">             
                    <table width="710" style="text-align:center; margin-left:10px;" >
                      <tr>
                        <td style="width:40px;">1</td>
                        <td style="text-align: left;">Nama Penyalur</td>
                        <td style="width:40px;"> :</td>
                        <td style="width:347px; border-bottom: 1.3px dashed #000; text-align:left;">{{ $po_keluar->penyalur }}</td>
                      </tr>
                        <td>2</td>
                        <td style="width: 150px; text-align: left;">No Skp Migas</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $po_keluar->no_skp_penyalur }}</td>
                      <tr>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td style="width: 150px; text-align: left;">Alamat Penyalur</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $po_keluar->alamat_penyalur }}</td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td style="width: 150px; text-align: left;">No Sold to Penyalur</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $po_keluar->no_sold_penyalur }}</td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td style="width: 150px; text-align: left;">NPWP Penyalur</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $po_keluar->npwp_penyalur }}</td>
                      </tr>
                      <tr>
                        <td>6</td>
                        <td style="width: 150px; text-align: left;">Jenis Konsumen</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $po_keluar->jenis_konsumen }}</td>
                      </tr>
                      <tr>
                        <td>7</td>
                        <td style="width: 150px; text-align: left;">Diserahkan Kepada</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $po_keluar->nama_konsumen }}</td>
                      </tr>
                      <tr>
                        <tr>
                        <td>8</td>
                        <td style="width: 150px; text-align: left;">Alamat Konsumen</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $po_keluar->alamat_konsumen }}</td>
                      </tr>
                      <tr>
                        <td>9</td>
                        <td style="width: 150px; text-align: left;">Sales District Konsumen </td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $po_keluar->sales_district }}</td>
                      </tr>
                      <tr>
                        <td>10</td>
                        <td style="width: 150px; text-align: left;">No Ship-to Konsumen</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $po_keluar->kode_lokasi_konsumen }}</td>
                      </tr>
                      <tr>
                        <td>11</td>
                        <td style="width: 150px; text-align: left;">Jenis Usaha Konsumen</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $po_keluar->jenis_konsumen }}</td>
                      </tr>
                      <tr>
                        <td>12</td>
                        <td style="width: 150px; text-align: left;">Jenis BBM </td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left;">{{ $po_keluar->jenis_bbm }}</td>
                      </tr>
                      <tr>
                        <td>13</td>
                        <td style="width: 150px; text-align:left;">Jumlah BBM</td>
                        <td> :</td>
                        <td style="text-align:left; padding-top: 18px;">
                          <div class="form1" style="width:24%; float:left; border-bottom: 1.3px dashed #000;">{{ $po_keluar->qty }} Liter</div>
                          <div class="pbbkb" style="float:left;  margin-left: 46px; border-bottom: 1.3px dashed #000;">*PBBKB : {{App\Utilities\Constant::$PBBKB[$po_keluar->pbbkb] }}</div>
                          <div class="pph">*PPH : {{App\Utilities\Constant::$PPH[$po_keluar->pph] }}</div>
                        </td>
                      </tr>
                      <tr>
                        <td>14</td>
                        <td style="width: 150px; text-align: left;">Harga Jual ke end User/Liter tanpa pajak</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; word-spacing: 120px;">Rp. {{ number_format( $po_keluar->harga_jual, 3, ",", ".") }}</td>
                      </tr>
                      <tr>
                        <td>15</td>
                        <td style="width: 150px; text-align: left;">Harga Dasar ke end User/Liter tanpa pajak</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; word-spacing: 120px;">Rp. {{ number_format( $po_keluar->pbbkb_dasar, 3, ",", ".") }}</td>
                      </tr>
                        <tr>
                        <td>16</td>
                        <td style="width: 150px; text-align: left; text-align:left;">Harga Tebus ke end User/Liter tanpa pajak</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; word-spacing: 120px;">Rp. {{ number_format( $po_keluar->harga_beli, 3, ",", ".") }}</td>
                      </tr>
                        <tr>
                        <td>17</td>
                        <td style="width: 150px; text-align: left;">Harga Dasar Penyalur/Liter tanpa pajak</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; word-spacing: 120px;">Rp. {{ number_format( $po_keluar->harga_dasar, 3, ",", ".") }}</td>
                      </tr>
                        <tr>
                        <td>18</td>
                        <td style="width: 150px; text-align: left;">Margin/Liter Penyalur (15-17)</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; word-spacing: 120px;">Rp. {{ number_format(($po_keluar->pbbkb_dasar-$po_keluar->harga_dasar), 3, ",", ".") }}</td>
                      </tr>
                        <tr>
                        <td>19</td>
                        <td style="width: 150px; text-align: left;">Cara Pembayaran</td>
                        <td> :</td>
                        <div style="width:39%; float: left; margin-left:0px; border-bottom: 1.3px dashed #000;">{{ $po_keluar->cara_pembayaran }}</div>
                        <div style="width:34%; float: left; margin-left: 70px; border-bottom: 1.3px dashed #000;">{{ $po_keluar->jatuh_tempo_hari }} Hari</div>
                      </tr>
                        <tr>
                        <td>20</td>
                        <td style="width: 150px; text-align: left;">Dasar Pengenaan PPN dan PPH</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; word-spacing: 120px;">Rp. {{ number_format( $po_keluar->dpp, 0, ",", ".") }}</td>
                      </tr>
                        <tr>
                        <td>21</td>
                        <td style="width: 150px; text-align: left;">PPN</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; word-spacing: 120px;">Rp. {{ number_format( $po_keluar->jumlah_ppn, 0, ",", ".") }}</td>
                      </tr>
                        <tr>
                        <td>22</td>
                        <td style="width: 150px; text-align: left;">PBBKB (DPP end user x tarif PBBKB)</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; word-spacing: 120px;">Rp. {{ number_format( $po_keluar->pbbkb_jual, 0, ",", ".") }}</td>
                      </tr>
                        <tr>
                        <td>23</td>
                        <td style="width: 150px; text-align: left;">PPH</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; word-spacing: 120px;">Rp.  {{  $po_keluar->pph_total }}</td>
                      </tr>
                        <tr>
                        <td>24</td>
                        <td style="width: 150px; text-align: left; text-align:left;">Nilai Transaksi (13x17) termasuk pajak</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; word-spacing: 120px;">Rp. {{ number_format( $po_keluar->jumlah, 0, ",", ".") }}</td>
                      </tr>
                      <tr>
                        <td>25</td>
                        <td style="width: 150px; text-align: left;">Bukti Setor Bank</td>
                        <td> :</td>
                        <td style="border-bottom: 1.3px dashed #000; text-align:left; float:left; width: 100%;">{{ $po_keluar->bukti_setor_bank }}</td>
                      </tr>
                      <tr>
                        <td>26</td>
                        <td style="width: 150px; text-align: left;">Alat angkut BBM Penyalur & tgl pengisian</td>
                        <td> :</td>
                        <td style="font-size: 12px;">(apabila kolom dibawah ini tidak mencukupi, harap buat lampiran tersendiri)</td>
                      </tr>
                    </table>   
                  <table style="margin-left: 24px; width: 570px; margin-top:6px; text-align:center;">
                    <tr >                        
                      <td style="width: 83px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000;">Tanggal Pengisian</td>
                      <td style="width:6px; text-align:center;"> :</td>
                       @foreach($alat_angkut as $val)
                      <td  style="width:189px; text-align:center; border-bottom: 1px solid #000; border-right: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000;">{{ $val->tanggal }}</td> 
                      @endforeach
                    </tr>
                    <tr> 
                      <td style=" border-bottom: 1px solid #000; border-right: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000;">No. Pol truk tanki/nama tongkang</td>
                      <td style="text-align:center;"> :</td>
                       @foreach($alat_angkut as $val)
                      <td style="text-align:center; border-bottom: 1px solid #000; border-right: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000;">{{ $val->nopol }}</td>
                       @endforeach
                    </tr>
                    <tr>
                      <td style=" border-bottom: 1px solid #000; border-right: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000;">Volume BBM per alat angkut (dlm KL) </td>
                      <td style="text-align:center;"> :</td>
                      @foreach($alat_angkut as $val)
                      <td style="text-align:center; border-bottom: 1px solid #000; border-right: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000;">{{ $val->volume }}</td>
                       @endforeach
                    </tr>
                    <tr>
                      <td style=" border-bottom: 1px solid #000; border-right: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000;">Nama Sopir/Nakhoda per alat angkut</td>
                      <td style="text-align:center;"> :</td>
                      @foreach($alat_angkut as $val)
                      <td style="text-align:center; border-bottom: 1px solid #000; border-right: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000;">{{ $val->nama_supir }}</td>
                       @endforeach
                    </tr>
                  </table>
                  <br>
                    <table width="710" style="text-align:center; margin-left:14px;">
                      <tr>
                        <td>27</td>
                        <td style="width: 201px; text-align: left;">Supply Point</td>
                        <td> :</td>
                        @foreach($alat_angkut as $val)
                        <td style="text-align:center; border-bottom: 1.3px dashed #000;">{{ $po_keluar->supply_point }}</td>
                       @endforeach
                      </tr>
                    </table>              
              </div>              
            </div>
            <br>
         <br>
           <!--  <p style="margin-top: -15px; margin-left: 44px;">Disetujui atau ditolak,</p><br>
            <p style="margin-left: 43px; margin-top: -25px;">PT Pertamina Patra Niaga</p>
            <br><br><br><br>
            <p style="margin-left: 75px; margin-top: -5px;">( {{ $po_keluar->nama_pic_agen}} ) </p> <br>
            <p style="margin-left: 559px; margin-top: -160px;">Jakarta,  {{ date('d M Y ') }} </p>          
            <p style="float: right; margin-right: 284px; margin-top: -10px;">{{ $po_keluar->penyalur}}</p><br> 
            <br><br><br><br>
            <p style="margin-left: 558px; margin-top: 21px;">( {{ $po_keluar->nama_pic_penyalur}} ) </p><br>
            <p style="font-size: 8.5px; margin-left: 50px; margin-top: -15px;">*) Nilai Transaksi adalah nilai yang dibayarkan kepada PT. Pertamina Patra Niaga (rounding 3 angka dibelakang apabila < 001-449 maka dibulatkan kebawah dan jika > 500-999  dibulatkan ke atas)</p> -->
          <div class="row">
            <div class="col-xs-12">
              <table border="0" style="width: 100%;margin-top: 20px;">
                <tr>
                  <td style="width: 30%">
                    <strong>Disetujui atau ditolak,</strong><br>
                    <strong>PT Pertamina Patra Niaga</strong><br>
                    <br><br><br><br><br><br>
                    {{ strtoupper($po_keluar->nama_pic_agen) }}<br>
                  </td> 
                  <td style="width: 50%"></td>
                  <td style="width: 20%">
                    <strong>Jakarta,  {{ date('d M Y ') }}</strong><br>
                    {{ strtoupper($po_keluar->penyalur) }}<br>
                    <br><br><br><br><br>
                    {{ strtoupper($po_keluar->nama_pic_penyalur) }}<br>
                  </td>
                </tr>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <table style="width:100%;margin-top: 20px;" border="0">
                <tr>
                  <td align="center">
                    @if($po_keluar->status == 0)
                    <a href="{{ url('po-keluar/approve?id='.$po_keluar->id.'&username='.App\Setting::select('field_value')->where('field_name', 'username_approval')->first()->field_value) }}" target="_blank" style="color:white;background: green; border: 1px solid white;border-radius: 5px;padding: 10px;">Approve</a>
                    <a href="{{ url('po-keluar/reject?id='.$po_keluar->id.'&username='.App\Setting::select('field_value')->where('field_name', 'username_approval')->first()->field_value) }}" style="color:white;background: red; border: 1px solid white;border-radius: 5px;padding: 10px;">Reject</a>
                    @else
                    @endif
                  </td>
                </tr>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <table style="width:100%;margin-top: 20px;" border="0">
                <tr>
                  <td align="left">
                    <p style="font-size: 8.5px; margin-left: 50px; margin-top: -15px;">*) Nilai Transaksi adalah nilai yang dibayarkan kepada PT. Pertamina Patra Niaga (rounding 3 angka dibelakang apabila < 001-449 maka dibulatkan kebawah dan jika > 500-999  dibulatkan ke atas)</p>
                  </td>
                </tr>
              </table>
            </div>
          </div>
      </section>
    </div>
  </body>
</html>
