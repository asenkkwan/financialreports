 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->  
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ url('dist/img/user-blue.png') }}" class="img-circle" alt="User Image">
        </div> 
        <div class="pull-left info"> 
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>   
     
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">NAVIGATION</li>
        <li class="">
          <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-th"></i> <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/user') }}"><i class="fa fa-circle-o"></i>User</a></li>
            <li><a href="{{ url('/company') }}"><i class="fa fa-circle-o"></i>Company</a></li>
            <li><a href="{{ url('/customer') }}"><i class="fa fa-circle-o"></i>Customer </a></li>
            <li><a href="{{ url('/agen')}}"><i class="fa fa-circle-o"></i>Agen</a></li>
            <li><a href="{{ url('/supply-point')}}"><i class="fa fa-circle-o"></i>Supply Point</a></li>
            <li><a href="{{ url('/jenis-bbm')}}"><i class="fa fa-circle-o"></i>Jenis BBM</a></li>      
          </ul>
        </li>
        <li class="header">PURCHASE ORDER</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-sign-in"></i> <span>PO Masuk</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/po-masuk') }}"><i class="fa fa-circle-o"></i> List</a></li>
            <li><a href="{{ url('/po-masuk/create') }}"><i class="fa fa-circle-o"></i> Buat Baru</a></li>       
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-sign-out"></i> <span>PO Keluar</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/po-keluar') }}"><i class="fa fa-circle-o"></i> List PO Patra</a></li> 
            <li><a href="{{ url('/po-keluar-agen') }}"><i class="fa fa-circle-o"></i> List PO Agen & Nasional</a></li>        
          </ul>
        </li>
        <li class="header">TRANSAKSI</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-share-square"></i> <span>Penjualan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/penjualan') }}"><i class="fa fa-circle-o"></i> List</a></li>
            <li><a href="{{ url('/penjualan/create') }}"><i class="fa fa-circle-o"></i> Buat Baru</a></li>       
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cart-plus"></i> <span>Pembelian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/pembelian') }}"><i class="fa fa-circle-o"></i> Pembelian Patra</a></li>
            <li><a href="{{ url('/pembelian-agen') }}"><i class="fa fa-circle-o"></i> Pembelian Agen & Nasional</a></li>       
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-money""></i> <span>Pembayaran</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/pembayaran') }}"><i class="fa fa-circle-o"></i>List</a></li>
            <li><a href="{{ url('/pembayaran/create') }}"><i class="fa fa-circle-o"></i>Buat Baru</a></li>       
          </ul>
        </li>
        <li class="">
          <a href="{{ url('piutang') }}">
            <i class="fa fa-credit-card"></i> <span>Piutang</span>
          </a>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-envelope"></i> <span>Surat Jalan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/surat-jalan') }}"><i class="fa fa-circle-o"></i> List</a></li>
            <li><a href="{{ url('/surat-jalan/create') }}"><i class="fa fa-circle-o"></i> Buat Baru</a></li>       
          </ul>
        </li>
         @if(Auth::user()->role == 'superadmin')
        <li class="header">PENGATURAN</li>
        <li class="">
          <a href="{{ url('setting') }}">
            <i class="fa fa-dashboard"></i> <span>Setting</span>
          </a>
        </li>
        @endif
       <!--  <li class="treeview">
          <a href="#">
            <i class="fa fa-archive"></i> <span>History</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{('general')}}"><i class="fa fa-circle-o"></i>Penjualan</a></li>
            <li><a href="{{('advanced')}}"><i class="fa fa-circle-o"></i>Pembelian</a></li>
          </ul>
        </li> -->
    </section>
    <!-- /.sidebar -->
  </aside>