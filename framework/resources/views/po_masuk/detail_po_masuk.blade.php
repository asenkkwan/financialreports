@extends('layouts.app')
 
@section('css')

@endsection
   
@section('content')
<style>
   @media print
    {    
        .no-print, .no-print *
        {
            display: none !important;
        }
        #print 
        { 
          display: none ;
        }
    } 
 
</style>
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Purchase Order
        <small>{{ $po_masuk->nomor_po}}</small> 
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Detail</a></li>
        <li class="active">PO Masuk</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="invoice">
      <!-- info row -->
       <div class="row">
        <div class="col-xs-12 invoice-col" style="text-align:center;">
          <table class="table">
            <tr>
              <td align="center">
                <font size="6">Order Pembelian</font><br>
                {{ $po_masuk->nomor_invoice }}
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <table border="0" style="width:100%">
            <tr>
              <td style="width: 30%">
                <b>Kepada (Order To)</b><br>
                {{ $po_masuk->po_tujuan}}<br>
                Telp/Fax : {{ $po_masuk->telp_po_tujuan }}<br>
                Up : {{ $po_masuk->pic_po_tujuan }}
              </td>
              <td style="width: 40%"></td>
              <td style="width: 30%">
                Nomor PO (Order No) : {{ $po_masuk->nomor_po }}<br>
                Tanggal PO (Order Date) : {{ date('d F Y', strtotime($po_masuk->tanggal_po)) }}<br>
                Tanggal Kirim (Delivery Date) : {{ date('d F Y', strtotime($po_masuk->tanggal_kirim)) }}<br>
                Mata Uang : IDR<br>
              </td>
            </tr>
            <tr> 
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 30%">
                <b>Delivery Office</b><br>
                {{ $po_masuk->nama_perusahaan}}<br>
               {{ $po_masuk->alamat_perusahaan}}<br>
              </td>
              <td style="width: 40%"></td>
              <td style="width: 30%">
                Contact Person 1 : {{ $po_masuk->contact_person_1 }}<br>
                Contact Person 2 : {{ $po_masuk->contact_person_2 }}<br>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row" style="margin-top: 20px;">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped table-bordered">
            <thead>
            <tr>
              <th style="width:5%;">No</th>
              <th style="width:9%;">Material</th>
              <th style="width:22%;">Description</th>
              <th style="width:7%;">UoM</th>
              <th style="width:8%;">Quantity</th>
              <th style="width:8%;">Unit Price</th>
              <th style="width:8%; text-align:center;">Amount</th>
            </tr>
            </thead>
            <tbody> 
              <?php 
                $total_amount = 0;
                $counter = 1;
              ?>
              @foreach($item_po_masuk as $val)
              <tr>
                <td>{{ $counter }}</td>
                <td>{{ $val->material }}</td>                  
                <td>{{ $val->description }}</td>
                <td>{{ $val->uom }}</td>
                <td style="text-align:right;">{{ number_format($val->qty, 0, ",", ".") }}</td>
                <td style="text-align:right;">{{ number_format($val->unit_price, 0, ",", ".") }}</td>
                <td style="text-align:right;">{{ number_format($val->amount, 0, ",", ".") }}</td>
              </tr>
              <?php 
                $counter++;
                $total_amount += ($val->amount);
              ?>
              @endforeach
              <?php
                $ppn = $total_amount*((int)$po_masuk->ppn/100);
                $pph = 0;
                if($po_masuk->pph != 0.000){
                  $pph = $total_amount*($po_masuk->pph/100);
                }
                $grandTotal = $total_amount + $pph + $po_masuk->add_cost;
                $grandTotal = ($grandTotal); 
              ?>
              <tr> 
                <td rowspan="3" colspan="5" >
                  <p>Payment Term : {{ $po_masuk->payment_term }} Hari</p>
                </td>
                <th>Subtotal:</th>
                <td style="text-align:right;">{{ number_format($total_amount, 0, ",", ".") }}</td>
              </tr>
              <tr>
                <th>PPN</th>
                <td style="text-align:right;" value="ppn" name="ppn">{{ number_format($ppn, 0, ",", ".") }}</td>
              </tr>
              <tr>
                <th>PPH</th>
                <td style="text-align:right;">{{ number_format($pph, 2, ",", ".") }}</td>
              </tr> 
              <tr>
                <td rowspan="2" colspan="5" >
                  <p>Terbilang :</p>
                  <p>{{ terbilang($grandTotal, 1) }} RUPIAH</p>
                </td>
                <th>Add Cost</th>
                <td style="text-align:right;">{{ number_format($po_masuk->add_cost, 3, ",", ".") }}</td>
              </tr>

              <tr>
                <th>TOTAL</th>
                <td style="text-align:right;">{{ number_format($grandTotal, 0, ",", ".") }}</td>
              </tr>
              <tr>
                <td colspan="7">Note : {{ $po_masuk->notes }}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-12">
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Terima kasih atas perhatian Saudara, Harap referensi Nomor PO diatas dicantumkan dalam setiap dokumen yang saudara kirim sehubungan dengan Order Pembelian ini, dan menginformasikan kembali order Pembelian ini dengan email dalam waktu 7 hari setelah tanggal Order Pembelian. Apabila kami tidak menerima konfirmasi tersebut, maka sebagai konsekuensinya kami menganggap Saudara telah menyetujui Order Pembelian tersebut.
          </p>
        </div>
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-12">
          <table border="0" style="width: 100%">
            <tr>
              <td style="width: 30%">
                <strong>Prepared By,</strong><br>
                <br><br><br><br><br><br>
                {{ strtoupper($po_masuk->prepared_by) }}<br>
              </td>
              <td style="width: 50%"></td>
              <td style="width: 20%">
                <strong>Approved By,</strong><br>
                {{ strtoupper($po_masuk->nama_perusahaan) }}<br>
                <br><br><br><br><br>
              <!--   {{ strtoupper($po_masuk->approved_by_client) }}<br> -->
              </td>
            </tr>
          </table>
          
        </div>
      </div> 
      <!-- this row will not appear when printing -->
      <div class="row" style="margin-top: 20px;">
        <div class="col-xs-12">
          @if($po_masuk->status == 0)
            @if(Auth::user()->enable_approval)
             <button type="button" class="btn btn-success pull-right" onclick="approve({{ $po_masuk->id }})"><i class="fa fa-credit-card"></i> Approve</button>
            <button type="button" class="btn btn-danger pull-right" onclick="reject({{ $po_masuk->id }})" style="margin-right: 5px;"><i class="fa fa-credit-card"></i> Reject</button>
            <form id="form-approval" action="{{ url('po-masuk/approve') }}" method="post">
               {{ csrf_field() }}
               <input type="hidden" name="po_masuk_id" value="{{ $po_masuk->id }}">
             </form>
             <form id="form-reject" action="{{ url('po-masuk/reject') }}" method="post">
               {{ csrf_field() }}
               <input type="hidden" name="po_masuk_id" value="{{ $po_masuk->id }}">
             </form>
            @endif
          @else
            <a href="{{ url('po-masuk/invoice-print/'.$po_masuk->id) }}" id="print" target="_blank" class="btn btn-default" ><i class="fa fa-print"onclick="window.print()"></i> Print</a>
          @endif 
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
@endsection
@section('javascript') 
<script>
  function approve(id){
    if(confirm("Apakah anda menyetujui PO Masuk ini?")){
      $("#form-approval").submit();
    }
  } 

  function reject(id){
    if(confirm("Apakah anda menolak PO Masuk ini?")){
      $("#form-reject").submit();
    }
  } 
</script>
@endsection