@extends('layouts.app')
@section('css')
   <!-- Select2 -->
  <link rel="stylesheet" href="{{ url('plugins/select2/select2.min.css') }}">
@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
        Form
        <small>Tambah PO Masuk</small>  
      </h1> 
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a>
      </li>
      <li><a href="#">Forms</a>
      </li> 
      <li class="active">Tambah PO Masuk</li>
    </ol>
  </section>  
  <!-- Main content --> 
  <section class="content">
    <div class="row">  
      <!-- left column -->
      <div class="col-md-12"> 
         @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border"> 
            <h3 class="box-title">Form</h3>
          </div>
          <!-- /.box-header -->  
         
              <form role="form" action="{{ url('po-masuk/store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- text input -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid #eee; ">
                      <div class="form-group">
                        <label for="nomor_po">Nomor PO *</label>
                          <input type="text" class="form-control" id="nomor_po" name="nomor_po" placeholder="Masukkan Nomor PO" required>
                      </div> 
                      <div class="form-group">
                        <label for="tanggal_po">Tanggal PO *</label> 
                          <input type="date" class="form-control" id="tanggal_po" name="tanggal_po"  placeholder="Masukkan Tanggal PO" required>
                      </div>
                      <div class="form-group">
                        <label for="tanggal_kirim">Tanggal Kirim *</label>
                        <input type="date" class="form-control" id="tanggal_kirim" name="tanggal_kirim" placeholder="Masukkan Tanggal Kirim" required>
                      </div> 
                      <div class="form-group">
                        <label for="role">Nama Perusahaan *</label>
                          <select class="form-control select2" name="nama_perusahaan" id="nama_perusahaan" onchange="fetchDataAddress(this)" required>
                            <option  selected="selected">-- Pilih Perusahaan--</option>
                            @foreach($nama_perusahaan as $val)
                            <option value="{{ $val->id }}">{{ $val->name }}</option>
                            @endforeach
                          </select>
                      </div>
                      <div class="form-group"> 
                        <label for="alamat_perusahaan">Alamat perusahaan *</label>
                          <select class="form-control" name="alamat_perusahaan" id="alamat_perusahaan" required>
                            <option value="">-- Silahkan Pilih Konsumen --</option>
                          </select>
                      </div>
                      <div class="form-group">
                        <label for="contact_person_1">Contact Person 1 *</label>
                          <input type="text" class="form-control" id="contact_person_1" name="contact_person_1"placeholder="Masukkan Nomor Contact Person 1 " required>
                      </div>
                      <div class="form-group">
                        <label for="contact_person_2">Contact Person 2 *</label>
                          <input type="text" class="form-control" id="contact_person_2" name="contact_person_2"placeholder="Masukkan Nomor Contact Person 2 " required>
                      </div>
                      <div class="form-group">
                        <label for="role">Order To *</label>
                        <select class="form-control" name="po_tujuan" id="po_tujuan" required>
                          <option value="">-- Pilih Kantor Cabang --</option>
                          @foreach($kantor_cabang as $val)
                          <option value="{{ $val->id }}">{{ $val->name }}</option>
                          @endforeach
                        </select>
                    </div>                          
                    </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>PIC PO Tujuan *</label>
                        <input type="text" class="form-control" id="pic_po_tujuan" name="pic_po_tujuan" placeholder="Masukkan Nama PIC " required>
                    </div> 
                    <div class="form-group">
                      <label for="ppn">PPN *</label>
                        <input type="text" class="form-control" id="ppn" name="ppn" placeholder="PPN" value="10" required>
                    </div>                      
                    <div class="form-group">
                      <label for="pph">PPH</label>
                        <select class="form-control" name="pph" id="pph">
                          <option value="0"> - </option>
                          <option value="0.300">0,300%</option>
                        </select>
                    </div> 
                    <div class="form-group">
                        <label for="pbbkb">PBBKB</label>
                         <select name="pbbkb" class="form-control" id='pbbkb'>
                          <option value="0"> - </option>
                          <option value="0.858">17,17% dari 5%</option>
                          <option value="1.288">1,288%</option>
                          <option value="4.5">90% dari 5%</option>
                          <option value="5">100% dari 5%</option>
                          <option value="7.5">150% dari 5%</option>
                          <option value="10">100% dari 10%</option>
                         </select>
                    </div>
                    <div class="form-group">
                      <label for="add_cost">Add Cost</label>
                        <input type="text" class="form-control" id="add_cost" name="add_cost" placeholder="Add Cost">
                    </div>                 
                    <div class="form-group">
                      <label for="payment_term">Payment Term</label>
                        <input type="text" class="form-control" id="payment_term" name="payment_term" placeholder="Payment Term">
                    </div>
                    <div class="form-group">
                      <label for="prepared_by">Disiapkan Oleh : *</label>
                        <input type="text" class="form-control" id="prepared_by" name="prepared_by" placeholder="Disiapkan Oleh" required>
                    </div>
                    <div class="form-group">
                      <label for="approved_by_client">Disetujui Oleh : *</label>
                        <input type="text" class="form-control" id="approved_by_client" name="approved_by_client" placeholder="Disetujui Oleh" required>
                    </div>    
                    <div class="form-group">
                      <label for="notes">Note</label>
                        <input type="text" class="form-control" id="notes" name="notes" placeholder="Note">
                    </div>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </form> 
            </div> 
          </div> 
        </section>
      </div> 
@endsection 
@section('javascript')
<script src="{{ url('plugins/select2/select2.full.min.js')}}"></script>
<script>
  $(function () 
    {
      //Initialize Select2 Elements
      $(".select2").select2();
    });

  function fetchDataAddress(elem){
    var customer = $(elem).val();

    $.ajax({
      url:"{{ url('customer/getAddress') }}?customer_id="+customer,
      type:"get",
      dataType:"json",
      beforeSend:function(){

      },
      success:function(result){
        if(result.status = "ok"){
          var element = $("#alamat_perusahaan");
          var html = "<option value=''>Pilih Alamat</option>";

          $.each(result.data, function(index, value){
            html += "<option value='"+value.id+"'>"+value.address+"</option>";
          });

          element.html(html);
        }else{
          var html = "<option value=''>Silahkan Pilih Konsumen</option>";
          element.html(html);
        }
      },
      error:function(){

      },
      complete:function(){

      }
    });
  }
</script>
@endsection
