@extends('layouts.app')

@section('css')

@endsection
  
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>  
        Data 
        <small>Po Masuk</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Po Masuk</li>
      </ol> 
    </section>
 
    <!-- Main content -->
      <!-- Main content --> 
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
              <div style="float: right">
                <button class="btn btn-primary btn-xs" onclick="window.location='<?php echo url('po-masuk/create'); ?>'">
                  <i class="fa fa-plus"></i> Tambah
                </button> 
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form method="get" action="#">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Start Date</label>
                    <input type="date" name="sdate" id="sdate" class="form-control" value="{{ $sdate }}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>End Date</label>
                    <input type="date" name="edate" id="edate" class="form-control" value="{{ $edate }}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Nomor PO</label>
                    <input type="text" name="nomor_po" id="nomor_po" class="form-control" placeholder="filter nomor po" value="{{ $nomor_po }}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Nama Perusahaan</label>
                    <input type="text" name="nama_perusahaan" id="nama_perusahaan" class="form-control" placeholder="filter nama perusahaan" value="{{ $nama_perusahaan }}" value="{{ $nama_perusahaan }}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Nama Penyalur</label>
                    <input type="text" name="po_tujuan" id="po_tujuan" class="form-control" placeholder="filter nama supplier" value="{{ $po_tujuan }}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Pic Po Tujuan</label>
                    <input type="text" name="pic_po_tujuan" id="pic_po_tujuan" class="form-control" placeholder="filter Pic Po Tujuan" value="{{ $pic_po_tujuan }}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <button type="submit" class="form-control btn btn-danger">Filter</button>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <a href="{{ url('po-masuk') }}" class="form-control btn btn-default">Clear</a>
                  </div>
                </div>
              </div>
              </form>
              <div class="table-responsive">
              <table id="datatable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th style="width: 7%;">Tanggal</th>
                  <th style="width: 12%;">Nomor PO</th>    
                  <th style="width: 16%;">Nama Perusahaan</th>
                  <th style="width: 21%;">Alamat Perusahaan</th>      
                  <th style="width: 7%;">Tanggal Kirim</th> 
                  <th style="width: 10%;">Nama Penyalur</th>                          
                  <th style="width: 10%;">PIC PO Tujuan</th>
                  <!-- <th>Telp PO Tujuan</th>
                  <th style="width: 41.01px;">Nomor Fax Tujuan</th>    -->  
                  <th style="width: 7%;">Status</th>
                  <th style="width: 92px;"><i class="fa fa-ellipsis-h"></i></th>
                </tr>
                </thead>
                <tbody>
                  @foreach($po_masuk as $val)
                  <tr>
                    <td>{{ date('d M Y', strtotime($val->tanggal_po)) }}</td>
                    <td>{{ $val->nomor_po }}</td>
                    <td>{{ $val->nama_perusahaan }}</td>
                    <td>{{ $val->alamat_perusahaan }}</td>
                    <td>{{ $val->tanggal_kirim }}</td>                 
                    <td>{{ $val->po_tujuan}}</td>   
                    <td>{{ $val->pic_po_tujuan }}</td>
                    <!-- <td>{{ $val->telp_po_tujuan }}</td>
                    <td>{{ $val->fax_po_tujuan }}</td>  -->
                    <th> 
                      @if($val->status == 1)
                        Approved
                      @elseif($val->status == -1)
                        Rejected
                      @else
                        Pending
                      @endif 
                    </th>
                    <td>  
                      <a href="{{ url('po-masuk/detail-po-masuk/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-server"></i> Detail PO</a>   
                      @if($val->status == 0)
                      <a href="{{ url('po-masuk/send-email/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-envelope"></i> Send Email</a>
                      <a href="{{ url('po-masuk/edit/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> Edit PO</a>
                      <a href="{{ url('po-masuk/add-item/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Edit Item</a>  
                      <a class="btn btn-danger btn-xs" href="{{ url('po-masuk/delete/'.$val->id) }}" onclick="event.preventDefault();if(confirm('Hapus Po Masuk ?')){ $('#form-{{ $val->id }}').submit() }"><i class="fa fa-trash"></i> Hapus Item</a>
                      <form action="{{ url('po-masuk/delete/'.$val->id) }}" method="post" id="form-{{ $val->id }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="delete">
                      </form>
                      @endif
                      
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              </div>
             {{ $po_masuk->appends(['nama_perusahaan' => $nama_perusahaan,'nomor_po' => $nomor_po,'po_tujuan' => $po_tujuan,'pic_po_tujuan' => $pic_po_tujuan])->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('javascript')
@endsection