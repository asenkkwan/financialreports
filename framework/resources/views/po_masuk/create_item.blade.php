@extends('layouts.app')
@section('css')
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
<!-- DataTables -->
<link rel="stylesheet" href="{{ url('plugins/datatables/dataTables.bootstrap.css') }}">
@endsection
   
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Item
        <small>{{ $po_masuk->nama_perusahaan}}</small>
      </h1> 
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Po Masuk</a></li>
        <li class="active">Add Item</li>
      </ol>
    </section> 
    <!-- Main content --> 
    <section class="invoice">
      <!-- Table row -->
      <div class="row" style="">
        <div class="col-xs-12 table-responsive">
          @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif
          <form action="{{ url('po-masuk/add-item') }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $po_masuk->id }}" name="id_po_masuk">
            <table class="table table-striped">
              <thead>
              <tr> 
                <th style="width:3%;"><i class="fa fa-ellipsis-v"></i></th>
                <th style="width:11%;">Material</th>
                <th style="width:22%;">Description</th>
                <th style="width:7%;">UoM</th>
                <th style="width:8%;">Quantity</th>
                <th style="width:8%;">Harga Dasar</th>
                <!-- <th style="width:8%;">Amount</th> -->
              </tr>
              </thead>
              <tbody id="item-body">
                @if(count($item_po_masuk) > 0)
                  @foreach($item_po_masuk as $val) 
                     <tr>
                      <td onclick="deleteItemField(this)"><i class="fa fa-trash"></i></td>
                      <td><input type="text" name="material[]" id="material" value="{{ $val->material }}" class="form-control"></td>
                      <td><input type="text" name="description[]" id="description" value="{{ $val->description }}" class="form-control"></td>
                      <td><input type="text" name="uom[]" id="uom" value="{{ $val->uom }}" class="form-control"></td>
                      <td><input type="text" name="quantity[]" id="quantity" value="{{ $val->qty }}" class="form-control"></td>
                      <td><input type="text" name="unit_price[]" id="unit_price" value="{{ $val->unit_price }}" class="form-control"></td>
                      <!-- <td><input type="text" name="amount[]" id="amount" value="{{ $val->amount }}" class="form-control"></td> -->
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
            <div style="text-align:right;">
              <button class="btn btn-success" type="button" onclick="addItemField()">Tambah Item Baru</button>
              <button class="btn btn-info" type="submit">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </section>
    <div class="clearfix"></div>
  </div>
@endsection
@section('javascript') 

<script>
  
  function addItemField(){
    var body = jQuery("#item-body");
    var html = "<tr>";
    html += '<td onclick="deleteItemField(this)"><i class="fa fa-trash"></i></td>';
    html += '<td><input type="text" name="material[]" id="material" class="form-control"></td>';
    html += '<td><input type="text" name="description[]" id="description" class="form-control"></td>';
    html += '<td><input type="text" name="uom[]" id="uom" class="form-control"></td>';
    html += '<td><input type="text" name="quantity[]" id="quantity" class="form-control"></td>';
    html += '<td><input type="text" name="unit_price[]" id="unit_price" class="form-control"></td>';
    // html += '<td><input type="text" name="amount[]" id="amount" class="form-control"></td>';
    html += "</tr>";           
    body.append(html);  
  }
  function deleteItemField(elem){
    $(elem).parent().remove();
  }
</script>
@endsection