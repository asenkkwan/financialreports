@extends('layouts.app')
 
@section('css')
   <!-- Select2 -->
  <link rel="stylesheet" href="{{ url('plugins/select2/select2.min.css') }}">
@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header"> 
      <h1>
        Form 
        <small>Edit Po Masuk</small> 
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Edit Po Masuk</li>
      </ol>
    </section>   
 
        <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
          <!-- general form elements -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Form</h3>
            </div>
            <form role="form" action="{{ url('po-masuk/update/'.$po_masuk->id) }}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="_method" value="put">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6"  style="border-right: 1px solid #eee; ">
                    <div class="form-group">
                      <label for="nomor_po">Nomor PO *</label>
                      <input type="text" class="form-control" id="nomor_po" name="nomor_po" placeholder="Masukkan Nomor PO" value="{{ $po_masuk->nomor_po }}">
                    </div>
                    <div class="form-group">
                      <label for="tanggal_po">Tanggal PO *</label>
                      <input type="date" class="form-control" id="tanggal_po" name="tanggal_po" placeholder="Masukkan Tanggal PO" value="{{ $po_masuk->tanggal_po }}">
                    </div>
                    <div class="form-group">
                        <label for="role">Nama Perusahaan *</label>
                        <select class="form-control select2" name="nama_perusahaan" id="nama_perusahaan" onchange="fetchDataAddress(this)" required>
                          <option value="">-- Nama Perusahaan --</option>
                          @foreach($nama_perusahaan as $val) 
                            @if($customer->id == $val->id)
                            <option value="{{ $val->id }}" selected>{{ $val->name }}</option>
                            @else
                            <option value="{{ $val->id }}">{{ $val->name }}</option>
                            @endif
                          @endforeach
                        </select> 
                      </div>
                      <div class="form-group"> 
                        <label for="alamat_perusahaan">Lokasi Perusahaan *</label>
                        <select class="form-control" name="alamat_perusahaan" id="alamat_perusahaan" required>
                          @foreach($alamat_perusahaan as $val)
                            @if($caddress->id == $val->id)
                            <option value="{{ $val->id }}" selected>{{ $val->address }}</option>
                            @else
                            <option value="{{ $val->id }}">{{ $val->address }}</option>
                            @endif
                          @endforeach
                        </select>
                      </div> 
                    <div class="form-group">
                      <label for="contact_person_1">Contact Person 1 *</label>
                        <input type="text" class="form-control" id="contact_person_1" name="contact_person_1"placeholder="Masukkan Nomor Contact Person 1 " value="{{ $po_masuk->contact_person_1 }}" required>
                    </div>
                    <div class="form-group">
                      <label for="contact_person_2">Contact Person 2 *</label>
                       <input type="text" class="form-control" id="contact_person_2" name="contact_person_2" placeholder="Masukkan Nomor Contact Person 2 " value="{{ $po_masuk->contact_person_2 }}" required>
                    </div>
                    <div class="form-group">
                      <label for="tanggal_kirim">Tanggal Kirim</label>
                        <input type="date" class="form-control" id="tanggal_kirim" name="tanggal_kirim" placeholder="Masukkan No Telpon" value="{{ $po_masuk->tanggal_kirim }}">
                    </div> 
                    <div class="form-group">
                        <label for="role">Order To</label>
                          <select class="form-control" name="po_tujuan" id="po_tujuan" required>
                              @foreach($kantor_cabang as $val)
                                @if($val->name == $po_masuk->po_tujuan)
                                <option value="{{ $val->id }}" selected>{{ $val->name }}</option>
                                @else
                                <option value="{{ $val->id }}">{{ $val->name }}</option>
                                @endif
                              @endforeach
                          </select>
                      </div>  
                  </div>                   
                    <div class="col-md-6">  
                      <div class="form-group">
                        <label>PIC PO Tujuan </label>
                          <input type="text" class="form-control" id="pic_po_tujuan" name="pic_po_tujuan"placeholder="Masukkan Nama PIC " value="{{ $po_masuk->pic_po_tujuan }}"required>
                      </div>           
                      <div class="form-group">
                        <label for="ppn">PPN</label>
                          <input type="text" class="form-control" id="ppn" name="ppn" value="{{ (int)$po_masuk->ppn }}" required>
                      </div>                      
                      <div class="form-group">
                        <label for="pph">PPH</label>
                         <select name="pph" class="form-control" id="pph" required>
                          <option value="0" @if($po_masuk->pph=="0") selected @endif>{{App\Utilities\Constant::$PPH['0.000']}}</option>
                          <option value="0.300" @if($po_masuk->pph=="0.300") selected @endif>0,300%</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="pbbkb">PBBKB</label> 
                         <select name="pbbkb" class="form-control" id='pbbkb'>
                          <option value="0" @if($po_masuk->pbbkb=="0") selected @endif>{{App\Utilities\Constant::$PBBKB['0.000']}}</option>
                          <option value="0.858" @if($po_masuk->pbbkb=="0.858") selected @endif>{{App\Utilities\Constant::$PBBKB['0.858']}}</option>
                          <option value="1.288" @if($po_masuk->pbbkb=="1.288") selected @endif>{{App\Utilities\Constant::$PBBKB['1.288']}}</option>
                          <option value="4.5" @if($po_masuk->pbbkb=="4.5") selected @endif>{{App\Utilities\Constant::$PBBKB['4.500']}}</option>
                          <option value="5" @if($po_masuk->pbbkb=="5") selected @endif>{{App\Utilities\Constant::$PBBKB['5.000']}}</option>
                          <option value="7.5" @if($po_masuk->pbbkb=="7.5") selected @endif>{{App\Utilities\Constant::$PBBKB['7.500']}}</option>
                          <option value="10" @if($po_masuk->pbbkb=="10") selected @endif>{{App\Utilities\Constant::$PBBKB['10.000']}}</option>
                         </select>
                      </div> 
                      <div class="form-group">
                        <label for="add_cost">Add Cost *</label>
                          <input type="text" class="form-control" id="add_cost" name="add_cost" value="{{ $po_masuk->add_cost }}" required>
                      </div>   
                      <div class="form-group">
                        <label for="add_cost">Note</label>
                          <input type="text" class="form-control" id="notes" name="notes" placeholder="Note" value="{{ $po_masuk->notes }}">
                      </div>
                      <div class="form-group">
                        <label for="add_cost">Payment Term</label>
                          <input type="text" class="form-control" id="payment_term" name="payment_term" placeholder="Payment Term" value="{{ $po_masuk->payment_term }}">
                      </div>     
                       <div class="form-group">
                        <label for="add_cost">Disiapkan Oleh : *</label>
                          <input type="text" class="form-control" id="prepared_by" name="prepared_by" placeholder="Disiapkan Oleh" value="{{ $po_masuk->prepared_by }}" required>
                      </div>
                       <div class="form-group">
                        <label for="add_cost">Disetujui Oleh : *</label>
                          <input type="text" class="form-control" id="approved_by_client" name="approved_by_client" placeholder="Disetujui Oleh" value="{{ $po_masuk->approved_by_client }}" required>
                      </div>                 
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button action="{{ url('po-masuk/add-item'.$po_masuk->id) }}" type="submit" class="btn btn-success pull-right">Simpan</button>
              </div>
            </form>
          </div>
      </div>
    </section>
   </div>
@endsection
@section('javascript')
<!-- Select2 -->
<script src="{{ url('plugins/select2/select2.full.min.js')}}"></script>
<script>
  $(function () 
    {
      //Initialize Select2 Elements
      $(".select2").select2();

      $('#datatable').DataTable({
        "columnDefs": [{ "orderable": false, "targets": 6 }]
      });
    });

  function fetchDataAddress(elem){
    var customer = $(elem).val();

    $.ajax({
      url:"{{ url('customer/getAddress') }}?customer_id="+customer,
      type:"get",
      dataType:"json",
      beforeSend:function(){

      },
      success:function(result){
        if(result.status = "ok"){
          var element = $("#alamat_perusahaan");
          var html = "<option value=''>Pilih Alamat</option>";

          $.each(result.data, function(index, value){
            html += "<option value='"+value.id+"'>"+value.address+"</option>";
          });

          element.html(html);
        }else{
          var html = "<option value=''>Silahkan Pilih Konsumen</option>";
          element.html(html);
        }
      },
      error:function(){

      },
      complete:function(){

      }
    });
  }
</script>
@endsection