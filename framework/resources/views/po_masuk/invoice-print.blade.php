
<!DOCTYPE html>
<html>
  <head>
    <link rel="shortcut icon" href="{{ url( 'dist/img/logoFR.png') }}"> 
    <meta charset="utf-8">  
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Financial Reports  | Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 --> 
    <link rel="stylesheet" href="{{ url('bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('dist/css/AdminLTE.min.css') }}">
      
      <style> 
        body{
                margin:auto;
                font-size: 12px !important; 
              }  
               page[size="A4"] {
                background: white;
                width: 21cm;
                height: 29.7cm;
                display: block;
                margin: 0 auto;
                margin-bottom: 0.5cm;
                box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
              }
               .kop_surat {
                  position: absolute;
                  width: 21cm;  
                  margin-top: -45px;
                }
              .invoice {
                  position: relative;
                   background: none; 
                  border: 0px solid #f4f4f4;
              }
               .table-bordered>thead>tr>th,{
                border: 1px solid #060606;
                }
              @media print {
                body, page[size="A4"] {
                  margin: 0;
                  box-shadow: 0;
                }
                .table-bordered>thead>tr>th,{
                border: 1px solid black;
                }
          }
      </style>
  </head>
<body >
<?php
 $image ="";
  if($company->kop_surat){
    $image = unserialize($company->kop_surat);
    if($image){
      $image = url('dist/img/')."/".$image['original'];
    }
  }
?>                    
  <page size="A4" layout="portrait">
    <img src='{{ $image }}' class="kop_surat">
<body onload="window.print();">
<div class="wrapper">
    <!-- Main content -->
    <section class="invoice" >
      <div class="row" style="margin-top: 150px; ">
        <div class="col-xs-12">
          <table border="0" style="width:101%">
            <tr>
              <td colspan="3" align="center">
                <font size="5">Order Pembelian</font><br>
                (Purchase Order)
              </td>
            </tr>
            <tr>
              <td style="width: 20%">
                <b>Kepada (Order To)</b><br>
                {{ $po_masuk->po_tujuan}}<br>
                Telp/Fax : {{ $po_masuk->telp_po_tujuan }}<br>
                Up : {{ $po_masuk->pic_po_tujuan }}
              </td>
              <td style="width: 20%"></td>
              <td style="width: 60%">
                Nomor PO (Order No) : {{ $po_masuk->nomor_po }}<br>
                Tanggal PO (Order Date) : {{ date('d M Y', strtotime($po_masuk->tanggal_po)) }}<br>
                Tanggal Kirim (Delivery Date) : {{ date('d M Y', strtotime($po_masuk->tanggal_kirim)) }}<br>
                Mata Uang : IDR<br>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 30%">
                <b>Delivery Office</b><br>
               {{ $po_masuk->alamat_perusahaan}}<br>
              </td>
              <td style="width: 40%"></td>
              <td style="width: 30%">
                Contact Person 1 : {{ $po_masuk->contact_person_1 }}<br>
                Contact Person 2 : {{ $po_masuk->contact_person_2 }}<br>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row" style="margin-top: 20px;">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped table-bordered">
            <thead>
            <tr>
              <th style="width:5%;">No</th>
              <th style="width:9%;">Material</th>
              <th style="width:22%;">Description</th>
              <th style="width:7%;">UoM</th>
              <th style="width:8%;">Quantity</th>
              <th style="width:8%;">Unit Price</th>
              <th style="width:8%; text-align:center;">Amount</th>
            </tr>
            </thead>
            <tbody> 
              <?php 
                $total_amount = 0;
                $counter = 1;
              ?> 
              @foreach($item_po_masuk as $val)
              <tr>
                <td>{{ $counter }}</td>
                <td>{{ $val->material }}</td>                  
                <td>{{ $val->description }}</td>
                <td>{{ $val->uom }}</td>
                <td style="text-align:right;">{{ $val->qty }}</td>
                <td style="text-align:right;">{{ number_format($val->unit_price, 6, ",", ".") }}</td>
                <td style="text-align:right;">{{ number_format($val->amount, 0, ",", ".") }}</td>
              </tr>
              <?php 
                $counter++;
                $total_amount += (int)$val->amount;
              ?>
              @endforeach
              <?php
                $ppn = $total_amount*((int)$po_masuk->ppn/100);
                $pph = 0;
                if($po_masuk->pph != 0.000){
                  $pph = $total_amount*($po_masuk->pph/100);
                }
                $grandTotal = $total_amount + $ppn + (int)$pph + (int)$po_masuk->add_cost;
                $grandTotal = ceil($grandTotal);
              ?>
              <tr>
                <td rowspan="3" colspan="5" >
                  <p>Payment Term : {{ $po_masuk->payment_term }}</p>
                </td>
                <th>Subtotal:</th>
                <td style="text-align:right;">{{ number_format($total_amount, 0, ",", ".") }}</td>
              </tr>
              <tr>
                <th>PPN</th>
                <td style="text-align:right;" value="ppn" name="ppn">{{ number_format($ppn, 0, ",", ".") }}</td>
              </tr>
              <tr>
                <th>PPH</th>
                <td style="text-align:right;">{{ number_format($pph, 2, ",", ".") }}</td>
              </tr>
              <tr>
                <td rowspan="2" colspan="5" >
                  <p>Terbilang :</p>
                  <p>{{ terbilang($grandTotal, 1) }} RUPIAH</p>
                </td>
                <th>Add Cost</th>
                <td style="text-align:right;">{{ number_format($po_masuk->add_cost, 2, ",", ".") }}</td>
              </tr>

              <tr>
                <th>TOTAL</th>
                <td style="text-align:right;">{{ number_format($grandTotal, 0, ",", ".") }}</td>
              </tr>
              <tr>
                <td colspan="7">Note : {{ $po_masuk->notes }}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-12">
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Terima kasih atas perhatian Saudara, Harap referensi Nomor PO diatas dicantumkan dalam setiap dokumen yang saudara kirim sehubungan dengan Order Pembelian ini, dan menginformasikan kembali order Pembelian ini dengan email dalam waktu 7 hari setelah tanggal Order Pembelian. Apabila kami tidak menerima konfirmasi tersebut, maka sebagai konsekuensinya kami menganggap Saudara telah menyetujui Order Pembelian tersebut.
          </p>
        </div>
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-12">
          <table border="0" style="width: 100%">
            <tr>
              
              <td style="width: 30%">
                <strong>Prepared By,</strong><br>
                <br><br><br><br><br><br>
                {{ strtoupper($po_masuk->prepared_by) }}<br>
              </td>
              <td style="width: 50%"></td>
              <td style="width: 20%">
                <strong>Approved By,</strong><br>
                {{ strtoupper($po_masuk->nama_perusahaan) }}<br>
                <br><br><br><br><br>
                <!-- {{ strtoupper($po_masuk->approved_by_client) }}<br> -->
              </td>
            </tr>
          </table>
        </div>
      </div>
       <div class="row" align="center">
        @if($company->id ==1)
        <div class="footer" style="    margin-top: 65px;" >
         <p style="color:#176db9 !important;">Jl. Turi Jaya Rt. 08/07 No. 34 Ds. Segaramakmur Kec. Tarumajaya - Bekasi 17211<br> tlp. (021) 88992082 - 88992083 Fax. (021) 443830 <br> <strong style="color:red !important;">E-mail : audrilutfiajaya@ymail.com </strong></p>
        </div>
        @else($company->id ==2)
        <div class="footer" style="    margin-top: 130px;" >
          <p style="color:#176db9 !important;">Jl. Turi Jaya Rt.10/07 Ds. Segaramakmur Kec. Tarumajaya - Bekasi <br> Telp. (021) 88992082 <strong style="color:red !important;">Email.Sumaadijaya.com</strong></p>
        </div>
        @endif
      </div>
    </section>
    <!-- /.content -->
    <!-- /.content -->
</div>
</body>
</html>
