@extends('layouts.app')

@section('css')
@endsection
  
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> 
        Total Piutang 
      </h1>
    </section> 
  
    <!-- Main content -->
      <!-- Main content -->   
    <section class="content"> 
      <div class="row">
        <div class="col-xs-12"> 
          @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <form method="get" action="#" id="form-filter">
              <div class="row">
               <!--  <div class="col-md-2">
                  <div class="form-group">
                    <label>Start Date</label>
                    <input type="date" name="sdate" id="sdate" class="form-control" value="{{ $sdate }}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>End Date</label>
                    <input type="date" name="edate" id="edate" class="form-control" value="{{ $edate }}">
                  </div>
                </div> -->
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Nama Perusahaan</label>
                    <input type="text" name="nama_perusahaan" id="filter_nama_perusahaan" class="form-control" placeholder="filter nama perusahaan" value="{{ $nama_perusahaan }}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <button type="submit" class="form-control btn btn-danger" type='button' onclick='filter()'>Filter</button>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <a href="{{ url('piutang') }}" class="form-control btn btn-default">Clear</a>
                  </div>
                </div>
                 <div class="col-md-2" style="float: right;">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <button style="margin-right: 335px;" class='btn btn-default pull-right' type='button' onclick='exportExcel()'>Export Excel</button>
                  </div>
                </div>
              </div>
              
              </form>
              <br>
              <div class="table-responsive">
              <table id="datatable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Perusahaan</th> 
                  <th>Quantity</th> 
                  <th>Total Tagihan</th>
                  <th>Total Pembayaran</th>
                  <th>Sisa Tagihan</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                    $total_amount = 0;
                    $total_qty = 0;
                    $total_tagihan = 0;
                    $total_bayar = 0;
                    $counter = 1;
                  ?>
                  @foreach($penjualan as $val) 
                  <tr>
                    <td>{{ $counter }}</td>
                    <td>
                     @if($val->nama_perusahaan)
                        <a href="{{url('penjualan?nama_perusahaan='.$val->nama_perusahaan)}}">{{ $val->nama_perusahaan }}</a>
                        @else
                       {{ $val->nama_perusahaan }}}}
                    @endif</td>
                    <td>{{ number_format($val->total_qty, 0, ",", ".") }}
                    <td>{{ number_format($val->total_tagihan, 0, ",", ".") }}</td>
                    <td>{{ number_format($val->total_bayar, 0, ",", ".") }}</td>
                    <td>{{ number_format(($val->total_tagihan - $val->total_bayar), 0, ",", ".") }}</td>
                  </tr>
                  <?php 
                    $total_qty += $val->total_qty;
                    $total_tagihan += $val->total_tagihan;
                    $total_bayar += $val->total_bayar;
                     $counter++;
                  ?>
                  @endforeach 
                  <tr style="background: #fff;text-align: center">
                    <td colspan="2"><b>TOTAL</b></td>
                    <td>{{ number_format($total_qty, 0, ",", ".") }}
                    <td>{{ number_format($total_tagihan, 0, ",", ".") }}</td>
                    <td>{{ number_format($total_bayar, 0, ",", ".") }}</td>
                    <td>{{ number_format(($total_tagihan - $total_bayar), 0, ",", ".") }}</td>
                 </tr>
                 <tr style="background: #eee;text-align: right">
                    <td colspan="2"><b>TOTAL ALL</b></td>
                    <td>{{ number_format($total_all_qty, 0, ",", ".") }}</td>
                    <td>{{ number_format($total_all_tagihan, 0, ",", ".") }}</td>
                    <td>{{ number_format($total_all_bayar, 0, ",", ".") }}</td>
                    <td>{{ number_format($total_all_tagihan - $total_all_bayar, 0, ",", ".") }}</td>
                 </tr> 
                </tbody>
                 
              </table>
              </div>
              {{ $penjualan->appends(['nama_perusahaan' => $nama_perusahaan,])->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('javascript')
<script>
  function exportExcel(){
    var form = $("#form-filter");
    form.attr("action","{{ url('piutang/export') }}")
    form.submit();
  }
  function filter(){
    var form = $("#form-filter");
    form.attr("action","#")
    form.submit();
  }
</script>
@endsection
