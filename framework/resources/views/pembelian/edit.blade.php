@extends('layouts.app')
@section('css')
   <!-- Select2 -->
  <link rel="stylesheet" href="{{ url('plugins/select2/select2.min.css') }}">
@endsection 
   
@section('content') 
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> 
        Form
        <small>Edit Pembelian </small>
      </h1> 
  </section>
  <!-- Main content --> 
  <section class="content">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12">
         @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger"> 
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border"> 
            <h3 class="box-title">Form</h3>
          </div>
          <!-- /.box-header -->  
         
              <form role="form" action="{{ url('pembelian/'.$pembelian->id) }}" method="post" enctype="multipart/form-data">
                 <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
                <!-- text input -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid #eee; ">
                      <div class="form-group">
                        <label for="no_invoice">No Invoice</label>
                        <input type="text" class="form-control" id="no_invoice" name="no_invoice" value="{{ $pembelian->no_invoice }}" required>
                      </div> 
                      <div class="form-group">
                        <label for="no_faktur_pajak">No Invoice</label>
                        <input type="text" class="form-control" id="no_faktur_pajak" name="no_faktur_pajak" value="{{ $pembelian->no_faktur_pajak }}" required>
                      </div>
                      <div class="form-group">
                        <label for="no_po">No Po</label>
                        <input type="text" class="form-control" id="no_po" name="no_po" value="{{ $pembelian->no_po }}" readonly  required>
                      </div>
                      <div class="form-group">
                        <label for="tanggal">Tanggal</label>
                        <input type="date" class="form-control" id="tanggal" name="tanggal" value="{{ $pembelian->tanggal }}" readonly  required>
                      </div>
                      <div class="form-group">
                        <label for="nama_perusahaan">Nama Perusahaan *</label>
                        <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="{{ $pembelian->nama_perusahaan }}" readonly required>
                      </div>
                      <div class="form-group"> 
                        <label for="lokasi_perusahaan">Alamat Perusahaan *</label>
                        <input type="text" class="form-control" id="lokasi_perusahaan" name="lokasi_perusahaan" value="{{ $pembelian->lokasi_perusahaan }}" readonly required>
                      </div> 
                      <div class="form-group">
                        <label for="nama_supplier">Nama Company * </label>
                        <input type="text" class="form-control" id="nama_supplier" name="nama_supplier" value="{{ $pembelian->nama_supplier }}" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="produk">Jenis BBM *</label>
                         <input type="text" class="form-control" id="produk" name="produk" value="{{ $pembelian->produk }}" readonly required>
                      </div> 
                      <div class="form-group">
                        <label for="qty">QTY *</label>
                         <input type="number" class="form-control" name="qty" id="qty" value="{{ $pembelian->qty }}" readonly required></input>
                      </div> 
                      <div class="form-group">
                        <label for="harga_beli">Harga Beli *</label>
                         <input type="number" class="form-control" name="harga_beli" id="harga_beli"  value="{{ $pembelian->harga_beli }}" readonly required></input>
                      </div>
                      <div class="form-group">
                        <label for="harga_jual">Harga Jual *</label>
                         <input type="number" class="form-control" name="harga_jual" id="harga_jual" value="{{ $pembelian->harga_jual }}" readonly required></input>
                      </div>
                      <div class="form-group">
                        <label for="harga_dasar">Harga Dasar *</label>
                         <input type="number" class="form-control" name="harga_dasar" id="harga_dasar" value="{{ $pembelian->harga_dasar }}" readonly required></input>
                      </div>
                      <div class="form-group">
                        <label for="pbbkb">PBBKB</label>
                        <input type="hidden" name="pbbkb" value="{{ $pembelian->pbbkb }}" hidden>
                        <input type="text" class="form-control" name="pbbkb2" id="pbbkb2" value="{{App\Utilities\Constant::$PBBKB[$pembelian->pbbkb] }}" readonly required></input>
                      </div>  
                      <div class="form-group">
                        <label for="ppn">PPN</label>
                         <input type="number" class="form-control" name="ppn" id="ppn" value="{{ $pembelian->ppn }}" readonly></input>
                      </div>
                    </div>
                  </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </form> 
            </div> 
          </div> 
        </section>
      </div>
@endsection 
@section('javascript')
<!-- Select2 -->
<script src="{{ url('plugins/select2/select2.full.min.js')}}"></script>
<script>
  $(".select2").select2();

  function openForm(elem){
    var data = $(elem).val();
    window.location = "{{ url('/pembelian/create?filter_po_keluar=') }}"+data;
  }
   function fetchDataAddress(elem){
    var customer = $(elem).val();

    $.ajax({
      url:"{{ url('customer/getAddress') }}?customer_id="+customer,
      type:"get",
      dataType:"json", 
      beforeSend:function(){

      },
      success:function(result){
        if(result.status = "ok"){
          var element = $("#lokasi_perusahaan");
          var html = "<option value=''>Pilih Alamat</option>";

          $.each(result.data, function(index, value){
            html += "<option value='"+value.id+"'>"+value.address+"</option>";
          });

          element.html(html);
        }else{
          var html = "<option value=''>Silahkan Pilih Konsumen</option>";
          element.html(html);
        }
      },
      error:function(){

      },
      complete:function(){

      }
    });
  }
</script>
@endsection
