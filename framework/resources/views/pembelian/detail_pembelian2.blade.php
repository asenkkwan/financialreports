@extends('layouts.app')
 
@section('css')

@endsection
   
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Purchase Order
        <small>{{ $pembelian->no_po}}</small>  
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Detail</a></li>
        <li class="active">Penjualan</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="invoice">

      <!-- info row -->
       <div class="row">
        <div class="col-xs-12 invoice-col" style="text-align:center;">
          <table class="table">
            <tr>
              <td align="center">
                <font size="6">Order Pembelian</font><br>
                (Purchase Order)
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <table border="0" style="width:100%">
            <tr>
              <td style="width: 30%">
                <b>Kepada (Order To)</b><br>
                {{ $po_keluar->nama_konsumen}}<br>
                <!-- Telp/Fax : {{ $po_keluar->no_sold_penyalur }}<br> -->
                Up : {{ $po_keluar->nama_pic_agen }}
              </td>
              <td style="width: 40%"></td>
              <td style="width: 30%">
                Nomor PO (Order No) : {{ $po_keluar->nomor_po }}<br>
                Nomor Faktur Pajak (Order No) : {{ $pembelian->no_faktur_pajak }}<br>
                Tanggal PO (Order Date) : {{ date('d F Y', strtotime($po_keluar->tanggal)) }}<br>
               <!--  Tanggal Kirim (Delivery Date) : {{ $pembelian->tanggal_kirim }}<br> -->
                Mata Uang : IDR<br>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 30%">
                <b>Delivery Office</b><br>
               {{ $po_keluar->alamat_konsumen}}<br>
              </td>
              <td style="width: 40%"></td>
              <td style="width: 30%">
               <b>Supply Point</b><br>
               {{ $po_keluar->supply_point}}<br>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row" style="margin-top: 20px;">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped table-bordered">
            <thead>
            <tr>
              <th style="width:5%;">No</th>
              <th style="width:22%;">Description</th>            
              <th style="width:8%;">Quantity</th>
              <th style="width:8%;">Harga Beli</th>
              <th style="width:8%;">Harga Jual</th>
              <th style="width:8%;">Harga Dasar</th>
              <th style="width:8%;">DPP</th>
              <th style="width:8%;">PPN</th>
              <th style="width:8%;">PBBKB Dasar</th>
              <th style="width:8%;">PBBKB Jual</th>
              <th style="width:8%; text-align:center;">Amount</th>
            </tr>
            </thead>
            <tbody> 
              <?php 
                // $total_amount = 0;
                $counter = 1;
              ?>
              @foreach($pembelian as $val)
              <tr>
                <td>{{ $counter }}</td>          
                <td>{{ $pembelian->produk }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->qty, 0, ",", ".") }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->harga_beli) }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->harga_jual, 6, ",", ".") }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->harga_dasar) }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->dpp) }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->jumlah_ppn) }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->pbbkb_dasar) }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->pbbkb_jual) }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->jumlah, 0, ",", ".") }}</td>
              </tr>
             <!-- <?php 
                $counter++;
              ?> -->
            <!--   @endforeach  -->
             
              <tr>
                <td rowspan="1" colspan="9" >
                  <p>Payment Term : {{ $pembelian->payment_term }} Hari</p>
                </td>
                <th>TOTAL:</th>
                <td style="text-align:right;">{{ number_format( $pembelian->jumlah, 0, ",", ".") }}</td>
              </tr>
              <tr>               
              </tr>
              <tr>
                <td rowspan="1" colspan="11" >
                  <p>Terbilang :</p>
                  <p>{{ terbilang($pembelian->jumlah, 1) }} RUPIAH</p>
                </td>
                <!-- <th>TOTAL</th>
                <td style="text-align:right;">{{ number_format($pembelian->jumlah, 2, ",", ".") }}</td> -->
              </tr>
              <tr>
                <td colspan="11">Note : {{ $pembelian->notes }}</td>
              </tr>
            </tbody> 
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
       <!--  <div class="col-xs-12">
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Keterangan :<br>
            1. Pembayaran mohon ditransfer atau Cek/Bilyet Giro atas nama :
              PT. AUDRI LUTFIA JAYA No. REK BRI : 0329.01.003702.30.2<br>
            2. Pembayaran dianggap sah bila Cek/Bilyet Giro yang diserahkan telah selesai di Kliring/Cair.
           
          </p>
        </div> -->
      </div>
      <!-- /.row -->

      <!-- <div class="row">
        <div class="col-xs-12">
          <table border="0" style="width: 100%">
            <tr>
              <td style="width: 30%">
                <strong>Prepared By,</strong><br>
                <br><br><br><br><br><br>
                {{ strtoupper($pembelian->prepared_by) }}<br>
              </td>
              <td style="width: 50%"></td>
              <td style="width: 20%">
                <strong>Approved By,</strong><br>
                {{ strtoupper($po_keluar->penyalur) }}<br>
                <br><br><br><br><br>
                {{ strtoupper($pembelian->approved_by_client) }}<br>
              </td>
            </tr>
          </table>
          
        </div>
      </div> -->
      <!-- this row will not appear when printing -->
      <div class="row" style="margin-top: 20px;">
        <div class="col-xs-12">
          <a href="{{ url('pembelian/invoice-print/'.$pembelian->id) }}"  target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print PO</a>
          <!-- <a href="{{ url('pembelian/kwitansi-print/'.$pembelian->id) }}"  target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print Kwitansi</a> -->
          <!-- @if($pembelian->status == 1)
            @if($pembelian->invoice_status == 1)
            <button type="button" class="btn btn-info pull-right"><i class="fa fa-credit-card"></i> Show Invoice </button>
            @else
            <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Generate Invoice </button>
            @endif
          @else
            @if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin')
             <button type="button" class="btn btn-success pull-right" onclick="approve({{ $pembelian->id }})"><i class="fa fa-credit-card"></i> Approve</button>
            <button type="button" class="btn btn-danger pull-right" onclick="reject({{ $pembelian->id }})" style="margin-right: 5px;"><i class="fa fa-credit-card"></i> Reject</button>
            <form id="form-approval" action="{{ url('pembelian/approve') }}" method="post">
               {{ csrf_field() }}
               <input type="hidden" name="pembelian_id" value="{{ $pembelian->id }}">
             </form>
             <form id="form-reject" action="{{ url('pembelian/reject') }}" method="post">
               {{ csrf_field() }}
               <input type="hidden" name="pembelian_id" value="{{ $pembelian->id }}">
             </form>
            @endif
          @endif   -->     
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
@endsection
@section('javascript')
<script>
  function approve(id){
    if(confirm("Apakah anda menyetujui pembelian ini?")){
      $("#form-approval").submit();
    }
  } 

  function reject(id){
    if(confirm("Apakah anda menyetujui pembelian ini?")){
      $("#form-reject").submit();
    }
  } 
</script>
@endsection