@extends('layouts.app')

@section('css')
@endsection
  
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> 
        Pembelian BBM
      </h1>
    </section>

    <!-- Main content -->
      <!-- Main content --> 
    <section class="content">
      <div class="row"> 
        <div class="col-xs-12">
          @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!}  
            </div>
          @endif
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
              <div style="float: right">
                <button class="btn btn-primary" onclick="window.location='<?php echo url('pembelian/create'); ?>'">
                  <i class="fa fa-plus"></i> Tambah
                </button> 
              </div>
            </div>
            <!-- /.box-header -->
          <div class="box-body">
             <form method="get" action="#" id="form-filter">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Start Date</label>
                    <input type="date" name="sdate" id="sdate" class="form-control" value="{{ $sdate }}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>End Date</label>
                    <input type="date" name="edate" id="edate" class="form-control" value="{{ $edate }}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>No PO</label>
                    <input type="text" name="no_po" id="no_po" class="form-control" placeholder="filter no po" value="{{ $no_po }}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Nama Perusahaan</label>
                    <input type="text" name="nama_perusahaan" id="nama_perusahaan" class="form-control" placeholder="filter nama perusahaan" value="{{ $nama_perusahaan }}" value="{{ $nama_perusahaan }}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Nama Supplier</label>
                    <input type="text" name="nama_supplier" id="nama_supplier" class="form-control" placeholder="filter nama supplier" value="{{ $nama_supplier }}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Produk</label>
                    <input type="text" name="produk" id="produk" class="form-control" placeholder="filter produk" value="{{ $produk }}">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <button type="submit" class="form-control btn btn-danger" type='button' onclick='filter()'>Filter</button>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <a href="{{ url('pembelian') }}" class="form-control btn btn-default">Clear</a>
                  </div>
                </div>
                <div class="col-md-2" style="float: right;">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <button class='btn btn-default pull-right' type='button' onclick='exportExcel()'>Export Excel</button>
                  </div>
                </div>
              </div>
              </form>
              <div class="table-responsive">
              <table id="datatable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Tanggal</th>
                  <th>Nama Perusahaan</th>
                  <th>Lokasi Perusahaan</th>
                  <th>Nama Supplier</th>
                  <th>Produk</th>
                  <th>No PO</th>
                  <th>Qty</th>           
                  <th>Harga Beli</th>                             
                  <th>Harga Jual</th>
                  <th>Harga Dasar</th>
                  <th>DPP</th>
                  <th>PPN</th>
                  <th>PBBKB Dasar</th>
                  <th>PBBKB Jual</th>
                  <th>Jumlah</th>
                  <th><i class="fa fa-ellipsis-h"></i></th>
                </tr>
                </thead>
                <tbody>
                  @foreach($pembelian as $val)
                  <tr>
                    <td>{{ date('d M Y', strtotime($val->tanggal)) }}</td>
                    <td>{{ $val->nama_perusahaan }}</td>
                    <td>{{ $val->lokasi_perusahaan }}</td>
                    <td>{{ $val->nama_supplier }}</td>
                    <td>{{ $val->produk }}</td>
                    <td>{{ $val->no_po }}</td> 
                    <td>{{ number_format($val->qty, 0, ",", ".") }}</td>
                    <td>{{ number_format($val->harga_beli, 3, ",", ".") }}</td>  
                    <td>{{ number_format($val->harga_jual, 3, ",", ".") }}</td>
                    <td>{{ number_format($val->harga_dasar, 3, ",", ".") }}</td>
                    <td>{{ number_format($val->dpp, 0, ",", ".") }}</td>
                    <td>{{ number_format($val->jumlah_ppn, 0, ",", ".") }}</td>
                    <td>{{ number_format($val->pbbkb_dasar, 3, ",", ".") }}</td>
                    <td>{{ number_format($val->pbbkb_jual, 0, ",", ".") }}</td>
                    <td>{{ number_format($val->jumlah, 0, ",", ".") }}</td>
                    <td>
                      <a href="{{ url('pembelian/'.$val->id) }}" class="btn btn-success btn-xs"><i class="fa fa-server"></i> Detail </a>  
                      <a href="{{ url('pembelian/'.$val->id.'/edit/') }}" class="btn btn-success btn-xs">
                        <i class="fa fa-pencil"></i> Edit
                      </a>
                      <a class="btn btn-danger btn-xs" href="{{ url('pembelian/'.$val->id) }}" onclick="event.preventDefault();if(confirm('Hapus Data Pembelian ?')){ $('#form-{{ $val->id }}').submit() }"><i class="fa fa-trash"></i> Hapus</a>
                      <form action="{{ url('pembelian/'.$val->id) }}" method="post" id="form-{{ $val->id }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="delete">
                      </form>
                    </td>
                  </tr>
                  @endforeach
                   <tr style="background: #eee;text-align: center">
                      <td colspan="6"><b>TOTAL ALL</b></td>
                      <td><b>{{ number_format($total_all_qty, 0, ",", ".") }}</b></td>
                      <td><b>{{ (int)$total_all_harga_beli }}</b></td>
                      <td>{{ number_format($total_all_harga_jual, 3, ",", ".") }}</td>  
                      <td>{{ number_format($total_all_harga_dasar, 3, ",", ".") }}</td>
                      <td>{{ number_format($total_all_dpp, 0, ",", ".") }}</td>
                      <td>{{ number_format($total_all_jumlah_ppn, 0, ",", ".") }}</td>
                      <td>{{ number_format($total_all_pbbkb_dasar, 3, ",", ".") }}</td>
                      <td>{{ number_format($total_all_pbbkb_jual, 3, ",", ".") }}</td>
                      <td>{{ number_format($total_all_jumlah, 0, ",", ".") }}</td>
                      <td></td>
                    </tr>
                </tbody>
              </table>
              </div>
              {{ $pembelian->appends(['nama_perusahaan' => $nama_perusahaan,'no_po' => $no_po,'nama_supplier' => $nama_supplier,'produk' => $produk])->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>


@endsection
@section('javascript')
<script>
  function exportExcel(){
    var form = $("#form-filter");
    form.attr("action","{{ url('pembelian/export') }}")
    form.submit();
  }
  function filter(){
    var form = $("#form-filter");
    form.attr("action","#")
    form.submit();
  }
</script>
@endsection
