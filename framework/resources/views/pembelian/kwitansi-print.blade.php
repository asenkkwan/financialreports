<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>A5 landscape</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>
@page { 
  size: A5 landscape 
}
p {
  font-family: helvetica
}
</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A5 landscape">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="sheet padding-0mm">

   <!--  <img src="{{ url('dist/img/kwitansiSumadi.jpeg') }}" style="width: 100%;"> -->
  <div style="z-index: 9999;position: absolute; top: -13px; margin-left: 15px;">
    <p style="margin-left: 200px; margin-top:90px;"><strong></strong> {{ $pembelian->no_invoice}} </p>
    <p style="margin-left: 159px; margin-top:-4px;"> {{ $pembelian->tanggal }}<br> </p>
    <p style=" margin-left: 159px; margin-top:-12px;"> {{ $pembelian->no_po }}</p>  
  </div>
   <div style="z-index: 9999;position: absolute;top: 0; margin-left: 345px; margin-top: 9px;">
    <p style="margin-left: 163px; margin-top:88px;"><strong></strong> {{ $pembelian->nama_perusahaan}} </p>
    <p style="margin-left: 162px; margin-top:-12px;"> {{ $pembelian->lokasi_perusahaan }}<br> </p>
  </div>
  <div style="z-index: 9999;position: absolute;top: 0; margin-left: 190px; margin-top: 92px;">
    <p style="margin-left: 88px; margin-top:88px;"><strong></strong> {{ $pembelian->jenis_kendaraan}} </p>
    <p style="margin-left: 225px; margin-top:-35px;"> {{ $pembelian->nopol }}<br> </p>
  </div>
   <div style="z-index: 9999;position: absolute;top: 0; margin-left: 205px; margin-top: 155px;">
    <p style="margin-left: -130px; margin-top:88px;"><strong></strong>  <?php     
                $counter = 1;
              ?> {{ $counter }}</p>
    <p style="margin-left: -30px; margin-top:-35px;"> {{ $pembelian->produk }}<br> </p>
    <p style="margin-left: 225px; margin-top:-35px;"> {{ $pembelian->qty }} Liter<br> </p>
    <p style="margin-left: 322px; margin-top:-35px;">{{ number_format($pembelian->harga_dasar) }}<br> </p>
    <p style="margin-left: 449px; margin-top:-35px;"> {{ number_format($pembelian->dpp) }}<br> </p>
  </div>
  <div style="z-index: 9999;position: absolute;top: 0; margin-left: 205px; margin-top: 150px;">
    <p style="margin-left: -55px; margin-top:287px; font-size: 11px;"><b> {{ terbilang($pembelian->jumlah, 1) }} </b><br> </p>
    <p style="margin-left: 450px; margin-top:-93px;"> {{ number_format($pembelian->jumlah_ppn) }}<br> </p>
    <p style="margin-left: 449px; margin-top:-15px;"> {{ number_format($pembelian->jumlah) }}<br> </p>
  </div>

  </section>

</body>

</html>