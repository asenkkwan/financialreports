
<!DOCTYPE html>
<html>
<head>
  <link rel="shortcut icon" href="{{ url( 'dist/img/logoFR.png') }}"> 
  <meta charset="utf-8"> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Financial Reports  | Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 --> 
  <link rel="stylesheet" href="{{ url('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('dist/css/AdminLTE.min.css') }}">
  
  <style>  
    body{
            margin:auto;
            font-size: 12px !important; 
          }  
           page[size="A4"] { 
            background: white;
            width: 21cm;
            height: 29.7cm;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
          }
           .kop_surat {
              position: absolute;
              width: 21cm;
              margin-top: -45px;  
            }
          .invoice {
              position: relative;
               background: none; 
              border: 0px solid #f4f4f4;
          }
          @media print {
            body, page[size="A4"] {
              margin: 0;
              box-shadow: 0;
            }
            .invoice {
              position: relative;
               background: none; 
              border: 0px solid #f4f4f4;
          }
      }
  </style>
</head>
<body >
<?php
 $image ="";
  if($company->kop_surat){
    $image = unserialize($company->kop_surat);
    if($image){
      $image = url('dist/img/')."/".$image['original'];
    }
  }
?>                    
<page size="A4" layout="portrait">
<img src='{{ $image }}' class="kop_surat">
<body onload="window.print();">
  <div class="wrapper">
    <!-- Main content -->
    <section class="invoice" >
      <div class="row" style="margin-top: 180px;">
        <div class="col-xs-12">
          <table border="0" style="width:100%">
            <tr>
              <td colspan="3" align="center">
                <font size="5">Order Pembelian</font><br>
                (Purchase Order)
              </td> 
            </tr>
            <tr>
              <td style="width: 30%">
                <b>Kepada (Order To)</b><br>
                {{ $pembelian->nama_perusahaan}}<br>
                <!-- No SH : {{ $po_keluar->no_sold_penyalur }}<br> -->
                Up : {{ $po_keluar->nama_pic_agen }}
              </td>
              <td style="width: 40%"></td>
              <td style="width: 30%">
                Nomor PO (Order No) : {{ $pembelian->no_po }}<br>
                Tanggal PO (Order Date) : {{ date('d M Y', strtotime($pembelian->tanggal)) }}<br>
               <!--  Tanggal Kirim (Delivery Date) : {{ $pembelian->tanggal_kirim }}<br> -->
                Mata Uang : IDR<br>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 30%">
                <b>Delivery Office</b><br>
               {{ $pembelian->lokasi_perusahaan}}<br>
              </td>
              <td style="width: 40%"></td>
              <td style="width: 30%">
              <b>Supply Point</b><br>
               {{ $po_keluar->supply_point}}<br>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row" style="margin-top: 20px;">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped table-bordered">
            <thead>
            <tr>
              <th style="width:5%;">No</th>
              <th style="width:22%;">Description</th>            
              <th style="width:8%;">Quantity</th>
              <th style="width:8%;">Harga Beli</th>
              <th style="width:8%;">Harga Jual</th>
              <th style="width:8%;">DPP</th>
              <th style="width:8%;">PPN</th>
              <th style="width:8%;">PBBKB Dasar</th>
              <th style="width:8%;">PBBKB Jual</th>
              <th style="width:8%; text-align:center;">Amount</th>
            </tr>
            </thead>
            <tbody> 
              <?php 
                // $total_amount = 0;
                $counter = 1;
              ?>
              @foreach($pembelian as $val)
              <tr>
                <td>{{ $counter }}</td>          
                <td>{{ $pembelian->produk }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->qty, 0, ",", ".") }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->harga_beli, 2, ",", ".") }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->harga_jual, 2, ",", ".") }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->pbbkb_dasar, 2, ",", ".") }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->pbbkb_jual, 2, ",", ".") }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->dpp, 2, ",", ".") }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->jumlah_ppn, 2, ",", ".") }}</td>
                <td style="text-align:right;">{{ number_format($pembelian->jumlah, 0, ",", ".") }}</td>
              </tr>
             <!-- <?php 
                $counter++;
              ?>
              @endforeach  -->
              <tr>
                <td rowspan="1" colspan="8" >
                  <p>Payment Term : {{ $pembelian->payment_term }}</p>
                </td>
                <th>TOTAL:</th>
                <td style="text-align:right;">{{ number_format( $pembelian->jumlah, 0, ",", ".") }}</td>
              </tr>
              <tr>               
              </tr>
              <tr>
                <td rowspan="1" colspan="10" >
                  <p>Terbilang :</p>
                  <p>{{ terbilang($pembelian->jumlah, 1) }} RUPIAH</p>
                </td>
                <!-- <th>TOTAL</th>
                <td style="text-align:right;">{{ number_format($pembelian->jumlah, 2, ",", ".") }}</td> -->
              </tr>
              <tr>
                <td colspan="10">Note : {{ $pembelian->keterangan }}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
     <div class="row">
          @if($company->id ==1)
          <div class="col-xs-12">
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
              Keterangan :<br>
              - Pembayaran mohon ditransfer atau Cek/bilyet Giro atas nama : <br> PT. AUDRI LUTFIA JAYA No. Rek. 414.1600.111 BCA KCP. Kramat Jaya Tj. Priok. <br>
              - Pembayaran dianggap sah bila Cek/bilyet Giro yang diserahkan telah selesai di kliring/cair.  <br>
            </p>
          </div>
           @else($company->id ==2)
           <div class="col-xs-12">
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
              Keterangan :<br>
              - Pembayaran mohon ditransfer atau Cek/bilyet Giro atas nama : PT. SUMA ADI JAYA No. Rek. Mandiri 125.007.8138. 138 <br>
              - Pembayaran dianggap sah bila Cek/bilyet Giro yang diserahkan telah selesai di kliring/cair.
            </p>
          </div>
          @endif  
        </div> 
      <!-- /.row -->

    <div class="row">
        <div class="col-xs-12">
          <table border="0" style="width: 100%">
            <tr>
              <td style="width: 40%">
                <strong>Prepared By,</strong><br>
                <br><br><br><br><br><br>
                {{ strtoupper($pembelian->prepared_by) }}<br>
              </td>
              <td style="width: 30%"></td>
              <td style="width: 30%">
                <strong>Approved By,</strong><br>
                <p style="font-size: 11px;">{{ strtoupper($pembelian->nama_perusahaan) }}</p><br>
                <br><br><br><br><br>
                {{ strtoupper($pembelian->approved_by_client) }}<br>
              </td>
            </tr>
          </table>
        </div>
      </div>

      <div class="row" align="center">
        @if($company->id ==1)
        <div class="footer" style="    margin-top: 60px;" >
         <p style="color:#176db9 !important;">Jl. Turi Jaya Rt. 08/07 No. 34 Ds. Segaramakmur Kec. Tarumajaya - Bekasi 17211<br> tlp. (021) 88992082 - 88992083 Fax. (021) 443830  <br> <strong style="color:red !important;">E-mail : audrilutfiajaya@ymail.com </strong></p>
        </div>
        @else($company->id ==2)
        <div class="footer" style="    margin-top: 175px;" >
          <p style="color:#176db9 !important;">Jl. Turi Jaya Rt.10/07 Ds. Segaramakmur Kec. Tarumajaya - Bekasi <br> Telp. (021) 88992082 <strong style="color:red !important;">Email.Sumaadijaya.com</strong></p>
        </div>
        @endif
      </div>
      
    </section>
    <!-- /.content -->
    <!-- /.content -->
</div>
</body>
</html>

