@extends('layouts.app')
 
@section('css')
    <!-- DataTables -->
  <link rel="stylesheet" href="{{('plugins/datatables/dataTables.bootstrap.css')}}">
@endsection
   
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header"> 
      <h1>
        Form
        <small>Edit Agen</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Edit Agen</li>
      </ol>
    </section>

        <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          @if (Session::has('message'))
            <div class="alert alert-{{Session::get('alert')}}">
              <button data-dismiss="alert" class="close"></button>
              {!! Session::get('message') !!} 
            </div>
          @endif

          @if ($errors->any())
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close"></button>
                {!! implode('', $errors->all('<p>:message</p>')) !!}
            </div>
          @endif
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form Edit Agen</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ url('agen/update/'.$agen->id) }}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="_method" value="put">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6"  style="border-right: 1px solid #eee; ">
                    <div class="form-group">
                      <label for="name">Nama</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan Nama" value="{{ $agen->name }}" required>
                    </div> 
                    <div class="form-group">
                      <label for="phone_number">No. Telpon</label>
                        <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Masukkan No Telpon" value="{{ $agen->phone_number }}">
                    </div>
                     <div class="form-group">
                      <label for="npwp">NPWP</label>
                       <input type="text" class="form-control" id="npwp" name="npwp" placeholder="Masukkan No Telpon" value="{{ $agen->npwp }}">
                    </div>
                    <div class="form-group">
                      <label for="address">Alamat</label>
                        <textarea class="form-control" name="address" id="address">{{ $agen->address }}</textarea>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="pic_name">Nama PIC</label>
                      <input type="text" class="form-control" id="pic_name" name="pic_name" placeholder="Masukkan Nama PIC" value="{{ $agen->pic_name }}">
                    </div>
                    <div class="form-group">
                      <label for="pic_email">Email PIC</label>
                      <input type="text" class="form-control" id="pic_email" name="pic_email" placeholder="Masukkan Email PIC" value="{{ $agen->pic_email }}">
                    </div>
                    <div class="form-group">
                      <label for="pic_phone">Nomor Telepon PIC</label>
                      <input type="text" class="form-control" id="pic_phone" name="pic_phone" placeholder="Masukkan Nomor Telepon PIC" value="{{ $agen->pic_phone }}">
                    </div>                  
                  </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.row -->
    </section>
        <!-- /.content -->
    </div>
@endsection
@section('javascript')
<!-- DataTables -->
<script src="{{('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script>
  $(function () {
    $('#datatable').DataTable({
      "columnDefs": [{ "orderable": false, "targets": 6 }]
    });

  });
</script>
@endsection