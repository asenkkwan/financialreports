<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoMasukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_masuk', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_customer')->unsigned();
            $table->integer('nomor_po');
            $table->string('tanggal_po');
            $table->string('tanggal_kirim');
            $table->string('mata_uang'); 
            $table->text('notes')->nullable();
            $table->integer('po_tujuan');
            $table->string('term');
            $table->string('telp_po_tujuan');
            $table->string('fax_po_tujuan');
            $table->string('pic_po_tujuan');
            $table->string('contact_person_1');
            $table->string('contact_person_2');
            $table->string('delivery_office');
            $table->integer('status')->default(0);
            $table->integer('updated_by')->unsigned();
            $table->integer('created_by')->unsigned();           
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('po_masuk');
    }
}
