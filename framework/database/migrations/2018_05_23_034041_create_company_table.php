<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id'); 
            $table->string('name');
            $table->string('address');
            $table->string('password');
            $table->text('logo')->nullable();
            $table->string('phone_number');
            $table->string('npwp');
            $table->string('fax');
            $table->string('no_skp_migas');
            $table->string('no_sold_to_penyalur');
            $table->boolean('status')->default(0);
            $table->string('permission');
            $table->string('role');
            $table->integer('updated_by')->unsigned();
            $table->integer('created_by')->unsigned();           
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}

