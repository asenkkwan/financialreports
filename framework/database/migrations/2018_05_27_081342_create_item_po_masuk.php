<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemPoMasuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_po_masuk', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_po_masuk')->unsigned();
            $table->string('material');
            $table->string('description');
            $table->string('uom');
            $table->integer('qty');
            $table->decimal('unit_price', 11, 2)->default(0);
            $table->decimal('amount', 11, 2)->default(0);
            $table->integer('updated_by')->unsigned();
            $table->integer('created_by')->unsigned();           
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_po_masuk');
    }
}