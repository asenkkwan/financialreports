<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoKeluarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_keluar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nomor_po')->unsigned();
            $table->integer('id_penyalur')->unsigned();
            $table->integer('id_customer')->unsigned();
            $table->integer('id_address')->unsigned();
            $table->integer('id_bbm')->unsigned();
            $table->integer('jumlah_bbm');
            $table->decimal('pbbkb', 11, 2);
            $table->decimal('pph', 11, 2);
            $table->decimal('ppn', 11, 2);
            $table->decimal('pbbkb_total', 11, 2);  
            $table->decimal('pph_total', 11, 2);
            $table->decimal('dasar_pengenaan_ppn_pph', 11, 2);  
            $table->decimal('nilai_transaksi', 11, 2);
            $table->string('bukti_setor_bank');
            $table->integer('supply_point');
            $table->string('nama_pic_penyalur');
            $table->integer('phone_pic_penyalur');
            $table->string('nama_pic_agen');
            $table->integer('phone_pic_agen');
            $table->decimal('harga_jual', 11, 2)->default(0);
            $table->decimal('harga_dasar', 11, 2)->default(0);
            $table->decimal('harga_jual_penyalur', 11, 2)->default(0);
            $table->decimal('harga_dasar_penyalur', 11, 2)->default(0);
            $table->decimal('margin_penyalur', 11, 2)->default(0);
            $table->string('cara_pembayaran');
            $table->date('jatuh_tempo_hari');
            $table->integer('status')->default(0);
            $table->integer('updated_by')->unsigned();
            $table->integer('created_by')->unsigned();           
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('po_keluar');
    }
}
 
