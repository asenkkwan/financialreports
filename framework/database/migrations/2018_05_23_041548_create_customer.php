<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->increments('id'); 
            $table->string('name');
            $table->string('jenis_usaha');
            $table->string('phone_number');
            $table->string('npwp');
            $table->string('pic_name');
            $table->string('pic_email');
            $table->string('pic_phone');
            $table->string('jenis_customer');
            $table->string('role');
            $table->string('permission');
            $table->integer('updated_by')->unsigned();
            $table->integer('created_by')->unsigned();           
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
