<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenjualanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penjualan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('no_invoice')->unsigned();
            $table->integer('no_po')->unsigned();
            $table->integer('id_customer')->unsigned();
            $table->string('tanggal');
            $table->string('nama_customer');
            $table->string('type');
            $table->string('no_faktur_pajak');
            $table->integer('qty');
            $table->decimal('harga_jual', 11, 2)->default(0);
            $table->decimal('harga_dasar', 11, 2)->default(0);
            $table->decimal('dpp', 11, 2)->default(0);
            $table->decimal('ppn', 11, 2)->default(0);
            $table->decimal('pbbkb', 11, 2)->default(0);
            $table->decimal('total', 11, 2)->unsigned();
            $table->integer('status')->default(0);
            $table->integer('updated_by')->unsigned();
            $table->integer('created_by')->unsigned();           
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penjualan');
    }
}

