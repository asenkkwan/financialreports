<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlatAngkut extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alat_angkut', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_po_keluar')->unsigned();
            $table->string('tanggal')->nullable();
            $table->string('nopol')->nullable();
            $table->integer('volume');
            $table->string('nama_supir');
            $table->integer('updated_by')->unsigned();
            $table->integer('created_by')->unsigned();           
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alat_angkut');
    }
}
