<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //
     //
    
    public $table = "setting";
    public $timestamps = false;
    
    protected $fillable = [
        'id', 
        'field_name',
        'field_value',
    ];

    public static $rules = array(
        'field_name' => 'required',
        'field_value' => 'required',
    );

    public static $updaterules = array(
        
       'field_name' => 'required',
       'field_value' => 'required',
       
    );  
          
}
