<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AlatAngkut extends Model
{
   use SoftDeletes;
    public $table = "alat_angkut";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id_po_keluar', 
        'tanggal',
        'nopol',
        'volume', 
        'nama_supir',
        'created_at',
        'updated_at',
    ];

    public static $rules = array(
        'id_po_keluar' => 'required|max:255',
        'tanggal' => 'required',
        'nopol' => 'required',
        'volume' => 'required',
        'nama_supir' => 'required',
        
        
    );

    public static $updaterules = array(
        'id_po_keluar' => 'required|max:255',
        'tanggal' => 'required',
        'nopol' => 'required',
        'volume' => 'required',
        'nama_supir' => 'required',
        
    );        
}