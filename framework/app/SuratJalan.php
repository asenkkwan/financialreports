<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SuratJalan extends Model
{
    use SoftDeletes;
 
    public $table = "surat_jalan";
    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'no_surat_jalan',
    	'no_po',
        'tanggal', 
        'nama_perusahaan',
        'alamat_perusahaan',
        'kode_lokasi_perusahaan',
        'nopol',
        'jenis_kendaraan',
        'nama_driver',
        'no_phone_driver',
        'sim_driver',
        'created_at',
        'updated_at',
    ];

    public static $rules = array( 
        'no_surat_jalan' => 'required',
        'no_po' => 'required',
        'nama_perusahaan' => 'required',
        'alamat_perusahaan' => '',
        'kode_lokasi_perusahaan' => 'required',
        'nopol' => '',
        'jenis_kendaraan' => '',
        'nama_driver' => '',
        'no_phone_driver' => '',
        'sim_driver' => '',
    );

    public static $updaterules = array(
     	'no_surat_jalan' => 'required',
        'no_po' => 'required',
        'nama_perusahaan' => 'required',
        'alamat_perusahaan' => '',
        'kode_lokasi_perusahaan' => 'required',
        'nopol' => '',
        'jenis_kendaraan' => '',
        'nama_driver' => '',
        'no_phone_driver' => '',
        'sim_driver' => '',
    ); 
}
