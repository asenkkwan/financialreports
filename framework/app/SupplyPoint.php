<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupplyPoint extends Model
{
    //
    use SoftDeletes;
    public $table = "supply_point";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id', 
        'supply_point',
        
        // 'created_at', 
        // 'updated_at',
    ];

    public static $rules = array(
        
        'supply_point' => 'required',
       
        
    );

    public static $updaterules = array(
        
        'supply_point' => 'required',
       
    );  
          
}