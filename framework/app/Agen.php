<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agen extends Model
{
    //
    use SoftDeletes;
    public $table = "agen";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 
        'address',
        'phone_number', 
        'npwp',
        'pic_name',
        'pic_email',
        'pic_phone',
        'created_at',
        'updated_at',
    ];

    public static $rules = array(
        'name' => 'required|max:255',
        'address' => 'required',
        'phone_number' => 'required',
        'address' => 'required',
        'npwp' => 'required',
        'pic_name' => 'required',
        'pic_email' => 'required',
        'pic_phone' => 'required',
    );

    public static $updaterules = array(
        'name' => 'required|max:255',
        'address' => 'required',
        'phone_number' => 'required',
        'address' => 'required',
        'npwp' => 'required',
        'pic_name' => 'required',
        'pic_email' => 'required',
        'pic_phone' => 'required',
    );        
}

