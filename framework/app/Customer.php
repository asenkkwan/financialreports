<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;

    protected $table = "customer";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 
        'jenis_usaha', 
        'phone_number',  
        // 'npwp',
        'pic_name',
        'pic_email',
        'pic_phone',
        'jenis_customer',
        'created_at',
        'updated_at',
    ];

    public static $rules = array(
        'name' => 'required|max:255',
        'jenis_usaha', 
        'phone_number', 
        // 'npwp',
        'pic_name',
        'pic_email',
        'pic_phone',
        'jenis_customer',
    );

    public static $updaterules = array(
        'name' => 'required|max:255',
        'jenis_usaha', 
        'phone_number', 
        // 'npwp',
        'pic_name',
        'pic_email',
        'pic_phone',
        'jenis_customer',
    );

    public function cabang(){
      return $this->belongsTo('App\Company', 'company_id', 'id');
    }
}

 