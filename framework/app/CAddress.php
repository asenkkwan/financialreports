<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CAddress extends Model
{
    //
    use SoftDeletes;
    public $table = "caddress";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id_customer', 
        'npwp',
        'address_code',
        'address',
        
        // 'created_at', 
        // 'updated_at',
    ];

    public static $rules = array(
        'id_customer' => 'required|max:255',
        'address_code' => 'required',
        'npwp' => 'required',
        'address' => 'required',
        
    );

    public static $updaterules = array(
        'id_customer' => 'required|max:255',
        'address_code' => 'required',
        'npwp' => 'required',
        'address' => 'required',
    );  
          
}

