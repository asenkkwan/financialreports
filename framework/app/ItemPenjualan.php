<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemPenjualan extends Model
{
    
    public $table = "item_penjualan";

    protected $fillable = [
        'id_penjualan', 
        'produk',
        'qty',
        'harga_beli', 
        'harga_jual',
        'pbbkb',  
        'ppn',
        'keterangan',
        'created_at',
        'updated_at',
    ];

    public static $rules = array(
        'id_penjualan' => 'required|max:255',
        'produk' => 'required',
        'qty' => 'required',
        'harga_jual' => 'required',
        'harga_beli' => 'required',
        'pbbkb' => 'required',
        'ppn' => 'required',
        'keterangan' => '',
        
    );

    public static $updaterules = array(
        'id_penjualan' => 'required|max:255',
        'produk' => 'required',
        'qty' => 'required',
        'harga_jual' => 'required',
        'harga_beli' => 'required',
        'pbbkb' => 'required',
        'ppn' => 'required',
        'keterangan' => '',
        
    );        
}

