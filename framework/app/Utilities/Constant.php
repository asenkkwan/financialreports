<?php

namespace App\Utilities;

class Constant {
	
	public static $EMAIL_RECEIVER = "ivaniriandi17@gmail.com";
	public static $EMAIL_SENDER = "corocot.design@gmail.com";
	public static $NAME_SENDER = "Financial Reports";
	public static $EMAIL_SUBJECT = "PO Masuk Baru";
	public static $EMAIL_SUBJECT2 = "PO Keluar Baru";
	public static $USERNAME = "catcat";
	public static $COMPANY_IMG_PROFILE_PATH = 'dist/img/'; 
	
	public static $PBBKB = ["0.000"=>"-" , 
							"NULL"=>"-" , 
							"0.858"=>"17,17% dari 5%" , 
							"1.288"=>"1,288%", "4.500"=>"90% dari 5%",  
							"5.000"=>"100% dari 5%", "6.000"=>"100% dari 6%",  
							"7.500"=>"150% dari 5%", 
							"10.000"=>"100% dari 10%"];

	public static $PPH = ["0.000"=>"-", "0"=>"-", "0.00"=>"-", "NULL"=>"-", "0.300"=>"0,300%",];
	public static $SHOW_PBBKB = ["NULL"=>"-", "0"=>"NO", "0.000"=>"-",  "2"=>"YES",  "-1"=>"--Tampilkan PBBKB--",];
	public static $SHOW_ADD_COST = ["NULL"=>"-", "0"=>"NO", "0.000"=>"-",  "2"=>"YES",  "-2"=>"--Tampilkan Add Cost--",];
	public static $STATUS = ["NULL"=>"Belum Lunas", "0"=>"Belum Lunas",  "1"=>"LUNAS",];
				  
}