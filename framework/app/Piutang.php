<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Piutang extends Model
{
    // 
    use SoftDeletes;
    public $table = "penjualan";
    protected $dates = ['deleted_at'];
   

    protected $fillable = [
        'tanggal', 
        'tanggal_pengaliran', 
        'no_invoice', 
        'no_faktur_pajak',
        'nama_perusahaan',
        'lokasi_perusahaan',
        'nama_supplier',
        'nama_bunker',
        'no_po',
        'produk',
        'ppn',
        'ppn_total',
        'telp',
        'show_pbbkb',
        'show_add_cost',
        'created_at',
        'updated_at',
        'approved_date',
    ];
 
    public static $rules = array( 
        'tanggal' => 'required',
        'tanggal_pengaliran' => 'required',
        'no_faktur_pajak' => 'required',
        'no_invoice' => '',
        'lokasi_perusahaan' => '',
        'nama_perusahaan' => 'required|max:255',
        'nama_supplier' => 'required',
        'no_po' => 'required',
        'telp' => '',   
        'show_pbbkb' => '',   
        'show_add_cost' => '',     
        'alamat_perusahaan' => '',
        'nama_bunker' => 'required',
        'produk' => 'required',
        'ppn' => '',
        'ppn_total' => '',
       
    );

    public static $updaterules = array(
        'tanggal' => 'required',
        'tanggal_pengaliran' => 'required',
        'no_faktur_pajak' => 'required',
        'no_invoice' => '', 
        'lokasi_perusahaan' => '',
        'nama_perusahaan' => 'required|max:255',
        'nama_supplier' => 'required',
        'no_po' => 'required',
        'telp' => '',     
        'show_pbbkb' => '',   
        'show_add_cost' => '',  
        'alamat_perusahaan' => '',
        'nama_bunker' => 'required',
        'produk' => 'required',
        'ppn' => '',
        'ppn_total' => '',
       
        
    ); 
    public function customer(){
        return $this->belongsTo('App\Customer', "name");
    }       

    public function items(){
        return $this->hasMany('App\ItemPenjualan', "id_penjualan", "id");
    }


}
