<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PembelianAgen extends Model
{
    use SoftDeletes;

    public $table = "pembelian_agen";
    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'no_po',
        'no_invoice',
        'nama_agen', 
        'alamat_agen',
        'alamat_kirim',
        'nama_supplier',
        'no_faktur_pajak',
        'telp',
        'produk',
        'qty',  
        'harga_beli', 
        'jumlah_ppn',
        'harga_jual',
        'pbbkb',
        'ppn',
        'notes',
        'tanggal',
        'created_at',
        'updated_at',
    ];

    public static $rules = array( 
        'no_invoice' => '',
        'nama_agen' => '',
        'alamat_agen' => '',
        'alamat_kirim' => '',
        'nama_supplier' => '',
        'no_faktur_pajak' => 'required',  
        'telp' => '',      
        'produk' => 'required',
        'qty' => 'required',
        'harga_beli' => 'required',
        'harga_jual' => 'required',
        'pbbkb' => '',
        'ppn' => '',
        'notes' => '',
        'tanggal' => 'required',
       
    );

    public static $updaterules = array(
     	 'no_invoice' => '',
        'nama_agen' => '',
        'alamat_agen' => '',
        'alamat_kirim' => '',
        'nama_supplier' => '',  
        'no_faktur_pajak' => 'required',
        'telp' => '',      
        'produk' => 'required',
        'qty' => 'required',
        'harga_beli' => 'required',
        'harga_jual' => 'required',
        'pbbkb' => '',
        'ppn' => '',
        'notes' => '',
        'tanggal' => 'required',
        
    ); 
}
