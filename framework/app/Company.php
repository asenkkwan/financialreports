<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model 
{
    //
    use SoftDeletes;
    public $table = "company";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'kop_surat', 
        'name', 
        'phone_number', 
        'address', 
        'fax',
        'npwp',
        'no_skp_migas',
        'no_sold_to_penyalur',
        'created_at',
        'updated_at',
    ];

    public static $rules = array(
        'name' => 'required|max:255',
        'phone_number' => 'required',
        'address' => 'required',
        'fax' => 'required',
        'npwp' => 'required',
        'no_skp_migas' => 'required',
        'no_sold_to_penyalur' => 'required',
        'kop_surat' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
    );

    public static $updaterules = array(
        'kop_surat' => '',
        'name' => 'required|max:255',
        'phone_number' => 'required',
        'address' => 'required',
        'fax' => 'required',
        'npwp' => 'required',
        'no_skp_migas' => 'required',
        'no_sold_to_penyalur' => 'required',
        'kop_surat' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
    );  
    
}
