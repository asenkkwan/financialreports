<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PoKeluar extends Model
{
    //
    use SoftDeletes;
    public $table = "po_keluar";
    protected $dates = ['deleted_at'];

    protected $fillable = [
         
        'nomor_po',
        'nama_agen', 
        'alamat_agen',
        // 'nama_supplier',
        // 'produk',
        'qty',  
        'harga_beli',
        'harga_jual',
        'pbbkb',
        'pph',
        'ppn',
        'penyalur',
        'nama_pic_penyalur',
        'nama_pic_agen',
        'tanggal',
        'sales_district',
        'jenis_bbm',
        'cara_pembayaran',
        'jatuh_tempo_hari',
        'bukti_setor_bank',
        'tanggal',
        'created_at',
        'updated_at',
    ];

    public static $rules = array(
        'tanggal'=>  'required',
        'nomor_po' => 'required',
        'nama_konsumen' => 'required',
        'alamat_konsumen' => 'required',
        // 'nama_supplier'=> 'required',
        // 'produk'=> 'required',
        'qty' => 'required',
        'harga_beli' => 'required',
        'harga_jual'=> 'required',
        'pbbkb'=> '',
        'ppn'=>'',
        'penyalur'=>'required',
        'nama_pic_penyalur'=> 'required',
        'nama_pic_agen'=> 'required',
        'tanggal' => 'required',
        'sales_district'=> 'required',
        'jenis_bbm'=> 'required',
        'cara_pembayaran'=> 'required',
        'pph'=>'',
        'jatuh_tempo_hari'=> '',
        'bukti_setor_bank'=> 'required',
        
    );

    public static $updaterules = array(
        'tanggal'=>  'required',
        'nomor_po' => 'required',
        'nama_konsumen' => 'required',
        'alamat_konsumen' => 'required',
        // 'nama_supplier'=> 'required',
        // 'produk'=> 'required',
        'pph'=>'',
        'qty' => 'required',
        'harga_beli' => 'required',
        'harga_jual'=> 'required',
        'pbbkb'=> '',
        'ppn'=>'',
        'penyalur'=>'required',
        'nama_pic_penyalur'=> 'required',
        'nama_pic_agen'=> 'required',
        'tanggal' => 'required',
        'sales_district'=> 'required',
        'jenis_bbm'=> 'required',
        'cara_pembayaran'=> 'required',
        'jatuh_tempo_hari'=> '',
        'bukti_setor_bank'=> 'required',
       
    );
        
     public function caddress(){
        return $this->belongsTo('App\Caddress', "address");
    }   
}

