<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemPoMasuk extends Model 
{
    //
    use SoftDeletes;
    public $table = "item_po_masuk";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id_po_masuk', 
        'material',
        'description',
        'uom', 
        'quantity',
        'unit_price',
        'amount',
        'created_at',
        'updated_at',
    ];

    public static $rules = array(
        'id_po_masuk' => 'required|max:255',
        'material' => 'required',
        'description' => 'required',
        'uom' => 'required',
        'quantity' => 'required',
        'unit_price' => 'required',
        
        
    );

    public static $updaterules = array(
        'id_po_masuk' => 'required|max:255',
        'material' => 'required',
        'description' => 'required',
        'uom' => 'required',
        'quantity' => 'required',
        'unit_price' => 'required',
        
    );        
}

