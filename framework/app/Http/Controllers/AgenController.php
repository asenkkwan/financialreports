<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request; 

use App\Agen;
use App\CAddress;
use App\ItemPoMasuk;
use App\Company;

use Validator; 
use Redirect;
 
 
use Auth;
use DB;

class AgenController extends Controller
{
   public $data;

    public function __construct(){
      $this->data = [];
    }
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
   {
      return view('agen.index', $this->data);
   }
   public function fetchDataAgen(Request $request){

        $user = Auth::user();
        
        $param['limit']     = $request->length;
        $param['offset']    = $request->start;
        $search    = $request->search['value'];
        $param['order_by']  = ($request->columns[$_GET['order'][0]['column']]['data'])?$request->columns[$_GET['order'][0]['column']]['data']:"name";
        $param['order_type']= ($request->order[0]['dir'])?$request->order[0]['dir']:"asc";

        $resultJson = array("draw"=>$_GET['draw'],
                            "recordsTotal"=>0,
                            "recordsFiltered"=>0,
                            "data"=>"");

        $agen = Agen::where(function($query) use ($search){
                            $query->orWhere("name", "LIKE", $search."%")
                                  ->orWhere("address", "LIKE", $search."%")
                                  ->orWhere("phone_number", "LIKE", $search."%")
                                  ->orWhere("npwp", "LIKE", $search."%")
                                  ->orWhere("pic_name", "LIKE", $search."%")
                                  ->orWhere("pic_email", "LIKE", $search."%")
                                  ->orWhere("pic_phone", "LIKE", $search."%");
                                 
                          })
                          ->skip($param['offset'])
                          ->take($param['limit'])
                          ->orderBy($param['order_by'], $param['order_type']);

        $total = Agen::where(function($query) use ($search){
                            $query->orWhere("name", "LIKE", $search."%")
                                  ->orWhere("address", "LIKE", $search."%")
                                  ->orWhere("phone_number", "LIKE", $search."%")
                                  ->orWhere("npwp", "LIKE", $search."%")
                                  ->orWhere("pic_name", "LIKE", $search."%")
                                  ->orWhere("pic_email", "LIKE", $search."%")
                                  ->orWhere("pic_phone", "LIKE", $search."%");
                                  
                          });

        $agen = $agen->get();
        $total = $total->count();

        if($total > 0){
            $data = array();
            for($i=0;$i<count($agen);$i++){
                foreach($agen[$i] as $key=>$val){
                  
                    $datatemp = array("name"=>$agen[$i]->name,
                                      "address"=>$agen[$i]->address, 
                                      "phone_number"=>$agen[$i]->phone_number,
                                      "npwp"=>$agen[$i]->npwp,
                                      "pic_name"=>$agen[$i]->pic_name,
                                      "pic_email"=>$agen[$i]->pic_email,
                                      "pic_phone"=>$agen[$i]->pic_phone,
                                      "created_at"=>$agen[$i]->created_at->format("Y-m-d H:i:s"),
                                      "action"=>'<a href="'.url('agen/edit/'.$agen[$i]->id).'" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> Edit</a>
                                         
                                        <a class="btn btn-danger btn-xs" href="'. url('agen/delete/'.$agen[$i]->id) .'" onclick="deleteData('.$agen[$i]->id.')"><i class="fa fa-trash"></i> Hapus</a>
                                        <form action="'.url('agen/delete/'.$agen[$i]->id).'" method="post" id="form-'. $agen[$i]->id.'">
                                          '.csrf_field().'
                                          <input type="hidden" name="_method" value="delete">
                                        </form>',
                                     );
                }
                array_push($data, $datatemp);   
            }

                $resultJson = array("draw"=>$request->draw,
                                    "recordsTotal"=>$total,
                                    "recordsFiltered"=>$total,
                                    "data"=>$data);
        }    

        return response()->json($resultJson);
    }
     public function create()
    {
        $user = Auth::user();

        $this->data['kantor_cabang'] = Agen::all();
        return view('agen.create', $this->data);
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), Agen::$rules);

        if($validation->passes()){
            $agen = new Agen();
            $agen->name = $request->name;
            $agen->address = $request->address;
            $agen->phone_number = $request->phone_number;
            $agen->npwp = $request->npwp;
            $agen->pic_name = $request->pic_name;
            $agen->pic_email = $request->pic_email;
            $agen->pic_phone = $request->pic_phone;

            $agen->save();

            /* redirect to page user */
            return Redirect::route('agen.index')->with('alert','success')->with('message', "Sukses: Agen $agen->name telah berhasil dibuat.");
        }

        /* redirect to page create user with input and error */
        return Redirect::route('agen.create')->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }
   
    public function edit($id)
    {
        $user = Auth::user();

        $this->data['agen'] = Agen::findOrFail($id);
        return view('agen.edit', $this->data);

    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();

         /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), agen::$updaterules);
        $agen = agen::findOrFail($id);

        if($validation->passes()){
            $agen->name = $request->name;
            $agen->address = $request->address;
            $agen->phone_number = $request->phone_number;
            $agen->npwp = $request->npwp;
            $agen->pic_name = $request->pic_name;
            $agen->pic_email = $request->pic_email;
            $agen->pic_phone = $request->pic_phone;

            $agen->save();

            /* redirect to page user */
            return Redirect::route('agen.index')->with('alert','success')->with('message', "Sukses: Agen $agen->nama telah berhasil diupdate.");
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }
    public function createAddress($id)
    {
        //
        $this->data['agen'] = Agen::findOrFail($id);
        $this->data['add_address'] = CAddress::where("id", $id)->get();
        $this->data['kantor_cabang'] = Company::all();
       
        return view('agen.create_address', $this->data);
    }
     public function addAdress($id, $company_id){

        $user = Auth::user();

        $item_po_masuk = ItemPoMasuk::join("id_po_masuk", "material", "=", "produk_items.produk_id")
                            ->select("item_po_masuk.id", "item_po_masuk.id_po_masuk", "item_po_masuk.material", "item_po_masuk.description", "item_po_masuk.uom", "item_po_masuk.qty", "item_po_masuk.unit_price", "item_po_masuk.amount")
                            ->where("item_po_masuk.id_po_masuk", $id)
                            ->where("item_po_masuk.cabang_id", $company_id)
                            ->firstOrFail();

        if($produk->qty > 0){
            return response()->json(["status"=>"berhasil", "data"=>$produk, "pesan"=>""]);
        }

        return response()->json(["status"=>"gagal", "data"=>"", "pesan"=>"Stok barang saat ini sedang kosong!"]);
        
    }

    public function destroy($id)
    {
        $user = Auth::user();
        $agen = agen::findOrFail($id);
        $agen->delete();

        /* redirect to page user */
        return Redirect::route('agen.index')->with('alert','success')->with('message', "Sukses: Agen $agen->nama telah berhasil dihapus.");
    }
    public function getAddress(Request $request){
      $id = $request->id;

      $data = Agen::findOrFail($id);

      return response()->json(["status"=>"ok", "data"=>$data]);
    }
}
