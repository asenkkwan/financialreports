<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 

use App\PoKeluar;
use App\ItemPoKeluar;
use App\Customer;   
use App\Company;  
use App\CAddress; 
use App\JenisBbm;
use App\SupplyPoint;
use App\AlatAngkut;
use App\Agen;
use App\Utilities\Constant; 
use App\User;
use App\Setting;


use Validator; 
use Redirect;
use Mail;  
use Auth; 
use DB;


class PoKeluarController extends Controller
{
   public $data;

    public function __construct(){
      $this->data = ['class'=>'po_keluar'];
    } 
    
    public function index(Request $request)
    {
        $this->data['nama_konsumen'] = $request->nama_konsumen;
        $this->data['nomor_po'] = $request->nomor_po;
        $this->data['penyalur'] = $request->penyalur;
        $this->data['jenis_bbm'] = $request->jenis_bbm;
        $this->data['sdate'] = $request->sdate;
        $this->data['edate'] = $request->edate;

        $po_keluar = PoKeluar::orderBy('tanggal', 'desc');

        if($this->data['nama_konsumen']){
            $po_keluar = $po_keluar->where("nama_konsumen", "LIKE", "%".$this->data['nama_konsumen']."%");
        }

        if($this->data['nomor_po']){
            $po_keluar = $po_keluar->where("nomor_po", "LIKE", "%".$this->data['nomor_po']."%");
        }

        if($this->data['penyalur']){
            $po_keluar = $po_keluar->where("penyalur","LIKE", "%".$this->data['penyalur']."%");
        }

        if($this->data['jenis_bbm']){
            $po_keluar = $po_keluar->where("jenis_bbm","LIKE", "%".$this->data['jenis_bbm']."%");
        }

        if($this->data['sdate'] && $this->data['edate']){
            $po_keluar = $po_keluar->whereBetween("tanggal", [$this->data['sdate'] , $this->data['edate'] ]);
        }

        $po_keluar = $po_keluar->where("archive",0)->paginate(20);
        
        $this->data['subclass'] = "list";
        $this->data['po_keluar'] = $po_keluar;
        return view('po_keluar.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
        $this->data['kantor_cabang'] = Company::all();
        $this->data['nama_konsumen'] = Customer::all();     
        $this->data['alamat_agen'] = Agen::all();
        $this->data['jenis_bbm'] = JenisBbm::all(); 
        $this->data['supply_point'] = SupplyPoint::all();
        
        return view('po_keluar.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) 
    {
        $user = Auth::user(); 

        $company = Company::findOrFail($request->penyalur);
        $customer = Customer::findOrFail($request->nama_konsumen);
        $jenis_bbm = JenisBbm::findOrFail($request->jenis_bbm);
        $supply_point = SupplyPoint::findOrFail($request->supply_point);
        $caddress = CAddress::findOrFail($request->alamat_konsumen);
        
        
        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), PoKeluar::$rules);
 
        if($validation->passes()){
            $po_keluar = new PoKeluar();
            $po_keluar->tanggal = $request->tanggal;
            $po_keluar->nomor_po = $request->nomor_po;
            $po_keluar->penyalur = $company->name;
            $po_keluar->id_customer = $customer->id;
            $po_keluar->alamat_penyalur = $company->address;
            $po_keluar->no_skp_penyalur = $company->no_skp_migas;
            $po_keluar->kode_lokasi_konsumen = $caddress->address_code;
            $po_keluar->npwp_penyalur = $company->npwp;
            $po_keluar->no_sold_penyalur = $company->no_sold_to_penyalur;
            $po_keluar->alamat_penyalur = $company->address;
            $po_keluar->nama_konsumen = $customer->name;
            $po_keluar->jenis_konsumen = $customer->jenis_customer;
            $po_keluar->jenis_usaha = $customer->jenis_usaha;
            $po_keluar->alamat_konsumen = $caddress->address;
            $po_keluar->sales_district = $request->sales_district;
            $po_keluar->jenis_bbm = $jenis_bbm->jenis_bbm;
            $po_keluar->qty = $request->qty;
            $po_keluar->harga_beli = $request->harga_beli;
            $po_keluar->harga_jual = $request->harga_jual;
            $po_keluar->pbbkb = $request->pbbkb; 
            $po_keluar->pph = $request->pph;
            $po_keluar->ppn = (int)$request->ppn; 
            $po_keluar->cara_pembayaran = $request->cara_pembayaran;
            $po_keluar->nama_pic_penyalur = $request->nama_pic_penyalur;
            $po_keluar->nama_pic_agen = $request->nama_pic_agen;
            $po_keluar->supply_point = $supply_point->supply_point;    
            $po_keluar->bukti_setor_bank = $request->bukti_setor_bank;
            $po_keluar->jatuh_tempo_hari = $request->jatuh_tempo_hari;  

            $po_keluar->harga_dasar = $po_keluar->harga_beli/(1+($po_keluar->ppn/100)+($po_keluar->pbbkb/100));
            $po_keluar->dpp = $po_keluar->qty*$po_keluar->harga_dasar;
            $po_keluar->harga_jual_penyalur = $po_keluar->harga_beli*$po_keluar->qty;
            $po_keluar->harga_ppn = $po_keluar->harga_jual_penyalur*$po_keluar->ppn/100;
            $po_keluar->harga_jual_ppn = $po_keluar->harga_beli+$po_keluar->harga_beli*$po_keluar->ppn/100;
            $po_keluar->total = $po_keluar->harga_jual_penyalur+$po_keluar->harga_ppn;

            $po_keluar->margin_penyalur = $po_keluar->pbbkb_dasar-$po_keluar->harga_dasar; 
            $po_keluar->jumlah_ppn = $po_keluar->dpp*($po_keluar->ppn/100);
            $po_keluar->pbbkb_dasar = $po_keluar->harga_jual/(1+($po_keluar->ppn/100)+($po_keluar->pbbkb/100));
            $po_keluar->pbbkb_jual = $po_keluar->qty*$po_keluar->pbbkb_dasar*($po_keluar->pbbkb/100);
            $po_keluar->jumlah = $po_keluar->dpp+$po_keluar->jumlah_ppn+$po_keluar->pbbkb_jual;  

            $po_keluar->save();

            /* redirect to page user */
            return redirect('po-keluar/add-item/'.$po_keluar->id);
        }

        /* redirect to page create user with input and error */
        return Redirect::route('po_keluar.create')->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }
    public function edit($id)
    {
        $this->data['po_keluar'] = PoKeluar::findOrFail($id);

        $caddress = CAddress::where("address_code", $this->data['po_keluar']->kode_lokasi_konsumen)->firstOrFail();

        $this->data['customer'] = Customer::findOrFail($caddress->id_customer);
        $this->data['caddress'] = $caddress;
        // print_r( $caddress->id);die();
        $this->data['pbbkb'] = Constant::$PBBKB;
        $this->data['pph'] = Constant::$PPH;

        $this->data['kantor_cabang'] = Company::all();
        $this->data['jenis_bbm'] = JenisBbm::all();
        $this->data['supply_point'] = SupplyPoint::all();

        $this->data['nama_konsumen'] = Customer::all();
        $this->data['alamat_konsumen'] = CAddress::all();

        $this->data['agen'] = Agen::all();

        $this->data['subclass'] = 'edit';
        return view('po_keluar.edit', $this->data);
    }

    
    public function update(Request $request, $id)
    {
        //
        $user = Auth::user();
        $po_keluar = PoKeluar::findOrFail($id);
        $company = Company::findOrFail($request->penyalur);
        $customer = Customer::findOrFail($request->nama_konsumen);
        $jenis_bbm = JenisBbm::findOrFail($request->jenis_bbm);
        $supply_point = SupplyPoint::findOrFail($request->supply_point);
        $caddress = CAddress::findOrFail($request->alamat_konsumen);

         /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), PoKeluar::$updaterules);
        

        if($validation->passes()){
            $po_keluar->tanggal = $request->tanggal;
            $po_keluar->nomor_po = $request->nomor_po;
            $po_keluar->penyalur = $company->name;
            $po_keluar->id_customer = $customer->id;
            $po_keluar->alamat_penyalur = $company->address;
            $po_keluar->no_skp_penyalur = $company->no_skp_migas;
            $po_keluar->kode_lokasi_konsumen = $caddress->address_code;
            $po_keluar->npwp_penyalur = $company->npwp;
            $po_keluar->no_sold_penyalur = $company->no_sold_to_penyalur;
            $po_keluar->alamat_penyalur = $company->address;
            $po_keluar->nama_konsumen = $customer->name;
            $po_keluar->jenis_konsumen = $customer->jenis_customer;
            $po_keluar->jenis_usaha = $customer->jenis_usaha;
            $po_keluar->alamat_konsumen = $caddress->address;
            $po_keluar->sales_district = $request->sales_district;
            $po_keluar->jenis_bbm = $jenis_bbm->jenis_bbm;
            $po_keluar->qty = $request->qty;
            $po_keluar->harga_beli = $request->harga_beli;
            $po_keluar->harga_jual = $request->harga_jual;
            $po_keluar->pbbkb = $request->pbbkb; 
            $po_keluar->pph = $request->pph;
            $po_keluar->ppn = (int)$request->ppn; 
            $po_keluar->cara_pembayaran = $request->cara_pembayaran;
            $po_keluar->nama_pic_penyalur = $request->nama_pic_penyalur;
            $po_keluar->nama_pic_agen = $request->nama_pic_agen; 
            $po_keluar->supply_point = $supply_point->supply_point;    
            $po_keluar->bukti_setor_bank = $request->bukti_setor_bank;
            $po_keluar->jatuh_tempo_hari = $request->jatuh_tempo_hari; 

            $po_keluar->harga_dasar = $po_keluar->harga_beli/(1+($po_keluar->ppn/100)+($po_keluar->pbbkb/100));
            $po_keluar->dpp = $po_keluar->qty*$po_keluar->harga_dasar;
            $po_keluar->harga_jual_penyalur = $po_keluar->harga_beli*$po_keluar->qty;
            $po_keluar->harga_ppn = $po_keluar->harga_jual_penyalur*$po_keluar->ppn/100;
            $po_keluar->harga_jual_ppn = $po_keluar->harga_beli+$po_keluar->harga_beli*$po_keluar->ppn/100;
            $po_keluar->total = $po_keluar->harga_jual_penyalur+$po_keluar->harga_ppn;
            $po_keluar->margin_penyalur = $po_keluar->pbbkb_dasar-$po_keluar->harga_dasar;
            $po_keluar->jumlah_ppn = $po_keluar->dpp*($po_keluar->ppn/100);
            $po_keluar->pbbkb_dasar = $po_keluar->harga_jual/(1+($po_keluar->ppn/100)+($po_keluar->pbbkb/100));
            $po_keluar->pbbkb_jual = $po_keluar->qty*$po_keluar->pbbkb_dasar*($po_keluar->pbbkb/100);
            $po_keluar->jumlah = $po_keluar->dpp+$po_keluar->jumlah_ppn+$po_keluar->pbbkb_jual;  

            // print_r($po_keluar->pbbkb_dasar);die();
            $po_keluar->save();

            /* redirect to page user */
             return Redirect::route('po_keluar.index')->with('alert','success')->with('message', "Sukses: PO Keluar $po_keluar->nomor_po telah berhasil diupdate.");
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }

    public function createItem($id)
    {
        //
        $this->data['po_keluar'] = PoKeluar::findOrFail($id);
        $this->data['alat_angkut'] = AlatAngkut::where("id_po_keluar", $id)->get();
        $this->data['kantor_cabang'] = Company::all();
        
        
       
        return view('po_keluar.create_item', $this->data);
    }

    public function alatAngkut(Request $request)
    { 
        $user = Auth::user(); 
        $tanggal = !empty($request->tanggal)?$request->tanggal:array();
        $nopol = !empty($request->nopol)?$request->nopol:array();
        $volume = !empty($request->volume)?$request->volume:array();
        $nama_supir = !empty($request->nama_supir)?$request->nama_supir:array();
        

        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), AlatAngkut::$rules);
        if($validation->passes()){

            if(AlatAngkut::where("id_po_keluar", $request->id_po_keluar)->count() > 0){
                DB::table('alat_angkut')->where('id_po_keluar', '=', $request->id_po_keluar)->delete();
            }

            for($i=0;$i<count($tanggal);$i++){

                if($tanggal[$i] != ""){
                    $alat_angkut = new AlatAngkut();
                    $alat_angkut->id_po_keluar = $request->id_po_keluar;
                    $alat_angkut->tanggal = $tanggal[$i];
                    $alat_angkut->nopol = $nopol[$i];
                    $alat_angkut->volume = $volume[$i];
                    $alat_angkut->nama_supir = $nama_supir[$i];
                
                    $alat_angkut->save();
                }
           }    

           //send Email
            $this->sendEmail($request->id_po_keluar);

           
            /* redirect to page user */
             return Redirect::route('po_keluar.index')->with('alert','success')->with('message', "Sukses: Item PO Keluar telah berhasil ditambahkan.");
        }

        /* redirect to page create user with input and error */
        return Redirect::route('po_keluar.create_item')->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }
  
    public function detailPoKeluar(Request $request,$id)
    {
        //
        $user = Auth::user();
        $this->data['po_keluar'] = PoKeluar::findOrFail($id); 
        $this->data['alat_angkut'] = AlatAngkut::where("id_po_keluar", $id)->get();
  
        return view('po_keluar.detail_po_keluar', $this->data);
    }
    public function invoicePrint(Request $request,$id)
    {
        // $this->data['item_po_masuk'] = ItemPoMasuk::where("id_po_masuk", $this->data['po_masuk']->id)->get();
        $user = Auth::user();
        $this->data['po_keluar'] = PoKeluar::findOrFail($id); 
        $this->data['alat_angkut'] = AlatAngkut::where("id_po_keluar", $id)->get();
        $this->data['company'] = Company::where("name", $this->data['po_keluar']->penyalur)->first();
        $this->data['pbbkb'] = Constant::$PBBKB;
        $this->data['pph'] = Constant::$PPH;
  
        return view('po_keluar.invoice-print', $this->data);
    }
    public function invoicePrintAgen(Request $request,$id)
    {
        //
        $user = Auth::user();
        $this->data['po_keluar'] = PoKeluar::findOrFail($id); 
        $this->data['company'] = Company::where("name", $this->data['po_keluar']->penyalur)->first();
        // print_r($this->data['company']);die();
        
        $this->data['alat_angkut'] = AlatAngkut::where("id_po_keluar", $id)->get();
  
        return view('po_keluar.invoice-printAgen', $this->data);
    }
    public function invoicePrintNasional(Request $request,$id) 
    {
        //
        $user = Auth::user();
        $this->data['po_keluar'] = PoKeluar::findOrFail($id); 
        $this->data['company'] = Company::where("name", $this->data['po_keluar']->penyalur)->first();
        $this->data['alat_angkut'] = AlatAngkut::where("id_po_keluar", $id)->get();
  
        return view('po_keluar.invoice-printNasional', $this->data);
    }
    public function editPoKeluar($id)
    {
        //
        $user = Auth::user();

        $this->data['po_keluar'] = PoKeluar::findOrFail($id);
        $this->data['kantor_cabang'] = Company::all();
        $this->data['nama_customer'] = Customer::all();
        $this->data['no_ship_to_konsumen'] = CAddress::all();
        return view('po_keluar.edit_item', $this->data);
    }

    public function destroy($id)
    {
        //
        $user = Auth::user();
        $po_keluar = PoKeluar::findOrFail($id);
        $po_keluar->delete();

        /* redirect to page user */
        return Redirect::route('po_keluar.index')->with('alert','success')->with('message', "Sukses: Po Keluar $po_keluar->id telah berhasil dihapus.");
    
    }
   public function approve(Request $request)
    {

       if(isset($_GET['id'])){ 
            $auth = User::where("username", $_GET['username'])->firstOrFail();
            $id = $_GET['id'];
        }else{
            $auth = Auth::user();
            if(!$auth->enable_approval){
                abort(403);
            }
            $id = $request->po_keluar_id;    
        }
        

        $po_keluar = PoKeluar::findOrFail($id);

        if($po_keluar->status == 0){
            $po_keluar->status = 1;
            $po_keluar->approved_by = $auth->username;
            $po_keluar->save();

            if(isset($_GET['id'])){
                echo "<div style='margin-top:50px;width:100%'><center><h2>Berhasil!</h2></center></div><script>setTimeout('window.close()', 5000);</script>";
            }else{
                return redirect()->back()->with('alert','success')->with("msg", "Po Masuk berhasil Di setujui");
            }    
        }else{
            echo "<div style='margin-top:50px;width:100%'><center><h2>Po tidak ditemukan!</h2></center></div><script>setTimeout('window.close()', 5000);</script>";
        }
        
    }

    public function reject(Request $request)
    {
        
        if(isset($_GET['id'])){
            $auth = User::where("username", $_GET['username'])->firstOrFail();
            $id = $_GET['id'];
        }else{
            $auth = Auth::user();
            if(!$auth->enable_approval){
                abort(403);
            }
            $id = $request->po_keluar_id;    
        }

        $po_keluar = PoKeluar::findOrFail($id);

        if($po_keluar->status == 0){
            $po_keluar->status = -1;
            $po_keluar->rejected_by = $auth->username;
            $po_keluar->save();

            if(isset($_GET['id'])){
                echo "<div style='margin-top:50px;width:100%'><center><h2>PO Di Tolak!</h2></center></div><script>setTimeout('window.close()', 3000);</script>";
            }else{
                return redirect()->back()->with('alert','success')->with("msg", "Po Keluar berhasil di tolak");
            }
        }else{
            echo "<div style='margin-top:50px;width:100%'><center><h2>Po tidak ditemukan!</h2></center></div><script>setTimeout('window.close()', 5000);</script>";
        }
        
    }
    public function generateInvoice(Request $request){
        $id = $request->po_keluar_id;

        $po_keluar = PoKeluar::findOrFail($id);

        $invoice = $po_keluar->id."/".$po_keluar->kode_po_tujuan."-DRT/".$this->getRomawiMonth()."/".date('y');
        $penjualan = new Penjualan();

    }
     public function sendEmail($id)
    {
        
        //send Email
        try{
            $po_keluar = PoKeluar::findOrFail($id);
            $alat_angkut = AlatAngkut::where("id_po_keluar", $id)->get();

            $dataEmail = ["po_keluar" => $po_keluar, 
                          "alat_angkut" => $alat_angkut];

            Mail::send('emails.po_keluar', $dataEmail, function ($message) {

                $message->from(Constant::$EMAIL_SENDER, Constant::$NAME_SENDER);
                $message->to(Setting::select('field_value')->where("field_name", "email_approval")->first()->field_value, null);
                $message->subject(Constant::$EMAIL_SUBJECT2);

            });

        }catch(\Exception $e){
            echo $e->getMessage();die;
        }
        //end send email

        return redirect('po-keluar')->with("message", "PO Created and sent to email!")->with("alert", "success");
    }
}

