<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\JenisBbm;


use Validator;  
use Redirect;

 
use Auth;

class JenisBbmController extends Controller
{
    public $data;
    

    public function index()
    {
        $this->data['jenis_bbm'] = JenisBbm::all();
        return view('jenis_bbm.index', $this->data);
    }

    public function create()
    {
        $this->data['jenis_bbm'] = JenisBbm::all();
        return view('jenis_bbm.create', $this->data);
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), JenisBbm::$rules);

        if($validation->passes()){
            $jenis_bbm = new JenisBbm();
            $jenis_bbm->jenis_bbm = $request->jenis_bbm;
            
            
            $jenis_bbm->save();

            /* redirect to page user */
            return Redirect::route('jenis_bbm.index')->with('alert','success')->with('message', "Sukses: Jenis Bbm $jenis_bbm->nama telah berhasil dibuat.");
        }

        /* redirect to page create user with input and error */
        return Redirect::route('jenis_bbm.create')->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $user = Auth::user();

        $this->data['jenis_bbm'] = JenisBbm::findOrFail($id);
        return view('jenis_bbm.edit', $this->data);

    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();

         /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), JenisBbm::$updaterules);
        $jenis_bbm = JenisBbm::findOrFail($id);

        if($validation->passes()){
            
            $jenis_bbm->jenis_bbm = $request->id;
            $jenis_bbm->jenis_bbm = $request->jenis_bbm;
  
            $jenis_bbm->save();

            /* redirect to page user */
            return Redirect::route('jenis_bbm.index')->with('alert','success')->with('message', "Sukses: Jenis Bbm $jenis_bbm->bbm telah berhasil diupdate.");
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }

    public function destroy($id)
    {
        $user = Auth::user();
        $jenis_bbm = JenisBbm::findOrFail($id);
        $jenis_bbm->delete();

        /* redirect to page user */
        return Redirect::route('jenis_bbm.index')->with('alert','success')->with('message', "Sukses: Jenis Bbm $jenis_bbm->bbm telah berhasil dihapus.");


    }
}
