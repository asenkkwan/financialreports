<?php

namespace App\Http\Controllers; 

use Illuminate\Http\Request;


use App\Company;
use App\Agen;
use App\Customer;
use App\PoMasuk;
use App\PoKeluar;
use App\Penjualan;
use App\Pembelian;
use App\SuratJalan;
use App\Setting;

use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public $data; 
  


    public function __construct()
    {
        $this->middleware('auth');
        $this->data = array();
        $this->data = array("class"=>"dashboard",
                            "subclass"=>"");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
       

        $this->data['total_company'] = Company::count();
        $this->data['total_agen'] = Agen::count();
        $this->data['total_customer'] = Customer::count();
        $this->data['total_po_masuk'] = PoMasuk::count();
        $this->data['total_po_keluar'] = PoKeluar::count();
        $this->data['total_penjualan'] = Penjualan::count();
        $this->data['total_pembelian'] = Pembelian::count();
        $this->data['total_surat_jalan'] = SuratJalan::count();

        return view('dashboard', $this->data);
    }

    public function statPenjualanJsonMonthly(Request $request){

        

        $sdate = date("Y-m-d",strtotime($request->sdate));
        $edate = date("Y-m-d",strtotime($request->edate));

        $data = DB::select("select DATE_FORMAT(tanggal,'%Y-%m') as month, sum(jumlah) as total from penjualan p join item_penjualan ip on p.id = ip.id_penjualan where DATE_FORMAT(tanggal,'%Y-%m') between ? and ? group by DATE_FORMAT(tanggal,'%Y-%m')", [$sdate, $edate]);

        $start    = (new DateTime($sdate))->modify('first day of this month');
        $end      = (new DateTime($edate))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($start, $interval, $end);

        foreach ($period as $dt) {
            echo $dt->format("Y-m") . "<br>\n";
        }
    }
    public function setting()
    {
        $user = Auth::user();
        if($user->role != 'superadmin'){
            abort(404);
        }
        $this->data['settings'] = Setting::all();

        return view('setting', $this->data);
    }

    public function settingSave(Request $request){
        $user = Auth::user();
        if($user->role != 'superadmin'){
            abort(404);
        }

        unset($_POST['_token']);
        foreach($_POST as $key=>$val){
            $data = Setting::where("field_name", $key)->first();
            $data->field_value = $val;
            $data->save();
        }

        return redirect()->back()->with("message", "Setting Updated")->with("alert", "success");
    }
}

