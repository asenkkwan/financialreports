<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SupplyPoint;


use Validator;  
use Redirect;

 
use Auth;

class SupplyPointController extends Controller
{
    public $data;
    

    public function index()
    {
        $this->data['supply_point'] = SupplyPoint::all();
        return view('supply_point.index', $this->data);
    }

    public function create()
    {
        $this->data['supply_point'] = SupplyPoint::all();
        return view('supply_point.create', $this->data);
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), SupplyPoint::$rules);

        if($validation->passes()){
            $supply_point = new SupplyPoint();
            $supply_point->supply_point = $request->supply_point;
            
            
            $supply_point->save();

            /* redirect to page user */
            return Redirect::route('supply_point.index')->with('alert','success')->with('message', "Sukses : Supply Point $supply_point->supply_point telah berhasil dibuat.");
        }

        /* redirect to page create user with input and error */
        return Redirect::route('supply_point.create')->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $user = Auth::user();

        $this->data['supply_point'] = SupplyPoint::findOrFail($id);
        return view('supply_point.edit', $this->data);

    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();

         /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), SupplyPoint::$updaterules);
        $supply_point = SupplyPoint::findOrFail($id);

        if($validation->passes()){
             
            $supply_point->supply_point = $request->id;
            $supply_point->supply_point = $request->supply_point;
  
            $supply_point->save();

            /* redirect to page user */
            return Redirect::route('supply_point.index')->with('alert','success')->with('message', "Sukses : Supply Point $supply_point->supply_point telah berhasil diupdate.");
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }

    public function destroy($id)
    {
        $user = Auth::user();
        $supply_point = SupplyPoint::findOrFail($id);
        $supply_point->delete();

        /* redirect to page user */
        return Redirect::route('supply_point.index')->with('alert','success')->with('message', "Sukses : Supply Point $supply_point->supply_point telah berhasil dihapus.");


    }
}
