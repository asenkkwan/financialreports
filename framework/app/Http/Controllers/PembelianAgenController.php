<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PembelianAgen;
use App\AlatAngkut;
use App\Customer;
use App\Agen;
use App\Company;
use App\CAddress;
use App\PoKeluarAgen;
use App\Utilities\Constant; 


use Validator;
use Redirect; 
use Auth;  

class PembelianAgenController extends Controller
{
    public $data;

    public function __construct(){
        $this->data = ['class'=>'pembelian_agen'];
    }

    public function index(Request $request)
    {
        $this->data['nama_agen'] = $request->nama_agen;
        $this->data['no_po'] = $request->no_po;
        $this->data['nama_supplier'] = $request->nama_supplier;
        $this->data['produk'] = $request->produk;
        $this->data['sdate'] = $request->sdate;
        $this->data['edate'] = $request->edate;
 
        $pembelian_agen = PembelianAgen::orderBy('tanggal', 'desc');
        $total_all_qty = new PembelianAgen();
        $total_all_harga_beli = new PembelianAgen();
        $total_all_harga_jual = new PembelianAgen();
        $total_all_harga_dasar = new PembelianAgen();
        $total_all_dpp = new PembelianAgen();
        $total_all_jumlah_ppn = new PembelianAgen();
        $total_all_pbbkb_dasar = new PembelianAgen();
        $total_all_pbbkb_jual = new PembelianAgen();
        $total_all_jumlah = new PembelianAgen();

        if($this->data['nama_agen']){
            $pembelian_agen = $pembelian_agen->where("nama_agen","LIKE", "%".$this->data['nama_agen']."%");
            $total_all_qty = $total_all_qty->where("nama_agen","LIKE", "%".$this->data['nama_agen']."%");
            $total_all_harga_beli = $total_all_harga_beli->where("nama_agen","LIKE", "%".$this->data['nama_agen']."%");
            $total_all_harga_jual = $total_all_harga_jual->where("nama_agen","LIKE", "%".$this->data['nama_agen']."%");
            $total_all_harga_dasar = $total_all_harga_dasar->where("nama_agen","LIKE", "%".$this->data['nama_agen']."%");
            $total_all_dpp = $total_all_dpp->where("nama_agen","LIKE", "%".$this->data['nama_agen']."%");
            $total_all_jumlah_ppn = $total_all_jumlah_ppn->where("nama_agen","LIKE", "%".$this->data['nama_agen']."%");
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->where("nama_agen","LIKE", "%".$this->data['nama_agen']."%");
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->where("nama_agen","LIKE", "%".$this->data['nama_agen']."%");
            $total_all_jumlah = $total_all_jumlah->where("nama_agen","LIKE", "%".$this->data['nama_agen']."%");
        }

        if($this->data['no_po']){
            $pembelian_agen = $pembelian_agen->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_qty = $total_all_qty->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_harga_beli = $total_all_harga_beli->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_harga_jual = $total_all_harga_jual->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_harga_dasar = $total_all_harga_dasar->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_dpp = $total_all_dpp->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_jumlah_ppn = $total_all_jumlah_ppn->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_jumlah = $total_all_jumlah->where("no_po","LIKE", "%".$this->data['no_po']."%");
        }

        if($this->data['nama_supplier']){
            $pembelian_agen = $pembelian_agen->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_qty = $total_all_qty->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_harga_beli = $total_all_harga_beli->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_harga_jual = $total_all_harga_jual->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_harga_dasar = $total_all_harga_dasar->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_dpp = $total_all_dpp->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_jumlah_ppn = $total_all_jumlah_ppn->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_jumlah = $total_all_jumlah->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
        }

        if($this->data['produk']){
            $pembelian_agen = $pembelian_agen->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_qty = $total_all_qty->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_harga_beli = $total_all_harga_beli->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_harga_jual = $total_all_harga_jual->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_harga_dasar = $total_all_harga_dasar->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_dpp = $total_all_dpp->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_jumlah_ppn = $total_all_jumlah_ppn->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_jumlah = $total_all_jumlah->where("produk","LIKE", "%".$this->data['produk']."%");
        }

        if($this->data['sdate'] && $this->data['edate']){
            $pembelian_agen = $pembelian_agen->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_qty = $total_all_qty->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_harga_jual = $total_all_harga_jual->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_harga_dasar = $total_all_harga_dasar->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_dpp = $total_all_dpp->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_jumlah_ppn = $total_all_jumlah_ppn->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_jumlah = $total_all_jumlah->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
        }


        $pembelian_agen = $pembelian_agen->paginate(20);
        $total_all_qty = $total_all_qty->sum("qty");
        $total_all_harga_beli = $total_all_harga_beli->sum("harga_beli");
        $total_all_harga_jual = $total_all_harga_jual->sum("harga_jual");
        $total_all_harga_dasar = $total_all_harga_dasar->sum("harga_dasar");
        $total_all_dpp = $total_all_dpp->sum("dpp");
        $total_all_jumlah_ppn = $total_all_jumlah_ppn->sum("jumlah_ppn");
        $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->sum("pbbkb_dasar");
        $total_all_pbbkb_jual = $total_all_pbbkb_jual->sum("pbbkb_jual");
        $total_all_jumlah = $total_all_jumlah->sum("jumlah");
        
        $this->data['subclass'] = "list";
        $this->data['pembelian_agen'] = $pembelian_agen;
        $this->data['total_all_qty'] = $total_all_qty;
        $this->data['total_all_harga_beli'] = $total_all_harga_beli;
        $this->data['total_all_harga_jual'] = $total_all_harga_jual;
        $this->data['total_all_harga_dasar'] = $total_all_harga_dasar;
        $this->data['total_all_dpp'] = $total_all_dpp;
        $this->data['total_all_jumlah_ppn'] = $total_all_jumlah_ppn;
        $this->data['total_all_pbbkb_dasar'] = $total_all_pbbkb_dasar;
        $this->data['total_all_pbbkb_jual'] = $total_all_pbbkb_jual;
        $this->data['total_all_jumlah'] = $total_all_jumlah;

        return view('pembelian_agen.index', $this->data);
    }

   public function create(Request $request)
    {
        //

        if($request->filter_po_keluar){
            $this->data['po_keluar_agen'] = PoKeluarAgen::where("nomor_po", $request->filter_po_keluar)->firstOrFail();
            // $this->data['item_po_keluar'] = AlatAngkut::where("id_po_keluar", $this->data['po_keluar']->id)->get();
        }

        $list_po_keluar = PoKeluarAgen::select("nomor_po")->where("status", 1)->get();
        $dataPo = [];
        foreach($list_po_keluar as $val){
            if(PembelianAgen::where("no_po", $val->nomor_po)->count() == 0){
                array_push($dataPo, $val->nomor_po);
            }
        }

        
        $this->data['list_po_keluar'] = $dataPo;
        $this->data['nama_perusahaan'] = Customer::all();
        $this->data['kantor_cabang'] = Company::all();


        $this->data['subclass'] = 'create';

        return view('pembelian_agen.create', $this->data);
    }

    public function store(Request $request)
    {
        $auth = Auth::user();

        // $po_keluar = PoKeluar::findOrFail($request->nama_perusahaan);
        

        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), PembelianAgen::$rules);

        

        if($validation->passes()){
            $pembelian_agen = new PembelianAgen();
            $pembelian_agen->no_po = $request->no_po;
            $pembelian_agen->no_invoice = $request->no_invoice;
            $pembelian_agen->no_faktur_pajak = $request->no_faktur_pajak;
            $pembelian_agen->tanggal = $request->tanggal;
            $pembelian_agen->nama_agen = $request->nama_agen;
            $pembelian_agen->alamat_agen = $request->alamat_agen;
            $pembelian_agen->alamat_kirim = $request->alamat_kirim;
            $pembelian_agen->nama_supplier = $request->nama_supplier;
            $pembelian_agen->produk = $request->produk;
            $pembelian_agen->qty = (int)$request->qty;
            $pembelian_agen->notes = $request->notes; 
            $pembelian_agen->harga_beli = (int)$request->harga_beli;
            $pembelian_agen->harga_jual = (int)$request->harga_jual;
            $pembelian_agen->pbbkb = $request->pbbkb;
            $pembelian_agen->ppn = (int)$request->ppn; 

            $pembelian_agen->harga_dasar = $pembelian_agen->harga_beli/(1+($pembelian_agen->ppn/100)+($pembelian_agen->pbbkb/100));
            $pembelian_agen->dpp = ((int)$pembelian_agen->qty)*$pembelian_agen->harga_dasar;
            $pembelian_agen->jumlah_ppn = $pembelian_agen->dpp*($pembelian_agen->ppn/100);
            $pembelian_agen->pbbkb_dasar = (int)$pembelian_agen->harga_jual/(1+($pembelian_agen->ppn/100)+($pembelian_agen->pbbkb/100));
            $pembelian_agen->pbbkb_jual = $pembelian_agen->qty*$pembelian_agen->pbbkb_dasar*($pembelian_agen->pbbkb/100);
            $pembelian_agen->jumlah = (int)$pembelian_agen->dpp+$pembelian_agen->jumlah_ppn+$pembelian_agen->pbbkb_jual;
           
            $pembelian_agen->created_by = $auth->name;

            $pembelian_agen->save();

            /* redirect to page user */
            return Redirect::route('pembelian-agen.index')->with('alert','success')->with('message', "pembelian agen baru berhasil disimpan");
        }

        /* redirect to page create user with input and error */
        return Redirect::route('pembelian_agen.create')->withInput()->withErrors($validation)->with('alert','error')->with('message', 'Silahkan periksa kembali data yang diinput');
    }

    public function show($id)
    {
        $user = Auth::user();

        $this->data['pembelian_agen'] = PembelianAgen::findOrFail($id);
        $this->data['po_keluar_agen'] = PoKeluarAgen::findOrFail($id);
        $this->data['nama_agen'] = Agen::all();
        $this->data['alamat_agen'] = Agen::all();
        
        $this->data['kantor_cabang'] = Company::all();

        // $caddress = CAddress::where("address_code", $this->data['pembelian_agen']->kode_lokasi_perusahaan)->firstOrFail();
        // $this->data['customer'] = Customer::findOrFail($caddress->id_customer);
        // $this->data['caddress'] = $caddress;
        $this->data['agen'] = Agen::all();
       
        $this->data['subclass'] = 'detail-pembelian2';
        return view('pembelian_agen.detail_pembelian2', $this->data);
    }

    public function edit($id)
    {
        $this->data['pembelian_agen'] = PembelianAgen::findOrFail($id);
        // print_r($this->data['pembelian_agen']);die;
        // $this->data['po_keluar'] = PoKeluar::where("nomor_po", $this->data['pembelian_agen']->no_po)->firstOrFail();
       

        $this->data['subclass'] = 'edit';
        return view('pembelian_agen.edit', $this->data);
    }

    public function update(Request $request, $id)
    {
        $auth = Auth::user();
        $pembelian_agen = PembelianAgen::findOrFail($id);
       
        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), PembelianAgen::$updaterules);

        if($validation->passes()){
            $pembelian_agen->no_po = $request->no_po;
            $pembelian_agen->no_invoice = $request->no_invoice;
            $pembelian_agen->no_faktur_pajak = $request->no_faktur_pajak;
            $pembelian_agen->tanggal = $request->tanggal;
            $pembelian_agen->nama_agen = $request->nama_agen;
            $pembelian_agen->alamat_agen = $request->alamat_agen;
            $pembelian_agen->alamat_kirim = $request->alamat_kirim;
            $pembelian_agen->nama_supplier = $request->nama_supplier;
            $pembelian_agen->produk = $request->produk;
            $pembelian_agen->qty = (int)$request->qty;
            $pembelian_agen->notes = $request->notes;
            $pembelian_agen->harga_beli = (int)$request->harga_beli;
            $pembelian_agen->harga_jual = (int)$request->harga_jual;
            $pembelian_agen->pbbkb = $request->pbbkb;
            $pembelian_agen->ppn = (int)$request->ppn;

            $pembelian_agen->harga_dasar = $pembelian_agen->harga_beli/(1+($pembelian_agen->ppn/100)+($pembelian_agen->pbbkb/100));
            $pembelian_agen->dpp = ((int)$pembelian_agen->qty)*$pembelian_agen->harga_dasar;
            $pembelian_agen->jumlah_ppn = $pembelian_agen->dpp*($pembelian_agen->ppn/100);
            $pembelian_agen->pbbkb_dasar = (int)$pembelian_agen->harga_jual/(1+($pembelian_agen->ppn/100)+($pembelian_agen->pbbkb/100));
            $pembelian_agen->pbbkb_jual = $pembelian_agen->qty*$pembelian_agen->pbbkb_dasar*($pembelian_agen->pbbkb/100);
            $pembelian_agen->jumlah = (int)$pembelian_agen->dpp+$pembelian_agen->jumlah_ppn+$pembelian_agen->pbbkb_jual;
            // $pembelian_agen->keterangan = $request->keterangan;
            $pembelian_agen->created_by = $auth->name;
 
            $pembelian_agen->save();

            /* redirect to page user */
            return Redirect::route('pembelian-agen.index')->with('alert','success')->with('message', "pembelian agen baru berhasil disimpan");
        }

        /* redirect to page create user with input and error */
        return Redirect::route('pembelian_agen.create')->withInput()->withErrors($validation)->with('alert','error')->with('message', 'Silahkan periksa kembali data yang diinput');
    } 

    public function destroy($id)
    {
        $data = PembelianAgen::findOrFail($id);
        $data->delete();

        return Redirect::route('pembelian-agen.index')->with('alert','success')->with('message', "Data pembelian agen berhasil dihapus");
    }
     public function invoicePrint(Request $request,$id)
    {
        $user = Auth::user();
        $this->data['pembelian_agen'] = PembelianAgen::findOrFail($id); 
        $this->data['po_keluar_agen'] = PoKeluarAgen::findOrFail($id);
        $this->data['company'] = Company::where("name", $this->data['pembelian_agen']->nama_supplier)->first();

        // $this->data['kantor_cabang'] = Company::all();
  
        return view('pembelian_agen.invoice-print2', $this->data);
    }
    public function kwitansiPrint(Request $request,$id)
    {
        $user = Auth::user();
        $this->data['pembelian_agen'] = PembelianAgen::findOrFail($id); 
        // $this->data['kantor_cabang'] = Company::all();
  
        return view('pembelian_agen.kwitansi-print', $this->data);
    }
}
