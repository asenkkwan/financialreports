<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penjualan;
use App\ItemPoMasuk;
use App\ItemPenjualan;
use App\Company; 
use App\Customer; 
use App\CAddress; 
use App\Agen;
use App\PoMasuk; 
use App\Utilities\Constant; 
 
use Validator; 
use Redirect;
  
 
use Auth;
use DB; 
use Excel; 

class PiutangController extends Controller
{
    public $data;

    public function __construct(){
        $this->data = ['class'=>'penjualan'];
    }
    public function index(Request $request) 
    {

        $this->data['nama_perusahaan'] = $request->nama_perusahaan;
        $this->data['sdate'] = $request->sdate;
        $this->data['edate'] = $request->edate;
        $this->data['status'] = $request->status;

         $penjualan = Customer::select(DB::raw("customer.name as nama_perusahaan, 
            (SELECT sum(ip.qty) FROM penjualan p JOIN item_penjualan ip ON p.id = ip.id_penjualan WHERE p.id_customer = customer.id) as total_qty, 
            (SELECT sum(ip.jumlah) FROM penjualan p JOIN item_penjualan ip ON p.id = ip.id_penjualan WHERE p.id_customer = customer.id) as total_tagihan, 
            (SELECT sum(pb.kredit) FROM penjualan p JOIN item_penjualan ip ON p.id = ip.id_penjualan JOIN pembayaran pb ON ip.id = pb.id_item_penjualan WHERE p.id_customer = customer.id) as total_bayar"));


        $total_all_qty = ItemPenjualan::leftJoin('penjualan', "penjualan.id", "=", "item_penjualan.id_penjualan")
                                        ->leftJoin('customer', "customer.id", "=","penjualan.id_customer")
                                        ->leftJoin('pembayaran', "pembayaran.id_item_penjualan", "=","item_penjualan.id");
        $total_all_tagihan = ItemPenjualan::leftJoin('penjualan', "penjualan.id", "=", "item_penjualan.id_penjualan")
                                        ->leftJoin('customer', "customer.id", "=","penjualan.id_customer")
                                        ->leftJoin('pembayaran', "pembayaran.id_item_penjualan", "=","item_penjualan.id");
        $total_all_bayar = ItemPenjualan::leftJoin('penjualan', "penjualan.id", "=", "item_penjualan.id_penjualan")
                                        ->leftJoin('customer', "customer.id", "=","penjualan.id_customer")
                                        ->leftJoin('pembayaran', "pembayaran.id_item_penjualan", "=","item_penjualan.id");

         if($this->data['nama_perusahaan']){
            $penjualan = $penjualan->where("customer.name","LIKE", "%".$this->data['nama_perusahaan']."%");
             $total_all_qty = $total_all_qty->where("customer.name", "LIKE", "%".$this->data['nama_perusahaan']."%");
        }
        $penjualan=$penjualan->paginate(15);
        $total_all_qty = $total_all_qty->sum("item_penjualan.qty");
        $total_all_tagihan = $total_all_tagihan->sum("item_penjualan.jumlah");
        $total_all_bayar = $total_all_bayar->sum("pembayaran.kredit");
        
         $this->data['subclass'] = "list";
         $this->data['penjualan'] = $penjualan;
         $this->data['total_all_qty'] = $total_all_qty;
         $this->data['total_all_tagihan'] = $total_all_tagihan;
         $this->data['total_all_bayar'] = $total_all_bayar;
         

         // print_r($penjualan);die();
       

        return view('piutang.index', $this->data);
    }
    public function piutangExport(Request $request){
        $user = Auth::user();

        $param['filter_sdate']    = $request->sdate;
        $param['filter_edate']    = $request->edate;
        $param['filter_nama_perusahaan']    = $request->nama_perusahaan;
        // print_r($request->status);die();
        $penjualan = Customer::select(DB::raw("customer.name as nama_perusahaan, customer.id as id_customer,
            (SELECT sum(ip.qty) FROM penjualan p JOIN item_penjualan ip ON p.id = ip.id_penjualan WHERE p.id_customer = customer.id) as total_qty, 
            (SELECT sum(ip.jumlah) FROM penjualan p JOIN item_penjualan ip ON p.id = ip.id_penjualan WHERE p.id_customer = customer.id) as total_tagihan, 
            (SELECT sum(pb.kredit) FROM penjualan p JOIN item_penjualan ip ON p.id = ip.id_penjualan JOIN pembayaran pb ON ip.id = pb.id_item_penjualan WHERE p.id_customer = customer.id) as total_bayar"));
            
                                 

        if($param['filter_nama_perusahaan']){
            $param['filter_nama_perusahaan'] = $param['filter_nama_perusahaan'].'%';
            $pembayaran = $penjualan->where("customer.name","LIKE", "%".$param['filter_nama_perusahaan']."%");
        }


       // print_r($penjualan->get());die();

       if($penjualan->count() > 0){
            try{
                return Excel::create('datapiutang', function($excel) use($penjualan){

                    $penjualan->chunk(3000, function ($data) use($excel) {
                        $collection = $this->transformCollectionPiutang($data);

                        $excel->sheet('Penjualan', function($sheet) use($collection){
                            $sheet->fromModel($collection, null, 'A1', true);
                        });
                    });

                })->export('xls');

            }catch(Exception $e)
            {
                return false;
            }
        }

        return redirect()->back(); 
    }

    private function transformCollectionPiutang($collection){
        
        return array_map([$this, 'transformPiutang'], $collection->toArray());
    }
    private function transformPiutang($collection){
      
            
        return [ 
            // 'Tanggal' => $collection['payment_date'],
            // 'Tanggal NTT' => $collection['ntt'],
            'Nama Perusahaan' => $collection['nama_perusahaan'],
            'Quantity' => $collection['total_qty'],
            'Jumlah Pembayaran' => number_format($collection['total_bayar'],0),
            'Jumlah Tagihan' => number_format($collection['total_tagihan'],0),        
            
        ];
    }
    public function piutangHistory(Request $request)
    {
        $this->data['nama_perusahaan'] = $request->nama_perusahaan;
        $this->data['no_po'] = $request->no_po;
        $this->data['no_invoice'] = $request->no_invoice;
        $this->data['nama_supplier'] = $request->nama_supplier;
        $this->data['sdate'] = $request->sdate;
        $this->data['edate'] = $request->edate;
        $this->data['status'] = $request->status;
         // print_r($this->data['status']);die();
        // $penjualan = Penjualan::orderBy('tanggal', 'desc');
        // $total_all_qty = new Penjualan();
        // $total_all_harga_beli = new Penjualan();
        // $total_all_harga_jual = new Penjualan();
        // $total_all_harga_dasar = new Penjualan();
        // $total_all_dpp = new Penjualan();
        // $total_all_jumlah_ppn = new Penjualan();
        // $total_all_pbbkb_dasar = new Penjualan();
        // $total_all_pbbkb_jual = new Penjualan();
        // $total_all_jumlah =  Penjualan::whereIsNull('delete_at');

         // $items = ItemPenjualan::all();
         //              print_r($items);die; 

        $penjualan = Penjualan::orderBy('tanggal', 'desc');
        $total_all_qty = ItemPenjualan::where("penjualan.deleted_at", Null)->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan");
        $total_all_harga_jual = ItemPenjualan::where("penjualan.deleted_at", Null)->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan");
        $total_all_harga_dasar = ItemPenjualan::where("penjualan.deleted_at", Null)->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan");
        $total_all_pbbkb_dasar = ItemPenjualan::where("penjualan.deleted_at", Null)->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan");
        $total_all_pbbkb_jual = ItemPenjualan::where("penjualan.deleted_at", Null)->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan");
        $total_all_dpp = ItemPenjualan::where("penjualan.deleted_at", Null)->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan");
        $total_all_ppn_total = Penjualan::where("penjualan.deleted_at", Null);
        $total_all_jumlah = ItemPenjualan::where("penjualan.deleted_at", Null)->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan");
        

        if($this->data['nama_perusahaan']){
            $penjualan = $penjualan->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_qty = $total_all_qty->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_harga_jual = $total_all_harga_jual->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_harga_dasar = $total_all_harga_dasar->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_dpp = $total_all_dpp->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_ppn_total = $total_all_ppn_total->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_jumlah = $total_all_jumlah->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
        }
        return view('penjualan.index', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
