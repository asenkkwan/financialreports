<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 

use App\PoKeluarAgen;
use App\ItemPoKeluar;
use App\Customer;   
use App\Company;  
use App\CAddress; 
use App\JenisBbm;
use App\SupplyPoint;
use App\AlatAngkut;
use App\Agen;
use App\Utilities\Constant; 
use App\User;
use App\Setting; 


use Validator; 
use Redirect;
use Mail;  
use Auth; 
use DB;


class PoKeluarAgenController extends Controller
{
   public $data;

    public function __construct(){
      $this->data = ['class'=>'po_keluar_agen'];
    } 
    
    public function index(Request $request)
    {
        $this->data['nama_agen'] = $request->nama_agen;
        $this->data['nomor_po'] = $request->nomor_po;
        $this->data['penyalur'] = $request->penyalur;
        $this->data['jenis_bbm'] = $request->jenis_bbm;
        $this->data['sdate'] = $request->sdate;
        $this->data['edate'] = $request->edate;

        $po_keluar_agen = PoKeluarAgen::orderBy('tanggal', 'desc');

        if($this->data['nama_agen']){
            $po_keluar_agen = $po_keluar_agen->where("nama_agen", "LIKE", "%".$this->data['nama_agen']."%");
        }

        if($this->data['nomor_po']){
            $po_keluar_agen = $po_keluar_agen->where("nomor_po", "LIKE", "%".$this->data['nomor_po']."%");
        }

        if($this->data['penyalur']){
            $po_keluar_agen = $po_keluar_agen->where("penyalur","LIKE", "%".$this->data['penyalur']."%");
        }

        if($this->data['jenis_bbm']){
            $po_keluar_agen = $po_keluar_agen->where("jenis_bbm","LIKE", "%".$this->data['jenis_bbm']."%");
        }

        if($this->data['sdate'] && $this->data['edate']){
            $po_keluar_agen = $po_keluar_agen->whereBetween("tanggal", [$this->data['sdate'] , $this->data['edate'] ]);
        }

        $po_keluar_agen = $po_keluar_agen->where("archive",0)->paginate(20);
        
        $this->data['subclass'] = "list";
        $this->data['po_keluar_agen'] = $po_keluar_agen;
        return view('po_keluar_agen.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
        $this->data['kantor_cabang'] = Company::all();
        $this->data['nama_agen'] = Agen::all();     
        $this->data['alamat_agen'] = Agen::all();
        $this->data['jenis_bbm'] = JenisBbm::all();
        $this->data['supply_point'] = SupplyPoint::all();
        
        return view('po_keluar_agen.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user(); 

        $company = Company::findOrFail($request->penyalur);
        $agen = Agen::findOrFail($request->nama_agen);
        $jenis_bbm = JenisBbm::findOrFail($request->jenis_bbm);
        $supply_point = SupplyPoint::findOrFail($request->supply_point);
        // $caddress = CAddress::findOrFail($request->alamat_konsumen);
        
        
        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), PoKeluarAgen::$rules);

        if($validation->passes()){
            $po_keluar_agen = new PoKeluarAgen();
            $po_keluar_agen->tanggal = $request->tanggal;
            $po_keluar_agen->tanggal_pengaliran = $request->tanggal_pengaliran;
            $po_keluar_agen->nomor_po = $request->nomor_po;
            $po_keluar_agen->penyalur = $company->name;
            $po_keluar_agen->alamat_penyalur = $company->address;
            $po_keluar_agen->no_skp_penyalur = $company->no_skp_migas;
            // $po_keluar_agen->kode_lokasi_konsumen = $caddress->address_code;
            $po_keluar_agen->npwp_penyalur = $company->npwp;
            $po_keluar_agen->no_sold_penyalur = $company->no_sold_to_penyalur;
            $po_keluar_agen->alamat_penyalur = $company->address;
            $po_keluar_agen->nama_agen = $agen->name;
            // $po_keluar_agen->jenis_konsumen = $customer->jenis_usaha; 
            $po_keluar_agen->alamat_agen = $agen->address;
            $po_keluar_agen->alamat_kirim = $request->alamat_kirim;
            $po_keluar_agen->supply_point = $supply_point->supply_point; 
            $po_keluar_agen->jenis_bbm = $jenis_bbm->jenis_bbm;
            $po_keluar_agen->qty = $request->qty;
            $po_keluar_agen->pecahan = $request->pecahan;
            $po_keluar_agen->harga_beli = $request->harga_beli;
            $po_keluar_agen->harga_jual = $request->harga_jual;
            $po_keluar_agen->pbbkb = $request->pbbkb; 
            $po_keluar_agen->pph = $request->pph;
            $po_keluar_agen->ppn = (int)$request->ppn; 
            $po_keluar_agen->cara_pembayaran = $request->cara_pembayaran;
            $po_keluar_agen->nama_pic_penyalur = $request->nama_pic_penyalur;
            $po_keluar_agen->nama_pic_agen = $request->nama_pic_agen;
            $po_keluar_agen->alat_angkut = $request->alat_angkut;
              
            // $po_keluar_agen->bukti_setor_bank = $request->bukti_setor_bank; 

            $po_keluar_agen->harga_dasar = $po_keluar_agen->harga_beli/(1+($po_keluar_agen->ppn/100)+($po_keluar_agen->pbbkb/100));
            $po_keluar_agen->dpp = $po_keluar_agen->qty*$po_keluar_agen->harga_dasar;
            $po_keluar_agen->harga_jual_penyalur = $po_keluar_agen->harga_beli/1.1*$po_keluar_agen->qty;
            $po_keluar_agen->harga_ppn = $po_keluar_agen->harga_jual_penyalur*$po_keluar_agen->ppn/100;
            $po_keluar_agen->harga_jual_ppn = $po_keluar_agen->harga_beli+$po_keluar_agen->harga_beli*$po_keluar_agen->ppn/100;
            $po_keluar_agen->total = $po_keluar_agen->harga_jual_penyalur+$po_keluar_agen->harga_ppn;
            $po_keluar_agen->margin_penyalur = $po_keluar_agen->pbbkb_dasar-$po_keluar_agen->harga_dasar;
            $po_keluar_agen->jumlah_ppn = $po_keluar_agen->dpp*($po_keluar_agen->ppn/100);
            $po_keluar_agen->pbbkb_dasar = $po_keluar_agen->harga_jual/(1+($po_keluar_agen->ppn/100)+($po_keluar_agen->pbbkb/100));
            $po_keluar_agen->pbbkb_jual = $po_keluar_agen->qty*$po_keluar_agen->harga_dasar*($po_keluar_agen->pbbkb/100);
            $po_keluar_agen->jumlah = $po_keluar_agen->dpp+$po_keluar_agen->jumlah_ppn+$po_keluar_agen->pbbkb_jual;  

            $po_keluar_agen->save();
           

             //send Email
            $this->sendEmail($po_keluar_agen->id);

         /* redirect to page user */
            return Redirect::route('po_keluar_agen.index')->with('alert','success')->with('message', "Sukses: Po Keluar Agen $po_keluar_agen->nomor_po telah berhasil ditambahkan.");
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');

    }
    public function edit($id)
    {
        $this->data['po_keluar_agen'] = PoKeluarAgen::findOrFail($id);
        $this->data['kantor_cabang'] = Company::all();
        $this->data['jenis_bbm'] = JenisBbm::all();
        $this->data['supply_point'] = SupplyPoint::all();
        $this->data['pbbkb'] = Constant::$PBBKB;
        $this->data['pph'] = Constant::$PPH;

        $this->data['nama_agen'] = Agen::all();
        // $this->data['alamat_agen'] = Agen::all();

        $this->data['agen'] = Agen::all();

        $this->data['subclass'] = 'edit';
        return view('po_keluar_agen.edit', $this->data);
    }

    
    public function update(Request $request, $id)
    {
        //
        $user = Auth::user();
        $po_keluar_agen = PoKeluarAgen::findOrFail($id);
         /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), PoKeluarAgen::$updaterules);
        $company = Company::findOrFail($request->penyalur);
        // print_r($company);die();
        $agen = Agen::findOrFail($request->nama_agen);
        $jenis_bbm = JenisBbm::findOrFail($request->jenis_bbm);
        $supply_point = SupplyPoint::findOrFail($request->supply_point);
        // $caddress = CAddress::findOrFail($request->alamat_konsumen);

        
        

        if($validation->passes()){
            $po_keluar_agen->tanggal = $request->tanggal;
            $po_keluar_agen->tanggal_pengaliran = $request->tanggal_pengaliran;
            $po_keluar_agen->nomor_po = $request->nomor_po;
            $po_keluar_agen->penyalur = $company->name;
            $po_keluar_agen->alamat_penyalur = $company->address;
            $po_keluar_agen->no_skp_penyalur = $company->no_skp_migas;
            // $po_keluar_agen->kode_lokasi_konsumen = $caddress->address_code;
            $po_keluar_agen->npwp_penyalur = $company->npwp;
            $po_keluar_agen->no_sold_penyalur = $company->no_sold_to_penyalur;
            $po_keluar_agen->alamat_penyalur = $company->address;
            $po_keluar_agen->nama_agen = $agen->name;
            // $po_keluar_agen->jenis_konsumen = $customer->jenis_usaha;
            $po_keluar_agen->alamat_agen = $agen->address;
            $po_keluar_agen->alamat_kirim = $request->alamat_kirim;
            $po_keluar_agen->supply_point = $supply_point->supply_point; 
            // $po_keluar_agen->sales_district = $request->sales_district;
            $po_keluar_agen->jenis_bbm = $jenis_bbm->jenis_bbm;
            $po_keluar_agen->qty = $request->qty;
            $po_keluar_agen->pecahan = $request->pecahan;
            $po_keluar_agen->harga_beli = $request->harga_beli;
            $po_keluar_agen->harga_jual = $request->harga_jual;
            $po_keluar_agen->pbbkb = $request->pbbkb; 
            $po_keluar_agen->pph = $request->pph;
            $po_keluar_agen->ppn = (int)$request->ppn; 
            $po_keluar_agen->cara_pembayaran = $request->cara_pembayaran;
            $po_keluar_agen->nama_pic_penyalur = $request->nama_pic_penyalur;
            $po_keluar_agen->nama_pic_agen = $request->nama_pic_agen;
            $po_keluar_agen->alat_angkut = $request->alat_angkut;
              
            // $po_keluar_agen->bukti_setor_bank = $request->bukti_setor_bank; 
            // print_r($po_keluar_agen->pbbkb);die();
            $po_keluar_agen->harga_dasar = $po_keluar_agen->harga_beli/(1+($po_keluar_agen->ppn/100)+($po_keluar_agen->pbbkb/100));

            $po_keluar_agen->dpp = $po_keluar_agen->qty*$po_keluar_agen->harga_dasar;
            $po_keluar_agen->harga_jual_penyalur = $po_keluar_agen->harga_beli/1.1*$po_keluar_agen->qty;
            $po_keluar_agen->harga_ppn = $po_keluar_agen->harga_jual_penyalur*$po_keluar_agen->ppn/100;
            $po_keluar_agen->harga_jual_ppn = $po_keluar_agen->harga_beli+$po_keluar_agen->harga_beli*$po_keluar_agen->ppn/100;
            $po_keluar_agen->total = $po_keluar_agen->harga_jual_penyalur+$po_keluar_agen->harga_ppn;
            $po_keluar_agen->margin_penyalur = $po_keluar_agen->pbbkb_dasar-$po_keluar_agen->harga_dasar;
            $po_keluar_agen->jumlah_ppn = $po_keluar_agen->dpp*($po_keluar_agen->ppn/100);
            $po_keluar_agen->pbbkb_dasar = $po_keluar_agen->harga_jual/(1+($po_keluar_agen->ppn/100)+($po_keluar_agen->pbbkb/100));
           $po_keluar_agen->pbbkb_jual = $po_keluar_agen->qty*$po_keluar_agen->harga_dasar*($po_keluar_agen->pbbkb/100);
            $po_keluar_agen->jumlah = $po_keluar_agen->dpp+$po_keluar_agen->jumlah_ppn+$po_keluar_agen->pbbkb_jual;   

            $po_keluar_agen->save();

            /* redirect to page user */
             return Redirect::route('po_keluar_agen.index')->with('alert','success')->with('message', "Sukses: PO Keluar $po_keluar_agen->id_customer telah berhasil diupdate.");
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }

    public function createItem($id)
    {
        //
        $this->data['po_keluar_agen'] = PoKeluar::findOrFail($id);
        $this->data['alat_angkut'] = AlatAngkut::where("id_po_keluar_agen", $id)->get();
        $this->data['kantor_cabang'] = Company::all();
        
       
        return view('po_keluar_agen.create_item', $this->data);
    }

    public function alatAngkut(Request $request)
    { 
        $user = Auth::user(); 
        $tanggal = !empty($request->tanggal)?$request->tanggal:array();
        $nopol = !empty($request->nopol)?$request->nopol:array();
        $volume = !empty($request->volume)?$request->volume:array();
        $nama_supir = !empty($request->nama_supir)?$request->nama_supir:array();
        

        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), AlatAngkut::$rules);
        if($validation->passes()){

            if(AlatAngkut::where("id_po_keluar_agen", $request->id_po_keluar_agen)->count() > 0){
                DB::table('alat_angkut')->where('id_po_keluar_agen', '=', $request->id_po_keluar_agen)->delete();
            }

            for($i=0;$i<count($tanggal);$i++){

                if($tanggal[$i] != ""){
                    $alat_angkut = new AlatAngkut();
                    $alat_angkut->id_po_keluar_agen = $request->id_po_keluar_agen;
                    $alat_angkut->tanggal = $tanggal[$i];
                    $alat_angkut->nopol = $nopol[$i];
                    $alat_angkut->volume = $volume[$i];
                    $alat_angkut->nama_supir = $nama_supir[$i];
                
                    $alat_angkut->save();
                }
           }    

           //send Email
            $this->sendEmail($request->id_po_keluar_agen);
           
            /* redirect to page user */
             return Redirect::route('po_keluar_agen.index')->with('alert','success')->with('message', "Sukses: Item PO Keluar telah berhasil ditambahkan.");
        }

        /* redirect to page create user with input and error */ 
        return Redirect::route('po_keluar_agen.create_item')->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }
  
    public function detailPoKeluar(Request $request,$id)
    {
        //
        $user = Auth::user();
        $this->data['po_keluar_agen'] = PoKeluarAgen::findOrFail($id); 
        $this->data['company'] =  Company::where("name", $this->data['po_keluar_agen']->penyalur)->first();
  
        return view('po_keluar_agen.detail_po_keluar', $this->data);
    }
    public function invoicePrint(Request $request,$id)
    {
        // $this->data['item_po_masuk'] = ItemPoMasuk::where("id_po_masuk", $this->data['po_masuk']->id)->get();
        $user = Auth::user();
        $this->data['po_keluar_agen'] = PoKeluar::findOrFail($id); 
        $this->data['alat_angkut'] = AlatAngkut::where("id_po_keluar_agen", $id)->get();
  
        return view('po_keluar_agen.invoice-print', $this->data);
    }
    public function invoicePrintAgen(Request $request,$id)
    {
        //
        $user = Auth::user();
        $this->data['po_keluar_agen'] = PoKeluarAgen::findOrFail($id); 
        $this->data['company'] = Company::where("name", $this->data['po_keluar_agen']->penyalur)->first();
        // print_r($this->data['company']);die();
        
        // $this->data['alat_angkut'] = AlatAngkut::where("id_po_keluar_agen", $id)->get();
  
        return view('po_keluar_agen.invoice-printAgen', $this->data);
    }
    public function invoicePrintNasional(Request $request,$id) 
    {
        //
        $user = Auth::user();
        $this->data['po_keluar_agen'] = PoKeluarAgen::findOrFail($id); 
        $this->data['company'] = Company::where("name", $this->data['po_keluar_agen']->penyalur)->first();
        // $this->data['alat_angkut'] = AlatAngkut::where("id_po_keluar_agen", $id)->get();
  
        return view('po_keluar_agen.invoice-printNasional', $this->data);
    }
    public function editPoKeluar($id)
    {
        //
        $user = Auth::user();

        $this->data['po_keluar_agen'] = PoKeluar::findOrFail($id);
        $this->data['kantor_cabang'] = Company::all();
        $this->data['nama_customer'] = Customer::all();
        $this->data['no_ship_to_konsumen'] = CAddress::all();
        return view('po_keluar_agen.edit_item', $this->data);
    }

    public function destroy($id)
    {
        //
        $user = Auth::user();
        $po_keluar_agen = PoKeluarAgen::findOrFail($id);
        $po_keluar_agen->delete();

        /* redirect to page user */
        return Redirect::route('po_keluar_agen.index')->with('alert','success')->with('message', "Sukses: Po Keluar $po_keluar_agen->id_customer telah berhasil dihapus.");
    
    }
   public function approve(Request $request)
    {

       if(isset($_GET['id'])){ 
            $auth = User::where("username", $_GET['username'])->firstOrFail();
            $id = $_GET['id'];
        }else{
            $auth = Auth::user();
            if(!$auth->enable_approval){
                abort(403);
            }
            $id = $request->po_keluar_agen_id;    
        }
        

        $po_keluar_agen = PoKeluarAgen::findOrFail($id);

        if($po_keluar_agen->status == 0){
            $po_keluar_agen->status = 1;
            $po_keluar_agen->approved_by = $auth->username;
            $po_keluar_agen->save();

            if(isset($_GET['id'])){
                echo "<div style='margin-top:50px;width:100%'><center><h2>Berhasil!</h2></center></div><script>setTimeout('window.close()', 5000);</script>";
            }else{
                return redirect()->back()->with('alert','success')->with("msg", "Po Masuk berhasil Di setujui");
            }    
        }else{
            echo "<div style='margin-top:50px;width:100%'><center><h2>Po tidak ditemukan!</h2></center></div><script>setTimeout('window.close()', 5000);</script>";
        }
        
    }

    public function reject(Request $request)
    {
        
        if(isset($_GET['id'])){
            $auth = User::where("username", $_GET['username'])->firstOrFail();
            $id = $_GET['id'];
        }else{
            $auth = Auth::user();
            if(!$auth->enable_approval){
                abort(403);
            }
            // $id = $request->po_keluar_agen_id;    
        }

        $po_keluar_agen = PoKeluarAgen::findOrFail($id);

        if($po_keluar_agen->status == 0){
            $po_keluar_agen->status = -1;
            $po_keluar_agen->rejected_by = $auth->username;
            $po_keluar_agen->save();

            if(isset($_GET['id'])){
                echo "<div style='margin-top:50px;width:100%'><center><h2>PO Di Tolak!</h2></center></div><script>setTimeout('window.close()', 3000);</script>";
            }else{
                return redirect()->back()->with('alert','success')->with("msg", "Po Keluar berhasil di tolak");
            }
        }else{
            echo "<div style='margin-top:50px;width:100%'><center><h2>Po tidak ditemukan!</h2></center></div><script>setTimeout('window.close()', 5000);</script>";
        }
        
    }
    public function generateInvoice(Request $request){
        $id = $request->po_keluar_agen_id;

        $po_keluar_agen = PoKeluar::findOrFail($id);

        $invoice = $po_keluar_agen->id."/".$po_keluar_agen->kode_po_tujuan."-DRT/".$this->getRomawiMonth()."/".date('y');
        $penjualan = new Penjualan(); 

    }
     public function sendEmail($id)
    {
        //send Email
        try{
            $user = Auth::user();
            $po_keluar_agen = PoKeluarAgen::findOrFail($id);
            $company = Company::where("name", $po_keluar_agen->penyalur)->first();
            // print_r($company);die();

            $dataEmail = ["po_keluar_agen" => $po_keluar_agen,
                          "company" => $company];

            Mail::send('emails.po_keluar_agen', $dataEmail, function ($message) {

                $message->from(Constant::$EMAIL_SENDER, Constant::$NAME_SENDER);
                $message->to(Setting::select('field_value')->where("field_name", "email_approval")->first()->field_value, null);
                $message->subject(Constant::$EMAIL_SUBJECT2);

            });

        }catch(\Exception $e){
            echo $e->getMessage();die;
        }
        //end send email

        return redirect('po-keluar-agen')->with("message", "PO Created and sent to email!")->with("alert", "success");
    }
}
