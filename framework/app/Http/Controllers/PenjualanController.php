<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;   
use App\Penjualan;
use App\ItemPoMasuk;
use App\ItemPenjualan;
use App\Company; 
use App\Customer; 
use App\CAddress; 
use App\Agen;
use App\PoMasuk; 
use App\Utilities\Constant; 
 
use Validator; 
use Redirect;
  
 
use Auth;
use DB; 
use Excel; 
 
class PenjualanController extends Controller
{
    public $data;

    public function __construct(){
        $this->data = ['class'=>'penjualan'];
    }
    public function index(Request $request)
    {
        $this->data['nama_perusahaan'] = $request->nama_perusahaan;
        $this->data['no_po'] = $request->no_po;
        $this->data['no_invoice'] = $request->no_invoice;
        $this->data['nama_supplier'] = $request->nama_supplier;
        $this->data['sdate'] = $request->sdate;
        $this->data['edate'] = $request->edate;
        $this->data['status'] = $request->status;
         // print_r($this->data['status']);die();
        // $penjualan = Penjualan::orderBy('tanggal', 'desc');
        // $total_all_qty = new Penjualan();
        // $total_all_harga_beli = new Penjualan();
        // $total_all_harga_jual = new Penjualan();
        // $total_all_harga_dasar = new Penjualan();
        // $total_all_dpp = new Penjualan();
        // $total_all_jumlah_ppn = new Penjualan();
        // $total_all_pbbkb_dasar = new Penjualan();
        // $total_all_pbbkb_jual = new Penjualan();
        // $total_all_jumlah =  Penjualan::whereIsNull('delete_at');

         // $items = ItemPenjualan::all();
         //              print_r($items);die; 

        $penjualan = Penjualan::orderBy('tanggal', 'desc');
        $total_all_qty = ItemPenjualan::where("penjualan.deleted_at", Null)->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan");
        $total_all_harga_jual = ItemPenjualan::where("penjualan.deleted_at", Null)->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan");
        $total_all_harga_dasar = ItemPenjualan::where("penjualan.deleted_at", Null)->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan");
        $total_all_pbbkb_dasar = ItemPenjualan::where("penjualan.deleted_at", Null)->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan");
        $total_all_pbbkb_jual = ItemPenjualan::where("penjualan.deleted_at", Null)->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan");
        $total_all_dpp = ItemPenjualan::where("penjualan.deleted_at", Null)->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan");
        $total_all_ppn_total = Penjualan::where("penjualan.deleted_at", Null);
        $total_all_jumlah = ItemPenjualan::where("penjualan.deleted_at", Null)->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan");
        

        if($this->data['nama_perusahaan']){
            $penjualan = $penjualan->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_qty = $total_all_qty->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_harga_jual = $total_all_harga_jual->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_harga_dasar = $total_all_harga_dasar->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_dpp = $total_all_dpp->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_ppn_total = $total_all_ppn_total->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_jumlah = $total_all_jumlah->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
        }

        if($this->data['no_po']){
            $penjualan = $penjualan->where("no_po", "LIKE", "%".$this->data['no_po']."%");
            $total_all_qty = $total_all_qty->where("no_po", "LIKE", "%".$this->data['no_po']."%");
            $total_all_harga_jual = $total_all_harga_jual->where("no_po", "LIKE", "%".$this->data['no_po']."%");
            $total_all_harga_dasar = $total_all_harga_dasar->where("no_po", "LIKE", "%".$this->data['no_po']."%");
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->where("no_po", "LIKE", "%".$this->data['no_po']."%");
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->where("no_po", "LIKE", "%".$this->data['no_po']."%");
            $total_all_dpp = $total_all_dpp->where("no_po", "LIKE", "%".$this->data['no_po']."%");
            $total_all_ppn_total = $total_all_ppn_total->where("no_po", "LIKE", "%".$this->data['no_po']."%");
            $total_all_jumlah = $total_all_jumlah->where("no_po", "LIKE", "%".$this->data['no_po']."%");
        }

        if($this->data['nama_supplier']){
            $penjualan = $penjualan->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_qty = $total_all_qty->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_harga_jual = $total_all_harga_jual->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_harga_dasar = $total_all_harga_dasar->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_dpp = $total_all_dpp->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_ppn_total = $total_all_ppn_total->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_jumlah = $total_all_jumlah->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
        }

        if($this->data['no_invoice']){
            $penjualan = $penjualan->where("no_invoice","LIKE", "%".$this->data['no_invoice']."%");
            $total_all_qty = $total_all_qty->where("no_invoice","LIKE", "%".$this->data['no_invoice']."%");
            $total_all_harga_jual = $total_all_harga_jual->where("no_invoice","LIKE", "%".$this->data['no_invoice']."%");
            $total_all_harga_dasar = $total_all_harga_dasar->where("no_invoice","LIKE", "%".$this->data['no_invoice']."%");
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->where("no_invoice","LIKE", "%".$this->data['no_invoice']."%");
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->where("no_invoice","LIKE", "%".$this->data['no_invoice']."%");
            $total_all_dpp = $total_all_dpp->where("no_invoice","LIKE", "%".$this->data['no_invoice']."%");
            $total_all_ppn_total = $total_all_ppn_total->where("no_invoice","LIKE", "%".$this->data['no_invoice']."%");
            $total_all_jumlah = $total_all_jumlah->where("no_invoice","LIKE", "%".$this->data['no_invoice']."%");
        }

        if($this->data['sdate'] && $this->data['edate']){
            $penjualan = $penjualan->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_qty = $total_all_qty->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_harga_jual = $total_all_harga_jual->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_harga_dasar = $total_all_harga_dasar->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_dpp = $total_all_dpp->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_ppn_total = $total_all_ppn_total->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_jumlah = $total_all_jumlah->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
        }
        if($this->data['status']){
            $penjualan = $penjualan->where("status", "LIKE", "%".$this->data['status']."%");
            $total_all_qty = $total_all_qty->where("status", "LIKE", "%".$this->data['status']."%");
            $total_all_harga_jual = $total_all_harga_jual->where("status", "LIKE", "%".$this->data['status']."%");
            $total_all_harga_dasar = $total_all_harga_dasar->where("status", "LIKE", "%".$this->data['status']."%");
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->where("status", "LIKE", "%".$this->data['status']."%");
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->where("status", "LIKE", "%".$this->data['status']."%");
            $total_all_dpp = $total_all_dpp->where("status", "LIKE", "%".$this->data['status']."%");
            $total_all_ppn_total = $total_all_ppn_total->where("status", "LIKE", "%".$this->data['status']."%");
            $total_all_jumlah = $total_all_jumlah->where("status", "LIKE", "%".$this->data['status']."%");
        }
 
        $penjualan = $penjualan->where("archive",0)->paginate(20);
        $total_all_qty = $total_all_qty->sum("qty");
        $total_all_harga_jual = $total_all_harga_jual->sum("harga_jual");
        $total_all_harga_dasar = $total_all_harga_dasar->sum("harga_dasar");
        $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->sum("pbbkb_dasar");
        $total_all_pbbkb_jual = $total_all_pbbkb_jual->sum("pbbkb_jual");
        $total_all_dpp = $total_all_dpp->sum("dpp");
        $total_all_ppn_total = $total_all_ppn_total->sum("ppn_total");
        $total_all_jumlah = $total_all_jumlah->sum("jumlah");
        
        $this->data['subclass'] = "list";
        $this->data['penjualan'] = $penjualan;
        $this->data['total_all_qty'] = $total_all_qty;
        $this->data['total_all_harga_jual'] = $total_all_harga_jual;
        $this->data['total_all_harga_dasar'] = $total_all_harga_dasar;
        $this->data['total_all_pbbkb_dasar'] = $total_all_pbbkb_dasar;
        $this->data['total_all_pbbkb_jual'] = $total_all_pbbkb_jual;
        $this->data['total_all_dpp'] = $total_all_dpp;
        $this->data['total_all_ppn_total'] = $total_all_ppn_total;
        $this->data['total_all_jumlah'] = $total_all_jumlah;

        return view('penjualan.index', $this->data);
    }
    
    public function penjualanExport(Request $request){
        $user = Auth::user();

        $param['filter_sdate']    = $request->sdate;
        $param['filter_edate']    = $request->edate;
        $param['filter_no_po']    = $request->no_po;
        $param['filter_no_invoice']    = $request->no_invoice;
        $param['filter_nama_perusahaan']    = $request->nama_perusahaan;
        $param['filter_nama_supplier']    = $request->filter_nama_supplier;
        $param['filter_status']    = $request->status;
        // print_r($request->status);die();
        
        $penjualan = Penjualan::join('item_penjualan', "penjualan.id", "=", "item_penjualan.id_penjualan")
                     ->select("penjualan.id as penjualan.id","penjualan.no_po","penjualan.no_invoice","penjualan.no_faktur_pajak","penjualan.ppn","penjualan.ppn_total","penjualan.tanggal","penjualan.nama_bunker", "penjualan.nama_perusahaan", "penjualan.telp", "penjualan.lokasi_perusahaan","penjualan.kode_lokasi_perusahaan","penjualan.nama_supplier","penjualan.status","penjualan.approved_date","item_penjualan.produk as produk","item_penjualan.jenis_bbm as jenis_bbm","item_penjualan.qty as qty","item_penjualan.harga_jual as harga_jual","item_penjualan.harga_dasar as harga_dasar","item_penjualan.pbbkb_jual as pbbkb_jual","item_penjualan.dpp as dpp","item_penjualan.jumlah as jumlah")
                     ->orderBy("penjualan.created_at", "asc");


         if($param['filter_sdate']){
            $penjualan = $penjualan->whereBetween("tanggal", [$param['filter_sdate'] , $param['filter_edate']]);

        } 

        if($param['filter_no_po']){
            $penjualan = $penjualan->where("no_po","LIKE",$param['filter_no_po']); 
        } 

        if($param['filter_nama_perusahaan']){
            $param['filter_nama_perusahaan'] = $param['filter_nama_perusahaan'].'%';
            $penjualan = $penjualan->where("nama_perusahaan","LIKE", "%".$param['filter_nama_perusahaan']."%");
        }

        if($param['filter_nama_supplier']){
            $param['filter_nama_supplier'] = $param['filter_nama_supplier'].'%';
            $penjualan = $penjualan->where("nama_supplier","LIKE",$param['filter_nama_supplier']);
        }

        if($param['filter_no_invoice']){
            $param['filter_no_invoice'] = $param['filter_no_invoice'].'%';
            $penjualan = $penjualan->where("produk","LIKE",$param['filter_no_invoice']);
        }

        if($param['filter_status']){
            $penjualan = $penjualan->where("status","=",$param['filter_status']);
        } 


       // print_r($penjualan->get());die();

       if($penjualan->count() > 0){
            try{
                return Excel::create('datapenjualan', function($excel) use($penjualan){

                    $penjualan->chunk(3000, function ($data) use($excel) {
                        $collection = $this->transformCollectionPenjualan($data);

                        $excel->sheet('Penjualan', function($sheet) use($collection){
                            $sheet->fromModel($collection, null, 'A1', true);
                        });
                    });

                })->export('xls');

            }catch(Exception $e)
            {
                return false;
            }
        }

        return redirect()->back(); 
    }

    private function transformCollectionPenjualan($collection){
        
        return array_map([$this, 'transformPenjualan'], $collection->toArray());
    }
    private function transformPenjualan($collection){
      

        if($collection['status'] == 1){
            $status = "Lunas";

        }elseif($collection['status'] == -1)
        {
            $status = "Reject";
        }else{
            $status = "Belum Lunas";
        }


        
            
        return [ 
            'Tanggal' => $collection['tanggal'],
            'Nama Perusahaan' => $collection['nama_perusahaan'],
            'No PO' => $collection['no_po'],
            'Nomor Invoice' => $collection['no_invoice'],
            'No Faktur Pajak' => $collection['no_faktur_pajak'],
            'Nama Bunker' => $collection['nama_bunker'],
            'Telepon' => $collection['telp'],
            'Lokasi Perusahaan' => $collection['lokasi_perusahaan'],
            'Nama Supplier' => $collection['nama_supplier'],
            'Produk' =>  $collection['produk'],
            'Jenis BBM' => $collection['jenis_bbm'],
            'Quantity' => $collection['qty'],
            'Harga Jual' => number_format($collection['harga_jual'],6),
            'Harga Dasar' => number_format($collection['harga_dasar'],3),
            'DPP' => number_format($collection['dpp'],0),
            'PPN' => number_format($collection['ppn_total'],0),
            'PBBKB Jual' => number_format($collection['pbbkb_jual'],3),
            'Jumlah' => number_format($collection['jumlah'],0),
            'Status' => $status,
            'Tanggal Pelunasan' => $collection['approved_date'],           
            
        ];
    }
    public function create(Request $request)
    {
        //

        if($request->filter_po_masuk){
            $this->data['po_masuk'] = PoMasuk::where("nomor_po", $request->filter_po_masuk)->firstOrFail();
            $this->data['item_po_masuk'] = ItemPoMasuk::where("id_po_masuk", $this->data['po_masuk']->id)->get();
        }

        $list_po_masuk = PoMasuk::select("nomor_po")->where("status", 1)->get();
        $dataPo = [];
        foreach($list_po_masuk as $val){
            if(Penjualan::where("no_po", $val->nomor_po)->count() == 0){
                array_push($dataPo, $val->nomor_po);
            }
        }

        
        $this->data['list_po_masuk'] = $dataPo;

        $this->data['kantor_cabang'] = Company::all();
        $this->data['nama_perusahaan'] = Customer::all();
        $this->data['lokasi_perusahaan'] = CAddress::all();
        $this->data['nama_supplier'] = Agen::all();

        $this->data['subclass'] = 'create';

        return view('penjualan.create', $this->data);
    }
    public function itemPenjualan(Request $request)
    {
        $user = Auth::user();
        $produk = !empty($request->produk)?$request->produk:array();
        $qty = !empty($request->qty)?$request->qty:array();
        $harga_dasar = !empty($request->harga_dasar)?$request->harga_dasar:array();
        $harga_jual = !empty($request->harga_jual)?$request->harga_jual:array();
        $pbbkb = !empty($request->pbbkb)?$request->pbbkb:array();
        // $ppn = !empty($request->ppn)?$request->ppn:array();
        $uom = !empty($request->uom)?$request->uom:array();
        // print_r($_POST);die;

        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), ItemPenjualan::$rules);
        if($validation->passes()){

            if(ItemPenjualan::where("id_penjualan", $request->id_penjualan)->count() > 0){
                DB::table('item_penjualan')->where('id_penjualan', '=', $request->id_penjualan)->delete();
            }

            // print_r($material);die;
            for($i=0;$i<count($produk);$i++){

                if($produk[$i] != ""){
                    $item_penjualan = new ItemPenjualan();
                    $item_penjualan->id_penjualan = $request->id_penjualan;
                    $item_penjualan->produk = $produk[$i];
                    $item_penjualan->qty = $qty[$i];
                    $item_penjualan->harga_dasar = $harga_dasar[$i];
                    $item_penjualan->harga_jual = $harga_jual[$i];
                    $item_penjualan->pbbkb = $pbbkb[$i];
                    // $item_penjualan->ppn = (int)$ppn[$i];
                    $item_penjualan->uom = $uom[$i];
             
                    $item_penjualan->save();    
                }
                
            }

            //send Email
            $this->sendEmail($request->id_penjualan);
           
            /* redirect to page user */
             return Redirect::route('penjualan.index')->with('alert','success')->with('message', "Sukses: Item Penjualan telah berhasil ditambahkan.");
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $produk = !empty($request->produk)?$request->produk:array();
        $jenis_bbm = !empty($request->jenis_bbm)?$request->jenis_bbm:array();
        $quantity = !empty($request->quantity)?$request->quantity:array();
        $uom = !empty($request->uom)?$request->uom:array();
        $harga_dasar = !empty($request->harga_dasar)?$request->harga_dasar:array();
        $harga_jual = !empty($request->harga_jual)?$request->harga_jual:array();
        $pbbkb = !empty($request->pbbkb)?$request->pbbkb:array();
        // $ppn = !empty($request->ppn)?$request->ppn:array();
        $keterangan = !empty($request->keterangan)?$request->keterangan:array();

        // print_r($harga_dasar);die;
        // $item_penjualan = ItemPenjualan::findOrFail($request->dpp);
        $validation = Validator::make($request->all(), Penjualan::$rules);

        if($validation->passes()){

            DB::beginTransaction();

            try{
                $penjualan = new Penjualan();
                $penjualan->tanggal_pengaliran = $request->tanggal_pengaliran;
                $penjualan->id_customer = $request->id_customer;
                $penjualan->no_invoice = $request->no_invoice;
                $penjualan->no_faktur_pajak = $request->no_faktur_pajak;
                $penjualan->no_po = $request->no_po;
                $penjualan->tanggal = $request->tanggal;
                $penjualan->ppn = $request->ppn;
                $penjualan->nama_supplier = $request->nama_supplier;
                $penjualan->nama_perusahaan = $request->nama_perusahaan;
                $penjualan->kode_lokasi_perusahaan = $request->kode_alamat_perusahaan;
                $penjualan->lokasi_perusahaan = $request->lokasi_perusahaan;
                $penjualan->nama_bunker = $request->nama_bunker;
                $penjualan->telp = $request->telp;
                $penjualan->show_pbbkb = $request->show_pbbkb;
                $penjualan->show_add_cost = $request->show_add_cost;
                $penjualan->created_by = $user->username;
                // $penjualan->prepared_by = $request->prepared_by;
                // $penjualan->approved_by_client = $request->approved_by_client;
                // print_r($request->show_add_cost);die;
                $penjualan->save();

                
                // $penjualan->qty = $request->qty;
                // $penjualan->pbbkb = $request->pbbkb;
                // $penjualan->harga_dasar = $request->harga_dasar;
                // $penjualan->harga_jual = $request->harga_jual;
                // $penjualan->keterangan = $request->keterangan;
                // $penjualan->produk = $request->produk;
                // $penjualan->ppn = (int)$request->ppn;

                // print_r($material);die;
                $ppn_total = 0;
                for($i=0;$i<count($produk);$i++){

                    if($produk[$i] != ""){
                        $item_penjualan = new ItemPenjualan();
                        $item_penjualan->id_penjualan = $penjualan->id;
                        $item_penjualan->produk = $produk[$i];
                        $item_penjualan->jenis_bbm = $jenis_bbm[$i];
                        $item_penjualan->qty = $quantity[$i]; 
                        $item_penjualan->uom = $uom[$i]; 
                        $item_penjualan->harga_jual = $harga_jual[$i];
                        $item_penjualan->pbbkb = $pbbkb[$i];
                        // $item_penjualan->ppn = (int)$ppn[$i]; 
                        // $item_penjualan->harga_dasar = (float)number_format(((int)$item_penjualan->harga_jual/1.1), 2, ".", "");   
                        // print_r($item_penjualan->harga_dasar);die();
                        $item_penjualan->harga_dasar = $item_penjualan->harga_jual/(1+($penjualan->ppn/100)+($item_penjualan->pbbkb/100));
                        $item_penjualan->dpp = ($quantity[$i])*$item_penjualan->harga_dasar;
                        // print_r($item_penjualan->harga_dasar);die();
                        
                        // $item_penjualan->pbbkb_dasar = (int)$item_penjualan->harga_jual/(1+($item_penjualan->ppn/100)+($item_penjualan->pbbkb/100));
                        $item_penjualan->pbbkb_jual = $item_penjualan->harga_dasar*$item_penjualan->qty*($item_penjualan->pbbkb/100);
                        $item_penjualan->jumlah = $item_penjualan->qty*$item_penjualan->harga_dasar+($penjualan->ppn_total);

                        $item_penjualan->keterangan = $keterangan[$i];

                        $item_penjualan->save();    
                        $ppn_total += $item_penjualan->dpp*($penjualan->ppn/100);
                    }
                    
                }   
                $penjualan->ppn_total = $ppn_total;
                $penjualan->save();

                DB::commit();
                /* redirect to page user */
                return Redirect::route('penjualan.index')->with('alert','success')->with('message', "Sukses: Penjualan $penjualan->name telah berhasil dibuat.");
            }catch(\Exception $e){
                DB::rollback();
                /* redirect to page create user with input and error */
                return redirect()->back()->withInput()->with('alert','error')->with('message', $e->getMessage());
            }
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }
      public function edit(Request $request,$id)
    {
       
        $this->data['penjualan'] = Penjualan::findOrFail($id);
       
        // $this->data['po_masuk'] = PoMasuk::where("nomor_po", $this->data['penjualan']->no_po)->firstOrFail();
        $this->data['item_penjualan'] = ItemPenjualan::where("id_penjualan", $this->data['penjualan']->id)->get();

        $caddress = CAddress::where("address_code", $this->data['penjualan']->kode_lokasi_perusahaan)->firstOrFail(); 

        $this->data['customer'] = Customer::findOrFail($caddress->id_customer);
        $this->data['caddress'] = $caddress;
        $this->data['show_pbbkb'] = Constant::$SHOW_PBBKB;

        $this->data['perusahaan'] = Customer::all();
        $this->data['lokasi_perusahaan'] = CAddress::where("id_customer", $caddress->id_customer)->get();

        $this->data['nama_supplier'] = Agen::all();

        $this->data['subclass'] = 'edit';
        return view('penjualan.edit', $this->data);
    }
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $penjualan = Penjualan::findOrFail($id);
        $id_item = !empty($request->id_item)?$request->id_item:array();
        $produk = !empty($request->produk)?$request->produk:array();
        $jenis_bbm = !empty($request->jenis_bbm)?$request->jenis_bbm:array();
        $quantity = !empty($request->quantity)?$request->quantity:array();
        $uom = !empty($request->uom)?$request->uom:array();
        $harga_dasar = !empty($request->harga_dasar)?$request->harga_dasar:array();
        $harga_jual = !empty($request->harga_jual)?$request->harga_jual:array();
        $pbbkb = !empty($request->pbbkb)?$request->pbbkb:array();
        // $ppn = !empty($request->ppn)?$request->ppn:array();
        $keterangan = !empty($request->keterangan)?$request->keterangan:array();

        // print_r($jenis_bbm);die;

        $validation = Validator::make($request->all(), Penjualan::$updaterules);
        

        if($validation->passes()){
            $penjualan->tanggal_pengaliran = $request->tanggal_pengaliran;
            $penjualan->no_invoice = $request->no_invoice;
            $penjualan->no_faktur_pajak = $request->no_faktur_pajak;
            $penjualan->no_po = $request->no_po;
            $penjualan->tanggal = $request->tanggal;
            $penjualan->ppn = $request->ppn;
            // $penjualan->ppn_total =  $request->ppn_total;
            $penjualan->nama_supplier = $request->nama_supplier;
            $penjualan->nama_perusahaan = $request->nama_perusahaan;
            $penjualan->kode_lokasi_perusahaan = $request->kode_alamat_perusahaan;
            $penjualan->lokasi_perusahaan = $request->lokasi_perusahaan;
            $penjualan->nama_bunker = $request->nama_bunker;
            $penjualan->telp = $request->telp;
            $penjualan->show_pbbkb = $request->show_pbbkb;
            $penjualan->show_add_cost = $request->show_add_cost;
            $penjualan->created_by = $user->username;
            // $penjualan->prepared_by = $request->prepared_by;
            // $penjualan->approved_by_client = $request->approved_by_client;
            $penjualan->save();
            
             $ppn_total = 0;
            for($i=0;$i<count($produk);$i++){

                if($produk[$i] != ""){
                    $item_penjualan = ItemPenjualan::findOrFail($id_item[$i]);

                    $item_penjualan->id_penjualan = $penjualan->id;
                    $item_penjualan->produk = $produk[$i];
                    $item_penjualan->jenis_bbm = $jenis_bbm[$i];
                    $item_penjualan->qty = $quantity[$i];
                    $item_penjualan->uom = $uom[$i]; 
                    $item_penjualan->harga_jual = $harga_jual[$i];
                    $item_penjualan->pbbkb = $pbbkb[$i];
                    // $item_penjualan->ppn = (int)$ppn[$i];
                    // $item_penjualan->harga_dasar = ((int)$item_penjualan->harga_jual)/1.1;
                    $item_penjualan->harga_dasar = $item_penjualan->harga_jual/(1+($penjualan->ppn/100)+($item_penjualan->pbbkb/100));
                    $item_penjualan->dpp = $quantity[$i]*$item_penjualan->harga_dasar;
                    // $item_penjualan->ppn_total = $item_penjualan->dpp*($item_penjualan->ppn/100);
                    // $item_penjualan->pbbkb_dasar = (int)$item_penjualan->harga_jual/(1+($item_penjualan->ppn/100)+($item_penjualan->pbbkb/100));
                    $item_penjualan->pbbkb_jual = $item_penjualan->harga_dasar*$item_penjualan->qty*($item_penjualan->pbbkb/100);
                    $item_penjualan->jumlah = $item_penjualan->qty*$item_penjualan->harga_dasar+($penjualan->ppn_total);
                    $item_penjualan->keterangan = $keterangan[$i];
                     $item_penjualan->save();    
                     $ppn_total += $item_penjualan->dpp*($penjualan->ppn/100);
                    }
                    
                }   
                $penjualan->ppn_total = $ppn_total;
                $penjualan->save();

            /* redirect to page user */
            return Redirect::route('penjualan.index')->with('alert','success')->with('message', "Sukses: Penjualan $penjualan->name telah berhasil diupdate.");
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }
    public function approve(Request $request)
    {

       if(isset($_GET['id'])){ 
            $auth = User::where("username", $_GET['username'])->firstOrFail();
            $id = $_GET['id'];
        }else{
            $auth = Auth::user();
            if(!$auth->enable_approval){
                abort(403);
            }
            $id = $request->penjualan_id;    
        }
        

        $penjualan = Penjualan::findOrFail($id);

        if($penjualan->status == 2){
            $penjualan->status = 1;
            $penjualan->approved_by = $auth->username;
            $penjualan->approved_date = date('Y-m-d H:i:s');
            $penjualan->save();

            if(isset($_GET['id'])){
                echo "<div style='margin-top:50px;width:100%'><center><h2>Lunas!</h2></center></div><script>setTimeout('window.close()', 5000);</script>";
            }else{
                return redirect()->back()->with('alert','success')->with("msg", "Tagihan Sudah di Lunasi");
            }    
        }else{
            echo "<div style='margin-top:50px;width:100%'><center><h2>Po tidak ditemukan!</h2></center></div><script>setTimeout('window.close()', 5000);</script>";
        }
    
    }

    public function reject(Request $request)
    {
        
        if(isset($_GET['id'])){
            $auth = User::where("username", $_GET['username'])->firstOrFail();
            $id = $_GET['id'];
        }else{
            $auth = Auth::user();
            if(!$auth->enable_approval){
                abort(403);
            }
            $id = $request->penjualan_id;    
        }

        $penjualan = Penjualan::findOrFail($id);

        if($penjualan->status == 2){
            $penjualan->status = -1;
            $penjualan->rejected_by = $auth->username;
            $penjualan->save();

            if(isset($_GET['id'])){
                echo "<div style='margin-top:50px;width:100%'><center><h2>PO Di Tolak!</h2></center></div><script>setTimeout('window.close()', 3000);</script>";
            }else{
                return redirect()->back()->with('alert','success')->with("msg", "Po Penjualan berhasil di tolak");
            }
        }else{
            echo "<div style='margin-top:50px;width:100%'><center><h2>Po tidak ditemukan!</h2></center></div><script>setTimeout('window.close()', 5000);</script>";
        }
        
    }
  
    public function detailPenjualan(Request $request,$id)
    {        
        $user = Auth::user(); 

        $this->data['penjualan'] = Penjualan::findOrFail($id);
        // $this->data['pembayaran'] = pembayaran::findOrFail($id);
        $this->data['po_masuk'] = PoMasuk::where("nomor_po", $this->data['penjualan']->no_po)->firstOrFail();
        $this->data['company'] = Company::where("name", $this->data['po_masuk']->po_tujuan)->first();
        $this->data['items'] = ItemPenjualan::where("id_penjualan", $id)->get();
        $this->data['grand_total'] = ItemPenjualan::where("id_penjualan", $id)->sum("jumlah");
       
        return view('penjualan.detail_penjualan2', $this->data);
    }

    public function destroy($id)
    {
    
        $user = Auth::user();
        $penjualan = Penjualan::findOrFail($id);
        $penjualan->delete();

        /* redirect to page user */
        return Redirect::route('penjualan.index')->with('alert','success')->with('message', "Sukses: Penjualan $penjualan->name telah berhasil dihapus.");
    }
     
     public function invoicePrint(Request $request,$id)
    {
        $user = Auth::user();
        
       
        $this->data['penjualan'] = Penjualan::findOrFail($id);
        $this->data['po_masuk'] = PoMasuk::where("nomor_po", $this->data['penjualan']->no_po)->firstOrFail();
        $this->data['company'] = Company::where("name", $this->data['po_masuk']->po_tujuan)->first();
        $this->data['items'] = ItemPenjualan::where("id_penjualan", $id)->get();
        $this->data['amount_total'] = ItemPoMasuk::where("id_po_masuk", $id)->sum("amount");
        $this->data['grand_total'] = ItemPenjualan::where("id_penjualan", $id)->sum("jumlah");
        // $this->data['ppn_total'] = Penjualan::where("id", $id)->sum("ppn_total");


       
        return view('penjualan.invoice-print2', $this->data);
    }
    
    public function kwitansiPrint(Request $request,$id)
    {
        $user = Auth::user();
        $this->data['penjualan'] = Penjualan::findOrFail($id);
        $this->data['po_masuk'] = PoMasuk::where("nomor_po", $this->data['penjualan']->no_po)->firstOrFail();
        $this->data['items'] = ItemPenjualan::where("id_penjualan", $id)->get();
        $this->data['grand_total'] = ItemPenjualan::where("id_penjualan", $id)->sum("jumlah");
        // $this->data['ppn_total'] = ItemPenjualan::where("id_penjualan", $id)->sum("ppn_total");
        // print_r( $this->data['item_penjualan']);die();
  
        return view('penjualan.kwitansi-print', $this->data);
    }
    public function kwitansiPrint2(Request $request,$id)
    {
        $user = Auth::user();
        $this->data['penjualan'] = Penjualan::findOrFail($id);
        $this->data['po_masuk'] = PoMasuk::where("nomor_po", $this->data['penjualan']->no_po)->firstOrFail();
        $this->data['items'] = ItemPenjualan::where("id_penjualan", $id)->get();
        $this->data['grand_total'] = ItemPenjualan::where("id_penjualan", $id)->sum("jumlah");
        // $this->data['ppn_total'] = ItemPenjualan::where("id_penjualan", $id)->sum("ppn_total");
  
        return view('penjualan.kwitansi-print2', $this->data);
    }

    public function generateInvoice(Request $request){
        $id = $request->penjualan_id;
        $penjualan = PoMasuk::findOrFail($id);

        $invoice = $penjualan->id."/".$penjualan->kode_po_tujuan."-DRT/".$this->getRomawiMonth()."/".date('y');
        $penjualan = new Penjualan();

    }

    public function getRomawiMonth(){
        $m = date('M');
        $month = "";
        switch($m){
            case "Jan":
                $month = "I";
            break;
            case "Feb":
                $month = "II";
            break;
            case "Mar":
                $month = "III";
            break;
            case "Apr":
                $month = "IV";
            break;
            case "May":
                $month = "V";
            break;
            case "Jun":
                $month = "VI";
            break;
            case "Jul":
                $month = "VII";
            break;
            case "Aug":
                $month = "VIII";
            break;
            case "Sep":
                $month = "IX";
            break;
            case "Oct":
                $month = "X";
            break;
            case "Nov":
                $month = "XI";
            break;
            case "Dec":
                $month = "XII";
            break;
        }

        return $month;
    }
}
