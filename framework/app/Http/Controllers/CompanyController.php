<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


use App\Utilities\Constant; 

use App\Company;

use Validator; 
use Redirect;
use Image;

 
use Auth;
 
class CompanyController extends Controller
{
    public $data;

    

    public function index()
    {
        $this->data['company'] = Company::all();
        return view('company.index', $this->data);
    }

    public function create()
    {
        $this->data['kantor_cabang'] = Company::all();
        return view('company.create', $this->data);
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), Company::$rules);

        if($validation->passes()){
            $company = new Company();
            $company->name = $request->name;
            $company->phone_number = $request->phone_number;
            $company->address = $request->address;
            $company->fax = $request->fax;
            $company->npwp = $request->npwp;
            $company->no_skp_migas = $request->no_skp_migas;
            $company->no_sold_to_penyalur = $request->no_sold_to_penyalur;
            
             /* variabel $imageSerialize default value */
            $imageSerialize = array();

            /* check $request post photo */
            if($request->file('kop_surat')) {
                /* set variabel property for file image */
                $image = $request->file('kop_surat');
                $fullFileName = time().'.'.$image->getClientOriginalExtension();
                $arrImageName = explode(".", $fullFileName); 
                $imageName = $arrImageName[0];
                $imageType = $arrImageName[1];



                /* local path file image */
                // $destinationPath = public_path('/img/profiles/');
                /* server path file image */

                $destinationPath = public_path(Constant::$COMPANY_IMG_PROFILE_PATH);
                if(!is_dir($destinationPath)){
                    mkdir($destinationPath, 0755, true);

                }
            // print_r($destinationPath);die();
                /* Intervention Image */
                $img = Image::make($image->getRealPath());

                /* upload image medium fit Crop and resize combined */
                $ImageNameMedium = $imageName.'_medium.'.$imageType;
                $img->fit(150,150, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.$ImageNameMedium);

                /* upload image small fit Crop and resize combined */
                $ImageNameSmall = $imageName.'_small.'.$imageType;
                $img->fit(100,100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.$ImageNameSmall);

                /* upload image original */
                $image->move($destinationPath, $fullFileName);

                /* set array for image name */
                $arrImage = array("original" => $fullFileName, "medium" => $ImageNameMedium, "small" => $ImageNameSmall);

                /* variabel $imageSerialize */
                $imageSerialize = serialize($arrImage);

                /* find user for update */
                $company->kop_surat = $imageSerialize;
            }

            $company->save();

            /* redirect to page user */
            return Redirect::route('company.index')->with('alert','success')->with('message', "Sukses: Kantor Cabang $company->nama telah berhasil dibuat.");
        }

        /* redirect to page create user with input and error */
        return Redirect::route('company.create')->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $user = Auth::user();

        $this->data['company'] = Company::findOrFail($id);
        return view('company.edit', $this->data);

    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();

         /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), Company::$updaterules);
        $company = Company::findOrFail($id);

        if($validation->passes()){
            
           
            $company->name = $request->name;
            $company->phone_number = $request->phone_number;
            $company->address = $request->address;
            $company->fax = $request->fax;
            $company->npwp = $request->npwp;
            $company->no_skp_migas = $request->no_skp_migas;
            $company->no_sold_to_penyalur = $request->no_sold_to_penyalur;

            /* variabel $imageSerialize default value */
            $imageSerialize = array();

            /* check $request post photo */
            if($request->file('kop_surat')) {
                /* set variabel property for file image */
                $image = $request->file('kop_surat');
                $fullFileName = time().'.'.$image->getClientOriginalExtension();
                $arrImageName = explode(".", $fullFileName); 
                $imageName = $arrImageName[0];
                $imageType = $arrImageName[1];

                /* local path file image */
                // $destinationPath = public_path('/img/profiles/');
                /* server path file image */
                $destinationPath = public_path(Constant::$COMPANY_IMG_PROFILE_PATH);
                if(!is_dir($destinationPath)){
                    mkdir($destinationPath, 0755, true);
                }
                // print_r($destinationPath);die();
                /* Intervention Image */
                $img = Image::make($image->getRealPath());

                /* upload image medium fit Crop and resize combined */
                $ImageNameMedium = $imageName.'_medium.'.$imageType;
                $img->fit(150,150, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.$ImageNameMedium);

                /* upload image small fit Crop and resize combined */
                $ImageNameSmall = $imageName.'_small.'.$imageType;
                $img->fit(100,100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.$ImageNameSmall);

                /* upload image original */
                $image->move($destinationPath, $fullFileName);

                /* set array for image name */
                $arrImage = array("original" => $fullFileName, "medium" => $ImageNameMedium, "small" => $ImageNameSmall);

                /* variabel $imageSerialize */
                $imageSerialize = serialize($arrImage);

                /* find user for update */
                $company->kop_surat = $imageSerialize;
            }

            $company->save();

            /* redirect to page user */
            return Redirect::route('company.index')->with('alert','success')->with('message', "Sukses: Kantor Cabang $company->name telah berhasil diupdate.");
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }

    public function destroy($id)
    {
        $user = Auth::user();
        $company = Company::findOrFail($id);
        $company->delete();

        /* redirect to page user */
        return Redirect::route('company.index')->with('alert','success')->with('message', "Sukses: Kantor $company->name telah berhasil dihapus.");


    }
}
