<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pembelian;
use App\AlatAngkut;
use App\Customer;
use App\Agen; 
use App\Company;
use App\CAddress;
use App\PoKeluar;
use App\Utilities\Constant; 


use Validator;
use Redirect; 
use Auth;  
use Excel;


class PembelianController extends Controller
{
    public $data;

    public function __construct(){
        $this->data = ['class'=>'pembelian'];
    }

    public function index(Request $request)
    {
        $this->data['nama_perusahaan'] = $request->nama_perusahaan;
        $this->data['no_po'] = $request->no_po;
        $this->data['nama_supplier'] = $request->nama_supplier;
        $this->data['produk'] = $request->produk;
        $this->data['sdate'] = $request->sdate;
        $this->data['edate'] = $request->edate;
 
        $pembelian = Pembelian::orderBy('tanggal', 'desc');
        $total_all_qty = new Pembelian();
        $total_all_harga_beli = new Pembelian();
        $total_all_harga_jual = new Pembelian();
        $total_all_harga_dasar = new Pembelian();
        $total_all_dpp = new Pembelian();
        $total_all_jumlah_ppn = new Pembelian();
        $total_all_pbbkb_dasar = new Pembelian();
        $total_all_pbbkb_jual = new Pembelian();
        $total_all_jumlah = new Pembelian();

        if($this->data['nama_perusahaan']){
            $pembelian = $pembelian->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_qty = $total_all_qty->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_harga_beli = $total_all_harga_beli->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_harga_jual = $total_all_harga_jual->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_harga_dasar = $total_all_harga_dasar->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_dpp = $total_all_dpp->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_jumlah_ppn = $total_all_jumlah_ppn->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_jumlah = $total_all_jumlah->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
        }

        if($this->data['no_po']){
            $pembelian = $pembelian->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_qty = $total_all_qty->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_harga_beli = $total_all_harga_beli->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_harga_jual = $total_all_harga_jual->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_harga_dasar = $total_all_harga_dasar->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_dpp = $total_all_dpp->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_jumlah_ppn = $total_all_jumlah_ppn->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->where("no_po","LIKE", "%".$this->data['no_po']."%");
            $total_all_jumlah = $total_all_jumlah->where("no_po","LIKE", "%".$this->data['no_po']."%");
        }

        if($this->data['nama_supplier']){
            $pembelian = $pembelian->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_qty = $total_all_qty->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_harga_beli = $total_all_harga_beli->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_harga_jual = $total_all_harga_jual->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_harga_dasar = $total_all_harga_dasar->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_dpp = $total_all_dpp->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_jumlah_ppn = $total_all_jumlah_ppn->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
            $total_all_jumlah = $total_all_jumlah->where("nama_supplier","LIKE", "%".$this->data['nama_supplier']."%");
        }

        if($this->data['produk']){
            $pembelian = $pembelian->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_qty = $total_all_qty->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_harga_beli = $total_all_harga_beli->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_harga_jual = $total_all_harga_jual->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_harga_dasar = $total_all_harga_dasar->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_dpp = $total_all_dpp->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_jumlah_ppn = $total_all_jumlah_ppn->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->where("produk","LIKE", "%".$this->data['produk']."%");
            $total_all_jumlah = $total_all_jumlah->where("produk","LIKE", "%".$this->data['produk']."%");
        }

        if($this->data['sdate'] && $this->data['edate']){
            $pembelian = $pembelian->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_qty = $total_all_qty->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_harga_jual = $total_all_harga_jual->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_harga_dasar = $total_all_harga_dasar->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_pbbkb_jual = $total_all_pbbkb_jual->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_dpp = $total_all_dpp->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_jumlah_ppn = $total_all_jumlah_ppn->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
            $total_all_jumlah = $total_all_jumlah->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
        }


        $pembelian = $pembelian->paginate(20);
        $total_all_qty = $total_all_qty->sum("qty");
        $total_all_harga_beli = $total_all_harga_beli->sum("harga_beli");
        $total_all_harga_jual = $total_all_harga_jual->sum("harga_jual");
        $total_all_harga_dasar = $total_all_harga_dasar->sum("harga_dasar");
        $total_all_dpp = $total_all_dpp->sum("dpp");
        $total_all_jumlah_ppn = $total_all_jumlah_ppn->sum("jumlah_ppn");
        $total_all_pbbkb_dasar = $total_all_pbbkb_dasar->sum("pbbkb_dasar");
        $total_all_pbbkb_jual = $total_all_pbbkb_jual->sum("pbbkb_jual");
        $total_all_jumlah = $total_all_jumlah->sum("jumlah");
        
        $this->data['subclass'] = "list";
        $this->data['pembelian'] = $pembelian;
        $this->data['total_all_qty'] = $total_all_qty;
        $this->data['total_all_harga_beli'] = $total_all_harga_beli;
        $this->data['total_all_harga_jual'] = $total_all_harga_jual;
        $this->data['total_all_harga_dasar'] = $total_all_harga_dasar;
        $this->data['total_all_dpp'] = $total_all_dpp;
        $this->data['total_all_jumlah_ppn'] = $total_all_jumlah_ppn;
        $this->data['total_all_pbbkb_dasar'] = $total_all_pbbkb_dasar;
        $this->data['total_all_pbbkb_jual'] = $total_all_pbbkb_jual;
        $this->data['total_all_jumlah'] = $total_all_jumlah;

        return view('pembelian.index', $this->data);
    }
    public function pembelianExport(Request $request){ 
        $user = Auth::user();

        $param['filter_no_po']    = $request->filter_no_po;
        $param['filter_nama_perusahaan']    = $request->filter_nama_perusahaan;
        $param['filter_nama_supplier']    = $request->filter_nama_supplier;
        $param['filter_produk']    = $request->filter_produk;

        
        $pembelian = Pembelian::select("pembelian.id as pembelian.id","pembelian.no_po","pembelian.no_invoice","pembelian.no_faktur_pajak","pembelian.tanggal","pembelian.nama_perusahaan","pembelian.lokasi_perusahaan","pembelian.lokasi_perusahaan","pembelian.nama_supplier","pembelian.produk","pembelian.qty","pembelian.ppn_total","pembelian.harga_beli","pembelian.harga_jual","pembelian.pbbkb","pembelian.ppn","pembelian.harga_dasar","pembelian.dpp","pembelian.jumlah_ppn","pembelian.pbbkb_dasar","pembelian.pbbkb_jual","pembelian.jumlah")
                     ->orderBy("pembelian.created_at", "asc");


        if($param['filter_no_po']){
            $pembelian = $pembelian->where("no_po","LIKE",$param['filter_no_po']);
        }

        if($param['filter_nama_perusahaan']){
            $param['filter_nama_perusahaan'] = $param['filter_nama_perusahaan'].'%';
            $pembelian = $pembelian->where("nama_perusahaan","LIKE",$param['filter_nama_perusahaan']);
        }

        if($param['filter_nama_supplier']){
            $param['filter_nama_supplier'] = $param['filter_nama_supplier'].'%';
            $pembelian = $pembelian->where("nama_supplier","LIKE",$param['filter_nama_supplier']);
        }

        if($param['filter_produk']){
            $param['filter_produk'] = $param['filter_produk'].'%';
            $pembelian = $pembelian->where("produk","LIKE",$param['filter_produk']);
        }


       if($pembelian->count() > 0){
            try{
                return Excel::create('datapembelian', function($excel) use($pembelian){

                    $pembelian->chunk(3000, function ($data) use($excel) {
                        $collection = $this->transformCollectionPembelian($data);

                        $excel->sheet('Pembelian', function($sheet) use($collection){
                            $sheet->fromModel($collection, null, 'A1', true);
                        });
                    });

                })->export('xls');

            }catch(Exception $e)
            {
                return false;
            }
        }

        return redirect()->back(); 
    }

    private function transformCollectionPembelian($collection){
        
        return array_map([$this, 'transformPembelian'], $collection->toArray());
    }
    private function transformPembelian($collection){
         
        return [ 
            'Tanggal' => $collection['tanggal'],
            'Nama Perusahaan' => $collection['nama_perusahaan'],
            'Lokasi Perusahaan' => $collection['lokasi_perusahaan'],
            'Nama Supplier' => $collection['nama_supplier'],
            'Produk' => $collection['produk'],
            'Nomor PO' => $collection['no_po'],
            'Quantity' => $collection['qty'],
            'Harga Beli' => number_format($collection['harga_beli'],3),
            'Harga Jual' => number_format($collection['harga_jual'],3),
            'Harga Dasar' =>  number_format($collection['harga_dasar'],3),
            'DPP' => number_format($collection['dpp'],0),
            'PPN' => number_format($collection['jumlah_ppn'],0),
            'PBBKB Dasar' => number_format($collection['pbbkb_dasar'],3),
            'PBBKB Jual' => number_format($collection['pbbkb_jual'],0),
            'Jumlah' => number_format($collection['jumlah'],0),
            
            
        ];
    }

   public function create(Request $request)
    {
        //

        if($request->filter_po_keluar){
            $this->data['po_keluar'] = PoKeluar::where("nomor_po", $request->filter_po_keluar)->firstOrFail();
            $this->data['item_po_keluar'] = AlatAngkut::where("id_po_keluar", $this->data['po_keluar']->id)->get();
        }

        $list_po_keluar = PoKeluar::select("nomor_po")->where("status", 1)->get();
        $dataPo = [];
        foreach($list_po_keluar as $val){
            if(Pembelian::where("no_po", $val->nomor_po)->count() == 0){
                array_push($dataPo, $val->nomor_po);
            }
        }

        
        $this->data['list_po_keluar'] = $dataPo;
        $this->data['nama_perusahaan'] = Customer::all();
        $this->data['kantor_cabang'] = Company::all();
       // print_r($this->data['po_keluar']);die();


        $this->data['subclass'] = 'create';

        return view('pembelian.create', $this->data);
    }

    public function store(Request $request)
    {
        $auth = Auth::user();

        // $po_keluar = PoKeluar::findOrFail($request->nama_perusahaan);
        

        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), Pembelian::$rules);

        

        if($validation->passes()){
            $pembelian = new Pembelian();
            $pembelian->no_po = $request->no_po;
            $pembelian->no_invoice = $request->no_invoice;
            $pembelian->no_faktur_pajak = $request->no_faktur_pajak;
            $pembelian->tanggal = $request->tanggal;
            $pembelian->nama_perusahaan = $request->nama_perusahaan;
            $pembelian->lokasi_perusahaan = $request->lokasi_perusahaan;
            $pembelian->nama_supplier = $request->nama_supplier;
            $pembelian->produk = $request->produk;
            $pembelian->qty = (int)$request->qty;
            $pembelian->harga_beli = (int)$request->harga_beli;
            $pembelian->harga_jual = (int)$request->harga_jual;
            $pembelian->pbbkb = $request->pbbkb;
            $pembelian->ppn = (int)$request->ppn;
            
            $pembelian->harga_dasar = $pembelian->harga_beli/(1+($pembelian->ppn/100)+($pembelian->pbbkb/100));
            $pembelian->dpp = ((int)$pembelian->qty)*$pembelian->harga_dasar;
            $pembelian->jumlah_ppn = $pembelian->dpp*($pembelian->ppn/100);
            $pembelian->pbbkb_dasar = (int)$pembelian->harga_jual/(1+($pembelian->ppn/100)+($pembelian->pbbkb/100));
            $pembelian->pbbkb_jual = $pembelian->qty*$pembelian->pbbkb_dasar*($pembelian->pbbkb/100);
            $pembelian->jumlah = (int)$pembelian->dpp+$pembelian->jumlah_ppn+$pembelian->pbbkb_jual;

            // $pembelian->keterangan = $request->keterangan;
            $pembelian->created_by = $auth->name;
             // print_r($pembelian->harga_dasar);die(); 
            $pembelian->save();

            /* redirect to page user */
            return Redirect::route('pembelian.index')->with('alert','success')->with('message', "Pembelian baru berhasil disimpan");
        }

        /* redirect to page create user with input and error */
        return Redirect::route('pembelian.create')->withInput()->withErrors($validation)->with('alert','error')->with('message', 'Silahkan periksa kembali data yang diinput');
    }

    public function show($id)
    {
        $user = Auth::user();

        $this->data['pembelian'] = Pembelian::findOrFail($id);
        $this->data['po_keluar'] = PoKeluar::findOrFail($id);
        $this->data['nama_perusahaan'] = Customer::all();
        $this->data['lokasi_perusahaan'] = CAddress::all();

        
        $this->data['kantor_cabang'] = Company::all(); 

        // $caddress = CAddress::where("address_code", $this->data['pembelian']->kode_lokasi_perusahaan)->firstOrFail();
        // $this->data['customer'] = Customer::findOrFail($caddress->id_customer);
        // $this->data['caddress'] = $caddress;
        $this->data['agen'] = Agen::all();
       
        $this->data['subclass'] = 'detail-pembelian';
        return view('pembelian.detail_pembelian2', $this->data);
    }

    public function edit($id)
    {

        $this->data['pembelian'] = Pembelian::findOrFail($id);
        // print_r($this->data['pembelian']);die;
        // $this->data['po_keluar'] = PoKeluar::where("nomor_po", $this->data['pembelian']->no_po)->firstOrFail();
       

        $this->data['subclass'] = 'edit';
        return view('pembelian.edit', $this->data);
    }
 
    public function update(Request $request, $id)
    {
        $auth = Auth::user();
        $pembelian = Pembelian::findOrFail($id);
       
        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), Pembelian::$updaterules);

        if($validation->passes()){
            $pembelian->no_po = $request->no_po;
            $pembelian->no_invoice = $request->no_invoice;
            $pembelian->no_faktur_pajak = $request->no_faktur_pajak;
            $pembelian->tanggal = $request->tanggal;
            $pembelian->nama_perusahaan = $request->nama_perusahaan;
            $pembelian->lokasi_perusahaan = $request->lokasi_perusahaan;
            $pembelian->nama_supplier = $request->nama_supplier;
            $pembelian->produk = $request->produk;
            $pembelian->qty = (int)$request->qty;
            $pembelian->harga_beli = (int)$request->harga_beli;
            $pembelian->harga_jual = (int)$request->harga_jual;
            $pembelian->pbbkb = $request->pbbkb;
            $pembelian->ppn = (int)$request->ppn;

            $pembelian->harga_dasar = $pembelian->harga_beli/(1+($pembelian->ppn/100)+($pembelian->pbbkb/100));
            $pembelian->dpp = ((int)$pembelian->qty)*$pembelian->harga_dasar;
            $pembelian->jumlah_ppn = $pembelian->dpp*($pembelian->ppn/100);
            $pembelian->pbbkb_dasar = (int)$pembelian->harga_jual/(1+($pembelian->ppn/100)+($pembelian->pbbkb/100));
            $pembelian->pbbkb_jual = $pembelian->qty*$pembelian->pbbkb_dasar*($pembelian->pbbkb/100);
            $pembelian->jumlah = (int)$pembelian->dpp+$pembelian->jumlah_ppn+$pembelian->pbbkb_jual;
            $pembelian->keterangan = $request->keterangan;
            $pembelian->created_by = $auth->name;
            // print_r($pembelian->pbbkb);die();
            $pembelian->save();

            /* redirect to page user */
            return Redirect::route('pembelian.index')->with('alert','success')->with('message', "Pembelian baru berhasil disimpan");
        }

        /* redirect to page create user with input and error */
        return Redirect::route('pembelian.create')->withInput()->withErrors($validation)->with('alert','error')->with('message', 'Silahkan periksa kembali data yang diinput');
    }

    public function destroy($id)
    {
        $data = Pembelian::findOrFail($id);
        $data->delete();

        return Redirect::route('pembelian.index')->with('alert','success')->with('message', "Data pembelian berhasil dihapus");
    }
     public function invoicePrint(Request $request,$id)
    {
        $user = Auth::user();
        $this->data['pembelian'] = Pembelian::findOrFail($id); 
        $this->data['po_keluar'] = PoKeluar::findOrFail($id);
        $this->data['company'] = Company::where("name", $this->data['pembelian']->nama_supplier)->first();

        // $this->data['kantor_cabang'] = Company::all();
  
        return view('pembelian.invoice-print2', $this->data);
    }
    public function kwitansiPrint(Request $request,$id)
    {
        $user = Auth::user();
        $this->data['pembelian'] = Pembelian::findOrFail($id); 
        // $this->data['kantor_cabang'] = Company::all();
  
        return view('pembelian.kwitansi-print', $this->data);
    }
}
