<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\CAddress;
use App\Customer;
use App\Company;

use Validator; 
use Redirect;
use Image; 
  
use Auth; 
use DB;


class CustomerController extends Controller
{
    public $data;

    public function __construct(){
      $this->data = [];
    }
    
    public function index()
   {
      return view('customer.index', $this->data);
   }
    
    public function fetchDataCustomer(Request $request){

        $user = Auth::user();
        
        $param['limit']     = $request->length;
        $param['offset']    = $request->start;
        $search    = $request->search['value'];
        $param['order_by']  = ($request->columns[$_GET['order'][0]['column']]['data'])?$request->columns[$_GET['order'][0]['column']]['data']:"name";
        $param['order_type']= ($request->order[0]['dir'])?$request->order[0]['dir']:"asc";

        $resultJson = array("draw"=>$_GET['draw'],
                            "recordsTotal"=>0,
                            "recordsFiltered"=>0,
                            "data"=>"");

        $customer = Customer::where(function($query) use ($search){
                            $query->orWhere("name", "LIKE", $search."%")
                                  ->orWhere("jenis_usaha", "LIKE", $search."%")
                                  ->orWhere("phone_number", "LIKE", $search."%")
                                  // ->orWhere("npwp", "LIKE", $search."%")
                                  ->orWhere("pic_name", "LIKE", $search."%")
                                  ->orWhere("pic_email", "LIKE", $search."%")
                                  ->orWhere("pic_phone", "LIKE", $search."%")
                                  ->orWhere("jenis_customer", "LIKE", $search."%");
                          })
                          ->skip($param['offset'])
                          ->take($param['limit'])
                          ->orderBy($param['order_by'], $param['order_type']);

        $total = Customer::where(function($query) use ($search){
                            $query->orWhere("name", "LIKE", $search."%")
                                  ->orWhere("jenis_usaha", "LIKE", $search."%")
                                  ->orWhere("phone_number", "LIKE", $search."%")
                                  // ->orWhere("npwp", "LIKE", $search."%")
                                  ->orWhere("pic_name", "LIKE", $search."%")
                                  ->orWhere("pic_email", "LIKE", $search."%")
                                  ->orWhere("pic_phone", "LIKE", $search."%")
                                  ->orWhere("jenis_customer", "LIKE", $search."%");
                          });

        $customer = $customer->get();
        $total = $total->count();

        if($total > 0){
            $data = array();
            for($i=0;$i<count($customer);$i++){
                foreach($customer[$i] as $key=>$val){
                  
                    $datatemp = array("name"=>$customer[$i]->name,
                                      "jenis_usaha"=>$customer[$i]->jenis_usaha,
                                      "phone_number"=>$customer[$i]->phone_number,
                                      // "npwp"=>$customer[$i]->npwp,
                                      "pic_name"=>$customer[$i]->pic_name,
                                      "pic_email"=>$customer[$i]->pic_email,
                                      "pic_phone"=>$customer[$i]->pic_phone,
                                      "jenis_customer"=>$customer[$i]->jenis_customer,
                                      "created_at"=>$customer[$i]->created_at->format("Y-m-d H:i:s"),
                                      "action"=>'<a href="'.url('customer/edit/'.$customer[$i]->id).'" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> Edit</a>
                                        <a href="'. url('customer/add-address/'.$customer[$i]->id).'" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Address</a>  
                                        <a class="btn btn-danger btn-xs" href="'. url('customer/delete/'.$customer[$i]->id) .'" onclick="deleteData('.$customer[$i]->id.')"><i class="fa fa-trash"></i> Hapus</a>
                                        <form action="'.url('customer/delete/'.$customer[$i]->id).'" method="post" id="form-'. $customer[$i]->id.'">
                                          '.csrf_field().'
                                          <input type="hidden" name="_method" value="delete">
                                        </form>',
                                     );
                }
                array_push($data, $datatemp);   
            }

                $resultJson = array("draw"=>$request->draw,
                                    "recordsTotal"=>$total,
                                    "recordsFiltered"=>$total,
                                    "data"=>$data);
        }    

        return response()->json($resultJson);
    }
    public function create()
    {
        $user = Auth::user();

        $this->data['company_id'] = Company::all();
        return view('customer.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        
        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), Customer::$rules);

        if($validation->passes()){
            $customer = new Customer();
            $customer->name = $request->name;
            $customer->jenis_usaha = $request->jenis_usaha;
            $customer->phone_number = $request->phone_number;
            $customer->pic_name = $request->pic_name;
            $customer->pic_email = $request->pic_email;
            $customer->pic_phone = $request->pic_phone;
            $customer->jenis_customer = $request->jenis_customer;

            $customer->save();

            /* redirect to page user */
            return Redirect::route('customer.index')->with('alert','success')->with('message', "Sukses: customer $customer->nama telah berhasil dibuat.");
        }

        /* redirect to page create user with input and error */
        return Redirect::route('customer.create')->withInput()->withErrors($validation)->with('alert','error')->with('message', 'Terjadi kesalahan pada saat validasi data.');
    }

    public function edit($id)
    {
        $user = Auth::user();

        $this->data['customer'] = Customer::findOrFail($id);
        $this->data['company_id'] = Company::all();
        
        return view('customer.edit', $this->data);

    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();

         /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), customer::$updaterules);
        $customer = customer::findOrFail($id);

        if($validation->passes()){
            $customer->name = $request->name;
            $customer->jenis_usaha = $request->jenis_usaha;
            $customer->phone_number = $request->phone_number;
            $customer->pic_name = $request->pic_name;
            $customer->pic_email = $request->pic_email;
            $customer->pic_phone = $request->pic_phone;
            $customer->jenis_customer = $request->jenis_customer;
           

            $customer->save();

            /* redirect to page user */
            return Redirect::route('customer.index')->with('alert','success')->with('message', "Sukses: Customer $customer->nama telah berhasil diupdate.");
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }
     public function createAddress($id)
    {
        //
        $this->data['customer'] = Customer::findOrFail($id);
        $this->data['add_address'] = CAddress::where("id_customer", $id)->get();
        $this->data['kantor_cabang'] = Company::all();
       
        return view('customer.create_address', $this->data);
    }
 
    public function addAddress(Request $request)
    {
        $user = Auth::user();
        $address_code = !empty($request->address_code)?$request->address_code:array();
        $address = !empty($request->address)?$request->address:array();
        $npwp = !empty($request->npwp)?$request->npwp:array();
       

        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), CAddress::$rules);
        if($validation->passes()){
 
            if(CAddress::where("id_customer", $request->id_customer)->count() > 0){
                DB::table('caddress')->where('id_customer', '=', $request->id_customer)->delete();
            }

            for($i=0;$i<count($address);$i++){

                if($address_code[$i] != ""){
                    $caddress = new CAddress();
                    $caddress->id_customer = $request->id_customer;
                    $caddress->npwp = $npwp[$i];
                    $caddress->address_code = $address_code[$i];
                    $caddress->address = $address[$i];
                    
                    $caddress->save();    
                }
                
            }
           
            /* redirect to page user */
             return Redirect::route('customer.index')->with('alert','success')->with('message', "Sukses: Alamat telah berhasil ditambahkan.");
        }

        /* redirect to page create user with input and error */
        return Redirect::route('customer.create_address')->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $customer = customer::findOrFail($id);
        $customer->delete();

        /* redirect to page user */
        return Redirect::route('customer.index')->with('alert','success')->with('message', "Sukses:  $customer->name telah berhasil dihapus.");


    }

    public function getAddress(Request $request){
      $id = $request->customer_id;

      $data = CAddress::where("id_customer",$id)->get();

      return response()->json(["status"=>"ok", "data"=>$data]);
    }
}
