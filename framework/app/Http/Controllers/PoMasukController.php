<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\PoMasuk;
use App\ItemPoMasuk;
use App\Company;
use App\Customer; 
use App\CAddress;
use App\Agen; 
use App\Utilities\Constant; 
use App\User;
use App\Setting;

use Validator; 
use Redirect;
use Auth; 
use Mail;
use DB; 


class PoMasukController extends Controller
{
   public $data;

    public function __construct(){
        $this->data = ['class'=>'po_masuk'];
    }

    public function index(Request $request)
    {
        
        $this->data['nama_perusahaan'] = $request->nama_perusahaan;
        $this->data['nomor_po'] = $request->nomor_po;
        $this->data['po_tujuan'] = $request->po_tujuan;
        $this->data['pic_po_tujuan'] = $request->pic_po_tujuan;
        $this->data['sdate'] = !empty($request->sdate)?$request->sdate:date("Y")."01-01";
        $this->data['edate'] = $request->edate;
 
        $po_masuk = PoMasuk::orderBy('tanggal_po', 'desc');

        if($this->data['nama_perusahaan']){
            $po_masuk = $po_masuk->where("nama_perusahaan", "LIKE", "%".$this->data['nama_perusahaan']."%");
        }

        if($this->data['nomor_po']){
            $po_masuk = $po_masuk->where("nomor_po","LIKE", "%".$this->data['nomor_po']."%");
        }

        if($this->data['po_tujuan']){
            $po_masuk = $po_masuk->where("po_tujuan", "LIKE", "%".$this->data['po_tujuan']."%");
        }

        if($this->data['pic_po_tujuan']){
            $po_masuk = $po_masuk->where("pic_po_tujuan", "LIKE", "%".$this->data['pic_po_tujuan']."%");
        }

        if($this->data['sdate'] && $this->data['edate']){
            $po_masuk = $po_masuk->whereBetween("tanggal_po", [$this->data['sdate'], $this->data['edate']]);
        }

        $po_masuk = $po_masuk->where("archive",0)->paginate(20);

        
        $this->data['subclass'] = "list";
        $this->data['po_masuk'] = $po_masuk;
 

        return view('po_masuk.index', $this->data);
    }

    public function create()
    {
        //
        $this->data['kantor_cabang'] = Company::all();
        $this->data['nama_perusahaan'] = Customer::all();
        
        return view('po_masuk.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user(); 
         /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), PoMasuk::$rules);
 
        $company = Company::findOrFail($request->po_tujuan);
        $customer = Customer::findOrFail($request->nama_perusahaan);
        $caddress = CAddress::findOrFail($request->alamat_perusahaan);

        if($validation->passes()){ 
            $po_masuk = new PoMasuk(); 
            $po_masuk->nama_perusahaan = $customer->name;
            $po_masuk->id_customer = $customer->id;
            $po_masuk->nomor_po = $request->nomor_po;
            $po_masuk->kode_po_tujuan = $request->kode_po_tujuan;
            $po_masuk->alamat_perusahaan = $caddress->address;
            $po_masuk->kode_lokasi_perusahaan = $caddress->address_code;
            $po_masuk->tanggal_po = $request->tanggal_po;
            $po_masuk->tanggal_kirim = $request->tanggal_kirim;
            $po_masuk->ppn = (int)$request->ppn;
            $po_masuk->pph = $request->pph;
            $po_masuk->pbbkb = $request->pbbkb;
            $po_masuk->add_cost = $request->add_cost;
            $po_masuk->po_tujuan = $company->name;
            $po_masuk->pic_po_tujuan = $request->pic_po_tujuan;
            $po_masuk->telp_po_tujuan = $company->phone_number;
            $po_masuk->fax_po_tujuan = $company->fax;
            $po_masuk->contact_person_1 = $request->contact_person_1;
            $po_masuk->contact_person_2 = $request->contact_person_2;
            $po_masuk->notes = $request->notes;
            $po_masuk->payment_term = $request->payment_term;
            $po_masuk->prepared_by = $request->prepared_by;
            $po_masuk->approved_by_client = $request->approved_by_client;

            $po_masuk->save();

            /* redirect to page user */
            // return Redirect::route('po_masuk.index')->with('alert','success')->with('message', "Sukses: PO masuk $po_masuk->name  telah berhasil dibuat.");
            return redirect('po-masuk/add-item/'.$po_masuk->id);
        }

        /* redirect to page create user with input and error */
        return Redirect::route('po_masuk.create')->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }


    public function edit($id)
   {
        $this->data['po_masuk'] = PoMasuk::findOrFail($id);

        $caddress = CAddress::where("address_code", $this->data['po_masuk']->kode_lokasi_perusahaan)->firstOrFail();
        $this->data['customer'] = Customer::findOrFail($caddress->id_customer);
        $this->data['caddress'] = $caddress;
        $this->data['pbbkb'] = Constant::$PBBKB;
        $this->data['pph'] = Constant::$PPH;

        $this->data['nama_perusahaan'] = Customer::all();
        $this->data['alamat_perusahaan'] = CAddress::where("id_customer", $caddress->id_customer)->get();

        $this->data['kantor_cabang'] = Company::all();

        $this->data['agen'] = Agen::all();

        $this->data['subclass'] = 'edit';
        return view('po_masuk.edit', $this->data);
    }

    public function update(Request $request, $id)
    {
        $user = Auth::user(); 
        $po_masuk = PoMasuk::findOrFail($id);
        $company = Company::findOrFail($request->po_tujuan);
        $customer = Customer::findOrFail($request->nama_perusahaan);
        // print_r($customer);die();
        $caddress = CAddress::findOrFail($request->alamat_perusahaan);
          /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), PoMasuk::$rules);

        if($validation->passes()){
            $po_masuk->nama_perusahaan = $customer->name;
            $po_masuk->nomor_po = $request->nomor_po;
            $po_masuk->kode_po_tujuan = $request->kode_po_tujuan;
            $po_masuk->alamat_perusahaan = $caddress->address;
            $po_masuk->kode_lokasi_perusahaan = $caddress->address_code;
            $po_masuk->tanggal_po = $request->tanggal_po;
            $po_masuk->ppn = $request->ppn;
            $po_masuk->pph = $request->pph;
            $po_masuk->pbbkb = $request->pbbkb;
            $po_masuk->add_cost = $request->add_cost;
            $po_masuk->tanggal_kirim = $request->tanggal_kirim;
            $po_masuk->po_tujuan = $company->name;
            $po_masuk->pic_po_tujuan = $request->pic_po_tujuan;
            $po_masuk->telp_po_tujuan = $company->phone_number;
            $po_masuk->fax_po_tujuan =  $company->fax;  
            $po_masuk->contact_person_1 = $request->contact_person_1;
            $po_masuk->contact_person_2 = $request->contact_person_2;
            $po_masuk->notes = $request->notes;
            $po_masuk->payment_term = $request->payment_term;
            $po_masuk->prepared_by = $request->prepared_by;
            $po_masuk->approved_by_client = $request->approved_by_client;

            $po_masuk->save();

            /* redirect to page user */
            return Redirect::route('po_masuk.index')->with('alert','success')->with('message', "Sukses: PO Masuk $po_masuk->nomor_po telah berhasil diupdate.");
            
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }
     public function createItem($id)
    {
        //
        $this->data['po_masuk'] = PoMasuk::findOrFail($id);
        $this->data['item_po_masuk'] = ItemPoMasuk::where("id_po_masuk", $id)->get();
        $this->data['kantor_cabang'] = Company::all();
       
        return view('po_masuk.create_item', $this->data);
    }

    public function itemPoMasuk(Request $request)
    {
        $user = Auth::user();
        $material = !empty($request->material)?$request->material:array();
        $description = !empty($request->description)?$request->description:array();
        $uom = !empty($request->uom)?$request->uom:array();
        $quantity = !empty($request->quantity)?$request->quantity:array();
        $unit_price = !empty($request->unit_price)?$request->unit_price:array();
        // $amount = !empty($request->amount)?$request->material:array();
        // print_r($_POST);die;

        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), ItemPoMasuk::$rules);
        if($validation->passes()){

            if(ItemPoMasuk::where("id_po_masuk", $request->id_po_masuk)->count() > 0){
                DB::table('item_po_masuk')->where('id_po_masuk', '=', $request->id_po_masuk)->delete();
            }

            // print_r($material);die;
            for($i=0;$i<count($material);$i++){

                if($material[$i] != ""){
                    $item_po_masuk = new ItemPoMasuk();
                    $item_po_masuk->id_po_masuk = $request->id_po_masuk;
                    $item_po_masuk->material = $material[$i];
                    $item_po_masuk->description = $description[$i];
                    $item_po_masuk->uom = $uom[$i];
                    $item_po_masuk->qty = (int)$quantity[$i];
                    $item_po_masuk->unit_price = $unit_price[$i];
                    $item_po_masuk->amount = $item_po_masuk->qty*$item_po_masuk->unit_price;
             
                    $item_po_masuk->save();    
                }
                
            }

            //send Email
            $this->sendEmail($request->id_po_masuk);
           
            /* redirect to page user */
             return Redirect::route('po_masuk.index')->with('alert','success')->with('message', "Sukses: Item PO Masuk telah berhasil ditambahkan.");
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }

     public function detailPoMasuk(Request $request,$id)
    {
        //
        $user = Auth::user();
        $this->data['po_masuk'] = PoMasuk::findOrFail($id); 
        $this->data['item_po_masuk'] = ItemPoMasuk::where("id_po_masuk", $id)->get();
        $this->data['amount_total'] = ItemPoMasuk::where("id_po_masuk", $id)->sum("amount");
        
        return view('po_masuk.detail_po_masuk', $this->data);
    }

    public function editPoMasuk($id)
    {
    
        $user = Auth::user();

        $this->data['po_masuk'] = PoMasuk::findOrFail($id);
        $this->data['kantor_cabang'] = Company::all();
        $this->data['nama_customer'] = Customer::all();
        $this->data['pbbkb'] = Constant::$PBBKB;
        $this->data['pph'] = Constant::$PPH;
        return view('po_masuk.edit_item', $this->data);
    }

    public function destroy($id)
    {
        
        $user = Auth::user();
        $po_masuk = PoMasuk::findOrFail($id);
        $po_masuk->delete();

        /* redirect to page user */
        return Redirect::route('po_masuk.index')->with('alert','success')->with('message', "Sukses: Po Masuk $po_masuk->nomor_po telah berhasil dihapus.");
    }

    public function approve(Request $request)
    {

       if(isset($_GET['id'])){ 
            $auth = User::where("username", $_GET['username'])->firstOrFail();
            $id = $_GET['id'];
        }else{
            $auth = Auth::user();
            if(!$auth->enable_approval){
                abort(403);
            }
            $id = $request->po_masuk_id;    
        }
        

        $po_masuk = PoMasuk::findOrFail($id);

        if($po_masuk->status == 0){
            $po_masuk->status = 1;
            $po_masuk->approved_by = $auth->username;
            $po_masuk->save();

            if(isset($_GET['id'])){
                echo "<div style='margin-top:50px;width:100%'><center><h2>Berhasil!</h2></center></div><script>setTimeout('window.close()', 5000);</script>";
            }else{
                return redirect()->back()->with('alert','success')->with("msg", "Po Masuk berhasil Di setujui");
            }    
        }else{
            echo "<div style='margin-top:50px;width:100%'><center><h2>Po tidak ditemukan!</h2></center></div><script>setTimeout('window.close()', 5000);</script>";
        }
        
    }

    public function reject(Request $request)
    {
        
        if(isset($_GET['id'])){
            $auth = User::where("username", $_GET['username'])->firstOrFail();
            $id = $_GET['id'];
        }else{
            $auth = Auth::user();
            if(!$auth->enable_approval){
                abort(403);
            }
            $id = $request->po_masuk_id;    
        }

        $po_masuk = PoMasuk::findOrFail($id);

        if($po_masuk->status == 0){
            $po_masuk->status = -1;
            $po_masuk->rejected_by = $auth->username;
            $po_masuk->save();

            if(isset($_GET['id'])){
                echo "<div style='margin-top:50px;width:100%'><center><h2>PO Di Tolak!</h2></center></div><script>setTimeout('window.close()', 3000);</script>";
            }else{
                return redirect()->back()->with('alert','success')->with("msg", "Po Masuk berhasil di tolak");
            }
        }else{
            echo "<div style='margin-top:50px;width:100%'><center><h2>Po tidak ditemukan!</h2></center></div><script>setTimeout('window.close()', 5000);</script>";
        }
        
    }
     public function invoicePrint(Request $request,$id)
    {
        $user = Auth::user();
        $this->data['po_masuk'] = PoMasuk::findOrFail($id); 
        $this->data['company'] = Company::where("name", $this->data['po_masuk']->po_tujuan)->first();
        $this->data['item_po_masuk'] = ItemPoMasuk::where("id_po_masuk", $id)->get();
        $this->data['amount_total'] = ItemPoMasuk::where("id_po_masuk", $id)->sum("amount");
        
  
        return view('po_masuk.invoice-print', $this->data);
    }

    public function generateInvoice(Request $request){
        $id = $request->po_masuk_id;

        $po_masuk = PoMasuk::findOrFail($id);

        $invoice = $po_masuk->id."/".$po_masuk->kode_po_tujuan."-DRT/".$this->getRomawiMonth()."/".date('y');
        $penjualan = new Penjualan();

    }

    public function getRomawiMonth(){
        $m = date('M');
        $month = "";
        switch($m){
            case "Januari":
                $month = "I";
            break;
            case "Februari":
                $month = "II";
            break;
            case "Maret":
                $month = "III";
            break;
            case "April":
                $month = "IV";
            break;
            case "Mei":
                $month = "V";
            break;
            case "Juni":
                $month = "VI";
            break;
            case "Juli":
                $month = "VII";
            break;
            case "Agustus":
                $month = "VIII";
            break;
            case "September":
                $month = "IX";
            break;
            case "Oktober":
                $month = "X";
            break;
            case "November":
                $month = "XI";
            break;
            case "Desember":
                $month = "XII";
            break;
        }

        return $month;
    }

    public function sendEmail($id)
    {
        //send Email
        // try{
            $po_masuk = PoMasuk::findOrFail($id);
            $item_po_masuk = ItemPoMasuk::where("id_po_masuk", $id)->get();

            $dataEmail = ["po_masuk" => $po_masuk,
                          "item_po_masuk" => $item_po_masuk];
            
            Mail::send('emails.po_masuk', $dataEmail, function ($message) {

                $message->from(Constant::$EMAIL_SENDER, Constant::$NAME_SENDER);
                $message->to(Setting::select('field_value')->where("field_name", "email_approval")->first()->field_value, null);
                $message->subject(Constant::$EMAIL_SUBJECT);

            });

        // }catch(\Exception $e){
        //     echo $e->getMessage();die;
        // }
        //end send email

        return redirect('po-masuk')->with("message", "PO Created and sent to email!")->with("alert", "success");
    }
}
