<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;  
use App\PoMasuk;
use App\Pembayaran;
use App\Company;  
use App\Customer; 
use App\CAddress;
use App\Agen;
use App\Penjualan; 
use App\ItemPenjualan; 
use App\Utilities\Constant;  
  
use Validator; 
use Redirect;
  
 
use Auth;
use DB; 
use Excel; 

class PembayaranController extends Controller
{
    public $data;

    public function __construct(){
        $this->data = ['class'=>'pembayaran'];
    }
    public function index(Request $request) 
    {

        $this->data['nama_perusahaan'] = $request->nama_perusahaan;
        $this->data['no_po'] = $request->no_po;
        $this->data['no_invoice'] = $request->no_invoice;
        $this->data['payment_number'] = $request->payment_number;
        $this->data['sdate'] = $request->sdate;
        $this->data['edate'] = $request->edate;
        $this->data['status'] = $request->status;
        

        $pembayaran = Pembayaran::orderBy('payment_date', 'desc')
                                ->join("item_penjualan", "item_penjualan.id", "=", "pembayaran.id_item_penjualan")
                                ->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan")
                                ->select(DB::raw("pembayaran.id, pembayaran.id_item_penjualan,pembayaran.ntt,pembayaran.payment_date,pembayaran.payment_number,pembayaran.kredit,pembayaran.notes, (SELECT SUM(jumlah) FROM item_penjualan WHERE id_penjualan = penjualan.id) as total_tagihan, penjualan.no_po, penjualan.no_invoice, penjualan.nama_perusahaan,item_penjualan.qty,item_penjualan.harga_jual,item_penjualan.pbbkb_jual,item_penjualan.dpp,penjualan.ppn_total,item_penjualan.jumlah,item_penjualan.produk,item_penjualan.jenis_bbm"));

         
        $total_all_qty = ItemPenjualan::join('penjualan', "penjualan.id", "=", "item_penjualan.id_penjualan")
                                        ->join('customer', "customer.id", "=","penjualan.id_customer")
                                        ->join('pembayaran', "pembayaran.id_item_penjualan", "=","item_penjualan.id");
        $total_all_harga_jual = ItemPenjualan::join('penjualan', "penjualan.id", "=", "item_penjualan.id_penjualan")
                                        ->join('customer', "customer.id", "=","penjualan.id_customer")
                                        ->join('pembayaran', "pembayaran.id_item_penjualan", "=","item_penjualan.id");
        $total_all_dpp = ItemPenjualan::join('penjualan', "penjualan.id", "=", "item_penjualan.id_penjualan")
                                        ->join('customer', "customer.id", "=","penjualan.id_customer")
                                        ->join('pembayaran', "pembayaran.id_item_penjualan", "=","item_penjualan.id");
        $total_all_ppn = ItemPenjualan::join('penjualan', "penjualan.id", "=", "item_penjualan.id_penjualan")
                                        ->join('customer', "customer.id", "=","penjualan.id_customer")
                                        ->join('pembayaran', "pembayaran.id_item_penjualan", "=","item_penjualan.id");
        $total_all_pbbkb = ItemPenjualan::join('penjualan', "penjualan.id", "=", "item_penjualan.id_penjualan")
                                        ->join('customer', "customer.id", "=","penjualan.id_customer")
                                        ->join('pembayaran', "pembayaran.id_item_penjualan", "=","item_penjualan.id");
        $total_all_kredit = ItemPenjualan::join('penjualan', "penjualan.id", "=", "item_penjualan.id_penjualan")
                                        ->join('customer', "customer.id", "=","penjualan.id_customer")
                                        ->join('pembayaran', "pembayaran.id_item_penjualan", "=","item_penjualan.id");
        $total_all_jumlah = ItemPenjualan::join('penjualan', "penjualan.id", "=", "item_penjualan.id_penjualan")
                                        ->join('customer', "customer.id", "=","penjualan.id_customer")
                                        ->join('pembayaran', "pembayaran.id_item_penjualan", "=","item_penjualan.id");
        
        if($this->data['nama_perusahaan']){
            $pembayaran = $pembayaran->where("penjualan.nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
            
            $total_all_qty = $total_all_qty->where("customer.name", "LIKE", "%".$this->data['nama_perusahaan']."%");
            $total_all_harga_jual = $total_all_harga_jual->where("customer.name", "LIKE", "%".$this->data['nama_perusahaan']."%");
             $total_all_dpp = $total_all_dpp->where("customer.name", "LIKE", "%".$this->data['nama_perusahaan']."%");
             $total_all_ppn = $total_all_ppn->where("customer.name", "LIKE", "%".$this->data['nama_perusahaan']."%");
             $total_all_pbbkb = $total_all_pbbkb->where("customer.name", "LIKE", "%".$this->data['nama_perusahaan']."%");
             $total_all_kredit = $total_all_kredit->where("customer.name", "LIKE", "%".$this->data['nama_perusahaan']."%");
             $total_all_jumlah = $total_all_jumlah->where("customer.name", "LIKE", "%".$this->data['nama_perusahaan']."%");
        }

        if($this->data['no_po']){
            $pembayaran = $pembayaran->where("penjualan.no_po", "LIKE", "%".$this->data['no_po']."%");

            $total_all_qty = $total_all_qty->where("penjualan.no_po", "LIKE", "%".$this->data['no_po']."%");
            $total_all_harga_jual = $total_all_qty->where("penjualan.no_po", "LIKE", "%".$this->data['no_po']."%");
            $total_all_dpp = $total_all_jumlah->where("penjualan.no_po", "LIKE", "%".$this->data['no_po']."%");
            $total_all_ppn = $total_all_jumlah->where("penjualan.no_po", "LIKE", "%".$this->data['no_po']."%");
            $total_all_pbbkb = $total_all_jumlah->where("penjualan.no_po", "LIKE", "%".$this->data['no_po']."%");
            $total_all_kredit = $total_all_jumlah->where("penjualan.no_po", "LIKE", "%".$this->data['no_po']."%");
            $total_all_jumlah = $total_all_jumlah->where("penjualan.no_po", "LIKE", "%".$this->data['no_po']."%");
        }
        if($this->data['no_invoice']){
            $pembayaran = $pembayaran->where("penjualan.no_invoice", "LIKE", "%".$this->data['no_invoice']."%");

            $total_all_qty = $total_all_qty->where("penjualan.no_invoice", "LIKE", "%".$this->data['no_invoice']."%");
            $total_all_harga_jual = $total_all_qty->where("penjualan.no_invoice", "LIKE", "%".$this->data['no_invoice']."%");
            $total_all_dpp = $total_all_jumlah->where("penjualan.no_invoice", "LIKE", "%".$this->data['no_invoice']."%");
            $total_all_ppn = $total_all_jumlah->where("penjualan.no_invoice", "LIKE", "%".$this->data['no_invoice']."%");
            $total_all_pbbkb = $total_all_jumlah->where("penjualan.no_invoice", "LIKE", "%".$this->data['no_invoice']."%");
            $total_all_kredit = $total_all_jumlah->where("penjualan.no_invoice", "LIKE", "%".$this->data['no_invoice']."%");
            $total_all_jumlah = $total_all_jumlah->where("penjualan.no_invoice", "LIKE", "%".$this->data['no_invoice']."%");
        }

        if($this->data['payment_number']){
            $pembayaran = $pembayaran->where("pembayaran.payment_number","LIKE", "%".$this->data['payment_number']."%");

            $total_all_qty = $total_all_qty->where("pembayaran.payment_number", "LIKE", "%".$this->data['payment_number']."%");
            $total_all_harga_jual = $total_all_jumlah->where("pembayaran.payment_number","LIKE", "%".$this->data['payment_number']."%");
            $total_all_dpp = $total_all_jumlah->where("pembayaran.payment_number","LIKE", "%".$this->data['payment_number']."%");
            $total_all_ppn = $total_all_jumlah->where("pembayaran.payment_number","LIKE", "%".$this->data['payment_number']."%");
            $total_all_pbbkb = $total_all_jumlah->where("pembayaran.payment_number","LIKE", "%".$this->data['payment_number']."%");
            $total_all_kredit = $total_all_jumlah->where("pembayaran.payment_number","LIKE", "%".$this->data['payment_number']."%");
            $total_all_jumlah = $total_all_jumlah->where("pembayaran.payment_number","LIKE", "%".$this->data['payment_number']."%");
        }

        if($this->data['sdate'] && $this->data['edate']){
            $pembayaran = $pembayaran->whereBetween("payment_date", [$this->data['sdate'], $this->data['edate']]);

            $total_all_qty = $total_all_qty->whereBetween("payment_date", [$this->data['sdate'], $this->data['edate']]);
            $total_all_harga_jual = $total_all_jumlah->whereBetween("payment_date", [$this->data['sdate'], $this->data['edate']]);
            $total_all_dpp = $total_all_jumlah->whereBetween("payment_date", [$this->data['sdate'], $this->data['edate']]);
            $total_all_ppn = $total_all_jumlah->whereBetween("payment_date", [$this->data['sdate'], $this->data['edate']]);
            $total_all_pbbkb = $total_all_jumlah->whereBetween("payment_date", [$this->data['sdate'], $this->data['edate']]);
            $total_all_kredit = $total_all_jumlah->whereBetween("payment_date", [$this->data['sdate'], $this->data['edate']]);
            $total_all_jumlah = $total_all_jumlah->whereBetween("payment_date", [$this->data['sdate'], $this->data['edate']]);
        }
        
 
        $pembayaran = $pembayaran->paginate(20);
        $total_all_qty = $total_all_qty->sum("qty");
        $total_all_harga_jual = $total_all_harga_jual->sum("harga_jual");
        $total_all_dpp = $total_all_dpp->sum("dpp");
        $total_all_ppn = $total_all_ppn->sum("ppn_total");
        $total_all_pbbkb = $total_all_pbbkb->sum("pbbkb_jual");
        $total_all_kredit = $total_all_kredit->sum("kredit");
        $total_all_jumlah = $total_all_jumlah->sum("jumlah");


        $this->data['subclass'] = "list";
        $this->data['pembayaran'] = $pembayaran;
        $this->data['total_all_qty'] = $total_all_qty;
        $this->data['total_all_harga_jual'] = $total_all_harga_jual;
        $this->data['total_all_dpp'] = $total_all_dpp;
        $this->data['total_all_ppn'] = $total_all_ppn;
        $this->data['total_all_pbbkb'] = $total_all_pbbkb;
        $this->data['total_all_kredit'] = $total_all_kredit;
        $this->data['total_all_jumlah'] = $total_all_jumlah;
        

        return view('pembayaran.index', $this->data);
    }

    public function pembayaranExport(Request $request){
        $user = Auth::user();

        $param['filter_sdate']    = $request->sdate;
        $param['filter_edate']    = $request->edate;
        $param['filter_no_po']    = $request->no_po;
        $param['filter_no_invoice']    = $request->no_invoice;
        $param['filter_payment_number']    = $request->payment_number;
        $param['filter_nama_perusahaan']    = $request->nama_perusahaan;
        // print_r($request->status);die();
        $pembayaran = Pembayaran::join("item_penjualan", "item_penjualan.id", "=", "pembayaran.id_item_penjualan")
                                ->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan")
                                ->select(DB::raw("pembayaran.id, pembayaran.id_item_penjualan,pembayaran.ntt,pembayaran.payment_date,pembayaran.payment_number,pembayaran.kredit,pembayaran.notes, (SELECT SUM(jumlah) FROM item_penjualan WHERE id_penjualan = penjualan.id) as total_tagihan, penjualan.no_po, penjualan.no_invoice, penjualan.nama_perusahaan,item_penjualan.qty,item_penjualan.harga_jual,item_penjualan.pbbkb_jual,item_penjualan.dpp,penjualan.ppn_total,item_penjualan.jumlah,item_penjualan.produk,item_penjualan.jenis_bbm"))
                                 ->orderBy("pembayaran.created_at", "asc");



         if($param['filter_sdate']){
            $pembayaran = $pembayaran->whereBetween("payment_date", [$param['filter_sdate'] , $param['filter_edate']]);

        } 

        if($param['filter_no_po']){
            $pembayaran = $penjualan->where("no_po","LIKE",$param['filter_no_po']); 
        } 
        if($param['filter_no_invoice']){
            $pembayaran = $penjualan->where("no_invoice","LIKE",$param['filter_no_invoice']); 
        } 

        if($param['filter_nama_perusahaan']){
            $param['filter_nama_perusahaan'] = $param['filter_nama_perusahaan'].'%';
            $pembayaran = $penjualan->where("nama_perusahaan","LIKE", "%".$param['filter_nama_perusahaan']."%");
        }

        if($param['filter_payment_number']){
            $param['filter_payment_number'] = $param['filter_payment_number'].'%';
            $pembayaran = $pembayaran->where("id","LIKE",$param['filter_payment_number']);
        }


       // print_r($penjualan->get());die();

       if($pembayaran->count() > 0){
            try{
                return Excel::create('datapembayaran', function($excel) use($pembayaran){

                    $pembayaran->chunk(3000, function ($data) use($excel) {
                        $collection = $this->transformCollectionPembayaran($data);

                        $excel->sheet('Pembayaran', function($sheet) use($collection){
                            $sheet->fromModel($collection, null, 'A1', true);
                        });
                    });

                })->export('xls');

            }catch(Exception $e)
            {
                return false;
            }
        }

        return redirect()->back(); 
    }

    private function transformCollectionPembayaran($collection){
        
        return array_map([$this, 'transformPembayaran'], $collection->toArray());
    }
    private function transformPembayaran($collection){
      
            
        return [ 
            'Tanggal' => $collection['payment_date'],
            'Tanggal NTT' => $collection['ntt'],
            'Nama Perusahaan' => $collection['nama_perusahaan'],
            'No PO' => $collection['no_po'],
            'No Invoice' => $collection['no_invoice'],
            'Nomor Pembayaran' => $collection['payment_number'],
            'Produk' =>  $collection['produk'],
            'Jenis BBM' => $collection['jenis_bbm'],
            'Quantity' => $collection['qty'],
            'Harga Jual' => number_format($collection['harga_jual'],0),
            'DPP' => number_format($collection['dpp'],0),
            'PPN' => number_format($collection['ppn_total'],0),
            'PBBKB Jual' => number_format($collection['pbbkb_jual'],3),
            'Jumlah Pembayaran' => number_format($collection['kredit'],0),
            'Jumlah Tagihan' => number_format($collection['jumlah'],0), 
            'Keterangan' => $collection['notes'],       
            
        ];
    }
    public function create(Request $request)
    {
        //
         if($request->filter_item_penjualan){
            $this->data['item_penjualan'] = ItemPenjualan::join('penjualan', "penjualan.id", "=", "item_penjualan.id_penjualan")
            ->where("item_penjualan.id", $request->filter_item_penjualan)
            ->select("item_penjualan.*", "penjualan.tanggal","penjualan.no_po","penjualan.no_invoice","penjualan.ppn_total", "penjualan.nama_perusahaan", "penjualan.lokasi_perusahaan")
            ->firstOrFail();  

            $this->data['sisa_tagihan'] = $this->data['item_penjualan']->jumlah - (Pembayaran::where("id_item_penjualan", $request->filter_item_penjualan)->sum('kredit'));
        }
        // print_r($this->data['item_penjualan']);die();
        $list_item_penjualan = ItemPenjualan::join('penjualan', "penjualan.id", "=", "item_penjualan.id_penjualan")
            ->select("penjualan.no_po","penjualan.no_invoice","item_penjualan.produk","item_penjualan.jenis_bbm","item_penjualan.id as id_item_penjualan")->where('penjualan.status', '<>', 1 )->get();
        // $dataPo = [];
        // foreach($list_item_penjualan as $val){
        //     if(Pembayaran::where("id", $val->no_po, $val->produk)->count() == 0){
        //         array_push($dataPo, $val->no_po);
        //     }
        // }

        
        $this->data['list_item_penjualan'] = $list_item_penjualan;
        $this->data['nama_perusahaan'] = Customer::all();

        $this->data['subclass'] = 'create';

        return view('pembayaran.create', $this->data);
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $validation = Validator::make($request->all(), Pembayaran::$rules);
       
        // DB::beginTransaction();

        if($validation->passes()){
            $total_pembayaran_item = DB::table('pembayaran')->where('id_item_penjualan','=',$request->id_item_penjualan)->sum('kredit');

            $total_tagihan_item = DB::table('item_penjualan')->where('id','=',$request->id_item_penjualan)->sum('jumlah');

            // if(($total_pembayaran_item+$request->kredit) >  $total_tagihan_item){
            //     return redirect()->back()->with('alert','danger')->with('message', "Gagal: Total Pembayaran melebihi jumlah tagihan.")->withInput();
            }

            $pembayaran = new Pembayaran();
            $pembayaran->id_item_penjualan = $request->id_item_penjualan;
            $pembayaran->id_penjualan = $request->id_penjualan;
            $pembayaran->nama_perusahaan = $request->nama_perusahaan;
            $pembayaran->payment_date = $request->payment_date;
            $pembayaran->payment_number = $request->payment_number;
            $pembayaran->ntt = $request->ntt;
            $pembayaran->kredit = $request->kredit;
            $pembayaran->notes = $request->notes;
            $pembayaran->created_by = $user->username;
            // $penjualan->prepared_by = $request->prepared_by;
            // $penjualan->approved_by_client = $request->approved_by_client;
            // print_r($request->show_add_cost);die;
            $pembayaran->save();

            $kredit = DB::table('pembayaran')->where('id_penjualan','=',$pembayaran->id_penjualan)->sum('kredit');
            $total_tagihan = DB::table('item_penjualan')->where('id_penjualan','=',$pembayaran->id_penjualan)->sum('jumlah');

            if($kredit == $total_tagihan OR $kredit >= $total_tagihan){
                Penjualan::where("id", "=", $pembayaran->id_penjualan)->update(["status"=>1]);
            }
           
            /* redirect to page user */
            return Redirect::route('pembayaran.index')->with('alert','success')->with('message', "Sukses: Pembayaran $pembayaran->name telah berhasil dibuat.");
        // }else{
        //     return redirect()->back()->with('alert','danger')->with('message', "Gagal: Tejadi kesalahan ketika input data, silahkan cek kembali.")->withInput();
        // }
    }

      public function edit(Request $request,$id)
    {
        
        $this->data['pembayaran'] = Pembayaran::join("item_penjualan", "item_penjualan.id", "=", "pembayaran.id_item_penjualan")
            ->join("penjualan", "penjualan.id", "=", "item_penjualan.id_penjualan")
            ->select("pembayaran.*","penjualan.tanggal","penjualan.no_po","penjualan.no_invoice","item_penjualan.produk","penjualan.ppn_total", "penjualan.nama_perusahaan", "penjualan.lokasi_perusahaan", "item_penjualan.jumlah")
            ->where("pembayaran.id", "=", $id)
            ->firstOrFail();

            $this->data['sisa_tagihan'] = $this->data['pembayaran']->jumlah - (Pembayaran::where("id_item_penjualan", $id)->sum('kredit'));
            // print_r($this->data['pembayaran']);die();

        return view('pembayaran.edit', $this->data);
    }
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $validation = Validator::make($request->all(), Pembayaran::$updaterules);
        

        if($validation->passes()){
            // $total_pembayaran_item = DB::table('pembayaran')->where('id_item_penjualan','=',$request->id_item_penjualan)->sum('kredit');

            $total_tagihan_item = DB::table('item_penjualan')->where('id','=',$request->id_item_penjualan)->sum('jumlah');

            // if(($total_pembayaran_item+$request->kredit) >  $total_tagihan_item){
            //     return redirect()->back()->with('alert','danger')->with('message', "Gagal: Total Pembayaran melebihi jumlah tagihan.")->withInput();
            // }
            $pembayaran = Pembayaran::findOrFail($id);
            $pembayaran->payment_date = $request->payment_date;
            $pembayaran->payment_number = $request->payment_number;
            $pembayaran->nama_perusahaan = $request->nama_perusahaan;
            $pembayaran->ntt = $request->ntt;
            $pembayaran->kredit = $request->kredit;
            $pembayaran->notes = $request->notes;
            $pembayaran->created_by = $user->username;
            
            $pembayaran->save();

            $kredit = DB::table('pembayaran')->where('id_penjualan','=',$pembayaran->id_penjualan)->sum('kredit');
            $total_tagihan = DB::table('item_penjualan')->where('id_penjualan','=',$pembayaran->id_penjualan)->sum('jumlah');

            if($kredit == $total_tagihan OR $kredit >= $total_tagihan){
                Penjualan::where("id", "=", $pembayaran->id_penjualan)->update(["status"=>1]);
            }

            /* redirect to page user */
            return Redirect::route('pembayaran.index')->with('alert','success')->with('message', "Sukses: Pembayaran $pembayaran->name telah berhasil diupdate.");
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }
   

    public function destroy($id)
    {
    
        $user = Auth::user();
        $pembayaran = Pembayaran::findOrFail($id);
        $pembayaran->delete();

        /* redirect to page user */
        return Redirect::route('pembayaran.index')->with('alert','success')->with('message', "Sukses: Pembayaran $pembayaran->name telah berhasil dihapus.");
    }
     
}
