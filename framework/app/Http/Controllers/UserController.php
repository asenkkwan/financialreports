<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Http\Utilities\Constants;
use App\Http\Utilities\SystemConstants;


use App\User;  
use App\Company;

use Validator;
use Redirect;
use Image;

use Auth; 

class UserController extends Controller
{
    public $data;

    public function __construct(){
      $this->data = [];
    }
    
    
    public function index()
    {
        $user = Auth::user(); 
        if($user->role == "superadmin"){
            $this->data['user'] = User::all();
        }else{
            $this->data['user'] = User::where("role", "<>", "superadmin")->get();
        }
        
        return view('users.index', $this->data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $user = Auth::user();

        if($user->role != 'superadmin'){
            abort(404);
        }

        $this->data['kantor_cabang'] = Company::all();

        return view('users.create', $this->data);
    }

    /** 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if($user->role != 'superadmin'){
            abort(404);
        }
        $company = Company::findOrFail($request->company_id);

        /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), User::$rules);

        if($validation->passes()){
            $user = new User();
            $user->name = $request->name;
            $user->phone_number = $request->phone_number;
            $user->address = $request->address;
            $user->email = $request->email;
            $user->company_id = $company->name;
            $user->role = $request->role;
            $user->username = $request->username;
            $user->password = bcrypt($request->password);
            $user->enable_approval = 0;
            $user->status = 1;
            if($request->enable_approval == "yes"){
                $user->enable_approval = 1;
            }

            $user->save();

            /* redirect to page user */
            return Redirect::route('users.index')->with('alert','success')->with('message', "Sukses: Pengguna $user->nama telah berhasil dibuat.");
        }

        /* redirect to page create user with input and error */
        return Redirect::route('users.create')->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $user = Auth::user();
        if($user->role != 'superadmin'){
             $this->data['user'] = User::findOrFail($user->id);

        }else{
           $this->data['user'] = User::findOrFail($id);
        }
        
        //
        
        $this->data['kantor_cabang'] = Company::all();
        return view('users.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $auth = Auth::user();
       
        if($auth->role != 'superadmin'){
            $user = User::findOrFail($auth->id);
        }else{
            
            $user = User::findOrFail($id);
        }
        
        $rules = User::$updaterules;

        $company = Company::findOrFail($request->company_id);
        /* variabel $validation result after make validator input by rules */
        if($request->password){
            $rules['password'] = 'min:6|confirmed';
        }

        $validation = Validator::make($request->all(), $rules);

        if($validation->passes()){
            
            $user->name = $request->name;
            $user->phone_number = $request->phone_number;
            $user->address = $request->address;
            $user->email = $request->email;
            $user->company_id = $company->name;
            $user->role = $request->role;
            $user->username = $request->username;
            if($request->password){
                $user->password = bcrypt($request->password);    
            }      
            $user->enable_approval = 0;
            if($request->enable_approval == "yes"){
                $user->enable_approval = 1;
            }

            $user->save();

            /* redirect to page user */
            return Redirect::route('users.index')->with('alert','success')->with('message', "Sukses: Pengguna $user->nama telah berhasil diupdate.");
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);  
        if($user->role != 'superadmin'){
            $user->delete();
        }

        if($id != 1){      
            $user->delete();
            /* redirect to page user */
            return Redirect::route('users.index')->with('alert','success')->with('message', "Sukses: Pengguna $user->nama telah berhasil dihapus.");
        }else{
            
            /* redirect to page user */
            return redirect()->back()->with('alert','danger')->with('message', "Warning: Pengguna $user->nama tidak bisa dihapus.");
        }
        


    }
}
