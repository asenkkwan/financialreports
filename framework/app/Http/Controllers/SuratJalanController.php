<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 


use App\SuratJalan;
use App\ItemPoMasuk;
use App\Company;
use App\Customer; 
use App\CAddress; 
use App\Agen; 
use App\AlatAngkut;
use App\PoMasuk;
use App\Penjualan;
use App\ItemPenjualan;

use Validator; 
use Redirect; 
 
 
use Auth;
use DB;
 
class SuratJalanController extends Controller
{
    public $data;

    public function __construct(){
        $this->data = ['class'=>'surat_jalan'];
    } 
    public function index(Request $request)
    {
        

        $this->data['nama_perusahaan'] = $request->nama_perusahaan;
        $this->data['nopol'] = $request->nopol;
        $this->data['no_surat_jalan'] = $request->no_surat_jalan;
        $this->data['no_po'] = $request->no_po;
        $this->data['sdate'] = $request->sdate;
        $this->data['edate'] = $request->edate;

        $surat_jalan = SuratJalan::orderBy('tanggal', 'desc');

        if($this->data['nama_perusahaan']){
            $surat_jalan = $surat_jalan->where("nama_perusahaan","LIKE", "%".$this->data['nama_perusahaan']."%");
        }

        if($this->data['nopol']){
            $surat_jalan = $surat_jalan->where("nopol", "LIKE", "%".$this->data['nopol']."%");
        }

        if($this->data['no_surat_jalan']){
            $surat_jalan = $surat_jalan->where("no_surat_jalan","LIKE", "%".$this->data['no_surat_jalan']."%");
        }

        if($this->data['no_po']){
            $surat_jalan = $surat_jalan->where("no_po","LIKE", "%".$this->data['no_po']."%");
        }

        if($this->data['sdate'] && $this->data['edate']){ 
            $surat_jalan = $surat_jalan->whereBetween("tanggal", [$this->data['sdate'], $this->data['edate']]);
        }

        $surat_jalan = $surat_jalan->paginate(20);
        
        $this->data['subclass'] = "list";
        $this->data['surat_jalan'] = $surat_jalan;

        return view('surat_jalan.index', $this->data);
    }
   public function create(Request $request)
    {
        //

        $this->data['filter_po_masuk'] = ($request->filter_po_masuk)?$request->filter_po_masuk:"";

        if($this->data['filter_po_masuk']){
            $this->data['penjualan'] = Penjualan::where('no_po', $this->data['filter_po_masuk'])->firstOrFail();
        }

        $this->data['list_po_masuk'] = Penjualan::select("penjualan.no_po")->leftJoin("surat_jalan", "surat_jalan.no_po", "=", "penjualan.no_po")

         ->where("penjualan.no_po", "<>", "surat_jalan.no_po")->get();
         $list_po_masuk = PoMasuk::select("nomor_po")->where("status", 0)->get();
        $dataPo = [];
        foreach($list_po_masuk as $val){
            if(Penjualan::where("no_po", $val->nomor_po)->count() == 0){
                array_push($dataPo, $val->nomor_po);
            }
        }
        

        // $this->data['kantor_cabang'] = Company::all();
        // $this->data['nama_perusahaan'] = Customer::all();
        // $this->data['alamat_perusahaan'] = CAddress::all();

        $this->data['subclass'] = 'create';

        return view('surat_jalan.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
   
    {
        $user = Auth::user();

        $validation = Validator::make($request->all(), SuratJalan::$rules);

        $penjualan = Penjualan::where("no_po", $request->no_po)->firstOrFail();

        if($validation->passes()){
            $surat_jalan = new SuratJalan();
            $surat_jalan->no_po = $request->no_po;
            $surat_jalan->id_penjualan = $penjualan->id;
            $surat_jalan->no_surat_jalan = $request->no_surat_jalan;
            $surat_jalan->tanggal = $request->tanggal;
            $surat_jalan->nama_perusahaan = $request->nama_perusahaan;
            $surat_jalan->alamat_perusahaan = $request->alamat_perusahaan;
            $surat_jalan->kode_lokasi_perusahaan = $request->kode_lokasi_perusahaan;
            $surat_jalan->nopol = $request->nopol;
            $surat_jalan->jenis_kendaraan = $request->jenis_kendaraan;
            $surat_jalan->nama_driver = $request->nama_driver;
            $surat_jalan->no_phone_driver = $request->no_phone_driver;
            $surat_jalan->sim_driver = $request->sim_driver;
                
            $surat_jalan->save();

            /* redirect to page user */
            return Redirect::route('surat_jalan.index')->with('alert','success')->with('message', "Sukses: Surat Jalan $surat_jalan->no_po telah berhasil ditambahkan.");
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'Terdapat Error pada input data.');
    }
      public function edit($id)
     {
        $this->data['surat_jalan'] = SuratJalan::findOrFail($id);
        $this->data['subclass'] = 'edit';
        return view('surat_jalan.edit', $this->data);
    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $surat_jalan = SuratJalan::findOrFail($id);

          /* variabel $validation result after make validator input by rules */
        $validation = Validator::make($request->all(), SuratJalan::$updaterules);
        

        if($validation->passes()){
            $surat_jalan->no_po = $request->no_po;
            $surat_jalan->no_surat_jalan = $request->no_surat_jalan;
            $surat_jalan->tanggal = $request->tanggal;
            $surat_jalan->nama_perusahaan = $request->nama_perusahaan;
            $surat_jalan->alamat_perusahaan = $request->alamat_perusahaan;
            $surat_jalan->kode_lokasi_perusahaan = $request->kode_lokasi_perusahaan;
            $surat_jalan->nopol = $request->nopol;
            $surat_jalan->jenis_kendaraan = $request->jenis_kendaraan;
            $surat_jalan->nama_driver = $request->nama_driver;
            $surat_jalan->no_phone_driver = $request->no_phone_driver;
            $surat_jalan->sim_driver = $request->sim_driver;

            $surat_jalan->save();

            /* redirect to page user */
            return Redirect::route('surat_jalan.index')->with('alert','success')->with('message', "Sukses: surat jalan $surat_jalan->no_po telah berhasil diupdate.");
        }

        /* redirect to page create user with input and error */
        return redirect()->back()->withInput()->withErrors($validation)->with('alert','error')->with('message', 'Terdapat Error pada input data.');
    }
   
  
    public function detailSuratJalan(Request $request,$id)
    {
        //
        $user = Auth::user();
        $this->data['surat_jalan'] = SuratJalan::findOrFail($id); 
        $this->data['data_produk'] = ItemPenjualan::where("id_penjualan", $this->data['surat_jalan']->id_penjualan)->get(); 
       
  
        return view('surat_jalan.detail_surat_jalan', $this->data);
    }
  
     public function detailsurat_jalan(Request $request,$id)
    {
        
        $user = Auth::user();
        $this->data['surat_jalan'] = SuratJalan::findOrFail($id); 
        $this->data['nama_perusahaan'] = Customer::all();
        return view('suratJalan.detail_surat_jalan', $this->data);
    }

    public function destroy($id)
    {
    
        $user = Auth::user();
        $surat_jalan = SuratJalan::findOrFail($id);
        $surat_jalan->delete();

        /* redirect to page user */
        return Redirect::route('surat_jalan.index')->with('alert','success')->with('message', "Sukses: surat jalan $surat_jalan->no_po telah berhasil dihapus.");
    }
    public function approve(Request $request){

        $auth = Auth::user();
        $id = $request->psurat_jalan_id;

        $surat_jalan = PoMasuk::findOrFail($id);
        $surat_jalan->status = 1;
        $surat_jalan->approved_by = $auth->username;
        $surat_jalan->save();

        return redirect()->back()->with('alert','success')->with("msg", "surat_jalan berhasil Di setujui");
    }

    public function reject(Request $request){
        $auth = Auth::user();
        $id = $request->surat_jalan_id;

        $surat_jalan = PoMasuk::findOrFail($id);
        $surat_jalan->status = -1;
        $surat_jalan->rejected_by = $auth->username;
        $surat_jalan->save();

        return redirect()->back()->with('alert','success')->with("msg", "surat_jalan berhasil di tolak");
    }
     public function invoicePrint(Request $request,$id)
    {
        $user = Auth::user();
        $this->data['surat_jalan'] = SuratJalan::findOrFail($id); 
        $this->data['data_produk'] = ItemPenjualan::where("id_penjualan", $this->data['surat_jalan']->id_penjualan)->get(); 
  
        return view('surat_jalan.invoice-print', $this->data);
    }

    public function generateInvoice(Request $request){
        $id = $request->surat_jalan_id;
        $surat_jalan = PoMasuk::findOrFail($id);

        $invoice = $surat_jalan->id."/".$surat_jalan->kode_po_tujuan."-DRT/".$this->getRomawiMonth()."/".date('y');
        $surat_jalan = new SuratJalan();

    }

    public function getRomawiMonth(){
        $m = date('M');
        $month = "";
        switch($m){
            case "Jan":
                $month = "I";
            break;
            case "Feb":
                $month = "II";
            break;
            case "Mar":
                $month = "III";
            break;
            case "Apr":
                $month = "IV";
            break;
            case "May":
                $month = "V";
            break;
            case "Jun":
                $month = "VI";
            break;
            case "Jul":
                $month = "VII";
            break;
            case "Aug":
                $month = "VIII";
            break;
            case "Sep":
                $month = "IX";
            break;
            case "Oct":
                $month = "X";
            break;
            case "Nov":
                $month = "XI";
            break;
            case "Dec":
                $month = "XII";
            break;
        }

        return $month;
    }
}
