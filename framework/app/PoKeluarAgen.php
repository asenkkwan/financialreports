<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PoKeluarAgen extends Model
{
    //
    use SoftDeletes;
    public $table = "po_keluar_agen";
    protected $dates = ['deleted_at'];

    protected $fillable = [
         
        'nomor_po',
        'nama_agen', 
        'alamat_agen',
        'alamat_kirim',
        'pecahan',
        'qty',  
        'harga_beli',
        'harga_jual',
        'pbbkb',
        'pph',
        'ppn',
        'penyalur',
        'nama_pic_penyalur',
        'nama_pic_agen',
        'tanggal',
        'jenis_bbm',
        'cara_pembayaran',
        'jatuh_tempo_hari',
        'tanggal',
        'tanggal_pengaliran',
        'alat_angkut',
        'show_pbbkb',
        'created_at',
        'updated_at',
    ];

    public static $rules = array(
        'tanggal'=>  'required',
        'tanggal_pengaliran'=>  'required',
        'nomor_po' => 'required',
        'nama_agen' => 'required',
        'alamat_agen' => 'required',
        'alamat_kirim' => '',
        'pecahan' => '',
        'qty' => 'required',
        'harga_beli' => 'required',
        'harga_jual'=> 'required',
        'pbbkb'=> '',
        'ppn'=>'',
        'penyalur'=>'required',
        'nama_pic_penyalur'=> 'required',
        'nama_pic_agen'=> 'required',
        'tanggal' => 'required',
        'jenis_bbm'=> 'required',
        'cara_pembayaran'=> 'required',
        'pph'=>'',
        'show_pbbkb'=> '',
        'alat_angkut'=> '',

        
    );

    public static $updaterules = array(
        'tanggal'=>  'required',
        'tanggal_pengaliran'=>  'required',
        'nomor_po' => 'required',
        'nama_agen' => 'required',
        'alamat_agen' => 'required',
        'alamat_kirim' => '',
        'pecahan' => '',
        'pph'=> '',
        'qty' => 'required',
        'harga_beli' => 'required',
        'harga_jual'=> 'required',
        'pbbkb'=> '',
        'ppn'=>'',
        'penyalur'=>'required',
        'nama_pic_penyalur'=> 'required',
        'nama_pic_agen'=> 'required',
        'tanggal' => 'required',
        'jenis_bbm'=> 'required',
        'cara_pembayaran'=> 'required',
        'show_pbbkb'=> '',
        'alat_angkut'=> '',
  
       
    );
        
     public function caddress(){
        return $this->belongsTo('App\Caddress', "address");
    }   
}

