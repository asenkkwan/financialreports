<?php

namespace App;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;

class PoMasuk extends Model
{
    // 
    use SoftDeletes;
    public $table = "po_masuk";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'nama_perusahaan', 
        'nomor_po',
        'alamat_perusahaan',
        'kode_lokasi_perusahaan',
        'tanggal_po',  
        'tanggal_kirim',
        'po_tujuan',
        'ppn',
        'pph',
        'pbbkb',
        'add_cost',
        'pic_po_tujuan',
        'telp_po_tujuan',
        'fax_po_tujuan', 
        'contact_person_1',
        'contact_person_2',
        'created_at',
        'updated_at',
    ];

    public static $rules = array(
        'nama_perusahaan' => 'required',
        'nomor_po' => 'required',
        'alamat_perusahaan' => 'required',
        // 'kode_lokasi_perusahaan' => 'required',
        'tanggal_po' => 'required',
        'tanggal_kirim' => 'required',
        'ppn',
        'pph',
        'pbbkb',
        'add_cost',
        'po_tujuan',
        'pic_po_tujuan',
        'contact_person_1',
        'contact_person_2',
        
    );

    public static $updaterules = array(
       'nama_perusahaan' => 'required',
        'nomor_po' => 'required',
        'alamat_perusahaan' => 'required',
        // 'kode_lokasi_perusahaan' => 'required',
        'tanggal_po' => 'required',
        'tanggal_kirim' => 'required',
        'ppn',
        'pph',
        'pbbkb',
        'add_cost',
        'po_tujuan',
        'pic_po_tujuan',
        'contact_person_1',
        'contact_person_2',
    );     
}

