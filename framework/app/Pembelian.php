<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pembelian extends Model
{
    use SoftDeletes;

    public $table = "pembelian";
    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'no_po',
        'nama_perusahaan', 
        'lokasi_perusahaan',
        'nama_supplier',
        'no_faktur_pajak',
        'telp',
        'produk',
        'qty',  
        'harga_beli', 
        'jumlah_ppn',
        'harga_jual',
        'pbbkb',
        'ppn',
        'keterangan',
        'tanggal',
        'created_at',
        'updated_at',
    ];

    public static $rules = array( 
        // 'no_po' => 'required|max:255',
        'nama_perusahaan' => '',
        'lokasi_perusahaan' => '',
        'nama_supplier' => '',
        'no_faktur_pajak' => 'required',  
        'telp' => '',      
        'produk' => 'required',
        'qty' => 'required',
        'harga_beli' => 'required',
        'harga_jual' => 'required',
        'pbbkb' => '',
        'ppn' => '',
        'keterangan' => '',
        'tanggal' => 'required',
       
    );

    public static $updaterules = array(
     	'nama_perusahaan' => '',
        'lokasi_perusahaan' => '',
        'nama_supplier' => '',  
        'no_faktur_pajak' => 'required',
        'telp' => '',      
        'produk' => 'required',
        'qty' => 'required',
        'harga_beli' => 'required',
        'harga_jual' => 'required',
        'pbbkb' => '',
        'ppn' => '',
        'keterangan' => '',
        'tanggal' => 'required',
        
    );  
    public function customer(){
        return $this->belongsTo('App\Customer', "name");
    }       

    public function items(){
        return $this->hasMany('App\ItemPoMasuk', "id_po_masuk", "id");
    }
}
