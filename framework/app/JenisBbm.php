<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JenisBbm extends Model
{
    //
    use SoftDeletes;
    public $table = "jenis_bbm";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id', 
        'jenis_bbm',
        
        // 'created_at', 
        // 'updated_at',
    ];

    public static $rules = array(
        
        'jenis_bbm' => 'required',
       
        
    );

    public static $updaterules = array(
        
        'jenis_bbm' => 'required',
       
    );  
          
}