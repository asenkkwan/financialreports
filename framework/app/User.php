<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 
        'username', 
        'phone_number', 
        'address', 
        'email',
        'password', 
        'photo',
        'role',
        'company_id',
        'created_at',
        'updated_at',
    ];

    public static $rules = array(
        'name' => 'required|max:255',
        'username' => 'required|unique:users',
        'phone_number' => '',
        'address' => '',
        'email' => '',
        'password' => 'required|min:6|confirmed',
        'role' => 'required',
        'company_id' => '',
        'photo' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
    );

    public static $updaterules = array(
        'name' => 'required|max:255',
        'username' => 'required|unique:users,id',
        'phone_number' => '',
        'address' => '',
        'email' => '',
        'role' => 'required',
        'company_id' => '',
        'photo' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
    );
    
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function company(){
        return $this->belongsTo('App\Company', 'company_id', 'id');
    }

    // public function hasPermission($user_id, $permission){
    //     return  User::where("id", $user_id)
    //                 ->where("permission", "LIKE", "%".$permission."%")
    //                 ->count();
    // }
}
