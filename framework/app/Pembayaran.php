<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Pembayaran extends Model 
{
 
    use SoftDeletes;

    public $table = "pembayaran";
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'nama_perusahaan',
        'ntt',
        'payment_date', 
        'payment_number', 
        'notes',
        'kredit',
        'created_at',
        'updated_at',
        
    ];
 
    public static $rules = array( 
        'nama_perusahaan' => 'required|max:255',
        'ntt' => '',
        'payment_date' => 'required',
        'payment_number' => 'required',
        'kredit' => '',
        'notes' => '',
        
       
    );

    public static $updaterules = array(
       'nama_perusahaan' => 'required|max:255',
        'ntt' => '',
        'payment_date' => 'required',
        'payment_number' => 'required',
        'kredit' => '',
        'notes' => '',
       
       
        
    ); 
    public function customer(){
        return $this->belongsTo('App\Customer', "name");
    }       

    public function pembayaran(){
        return $this->hasMany('App\Pembayaran', "id_item_penjualan", "id");
    }
}
