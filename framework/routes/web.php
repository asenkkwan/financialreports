<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/  
 
 





Route::get('/', function(){
	return redirect('/dashboard');
});

Route::match(['get', 'post'], 'po-masuk/approve', 'PoMasukController@approve');
Route::match(['get', 'post'], 'po-masuk/reject', 'PoMasukController@reject');

Route::match(['get', 'post'], 'po-keluar/approve', 'PoKeluarController@approve');
Route::match(['get', 'post'], 'po-keluar/reject', 'PoKeluarController@reject');

Route::match(['get', 'post'], 'po-keluar-agen/approve', 'PoKeluarAgenController@approve');
Route::match(['get', 'post'], 'po-keluar-agen/reject', 'PoKeluarAgenController@reject');

Route::match(['get', 'post'], 'penjualan/approve', 'PenjualanController@approve');
Route::match(['get', 'post'], 'penjualan/reject', 'PenjualanController@reject');

Route::get("statPenjualanJsonMonthly", 'HomeController@statPenjualanJsonMonthly');

Auth::routes();
//user 
Route::middleware(['auth'])->group(function () {
	 Route::get('/dashboard', 'HomeController@index');
	 Route::get('/setting', 'HomeController@setting');
	 Route::post('/settingSave', 'HomeController@settingSave');
	 
	 Route::get('/user', 'UserController@index')->name('users.index');
	 Route::get('user/create', 'UserController@create')->name('users.create');
	 Route::put('user/update/{id}', 'UserController@update');
	 Route::get('user/edit/{id}', 'UserController@edit');
	 Route::post('user/store', 'UserController@store');
	 Route::delete('user/delete/{id}', 'UserController@destroy');

	//company
	 Route::get('/company', 'CompanyController@index')->name('company.index');
	 Route::get('company/create', 'CompanyController@create')->name('company.create');
	 Route::get('company/edit/{id}', 'CompanyController@edit');
	 Route::put('company/update/{id}', 'CompanyController@update');
	 Route::post('company/store', 'CompanyController@store');
	 Route::delete('company/delete/{id}', 'CompanyController@destroy');
	 

	//customer	
	Route::get('customer', 'CustomerController@index')->name('customer.index');
	Route::get('customer/fetchData', 'CustomerController@fetchDataCustomer');
	Route::get('customer/getAddress', 'CustomerController@getAddress');
	Route::get('customer/create', 'CustomerController@create')->name('customer.create');
	Route::post('customer/store', 'CustomerController@store');
	Route::get('customer/edit/{id}', 'CustomerController@edit');
	Route::put('customer/update/{id}', 'CustomerController@update');
	Route::post('customer/add-address', 'CustomerController@addAddress');
	Route::get('customer/add-address/{id}', 'CustomerController@createAddress')->name('customer.create_address');
	Route::delete('customer/delete/{id}', 'CustomerController@destroy');


	//agen
	Route::get('agen', 'AgenController@index')->name('agen.index');
	Route::get('agen/fetchData', 'AgenController@fetchDataAgen');
	Route::get('agen/getAddress', 'AgenController@getAddress');
	Route::get('agen/create', 'AgenController@create')->name('agen.create');
	Route::post('agen/store', 'AgenController@store');
	Route::get('agen/edit/{id}', 'AgenController@edit');
	Route::put('agen/update/{id}', 'AgenController@update');
	Route::post('agen/add-address', 'AgenController@addAddress');
	Route::get('agen/add-address/{id}', 'AgenController@createAddress')->name('agen.create_address');
	Route::delete('agen/delete/{id}', 'AgenController@destroy');

	//jenis BBM
	Route::get('/jenis-bbm', 'JenisBbmController@index')->name('jenis_bbm.index');
	// Route::get('jenis-bbm/fetchDataAgen', 'JenisBbmController@fetchDataAgen');
	Route::get('jenis-bbm/create', 'JenisBbmController@create')->name('jenis_bbm.create');
	Route::get('jenis-bbm/edit/{id}', 'JenisBbmController@edit');
	Route::put('jenis-bbm/update/{id}', 'JenisBbmController@update');
	Route::post('jenis-bbm/store', 'JenisBbmController@store');
	Route::delete('jenis-bbm/delete/{id}', 'JenisBbmController@destroy');

	//Supply Point
	Route::get('/supply-point', 'SupplyPointController@index')->name('supply_point.index');
	// Route::get('supply-point/fetchDataAgen', 'SupplyPointController@fetchDataSupply');
	Route::get('supply-point/create', 'SupplyPointController@create')->name('supply_point.create');
	Route::get('supply-point/edit/{id}', 'SupplyPointController@edit');
	Route::put('supply-point/update/{id}', 'SupplyPointController@update');
	Route::post('supply-point/store', 'SupplyPointController@store');
	Route::delete('supply-point/delete/{id}', 'SupplyPointController@destroy');

	//PO masuk
	Route::get('/po-masuk', 'PoMasukController@index')->name('po_masuk.index');
	Route::get('/po-masuk/send-email/{id}', 'PoMasukController@sendEmail');

	Route::post('po-masuk/proses-po-masuk', 'PoMasukController@prosesPoMasuk');
	Route::get('po-masuk/create', 'PoMasukController@create')->name('po_masuk.create');
	Route::get('po-masuk/edit/{id}', 'PoMasukController@edit');
	Route::put('po-masuk/update/{id}', 'PoMasukController@update');
	Route::post('po-masuk/store', 'PoMasukController@store');
	Route::delete('po-masuk/delete/{id}', 'PoMasukController@destroy');
	Route::get('po-masuk/detail-po-masuk/{id}', 'PoMasukController@detailPoMasuk');
	Route::get('po-masuk/invoice-print/{id}', 'PoMasukController@invoicePrint');
	Route::get('po-masuk/add-item/{id}', 'PoMasukController@createItem');
	Route::post('po-masuk/add-item', 'PoMasukController@itemPoMasuk');
	Route::post('po-masuk/proses-po-masuk', 'PoMasukController@prosesPoMasuk');


	//PO keluar 
	Route::get('/po-keluar', 'PoKeluarController@index')->name('po_keluar.index');
	Route::get('/po-keluar/send-email/{id}', 'PoKeluarController@sendEmail');

	// Route::post('po-keluar/approve', 'PoKeluarController@approve');
	// Route::post('po-keluar/reject', 'PoKeluarController@reject');
	
	Route::get('po-keluar/create', 'PoKeluarController@create')->name('po_keluar.create');
	Route::get('po-keluar/edit/{id}', 'PoKeluarController@edit');
	Route::put('po-keluar/update/{id}', 'PoKeluarController@update');
	Route::post('po-keluar/store', 'PoKeluarController@store');
	Route::delete('po-keluar/delete/{id}', 'PoKeluarController@destroy');
	Route::get('po-keluar/detail-po-keluar/{id}', 'PoKeluarController@detailPokeluar');
	Route::get('po-keluar/invoice-print/{id}', 'PoKeluarController@invoicePrint');
	Route::get('po-keluar/invoice-printAgen/{id}', 'PoKeluarController@invoicePrintAgen');
	Route::get('po-keluar/invoice-printNasional/{id}', 'PoKeluarController@invoicePrintNasional');
	Route::get('po-keluar/add-item/{id}', 'PoKeluarController@createItem')->name('po_keluar.create_item'); 
	Route::post('po-keluar/add-item', 'PoKeluarController@alatAngkut');
	Route::post('po-keluar/proses-po-keluar', 'PoKeluarController@prosesPoKeluar');

	//PO Keluar Agen 
	Route::get('/po-keluar-agen', 'PoKeluarAgenController@index')->name('po_keluar_agen.index');
	Route::get('/po-keluar-agen/send-email/{id}', 'PoKeluarAgenController@sendEmail');

	// Route::post('po-keluar/approve', 'PoKeluarAgenController@approve');
	// Route::post('po-keluar/reject', 'PoKeluarAgenController@reject');
	
	Route::get('po-keluar-agen/create', 'PoKeluarAgenController@create')->name('po_keluar_agen.create');
	Route::get('po-keluar-agen/edit/{id}', 'PoKeluarAgenController@edit');
	Route::put('po-keluar-agen/update/{id}', 'PoKeluarAgenController@update');
	Route::post('po-keluar-agen/store', 'PoKeluarAgenController@store');
	Route::delete('po-keluar-agen/delete/{id}', 'PoKeluarAgenController@destroy');
	Route::get('po-keluar-agen/detail-po-keluar/{id}', 'PoKeluarAgenController@detailPokeluar');
	Route::get('po-keluar-agen/invoice-print/{id}', 'PoKeluarAgenController@invoicePrint');
	Route::get('po-keluar-agen/invoice-printAgen/{id}', 'PoKeluarAgenController@invoicePrintAgen');
	Route::get('po-keluar-agen/invoice-printNasional/{id}', 'PoKeluarAgenController@invoicePrintNasional');
	Route::get('po-keluar-agen/add-item/{id}', 'PoKeluarAgenController@createItem')->name('po_keluar.create_item'); 
	Route::post('po-keluar-agen/add-item', 'PoKeluarAgenController@alatAngkut');
	Route::post('po-keluar-agen/proses-po-keluar', 'PoKeluarAgenController@prosesPoKeluar');
 

	//Penjualan
	Route::post('penjualan/add-item', 'PenjualanController@itemPenjualan');

	Route::get('/penjualan', 'PenjualanController@index')->name('penjualan.index');
	Route::get('penjualan/fetchDatapenjualan', 'PenjualanController@fetchDataPenjualan');
	Route::get('penjualan/create', 'PenjualanController@create')->name('penjualan.create');
	Route::get('penjualan/edit/{id}', 'PenjualanController@edit');
	Route::put('penjualan/update/{id}', 'PenjualanController@update');
	Route::post('penjualan/store', 'PenjualanController@store');
	Route::delete('penjualan/delete/{id}', 'PenjualanController@destroy');
	Route::get('penjualan/detail-penjualan/{id}', 'PenjualanController@detailPenjualan');
	Route::get('penjualan/invoice-print/{id}', 'PenjualanController@invoicePrint');
	Route::get('penjualan/kwitansi-print/{id}', 'PenjualanController@kwitansiPrint');
	Route::get('penjualan/kwitansi-print2/{id}', 'PenjualanController@kwitansiPrint2');
	Route::post('penjualan/proses-penjualan', 'PenjualanController@prosesPenjualan');
	Route::get('penjualan/export', 'PenjualanController@penjualanExport');
	
	//pembelian
	Route::get('pembelian/export', 'PembelianController@pembelianExport'); 
	Route::resource('pembelian', 'PembelianController');
	Route::get('pembelian/invoice-print/{id}', 'PembelianController@invoicePrint');
	Route::get('pembelian/kwitansi-print/{id}', 'PembelianController@kwitansiPrint');
	

	//pembelian Agen
	Route::resource('pembelian-agen', 'PembelianAgenController');
	Route::get('pembelian-agen/invoice-print/{id}', 'PembelianAgenController@invoicePrint');
	Route::get('pembelian-agen/kwitansi-print/{id}', 'PembelianAgenController@kwitansiPrint');

	//Pembayaran
	Route::get('/pembayaran', 'PembayaranController@index')->name('pembayaran.index');
	Route::get('pembayaran/fetchDatapembayaran', 'PembayaranController@fetchDataPembayaran');
	Route::get('pembayaran/create', 'PembayaranController@create')->name('pembayaran.create');
	Route::get('pembayaran/edit/{id}', 'PembayaranController@edit');
	Route::put('pembayaran/update/{id}', 'PembayaranController@update');
	Route::post('pembayaran/store', 'PembayaranController@store');
	Route::delete('pembayaran/delete/{id}', 'PembayaranController@destroy');
	Route::get('pembayaran/detail-pembayaran/{id}', 'PembayaranController@detailPembayaran');
	Route::get('pembayaran/export', 'PembayaranController@pembayaranExport');

	//Piutang
	Route::get('/piutang', 'PiutangController@index')->name('piutang.index');
	Route::get('piutang/export', 'PiutangController@piutangExport');
	
	//SuratJalan
	Route::get('/surat-jalan', 'SuratJalanController@index')->name('surat_jalan.index');
	Route::get('surat-jalan/fetchDatasurat-jalan', 'SuratJalanController@fetchDataSuratjalan');
	Route::get('surat-jalan/create', 'SuratJalanController@create')->name('surat-jalan.create');
	Route::get('surat-jalan/edit/{id}', 'SuratJalanController@edit');
	Route::put('surat-jalan/update/{id}', 'SuratJalanController@update');
	Route::post('surat-jalan/store', 'SuratJalanController@store');
	Route::delete('surat-jalan/delete/{id}', 'SuratJalanController@destroy');
	Route::get('surat-jalan/detail-surat-jalan/{id}', 'SuratJalanController@detailSuratjalan');
	Route::get('surat-jalan/invoice-print/{id}', 'SuratJalanController@invoicePrint');
	Route::post('surat-jalan/proses-surat-jalan', 'SuratJalanController@proses-surat-jalan');
	Route::get('surat-jalan/add-item/{id}', 'SuratJalanController@createItem')->name('surat_jalan.create_item'); 
	Route::post('surat-jalan/add-item', 'SuratJalanController@alatAngkut');

	
	

});
 
